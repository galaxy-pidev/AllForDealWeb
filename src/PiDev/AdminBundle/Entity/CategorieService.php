<?php

namespace PiDev\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class CategorieService {
     /**
 * @ORM\Id
 * @ORM\Column(type="string",length=255) 
 */
    private $id;
/**
 * @ORM\Column(type="string",length=255) 
 */    
    private $Description;
    public function getId() {
        return $this->id;
    }

    public function getDescription() {
        return $this->Description;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDescription($Description) {
        $this->Description = $Description;
    }

function __toString() {
    
    return (string )$this -> getId();
}
}
