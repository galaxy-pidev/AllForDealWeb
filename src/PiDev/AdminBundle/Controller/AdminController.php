<?php

namespace PiDev\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\Produit;
use PiDev\ClientBundle\Entity\Image;
use PiDev\AdminBundle\Entity\Categorie;
use PiDev\AdminBundle\Form\CategorieType;
use PiDev\ClientBundle\Form\ImageForm;

class AdminController extends Controller {

    public function SupprimerUtilisateurAction($id) {
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository('PiDevClientBundle:User')->findOneByid($id);
        $em->remove($modele);
        $em->flush();
        return $this->redirect($this->generateUrl("admin_liste_utilisateurs"));
    }

    public function ShowAllUtilisateurAction() {

        $em = $this->getDoctrine()->getManager();
        $prods = $em->getRepository('PiDevClientBundle:Produit')->findAll();
        $sevices = $em->getRepository('PiDevClientBundle:Service')->findAll();
        $appelOffre = $em->getRepository('PiDevClientBundle:Appeldoffre')->findAll();
        $sujet = $em->getRepository('PiDevClientBundle:Sujet')->findAll();
        $User = $em->getRepository('PiDevClientBundle:User')->findAll();
       $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
        $role = array($request->get('role'), $request->get('role'));
        $id = array($request->get('role'), $request->get($User->getId()));
        $modele = $em->getRepository('PiDevClientBundle:User')->find($id);
            $modele->setRoles($role);
            $em = $this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();
              return $this->redirect($this->generateUrl("admin_liste_utilisateurs"));
        var_dump($id);
        die;
        }

        return $this->render('PiDevAdminBundle:Admin:ListeUtilisateurs.html.twig', array('mods' => $User,
                    'prods' => $prods,
                    'services' => $sevices,
                    'appeloffre' => $appelOffre,
                    'sujet' => $sujet
        ));
    }

    public function ModifierUtilisateurAction($id) {

        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository('PiDevClientBundle:User')->findOneByid($id);

        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $role = array($request->get('role'), $request->get('role'));
            $modele->setRoles($role);
            $em = $this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();

            return $this->redirect($this->generateUrl("admin_liste_utilisateurs"));
        }


        return $this->render('PiDevAdminBundle:Admin:ModifierUtilisateur.html.twig');
    }

    public function ListeProduitsNonValidésAction() {

        $em = $this->getDoctrine()->getManager();
        /* getRespository  prendre le nom de l'entité */
        $modeles = $em->getRepository('PiDevClientBundle:Produit')->findByValider(0);

        return $this->render('PiDevAdminBundle:Admin:ListeProduitsNonValides.html.twig', array('m' => $modeles));
    }

    public function ValiderProduitAction($id) {
        $em = $this->getDoctrine()->getManager();

        $model = $em->getRepository('PiDevClientBundle:Produit')->find($id);
        $model->setValider(1);
        $em->persist($model);
        $em->flush();
        $mmm = $em->getRepository('PiDevClientBundle:Produit')->findAll();
        return $this->render('PiDevAdminBundle:Admin:ListeProduitsNonValides.html.twig', array(
                    'm' => $mmm));
    }

    public function ListeServicesNonValidésAction() {
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository
                        ('PiDevClientBundle:Service')->findBy(array("valider" => NULL));
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $modeles = $em->getRepository
                            ('PiDevClientBundle:Service')->
                    findBy(array("Categorie" => $search), array("valider" => NULL));
        }
        return $this->render
                        ('PiDevAdminBundle:Admin:ListeServiceNonValider.html.twig'
                        , array("modeles" => $modeles));
    }

    public function ValiderServiceAction($id) {
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository('PiDevClientBundle:Service')
                ->find($id);

        $modele->setValider(1);
        $em->persist($modele);
        $em->flush();

        return $this->redirect($this->generateUrl("admin_services_nonvalidés"));
    }

    public function SupprimerProduitAction($id) {
        $em = $this->getDoctrine()->getManager();
// findAll pour faire l'affichage 
        $model = $em->geTrepository('PiDevClientBundle:Produit')->find($id);

        $em->remove($model);
        $em->flush();
        return $this->redirect($this->generateUrl("show"));
    }

}
