<?php

namespace PiDev\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use PiDev\AdminBundle\Entity\CategorieService;
use PiDev\AdminBundle\Form\CategorieServiceType;

/**
 * CategorieService controller.
 *
 */
class CategorieServiceController extends Controller
{

    /**
     * Lists all CategorieService entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PiDevAdminBundle:CategorieService')->findAll();

        return $this->render('PiDevAdminBundle:CategorieService:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CategorieService entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CategorieService();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('categorieservice_show', array('id' => $entity->getId())));
        }

        return $this->render('PiDevAdminBundle:CategorieService:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a CategorieService entity.
     *
     * @param CategorieService $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CategorieService $entity)
    {
        $form = $this->createForm(new CategorieServiceType(), $entity, array(
            'action' => $this->generateUrl('categorieservice_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CategorieService entity.
     *
     */
    public function newAction()
    {
        $entity = new CategorieService();
        $form   = $this->createCreateForm($entity);

        return $this->render('PiDevAdminBundle:CategorieService:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CategorieService entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevAdminBundle:CategorieService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategorieService entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevAdminBundle:CategorieService:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    public function showAllAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cats = $em->getRepository('PiDevAdminBundle:CategorieService')->findAll();


        return $this->render('PiDevAdminBundle:CategorieService:showAll.html.twig', array(
            'cats'      => $cats,
  
        ));
    }
    /**
     * Displays a form to edit an existing CategorieService entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevAdminBundle:CategorieService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategorieService entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevAdminBundle:CategorieService:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CategorieService entity.
    *
    * @param CategorieService $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CategorieService $entity)
    {
        $form = $this->createForm(new CategorieServiceType(), $entity, array(
            'action' => $this->generateUrl('categorieservice_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CategorieService entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevAdminBundle:CategorieService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategorieService entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('categorieservice_edit', array('id' => $id)));
        }

        return $this->render('PiDevAdminBundle:CategorieService:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CategorieService entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PiDevAdminBundle:CategorieService')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CategorieService entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('categorieservice'));
    }

    /**
     * Creates a form to delete a CategorieService entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorieservice_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
