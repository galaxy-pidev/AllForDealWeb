<?php

namespace PiDev\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\AdminBundle\Entity\Publicite;
use PiDev\AdminBundle\Form\PubliciteType;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Publicite controller.
 *
 */
class PubliciteController extends Controller {

    /**
     * Lists all Publicite entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PiDevAdminBundle:Publicite')->findAll();

        return $this->render('PiDevAdminBundle:Publicite:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Publicite entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Publicite();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $stream = fopen($entity->getFile(), 'rb');
            $entity->setImg(stream_get_contents($stream));
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('publicite_show', array('id' => $entity->getId())));
        }

        return $this->render('PiDevAdminBundle:Publicite:ajout.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Publicite entity.
     *
     * @param Publicite $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Publicite $entity) {
        $form = $this->createForm(new PubliciteType(), $entity, array(
            'action' => $this->generateUrl('publicite_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Finds and displays a Publicite entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevAdminBundle:Publicite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Publicite entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevAdminBundle:Publicite:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Publicite entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevAdminBundle:Publicite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Publicite entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevAdminBundle:Publicite:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Publicite entity.
     *
     * @param Publicite $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Publicite $entity) {
        $form = $this->createForm(new PubliciteType(), $entity, array(
            'action' => $this->generateUrl('publicite_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Publicite entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevAdminBundle:Publicite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Publicite entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('publicite_edit', array('id' => $id)));
        }

        return $this->render('PiDevAdminBundle:Publicite:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Publicite entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PiDevAdminBundle:Publicite')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Publicite entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('publicite'));
    }

    /**
     * Creates a form to delete a Publicite entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('publicite_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }
    
        public function AfficheImageAction ($id) {
        $em = $this->getDoctrine()->getManager();

        $image_obj = $em->getRepository('PiDevAdminBundle:Publicite')->find($id);

        $photo = $image_obj->getImg();

        $response = new StreamedResponse(function () use ($photo) {

            echo stream_get_contents($photo);
        });

        $response->headers->set('Content-Type', 'image/jpeg');

        return $response;
    }
    
        public function showAllAction () {
        $em = $this->getDoctrine()->getManager();

        $pubs = $em->getRepository('PiDevAdminBundle:Publicite')->findAll();
              
        return $this->render('PiDevAdminBundle:Publicite:showAll.html.twig', array(
                    'pubs' => $pubs,
               
        ));
}
}
