<?php

namespace PiDev\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
   
    public function indexAction()
    {
        return $this->render('PiDevAdminBundle:Default:index.html.twig');
    }
    public function okAction()
    {
        return $this->render('PiDevAdminBundle:Default:index.html.twig', array('name' => 'oki'));
    }
    
    
}