<?php

namespace PiDev\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\Produit;
use PiDev\ClientBundle\Entity\Image;
use PiDev\AdminBundle\Entity\Categorie;
use PiDev\AdminBundle\Form\CategorieType;
use PiDev\ClientBundle\Form\ImageForm;


/**
 * Categorie controller.
 *
 */
class CategorieController extends Controller
{

    
    /**
     * Lists all Categorie entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PiDevAdminBundle:Categorie')->findAll();

        return $this->render('PiDevAdminBundle:Categorie:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Categorie entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevAdminBundle:Categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorie entity.');
        }

        return $this->render('PiDevAdminBundle:Categorie:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
public function createAction(Request $request)
{
   $entity = new Categorie();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('categorie_show', array('id' => $entity->getId())));
        }

        return $this->render('PiDevAdminBundle:Categorie:ajout.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
 private function createCreateForm(Categorie $entity)
    {
        $form = $this->createForm(new CategorieType(), $entity, array(
            'action' => $this->generateUrl('categorie_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    
    public function newAction()
    {
        $entity = new Categorie();
        $image = new Image();
        $form   = $this->createCreateForm($entity);
        $form1 = $this->createForm(new ImageForm(), $image);
        return $this->render('PiDevAdminBundle:Categorie:ajout.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'form1'   => $form1->createView(),
            
            
            
        ));
    }
    
    public function RechercheAction()
    {
        $em = $this->getDoctrine()->getManager();
$modeles = $em->getRepository
        ('PiDevAdminBundle:Categorie')->findAll();
        $request = $this->get('request');
        if ($request->getMethod()=="POST")
        {
$search=$request->get('search');
$modeles=$em->getRepository
        ('PiDevAdminBundle:Categorie')->
                findBy(array("nom"=>$search));  
        
    }
    return $this->render
('PiDevAdminBundle:Categorie:recherche.html.twig'
                ,array("mod"=>$modeles));
    }
    
 public function listAction(){
      $em=$this->getDoctrine()->getManager();
        $modeles=$em->getRepository('PiDevAdminBundle:Categorie')->findAll();
        return $this->render('PiDevAdminBundle:Categorie:list.html.twig',array('m'=>$modeles));
}
public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevAdminBundle:Categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorie entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevAdminBundle:Categorie:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
     private function createEditForm(Categorie $entity)
    {
        $form = $this->createForm(new CategorieType(), $entity, array(
            'action' => $this->generateUrl('categorie_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
     public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevAdminBundle:Categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('categorie_edit', array('id' => $id)));
        }

        return $this->render('PiDevAdminBundle:Categorie:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
 public function deleteAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $modeles=$em->getRepository('PiDevAdminBundle:Categorie')->find($id);
        $em->remove($modeles);
        $em->flush();

        return $this->redirect($this->generateUrl('categorie'));
    }

    /**
     * Creates a form to delete a Produit entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorie_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
  public function searchAction()
    {
            $e = $this->getDoctrine()->getManager();
           $categories = $e->getRepository
        ('PiDevAdminBundle:Categorie')->findAll();
        $em = $this->getDoctrine()->getManager();
$modeles = $em->getRepository
        ('PiDevClientBundle:Produit')->findBy(array("valider"=>1));
        $request = $this->get('request');
        if ($request->getMethod()=="POST")
        {
$search=$request->get('search');
$modeles=$em->getRepository
         ('PiDevClientBundle:Produit')->
                findBy(array("Categorie" => $search, "valider" => 1));        
        }
        
        return $this->render
('PiDevAdminBundle:Categorie:search.html.twig'
                ,array("modeles"=>$modeles, "categories"=> $categories));
    }  
}
