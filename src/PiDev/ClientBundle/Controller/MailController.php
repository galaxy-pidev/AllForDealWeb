<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use PiDev\ClientBundle\Entity\Mail;
use PiDev\ClientBundle\Form\MailType;
use Symfony\Component\HttpFoundation\Request;
use Swift_Message;
/**
 * Mail controller.
 *
 */
class MailController extends Controller {

    /**
     * Lists all Mail entities.
     *
     */
    public function indexAction() {
        return $this->render('PiDevClientBundle:Mail:mail.html.twig', array());
    }

    public function newAction() {
          $user=$this->get('security.context')->getToken()->getUser();
        $mail = new Mail();
        $form = $this->container->get('form.factory')->create(new MailType(), $mail);
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $this->sendMailAction('dridi.cirine@gmail.com', $mail->getFrom(), $mail->getNom(), $mail->getText());
            }
        }
        return $this->render('PiDevClientBundle:Mail:new.html.twig', array(
            'user'=>$user,
            'form' => $form->createView()));
    }

    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();
    
        $user=$this->get('security.context')->getToken()->getUser();
        $email=$user->getEmail();
       // $email=('dridi.cirine@gmail.com');
        $liste = $em->getRepository('PiDevClientBundle:Mail')->findByDestinaire($email);
        $entity = $em->getRepository('PiDevClientBundle:Mail')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mail entity.');
        }

        return $this->render('PiDevClientBundle:Mail:show.html.twig', array(
                    'entity' => $entity,
                           'user'=>$user,
            'mail'=>$liste,
        ));
    }

    public function sendMailAction(Request $request) {
        $to = "dridi.cirine@gmail.com";
        $mail = new Mail();
         $user = $this->get('security.context')->getToken()->getUser();
         $from= $user->getEmail();
        
        $form = $this->createForm(new MailType(), $mail);
                $request=$this->get('request');
                $message = Swift_Message::newInstance()
                        ->setSubject($request->get('subject'))
                        ->setFrom($from)
                        ->setTo($request->get('to'))
                        ->setBody($request->get('message'));
                $this->get('mailer')->send($message);
              
                
                $mail->setExpediteur($from);
                $mail->setNom($request->get('subject'));
                $mail->setText($request->get('message'));
                $mail->setDestinaire($request->get('to'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($mail);
                $em->flush();
                return $this->render('PiDevClientBundle:Mail:mail.html.twig', array('to' => $to,
                            'from' => $from,
                    
                ));
            
        return $this->redirect($this->generateUrl('AccueilClient'));
    }

}
