<?php
namespace PiDev\ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\Panier;
use PiDev\ClientBundle\Form\PanierType;

class AccueilController extends Controller {

    public function AccueilAction(Request $request) {

        $em1 = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT a FROM PiDevClientBundle:Produit a where a.valider=1";
        $query = $em1->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 6/* limit per page */
        );
        $em = $this->getDoctrine()->getManager();
        $entity = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('PiDevClientBundle:Produit')->findByValider(1);
   
        $publicites = $em->getRepository('PiDevAdminBundle:Publicite')->findAll();
        $categories = $em->getRepository('PiDevAdminBundle:Categorie')->findAll();
        $favoris = $entity->getFavoris();
        $panier = $entity->getPanier();
        $existe = false;
        // $produit = $em->getRepository('PiDevClientBundle:Produit')->findAll();
        $article = new Panier();
        $form = $this->createForm(new PanierType(), $article);

        return $this->render('PiDevClientBundle:Pages:Accueil.html.twig', array(
                    'entities' => $entities,
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'favoris' => $favoris,
                    'panier' => $panier,
                    'pagination' => $pagination,
                    'existe' => $existe,
                    'publicites' => $publicites,
            'categories'=>$categories,
        ));
    }

}
