<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\ImageProd;
use PiDev\ClientBundle\Form\ImageForm;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ImageController extends Controller {

    public function uploadAction() {

        $im = new ImageProd();

        $form = $this->createForm(new ImageForm(), $im);


        $request = $this->get('request_stack')->getCurrentRequest();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $stream = fopen($im->getFile(), 'rb');

            $im->setImg(stream_get_contents($stream));

            $em->persist($im);

            $em->flush();

            return $this->render('PiDevClientBundle:image:index.html.twig', array());
        }

        return $this->render('PiDevClientBundle:image:upload.html.twig', array('form' => $form->createView()));
    }

    public function listAction() {

        $em = $this->getDoctrine()->getManager();

        $image = $em->getRepository('PiDevClientBundle:ImageProd')->findAll();

        return $this->render('PiDevClientBundle:image:list.html.twig', array('images' => $image));
    }

    public function afficheAction($id) {

        $em = $this->getDoctrine()->getManager();

        $image = $em->getRepository('PiDevClientBundle:ImageProd')->find($id);

        return $this->render('PiDevClientBundle:image:affiche.html.twig', array('images' => $image));
    }

    public function photoAction($id) {

        $em = $this->getDoctrine()->getManager();

        $image_obj = $em->getRepository('PiDevClientBundle:ImageProd')->find($id);
     

        $photo = $image_obj->getImg();

        $response = new StreamedResponse(function () use ($photo) {

            echo stream_get_contents($photo);
        });
        

        $response->headers->set('Content-Type', 'image/jpeg');

        return $response;
    }

}
