<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\Livraison;
use PiDev\ClientBundle\Entity\Commande;
use PiDev\ClientBundle\Entity\LigneCommande;
use PiDev\ClientBundle\Form\CommandeType;
use Knp\Snappy\Pdf;

/**
 * Commande controller.
 *
 */
class CommandeController extends Controller {

    public function PayerCommandeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $panier = $user->getPanier();
        $commande = $em->getRepository('PiDevClientBundle:Commande')->find($id);
        $lignecommande = $em->getRepository('PiDevClientBundle:LigneCommande')->findByPanier($panier);
 

        $i = 0;
      
        foreach ($lignecommande as $lg) {
            $id = $lg->getId();
        
            $ligne = $em->getRepository('PiDevClientBundle:LigneCommande')->find($id);
        
            
            $prod = $ligne->getProduit()->getId();
            
            $qte = $ligne->getQte();
            $produit = $em->getRepository('PiDevClientBundle:Produit')->find($prod);
            $produit->setQantite($produit->getQantite() - $qte);
            $produit->setNbVente($produit->getNbVente() + $qte);
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();
            $i++;
        }
        $taux = ($commande->getTotalcommande() * 10) / 100;
        $credit = $user->getCredit();
        $ptbonus = $user->getPtbonus();
        $request = $this->getRequest();
        $impossible = false;
        if ($request->getMethod() == 'POST') {
            $mode = $request->get('type');

            if ($mode == 1) {
                if ($ptbonus > $taux) {
                    $impossible = true;
                    $em = $this->getDoctrine()->getManager();
                    $commande->setModepaiement($mode);
                    $em->persist($commande);
                    $em->flush();
                } else {
                    $impossible = false;
                }
            }
            if ($mode == 2) {
                if ($credit > $commande->getTotalcommande()) {
                    if ($ptbonus > $taux) {
                        $impossible = true;
                        $em = $this->getDoctrine()->getManager();
                        $commande->setModepaiement($mode);
                        $em->persist($commande);
                        $em->flush();
                    } else {
                        $impossible = false;
                    }
                }
            }
            $commande->setModepaiement($mode);
            $em->persist($commande);
            $em->flush();
            return $this->redirect($this->generateUrl('confirmer_commande', array('id' => $commande->getId())));
        }

        return $this->render('PiDevClientBundle:Commande:Payer.html.twig', array(
                    'id' => $id,
                    'commande' => $commande,
                    'user' => $user,
                    'impo' => $impossible,
           
        ));
    }

    public function ConfirmerCommandeAction($id) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $panier = $user->getPanier();
        $commande = $em->getRepository('PiDevClientBundle:Commande')->find($id);
        $lignecommande = $em->getRepository('PiDevClientBundle:LigneCommande')->findByPanier($panier);
        $i = 0;
        $prod = NULL;
        foreach ($lignecommande as $lg) {
            $id = $lg->getId();
       
            $ligne = $em->getRepository('PiDevClientBundle:LigneCommande')->find($id);

            $prod = $ligne->getProduit()->getId();
            $qte = $ligne->getQte();
            $produit = $em->getRepository('PiDevClientBundle:Produit')->find($prod);
            $produit->setQantite($produit->getQantite() - $qte);
            $produit->setNbVente($produit->getNbVente() + $qte);
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();
            $i++;
        }
        $ptBonus = $user->getPtBonus() + ($commande->getTotalcommande() * 10) / 100;
        $credit = ($user->getCredit() - $commande->getTotalcommande());
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $user->setCredit($credit);
            $user->setPtBonus($ptBonus);
            $commande->setValider(1);
            $em->persist($commande);
            $em->persist($user);
            $em->flush();
            return $this->redirect($this->generateUrl('reponse_commande', array('id' => $commande->getId())));
        }

        return $this->render('PiDevClientBundle:Commande:Confirmer.html.twig', array(
                    'commande' => $commande,
                    'user' => $user
        ));
    }

    public function ReponseCommandeAction($id) { {
            $em = $this->getDoctrine()->getManager();
            $user = $this->get('security.context')->getToken()->getUser();
            $panier = $user->getPanier();
            $commande = $em->getRepository('PiDevClientBundle:Commande')->find($id);
            $lignecommande = $em->getRepository('PiDevClientBundle:LigneCommande')->findByPanier($panier);
            $request = $this->getRequest();
            if ($request->getMethod() == 'POST') {

                return $this->redirect($this->generateUrl('reponse_commande', array('id' => $commande->getId())));
            }

            return $this->render('PiDevClientBundle:Commande:reponse.html.twig', array(
                        'id' => $id,
                        'commande' => $commande,
                        'lignecommande' => $lignecommande,
                        'user' => $user
            ));
        }
    }

    public function FactureCommandeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $panier = $user->getPanier();
        $commande = $em->getRepository('PiDevClientBundle:Commande')->find($id);
        $lignecommande = $em->getRepository('PiDevClientBundle:LigneCommande')->findByPanier($panier);
        $html = $this->renderView('PiDevClientBundle:Commande:facturation.html.twig', array(
            'entity' => $panier,
            'user' => $user,
            'commande' => $commande,
            'ligne' => $lignecommande,
        ));

        return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="facture.pdf"'
                )
        );
    }

}
