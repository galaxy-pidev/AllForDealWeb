<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\CommentaireService;
use PiDev\ClientBundle\Form\CommentaireServiceType;
use PiDev\ClientBundle\Entity\EvaluationService;
use PiDev\ClientBundle\Form\EvaluationServiceType;

/**
 * CommentaireService controller.
 *
 */
class CommentaireServiceController extends Controller {

    /**
     * Lists all CommentaireService entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PiDevClientBundle:CommentaireService')->findAll();

        return $this->render('PiDevClientBundle:CommentaireService:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a form to create a CommentaireService entity.
     *
     * @param CommentaireService $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CommentaireService $entity) {
        $form = $this->createForm(new CommentaireServiceType(), $entity, array(
            'action' => $this->generateUrl('commentaireservice_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Finds and displays a CommentaireService entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:CommentaireService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CommentaireService entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:CommentaireService:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CommentaireService entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:CommentaireService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CommentaireService entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:CommentaireService:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a CommentaireService entity.
     *
     * @param CommentaireService $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CommentaireService $entity) {
        $form = $this->createForm(new CommentaireServiceType(), $entity, array(
            'action' => $this->generateUrl('commentaireservice_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing CommentaireService entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:CommentaireService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CommentaireService entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('commentaireservice_edit', array('id' => $id)));
        }

        return $this->render('PiDevClientBundle:CommentaireService:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a CommentaireService entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PiDevClientBundle:CommentaireService')->find($id);
        $service = $entity->getService()->getId();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CommentaireService entity.');
        }

        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('service_details', array('service' => $service)));
    }

    /**
     * Creates a form to delete a CommentaireService entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('commentaireservice_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
