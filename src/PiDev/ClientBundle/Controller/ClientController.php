<?php

namespace PiDev\ClientBundle\Controller;

use PiDev\ClientBundle\Entity\User;
use PiDev\ClientBundle\Entity\Panier;
use PiDev\ClientBundle\Entity\Produit;
use PiDev\ClientBundle\Form\ProduitType;
use PiDev\ClientBundle\Form\PanierType;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ClientController extends Controller {

    public function homeAction() {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $id = $user->getId();
        $entity = $em->getRepository('PiDevClientBundle:User')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prestataire entity.');
        }
        return $this->render('PiDevClientBundle:base.html.twig', array(
                    'entity' => $entity,
        ));

        return $this->render('PiDevClientBundle:base.html.twig');
    }

    public function listerProduitsAction() {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('PiDevClientBundle:Produit')->findByuser($user);
 return $this->render('PiDevClientBundle:Client:listProduits.html.twig', array(
                    'user' => $user,
                    'produits' => $produits,
               
        ));
    }
    public function listerServicesAction() {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $services = $em->getRepository ('PiDevClientBundle:Service')->findByProprietaire($user);
        return $this->render
                        ('PiDevClientBundle:Client:listServices.html.twig', array(
                            'services' => $services,
                            'user'=>$user));
    }
        public function listerAppelOffresAction() {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $appeloffres = $em->getRepository ('PiDevClientBundle:Appeldoffre')->findByProprietaire($user);
        return $this->render
                        ('PiDevClientBundle:Client:listAppelOffres.html.twig', array(
                            'appeloffres' => $appeloffres,
                            'user'=>$user));
    }
        public function listerForumsAction() {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $forums = $em->getRepository ('PiDevClientBundle:Sujet')->findByUser($user);
        return $this->render
                        ('PiDevClientBundle:Client:listForums.html.twig', array(
                            'forums' => $forums,
                            'user'=>$user));
    }

    public function listerPanierAction() {
        $em = $this->getDoctrine()->getManager();
        $id = $this->get('security.context')->getToken()->getUser();
        $panier = $id->getPanier();
        if (!$panier) {
            throw $this->createNotFoundException('Unable to find Prestataire entity.');
        }
        return $this->render('PiDevClientBundle:Panier:showPanierlist.html.twig', array(
                    'entity' => $panier,
        ));
    }

    public function listerCommandeAction() {
        $em = $this->getDoctrine()->getManager();
        $id = $this->get('security.context')->getToken()->getUser();
        $panier = $id->getPanier();
        $entities = $em->getRepository('PiDevClientBundle:Commande')->findByPanier($panier);

        return $this->render('PiDevClientBundle:Commande:showCommandelist.html.twig', array(
                    'entity' => $panier,
                    'entities' => $entities,
        ));
    }

    public function MyprofileAction() {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $panier = $user->getPanier();
        $entities = $em->getRepository('PiDevClientBundle:Commande')->findByPanier($panier);
        $photo = $user->getImg();




        return $this->render('PiDevClientBundle:Client:profile.html.twig', array(
                    'user' => $user,
                    'entity' => $panier,
                    'entities' => $entities,
                    'photo' => $photo
        ));
    }

    public function DesactiverCompteAction() {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PiDevClientBundle:User')->find($user->getId());

        $entity->setEnabled(0);
        $em->persist($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('fos_user_security_logout'));
    }

    public function InboxAction() {


        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $email = $user->getEmail();
        //   $email=('dridi.cirine@gmail.com');
        $liste = $em->getRepository('PiDevClientBundle:Mail')->findByDestinaire($email);
        return $this->render('PiDevClientBundle:Mail:Inbox.html.twig', array(
                    'user' => $user,
                    'mail' => $liste,
        ));
    }

    public function afficheImageAction() {

        $user = $this->get('security.context')->getToken()->getUser();
        $photo = $user->getImg();
        $response = new StreamedResponse(function () use ($photo) {
            echo stream_get_contents($photo);
        });
        $response->headers->set('Content-Type', 'image/jpeg');
        return $response;
    }

    public function afficheImageUserAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('PiDevClientBundle:User')->find($id);
        $photo = $user->getImg();
        $response = new StreamedResponse(function () use ($photo) {
            echo stream_get_contents($photo);
        });
        $response->headers->set('Content-Type', 'image/jpeg');
        return $response;
    }

    public function ParametresAction() {
        $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('PiDevClientBundle:Client:parametre.html.twig', array(
                    'user' => $user,
        ));
    }

}
