<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\Livraison;
use PiDev\ClientBundle\Entity\Adresse;
use PiDev\ClientBundle\Entity\Commande;
use PiDev\ClientBundle\Form\AdresseType;

/**
 * Adresse controller.
 *
 */
class AdresseController extends Controller {

    /**
     * Lists all Adresse entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PiDevClientBundle:Adresse')->findAll();

        return $this->render('PiDevClientBundle:Adresse:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    public function ChoisirAdresseAction() {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $panier = $user->getPanier();
        $commande = new Commande();
        
        $adressee = $em->getRepository('PiDevClientBundle:Adresse')->findByUser($user);
        $lignecommande = $em->getRepository('PiDevClientBundle:LigneCommande')->findByPanier($panier);
        $i = 0;
   $totale =0;
        foreach ($lignecommande as $lg) {
            $id[$i] = $lg->getId();
            $ligne = $em->getRepository('PiDevClientBundle:LigneCommande')->find($id[$i]);
            $qte[$i] = $ligne->getQte();
            $prix[$i] = $ligne->getProduit()->getPrix();
            $totale = $totale +$prix[$i]*$qte[$i];              
         $i++;   
        }
        $livraison = new Livraison();
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $adresses = $request->get('adresse');  
            $adresseee = $em->getRepository('PiDevClientBundle:Adresse')->find($adresses);
            $modeLivraison = $request->get('modeLivraison');
            $livraison->setAdresse($adresseee);
            $livraison->setModelivraison($modeLivraison);
            $em = $this->getdoctrine()->getmanager();
            $em->persist($livraison);            
            $commande->setLivraison($livraison);
            $commande->setPanier($panier);
            $commande->setTotalcommande($totale);
           $commande->setModepaiement('pas_encore');
            $em->persist($commande);
            $em->flush();
            return $this->redirect($this->generateUrl('payer_commande', array('id' => $commande->getId())));
            
        }
        return $this->render('PiDevClientBundle:adresse:choisir.html.twig', array('adresse' => $adressee,
           'cm'=> $lignecommande,
        
        ));
    }

    /**
     * Creates a new Adresse entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $adresse = $em->getRepository('PiDevClientBundle:Adresse')->findByUser($user);


        $entity = new Adresse();
        $form = $this->createForm(new AdresseType(), $entity);
        $request = $this->getRequest();

        if ($request->isMethod('post')) {
            $form->bind($request);
            $entity = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($user);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('choisir_adresse'));
        }

        return $this->render('PiDevClientBundle:Adresse:ajout.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'adresse' => $adresse
        ));
    }

    /**
     * Creates a form to create a Adresse entity.
     *
     * @param Adresse $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Adresse $entity) {
        $form = $this->createForm(new AdresseType(), $entity, array(
            'action' => $this->generateUrl('adresse_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

     /**
     * Finds and displays a Adresse entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:Adresse')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Adresse entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:Adresse:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Adresse entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:Adresse')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Adresse entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:Adresse:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Adresse entity.
     *
     * @param Adresse $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Adresse $entity) {
        $form = $this->createForm(new AdresseType(), $entity, array(
            'action' => $this->generateUrl('adresse_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Adresse entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PiDevClientBundle:Adresse')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Adresse entity.');
        }
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('adresse_edit', array('id' => $id)));
        }
        return $this->render('PiDevClientBundle:Adresse:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Adresse entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PiDevClientBundle:Adresse')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Adresse entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('adresse'));
    }

    /**
     * Creates a form to delete a Adresse entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('adresse_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function LocaliserAdresseAction() {
        $Latitude = '-24';
        $Longitude = '142';

        return $this->render('PiDevClientBundle:Adresse:map.html.twig', array
                    (
                    'Latitudes' => $Latitude,
                    'Longitudes' => $Longitude
        ));
    }

}
