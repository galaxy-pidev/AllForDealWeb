<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\Favoris;
use PiDev\ClientBundle\Entity\Panier;

use PiDev\ClientBundle\Form\PanierType;
use PiDev\ClientBundle\Entity\Commande;

/**
 * Panier controller.
 */
class PanierController extends Controller{
    /**
     * Creates a form to create a Panier entity.
     *
     * @param Panier $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Panier $entity)
    {
        $form = $this->createForm(new PanierType(), $entity, array(
            'action' => $this->generateUrl('panier_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Panier entity.
     *
     */
    public function newAction()
    {
        $entity = new Panier();
        $form   = $this->createCreateForm($entity);

        return $this->render('PiDevClientBundle:Panier:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            
        ));
    }

    public function removeAction(Request $request, $id, $produit) {

        $em = $this->getDoctrine()->getManager();
        $entity = $this->get('security.context')->getToken()->getUser();
        $panier = $entity->getPanier();
        $produit = $em->getRepository('PiDevClientBundle:Produit')->findOneById($produit);
        $panier->removeProduit($produit);
        $em->persist($panier);
        $em->flush();


        return $this->redirect($this->generateUrl('panier_show', array('id' => $id)));



        return $this->render('PiDevClientBundle:Panier:new.html.twig', array(
                    'entity' => $entity,
                    'panier' => $panier,
                    'produit' => $produit,
        ));
    }    
    /**
     * Finds and displays a Panier entity.
     *
     */
    
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->get('security.context')->getToken()->getUser();
        $request = $this->getRequest();
         
        $panier = $entity->getPanier();
        $lignecommande=  $em->getRepository('PiDevClientBundle:LigneCommande')->findByPanier($panier->getId());
               
        $produits=null;
        $prods=null;
      
        
        
     foreach ( $lignecommande as $produit ){
         $ids = $produit->getProduit()->getId();
          
            $prods=$em->getRepository('PiDevClientBundle:Produit')->findAll();  
     $produits[]=$em->getRepository('PiDevClientBundle:Produit')->findById($ids);
         $request = $this->getRequest();    
            if($request->getMethod()=='POST')
            {
               $ii=($em->getRepository('PiDevClientBundle:LigneCommande')->findByProduit($ids));
            foreach ( $ii as $ii ){
           $qte=$request->get($ii->getId());         
            $produit->setQte($qte);
            $em=  $this->getdoctrine()->getmanager();           
            $em->persist($produit); 
            $em->flush();
            }}
     }
    
          

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prestataire entity.');
        }



        return $this->render('PiDevClientBundle:Panier:affiche.html.twig', array(
                    'entity' => $entity,
                    'panier' => $panier,
                   'produits' => $produits,
            'prods'=>$prods,
            'cm'=>$lignecommande,
                
        ));
    }

    /**
     * Displays a form to edit an existing Panier entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:Panier')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Panier entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:Panier:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Panier entity.
    *
    * @param Panier $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Panier $entity)
    {
        $form = $this->createForm(new PanierType(), $entity, array(
            'action' => $this->generateUrl('panier_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    
    /**
     * Edits an existing Panier entity.
     *
     */
    
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:Panier')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Panier entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('panier_edit', array('id' => $id)));
        }

        return $this->render('PiDevClientBundle:Panier:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Panier entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PiDevClientBundle:Panier')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Panier entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('panier'));
    }

    /**
     * Creates a form to delete a Panier entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('panier_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
