<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Form\ProduitRechercheForm;
use PiDev\ClientBundle\Form\ImageForm;
use PiDev\ClientBundle\Entity\Produit;
use PiDev\ClientBundle\Entity\ImageProd;
use PiDev\ClientBundle\Entity\Panier;
use PiDev\ClientBundle\Entity\Favoris;
use PiDev\ClientBundle\Entity\Commentaire;
use PiDev\ClientBundle\Entity\EvaluationProduit;
use PiDev\ClientBundle\Form\CommentaireType;
use PiDev\ClientBundle\Form\ProduitType;
use PiDev\ClientBundle\Form\FavorisType;
use PiDev\ClientBundle\Form\PanierType;

/**
 * Produit controller.
 *
 */
class ProduitController extends Controller {

    public function showAllAction(Request $request) {

        $em1 = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT a FROM PiDevClientBundle:Produit a";
        $query = $em1->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 8/* limit per page */
        );
        $em = $this->getDoctrine()->getManager();
        $entity = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('PiDevClientBundle:Produit')->findAll();
        $favoris = $entity->getFavoris();
        $panier = $entity->getPanier();
        
        $existe = false;
        // $produit = $em->getRepository('PiDevClientBundle:Produit')->findAll();
        $article = new Panier();
        $form = $this->createForm(new PanierType(), $article);

        return $this->render('PiDevClientBundle:Produit:showall.html.twig', array(
                    'entities' => $entities,
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'favoris' => $favoris,
                    'panier' => $panier,
                    'pagination' => $pagination,
                    'existe' => $existe,
        ));
    }

    /**
     * Lists all Produit entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('PiDevClientBundle:Produit')->findAll();

        return $this->render('PiDevClientBundle:Produit:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Produit entity.
     *
     */
    public function AjouterAction(Request $request) {
        $entity = new Produit();
        $im = new ImageProd();

        $formImg = $this->createForm(new ImageForm(), $im);
        $form = $this->createForm(new ProduitType(), $entity);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);
        $formImg->handleRequest($request);

        $iduser = $this->container->get('security.context')->getToken()->getUser();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($iduser);
            $em->persist($entity);
            $em->flush();
            $stream = fopen($im->getFile(), 'rb');

            $im->setImg(stream_get_contents($stream));
            
            $im->setTitre($entity->getNom());
            $im->setProduit($entity);
            $em->persist($im);

            $em->flush();

            return $this->redirect($this->generateUrl('client_list_produit'));
        }

        return $this->render('PiDevClientBundle:Produit:new.html.twig', array(
                    'entity' => $entity,
                    'image' => $im,
                    'form' => $form->createView(),
                    'formImg' => $formImg->createView(),
        ));
    }

    /**
     * Creates a form to create a Produit entity.
     *
     * @param Produit $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Produit $entity, ImageProd $image) {
        $form = $this->createForm(new ProduitType(), $entity, array(
            'action' => $this->generateUrl('produit_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Finds and displays a Produit entity.
     *
     */
    public function ProduitDetailAction(Request $request, $id) {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PiDevClientBundle:Produit')->find($id);
        $entities = $em->getRepository('PiDevClientBundle:Commentaire')->findByProduit($id);
        $evaluations = $em->getRepository('PiDevClientBundle:EvaluationProduit')->findByProduit($id);

        $ev = $em->getRepository('PiDevClientBundle:EvaluationProduit')->findByUser($user);
        $i = 0;
        $c = null;
        $evaluation = null;
        $vote = false;
        foreach ($ev as $e) {
            $u[$i] = $e->getProduit()->getId();
            if ($entity->getId() == $u[$i]) {
                $vote = true;
                $evaluation = $em->getRepository('PiDevClientBundle:EvaluationProduit')->find($e->getId());
                break;
            } else {
                $evaluation = null;
                $vote = false;
            }
        }
        $article = new Commentaire;
        $eval = new EvaluationProduit();
        $image = $em->getRepository('PiDevClientBundle:ImageProd')->findByProduit($id);

        $form = $this->createForm(new CommentaireType(), $article);

        if ($request->isMethod('post')) {
            $rating = $request->get('adresse');
            $form->bind($request);
            $com = $form->getData();


            if ($rating == null) {
                $em = $this->getDoctrine()->getManager();
                $com->setProduit($entity);
                $com->setUser($user);
                $em->persist($com);
                $em->flush();
            } else {
                if ($vote == false) {
                    $em = $this->getDoctrine()->getManager();
                    $eval->setRating($rating);
                    $eval->setUser($user);
                    $eval->setProduit($entity);
                    $em->persist($eval);
                    $em->flush();
                } else {
                    $em = $this->getDoctrine()->getManager();
                    $evaluation->setRating($rating);
                    $em->persist($evaluation);
                    $em->flush();
                }
            }
            return $this->redirect($this->generateUrl('produit_detail', array('id' => $id)));
        }


        return $this->render('PiDevClientBundle:Produit:Detail.html.twig', array(
                    'entity' => $entity,
                    'entities' => $entities,
                    'form' => $form->createView(),
                    'utilisateur' => $user,
                    'images' => $image,
                    'eval' => $eval,
                    'vote' => $vote,
                    'evaluation' => $evaluation,
                    'evaluations' => $evaluations,
        ));
    }

    /**
     * Displays a form to edit an existing Produit entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:Produit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Produit entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:Produit:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Produit entity.
     *
     * @param Produit $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Produit $entity) {
        $form = $this->createForm(new ProduitType(), $entity, array(
            'action' => $this->generateUrl('produit_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Produit entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:Produit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Produit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('produit_edit', array('id' => $id)));
        }

        return $this->render('PiDevClientBundle:Produit:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Produit entity.
     *
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository('PiDevClientBundle:Produit')->find($id);
        $em->remove($modeles);
        $em->flush();

        return $this->redirect($this->generateUrl('produit'));
    }

    /**
     * Creates a form to delete a Produit entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('produit_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    function listProdAction() {


        $em = $this->getDoctrine()->getManager();
        /* getRespository  prendre le nom de l'entitÃ© */
        $modeles = $em->getRepository('PiDevClientBundle:Produit')->findAll();

        return $this->render('PiDevClientBundle:Produit:list.html.twig', array('m' => $modeles));
    }

    function rechercherProdAction() {
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository
                        ('PiDevClientBundle:Produit')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $modeles = $em->getRepository
                            ('PiDevClientBundle:Produit')->
                    findBy(array("nom" => $search));
        }
        return $this->render
                        ('PiDevClientBundle:Produit:recherche.html.twig'
                        , array("mod" => $modeles));
    }

   
    function rechercheAction() {
        $e = $this->getDoctrine()->getManager();
        $categories = $e->getRepository
                        ('PiDevAdminBundle:Categorie')->findAll();


        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository
                        ('PiDevClientBundle:Produit')->findBy(array("valider" => 1));
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $categoriesNom = $e->getRepository
                            ('PiDevAdminBundle:Categorie')->findOneByNom($search);
//           var_dump($categoriesNom);
//        die;
            $modeles = $em->getRepository
                            ('PiDevClientBundle:Produit')->
                    findBy(array("categorie" => $categoriesNom->getId(), "valider" => 1));
        }

        return $this->render
                        ('PiDevClientBundle:Produit:recherche.html.twig'
                        , array("modeles" => $modeles, "categories" => $categories));
    }

}
