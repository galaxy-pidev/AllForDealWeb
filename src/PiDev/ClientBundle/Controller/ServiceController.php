<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\Service;
use PiDev\ClientBundle\Form\ServiceType;
use PiDev\ClientBundle\Entity\EvaluationService;
use PiDev\ClientBundle\Form\EvaluationServiceType;
use PiDev\ClientBundle\Entity\CommentaireService;
use PiDev\ClientBundle\Form\CommentaireServiceType;
use PiDev\ClientBundle\Form\SType;

class ServiceController extends Controller {

    public function AjouterAction(Request $request) {
        $user = $this->get('security.context')->getToken()->getUser();
        $service = new Service();
        $Form = new ServiceType();
        $form = $this->createForm($Form, $service);
        $form->handleRequest($request);
        $service->setProprietaire($user);
        if ($request->getMethod() == 'POST') {

            $em = $this->getDoctrine()->getManager();

            $em->persist($service);
            $em->flush();
            return $this->redirect($this->generateUrl("client_list_services"));
        }
        return
                $this->render('PiDevClientBundle:Service:ajout.html.twig', array('f' => $form->createView()));
    }
    
    public function DetailServiceAction(Request $request, $service) {
   $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PiDevClientBundle:Service')->find($service);
        $entities = $em->getRepository('PiDevClientBundle:CommentaireService')->findByService($service);
        $ev = $em->getRepository('PiDevClientBundle:EvaluationService')->findByUser($user);
        $evaluations = $em->getRepository('PiDevClientBundle:EvaluationService')->findByService($service);
        $i = 0;
        $evaluation = null;
        $vote = false;
        foreach ($ev as $e) {
            $u[$i] = $e->getService()->getId();
            if ($entity->getId() == $u[$i]) {
                $vote = true;
                $evaluation = $em->getRepository('PiDevClientBundle:EvaluationService')->find($e->getId());
                break;
            } else {
                $evaluation = null;
                $vote = false;
            }
        }
        $eval = new EvaluationService();
        $com = new CommentaireService();

        $form = $this->createForm(new CommentaireServiceType(), $com);
        if ($request->isMethod('post')) {
            $rating = $request->get('adresse');
            $form->bind($request);
            $com = $form->getData();
            if ($rating == null) {
                $em = $this->getDoctrine()->getManager();
                $com->setService($entity);
                $com->setUser($user);
                $em->persist($com);
                $em->flush();
            } else {
                if ($vote == false) {
                    $em = $this->getDoctrine()->getManager();
                    $eval->setRating($rating);
                    $eval->setUser($user);
                    $eval->setService($entity);
                    $em->persist($eval);
                    $em->flush();
                } else {
                    $em = $this->getDoctrine()->getManager();
                    $evaluation->setRating($rating);
                    $em->persist($evaluation);
                    $em->flush();
                }
            }
            return $this->redirect($this->generateUrl('service_details', array('service' => $service)));
        }
        return $this->render('PiDevClientBundle:Service:Detail.html.twig', array(
                    'entity' => $entity,
                    'entities' => $entities,
                    'utilisateur' => $user,
                    'form' => $form->createView(),
                    'eval' => $eval,
                    'vote' => $vote,
                    'evaluation' => $evaluation,
                'evaluations' => $evaluations,
        ));
    }

    public function RechercheAction() {
        $e = $this->getDoctrine()->getManager();
        $categories = $e->getRepository
                        ('PiDevAdminBundle:CategorieService')->findAll();
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository
                        ('PiDevClientBundle:Service')->findBy(array("valider" => 1));
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $modeles = $em->getRepository
                            ('PiDevClientBundle:Service')->
                    findBy(array("Categorie" => $search, "valider" => 1));
            ;
        }

        return $this->render
                        ('PiDevClientBundle:Service:recherche.html.twig'
                        , array("modeles" => $modeles, "categories" => $categories));
    }

    public function supprimerAction($id) {
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository
                        ('PiDevClientBundle:Service')->find($id);
        $em->remove($modeles);
        $em->flush();
        return $this->redirect($this->generateUrl("moi"));
    }

    public function ModifierAction($id) {
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository('PiDevClientBundle:Service')
                ->find($id);
        $formv = new ServiceType;
        $form = $this->createForm($formv, $modele);
        $Request = $this->get('request');
        if ($form->handleRequest($Request)->isValid()) {
            $em->persist($modele);
            $em->flush();
            return $this->redirect($this->generateUrl("moi"));
        }
        return $this->render ('PiDevClientBundle:Service:modifier.html.twig', array(
                            'form' => $form->createView()));
    }
    
    public function FindAllAction (Request $request){
        
      $em1 = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT a FROM PiDevClientBundle:Service a";
        $query = $em1->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 9/* limit per page */
        );
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository
                        ('PiDevClientBundle:Service')->findAll();

        return $this->render ('PiDevClientBundle:Service:showAll.html.twig' , array(
            "services" => $services,
            "pagination"=>$pagination
                            ));
    }

    

}
