<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\Produit;
use PiDev\ClientBundle\Entity\EvaluationProduit;
use PiDev\ClientBundle\Form\EvaluationProduitType;

/**
 * EvaluationProduit controller.
 *
 */
class EvaluationProduitController extends Controller {

    /**
     * Lists all EvaluationProduit entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PiDevClientBundle:EvaluationProduit')->findAll();

        return $this->render('PiDevClientBundle:EvaluationProduit:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new EvaluationProduit entity.
     *
     */
    public function createAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $produit = $em->getRepository('PiDevClientBundle:Produit')->findOneById($id);
        $entity = new EvaluationProduit();
        $form = $this->createCreateForm($entity, $id);
        //  $form->handleRequest($request);
         $request = $this->getRequest();    
        if($request->getMethod()=='POST'){
           $rating=$request->get('adresse');           
            $entity = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $entity->setRating($rating);
            $entity->setUser($user);
            $entity->setProduit($produit);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('evaluationproduit_show', array('id' => $entity->getId())));
        }

        return $this->render('PiDevClientBundle:EvaluationProduit:new.html.twig', array(
                    'entity' => $entity,
                    'produit' => $produit,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EvaluationProduit entity.
     *
     * @param EvaluationProduit $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EvaluationProduit $entity, $id) {
        $form = $this->createForm(new EvaluationProduitType(), $entity, array(
            'action' => $this->generateUrl('evaluationproduit_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new EvaluationProduit entity.
     *
     */
//    public function newAction()
//    {
//        $entity = new EvaluationProduit();
//        $form   = $this->createCreateForm($entity);
//
//        return $this->render('PiDevClientBundle:EvaluationProduit:new.html.twig', array(
//            'entity' => $entity,
//            'form'   => $form->createView(),
//        ));
//    }

    /**
     * Finds and displays a EvaluationProduit entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:EvaluationProduit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EvaluationProduit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:EvaluationProduit:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing EvaluationProduit entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:EvaluationProduit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EvaluationProduit entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:EvaluationProduit:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a EvaluationProduit entity.
     *
     * @param EvaluationProduit $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EvaluationProduit $entity) {
        $form = $this->createForm(new EvaluationProduitType(), $entity, array(
            'action' => $this->generateUrl('evaluationproduit_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing EvaluationProduit entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:EvaluationProduit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EvaluationProduit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('evaluationproduit_edit', array('id' => $id)));
        }

        return $this->render('PiDevClientBundle:EvaluationProduit:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a EvaluationProduit entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PiDevClientBundle:EvaluationProduit')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EvaluationProduit entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('evaluationproduit'));
    }

    /**
     * Creates a form to delete a EvaluationProduit entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('evaluationproduit_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
