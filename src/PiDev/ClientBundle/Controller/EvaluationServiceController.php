<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use PiDev\ClientBundle\Entity\EvaluationService;
use PiDev\ClientBundle\Form\EvaluationServiceType;

/**
 * EvaluationService controller.
 *
 */
class EvaluationServiceController extends Controller
{

    /**
     * Lists all EvaluationService entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PiDevClientBundle:EvaluationService')->findAll();

        return $this->render('PiDevClientBundle:EvaluationService:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new EvaluationService entity.
     *
     */
    public function createAction($id)
        {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $service = $em->getRepository('PiDevClientBundle:Service')->findOneById($id);
        $entity = new EvaluationService();
        $form = $this->createCreateForm($entity,$id);

        $request = $this->getRequest();
        
        if($request->isMethod('post')){
            $form->bind($request);
            $entity = $form->getData();   
        
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($user);
            $entity->setService($service);
            $em->persist($entity);
            $em->flush(); 
                    return $this->redirect($this->generateUrl('evaluationservice_show', array('id' => $id)));
        
                  }

        return $this->render('PiDevClientBundle:EvaluationService:new.html.twig', array(
            'entity' => $entity,
            'service'=> $service,
            'form'   => $form->createView(),  
    
  ));}
        public function AjoutAction($id)
        {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $service = $em->getRepository('PiDevClientBundle:Service')->findOneById($id);
        $entity = new EvaluationService();
        $form = $this->createCreateForm($entity,$id);

        $request = $this->getRequest();
        
        if($request->isMethod('post')){
            $form->bind($request);
            $entity = $form->getData();   
        
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($user);
            $entity->setService($service);
            $em->persist($entity);
            $em->flush(); 
            return $this->redirect($this->generateUrl('evaluationservice_show', array('id' => $id)));
        }

        return $this->render('PiDevClientBundle:EvaluationService:new.html.twig', array(
            'entity' => $entity,
            'service'=> $service,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EvaluationService entity.
     *
     * @param EvaluationService $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EvaluationService $entity,$id)
    {
        $form = $this->createForm(new EvaluationServiceType(), $entity, array(
            'action' => $this->generateUrl('evaluationservice_create',array('id' =>$id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Finds and displays a EvaluationService entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:EvaluationService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EvaluationService entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:EvaluationService:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing EvaluationService entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:EvaluationService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EvaluationService entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:EvaluationService:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a EvaluationService entity.
    *
    * @param EvaluationService $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(EvaluationService $entity)
    {
        $form = $this->createForm(new EvaluationServiceType(), $entity, array(
            'action' => $this->generateUrl('evaluationservice_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing EvaluationService entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:EvaluationService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EvaluationService entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('evaluationservice_edit', array('id' => $id)));
        }

        return $this->render('PiDevClientBundle:EvaluationService:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a EvaluationService entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PiDevClientBundle:EvaluationService')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EvaluationService entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('evaluationservice'));
    }

    /**
     * Creates a form to delete a EvaluationService entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('evaluationservice_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
