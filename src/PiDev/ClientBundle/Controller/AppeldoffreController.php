<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\Appeldoffre;
use PiDev\ClientBundle\Form\AppeldoffreType;
use Symfony\Component\HttpFoundation\Request;

class AppeldoffreController extends Controller {

    public function AjouterAction(Request $request) {
      $user = $this->get('security.context')->getToken()->getUser();


        $model = new Appeldoffre();
        $modelForm = new AppeldoffreType();
        $form = $this->createForm($modelForm, $model);
        $form->handleRequest($request);
        
        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $model->setProprietaire($user);
            $em->persist($model);
            $em->flush();
            return $this->redirect($this->generateUrl('detail_appel_offre', array('id' => $model->getId())));
        }
        return$this->render('PiDevClientBundle:Appeldoffre:new.html.twig', array(
            'f' => $form->createView()));
    }

    public function RechercheAction(Request $request) {
              $em1 = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT a FROM PiDevClientBundle:Appeldoffre a";
        $query = $em1->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 5/* limit per page */
        );
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository ('PiDevClientBundle:Appeldoffre')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $services = $em->getRepository ('PiDevClientBundle:Appeldoffre')->findBy(array("nom" => $search));
        }

        return $this->render ('PiDevClientBundle:Appeldoffre:showAll.html.twig', array(
            "services" => $services,
            "pagination"=>$pagination
                
                ));
    }

    function DetailsAction($id) {
        $em = $this->getDoctrine()->getManager();
        $model = $em->getRepository
                        ('PiDevClientBundle:Appeldoffre')->find($id);
        return $this->render
                        ('PiDevClientBundle:Appeldoffre:Detail.html.twig', array('mod' => $model));
    }

    public function supprimerAction($id) {
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository
                        ('PiDevClientBundle:Appeldoffre')->find($id);
        $em->remove($modeles);
        $em->flush();
        return $this->redirect($this->generateUrl("prop/{proprietaire}"));
    }

    public function ModifierAction($id) {
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository('PiDevClientBundle:Appeldoffre')
                ->find($id);
        $formv = new AppeldoffreType;
        $form = $this->createForm($formv, $modele);
        $Request = $this->get('request');
        if ($form->handleRequest($Request)->isValid()) {
            $em->persist($modele);
            $em->flush();
            return $this->redirect($this->generateUrl("new_appel_offre"));
        }
        return $this->render
                        ('PiDevClientBundle:Appeldoffre:modifier.html.twig', array('form' => $form->createView()));
    }

}
