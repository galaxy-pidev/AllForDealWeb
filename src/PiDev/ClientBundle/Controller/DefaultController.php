<?php

namespace PiDev\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('PiDevClientBundle::base.html.twig',array(
            'user' => $user
            ));
}}