<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace PiDev\ClientBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class LigneCommande {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    

         /**
     * @var \Produit
     *
     * @ORM\ManyToOne(targetEntity="Produit" )
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="Produit_id", referencedColumnName="id" ,onDelete="CASCADE")
     * })
     */
    private $produit;
             /**
     * @var \Produit
     *
     * @ORM\ManyToOne(targetEntity="Panier")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="Panier_id", referencedColumnName="id" , onDelete="CASCADE")
     * })
     */
    private $panier;
     /**
     * @var integer
     *
     * @ORM\Column(name="qte", type="integer", nullable=false)
     */
    private $qte=1;
    /**
     * Constructor
     */
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     * @return LigneCommande
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return integer 
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set produit
     *
     * @param \PiDev\ClientBundle\Entity\Produit $produit
     * @return LigneCommande
     */
    public function setProduit(\PiDev\ClientBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \PiDev\ClientBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set panier
     *
     * @param \PiDev\ClientBundle\Entity\Panier $panier
     * @return LigneCommande
     */
    public function setPanier(\PiDev\ClientBundle\Entity\Panier $panier = null)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Get panier
     *
     * @return \PiDev\ClientBundle\Entity\Panier 
     */
    public function getPanier()
    {
        return $this->panier;
    }
}
