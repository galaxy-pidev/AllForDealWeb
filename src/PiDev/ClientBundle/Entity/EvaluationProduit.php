<?php

namespace PiDev\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EvaluationProduit
 *
 * @ORM\Table(name="EvaluationProduit", indexes={@ORM\Index(name="IDX_FBF7353568D3EA09", columns={"User_id"}), @ORM\Index(name="IDX_FBF7353540697D2B", columns={"Produit_id"})})
 * @ORM\Entity
 */
class EvaluationProduit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
     /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer", nullable=false)
     */
    private $rating;

    /**
     * @var \Produit
     *
     * @ORM\ManyToOne(targetEntity="Produit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Produit_id", referencedColumnName="id",onDelete="CASCADE")
     * })
     */
    private $produit;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="User_id", referencedColumnName="id",onDelete="CASCADE")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set produit
     *
     * @param \PiDev\ClientBundle\Entity\Produit $produit
     * @return EvaluationProduit
     */
    public function setProduit(\PiDev\ClientBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \PiDev\ClientBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set user
     *
     * @param \PiDev\ClientBundle\Entity\User $user
     * @return EvaluationProduit
     */
    public function setUser(\PiDev\ClientBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \PiDev\ClientBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return EvaluationProduit
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }
    
    public function __toString() {
        return (string)( $this->getUser()+$this->getProduit());
    }

}
