<?php


namespace PiDev\ClientBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class Commande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCommande", type="date", nullable=false)
     */
    private $datecommande;
    /**
     * @var string
     *
     * @ORM\Column(name="modepaiement", type="string", length=45, nullable=false)
     */
    private $modepaiement;
    /**
     * @var integer
     *
     * @ORM\Column(name="totalCommande", type="integer", nullable=true)
     */
    private $totalcommande;

    /**
     * @var boolean
     *
     * @ORM\Column(name="valider", type="boolean")
     */
    private $valider=0;


    /**
     * @var \Panier
     *
     * @ORM\ManyToOne(targetEntity="Panier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Panier_id", referencedColumnName="id")
     * })
     */
    private $panier;


    /**
     * @var \Livraison
     *
     * @ORM\OneToOne(targetEntity="Livraison")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Livraison_id", referencedColumnName="id")
     * })
     */
    private $livraison;
    
  public function __construct()
    {
        $this->datecommande = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datecommande
     *
     * @param \DateTime $datecommande
     * @return Commande
     */
    public function setDatecommande($datecommande)
    {
        $this->datecommande = $datecommande;

        return $this;
    }

    /**
     * Get datecommande
     *
     * @return \DateTime 
     */
    public function getDatecommande()
    {
        return $this->datecommande;
    }

    /**
     * Set totalcommande
     *
     * @param integer $totalcommande
     * @return Commande
     */
    public function setTotalcommande($totalcommande)
    {
        $this->totalcommande = $totalcommande;

        return $this;
    }

    /**
     * Get totalcommande
     *
     * @return integer 
     */
    public function getTotalcommande()
    {
        return $this->totalcommande;
    }

    /**
     * Set valider
     *
     * @param boolean $valider
     * @return Commande
     */
    public function setValider($valider)
    {
        $this->valider = $valider;

        return $this;
    }

    /**
     * Get valider
     *
     * @return boolean 
     */
    public function getValider()
    {
        return $this->valider;
    }



    /**
     * Set panier
     *
     * @param \PiDev\ClientBundle\Entity\Panier $panier
     * @return Commande
     */
    public function setPanier(\PiDev\ClientBundle\Entity\Panier $panier = null)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Get panier
     *
     * @return \PiDev\ClientBundle\Entity\Panier 
     */
    public function getPanier()
    {
        return $this->panier;
    }

    /**
     * Set livraison
     *
     * @param \PiDev\ClientBundle\Entity\Livraison $livraison
     * @return Commande
     */
    public function setLivraison(\PiDev\ClientBundle\Entity\Livraison $livraison = null)
    {
        $this->livraison = $livraison;

        return $this;
    }

    /**
     * Get livraison
     *
     * @return \PiDev\ClientBundle\Entity\Livraison 
     */
    public function getLivraison()
    {
        return $this->livraison;
    }
    
    public function __toString() {
     return $this->setLivraison()  ; 
    }
    public function getModepaiement() {
        return $this->modepaiement;
    }

    public function setModepaiement($modepaiement) {
        $this->modepaiement = $modepaiement;
    }


}
