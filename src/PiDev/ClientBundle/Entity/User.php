<?php
namespace PiDev\ClientBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="user")
*/
class User extends BaseUser
{
/**
* @ORM\Id
* @ORM\Column(type="integer")
* @ORM\GeneratedValue(strategy="AUTO")
*/
protected $id;
/**
* @ORM\Column(type="integer", nullable=true)
*/
protected $credit;

/**
* @ORM\Column(type="integer", nullable=true)
*/
protected $ptBonus;
 /**
  * @ORM\Column(name="img", type="blob", nullable=false)
 */
 private $img;
 /**
 * @Assert\File(maxSize="6000000")
 */
  public $file;
  
     /**
     * @var \Favoris
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="message_seq", initialValue=100, allocationSize=200)
     * @ORM\OneToOne(targetEntity="Favoris",cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="Favoris_id", referencedColumnName="id")
     * })
     */
        private $favoris;
                 /**
     * @var \Panier
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="message_seq", initialValue=100, allocationSize=200)
     * @ORM\OneToOne(targetEntity="Panier",cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="Panier_id", referencedColumnName="id")
     * })
     */
        private $panier;

public function __construct()
{
parent::__construct();
// your own logic
}
public function getId() {
    return $this->id;
}

public function setId($id) {
    $this->id = $id;
}




    /**
     * Set favoris
     *
     * @param \PiDev\ClientBundle\Entity\Favoris $favoris
     * @return User
     */
    public function setFavoris(\PiDev\ClientBundle\Entity\Favoris $favoris = null)
    {
        $this->favoris = $favoris;

        return $this;
    }

    /**
     * Get favoris
     *
     * @return \PiDev\ClientBundle\Entity\Favoris 
     */
    public function getFavoris()
    {
        return $this->favoris;
    }

    /**
     * Set panier
     *
     * @param \PiDev\ClientBundle\Entity\Panier $panier
     * @return User
     */
    public function setPanier(\PiDev\ClientBundle\Entity\Panier $panier = null)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Get panier
     *
     * @return \PiDev\ClientBundle\Entity\Panier 
     */
    public function getPanier()
    {
        return $this->panier;
    }
    

    public function __toString() {
        return (string) $this->getFavoris()+$this->getPanier() +$this->getId();
        
  
        
    }

    /**
     * Set credit
     *
     * @param integer $credit
     * @return User
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return integer 
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set ptBonus
     *
     * @param integer $ptBonus
     * @return User
     */
    public function setPtBonus($ptBonus)
    {
        $this->ptBonus = $ptBonus;

        return $this;
    }

    /**
     * Get ptBonus
     *
     * @return integer 
     */
    public function getPtBonus()
    {
        return $this->ptBonus;
    }

    /**
     * Set img
     *
     * @param string $img
     * @return User
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string 
     */
    public function getImg()
    {
        return $this->img;
    }
    function getFile() {
      return $this->file;
  }
  function setFile($file) {
      $this->file = $file;
  } 
}
