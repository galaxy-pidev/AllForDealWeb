<?php
namespace PiDev\ClientBundle\Entity ;
use Doctrine\ORM\Mapping as ORM ; 
use Symfony\Component\Validator\Constraints as Assert;
/**

* 

*

* @ORM\Entity

*/

class ImageProd 

{

 /**

 * @var integer

 *

 * @ORM\Column(name="id", type="integer", nullable=false)

 * @ORM\Id

 * @ORM\GeneratedValue(strategy="IDENTITY")

 */

 private $id;

 /**

  * @ORM\Column(name="img", type="blob", nullable=false)

 */

 private $img;
 /**
 * @var string
 * @ORM\Column(name="titre", type="string", nullable=false)
 */
 private $titre;
 /**
 * @Assert\File(maxSize="6000000")
 */
  public $file;
     /**
     * @ORM\ManyToOne(targetEntity="\PiDev\ClientBundle\Entity\Produit")
     *   @ORM\JoinColumn(name="produit_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $produit;
  function getFile() {
      return $this->file;
  }
  function setFile($file) {
      $this->file = $file;
  } 
  function getId() {
    return $this->id;
}
function getImg() {
    return $this->img;
}
function getTitre() {
    return $this->titre;
}
function setId($id) {
    $this->id = $id;
}
function setImg($img) {
    $this->img = $img;
}
function setTitre($titre) {
    $this->titre = $titre;
}

    
    public function getProduit() {
        return $this->produit;
    }

    public function setProduit($produit) {
        $this->produit = $produit;
    }



}