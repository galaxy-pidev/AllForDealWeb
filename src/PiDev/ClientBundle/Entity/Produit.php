<?php

namespace PiDev\ClientBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="PiDev\ClientBundle\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=45, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var integer
     *
     * @ORM\Column(name="qantite", type="integer", nullable=false)
     */
    private $qantite;

    /**
     * @var string
     *
     * @ORM\Column(name="promotion", type="string", length=45, nullable=false)
     */
    private $promotion;

    /**
     * @var string
     *
     * @ORM\Column(name="marque", type="string", length=45, nullable=false)
     */
    private $marque;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbVente", type="integer", nullable=false)
     */
    private $nbvente=0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="valider", type="boolean", nullable=false)
     */
    private $valider = false;




    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAjout", type="date", nullable=false)
     */
    private $dateajout;
         /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="PiDev\ClientBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     * })
     */
    private $user ;

     /**
     * @ORM\ManyToOne(targetEntity="\PiDev\AdminBundle\Entity\Categorie" )
      * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categorie_id", referencedColumnName="id" ,onDelete="CASCADE")
      *  })
     */
    private $categorie;
     /**
     *
     * @ORM\OneToMany(targetEntity="\PiDev\ClientBundle\Entity\ImageProd", mappedBy="produit")
     */
    private $images;
    
    
    
   
  public function __construct()
    {
        $this->dateajout = new \DateTime();
        $this->images=new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    
    /**
     * Set nom
     *
     * @param string $nom
     * @return Produit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Produit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Produit
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set qantite
     *
     * @param integer $qantite
     * @return Produit
     */
    public function setQantite($qantite)
    {
        $this->qantite = $qantite;

        return $this;
    }

    /**
     * Get qantite
     *
     * @return integer 
     */
    public function getQantite()
    {
        return $this->qantite;
    }

    /**
     * Set promotion
     *
     * @param string $promotion
     * @return Produit
     */
    public function setPromotion($promotion)
    {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion
     *
     * @return string 
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Set marque
     *
     * @param string $marque
     * @return Produit
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque
     *
     * @return string 
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Set nbvente
     *
     * @param integer $nbvente
     * @return Produit
     */
    public function setNbvente($nbvente)
    {
        $this->nbvente = $nbvente;

        return $this;
    }

    /**
     * Get nbvente
     *
     * @return integer 
     */
    public function getNbvente()
    {
        return $this->nbvente;
    }

    /**
     * Set valider
     *
     * @param boolean $valider
     * @return Produit
     */
    public function setValider($valider)
    {
        $this->valider = $valider;

        return $this;
    }

    /**
     * Get valider
     *
     * @return boolean 
     */
    public function getValider()
    {
        return $this->valider;
    }

    

    /**
     * Set dateajout
     *
     * @param \DateTime $dateajout
     * @return Produit
     */
    public function setDateajout($dateajout)
    {
        $this->dateajout = new \DateTime('now');

        return $this;
    }

    /**
     * Get dateajout
     *
     * @return \DateTime 
     */
    public function getDateajout()
    {
        return $this->dateajout;
    }

      /**
     * Set user
     *
     * @param \PiDev\ClientBundle\Entity\User
     * @return Adresse
     */
    public function setUser(\PiDev\ClientBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \PiDev\ClientBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

   
    /**
     * Set categorie
     *
     * @param \PiDev\AdminBundle\Entity\Categorie $categorie
     * @return Produit
     */
    public function setCategorie(\PiDev\AdminBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \PiDev\AdminBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
    

       public function __toString() {
        return $this->getCategorie();
    } 

}
