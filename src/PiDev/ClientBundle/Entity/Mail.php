<?php

namespace PiDev\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mail
 *
 * @ORM\Table(name="mail")
 * @ORM\Entity
 */
class Mail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="expediteur", type="string", length=255, nullable=false)
     */
    private $expediteur;

    /**
     * @var string
     *
     * @ORM\Column(name="Text", type="string", length=255, nullable=false)
     */
    private $text;
    
    /**
     * @var string
     *
     * @ORM\Column(name="destinaire", type="string", length=255, nullable=false)
     */
    private $destinaire;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateAjout", type="datetime", nullable=false)
     */
    private $dateAjout;
    
       public function __construct()
    {
        $this->dateAjout = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Mail
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

   

    /**
     * Set text
     *
     * @param string $text
     * @return Mail
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set destinaire
     *
     * @param string $destinaire
     * @return Mail
     */
    public function setDestinaire($destinaire)
    {
        $this->destinaire = $destinaire;

        return $this;
    }

    /**
     * Get destinaire
     *
     * @return string 
     */
    public function getDestinaire()
    {
        return $this->destinaire;
    }

    /**
     * Set expediteur
     *
     * @param string $expediteur
     * @return Mail
     */
    public function setExpediteur($expediteur)
    {
        $this->expediteur = $expediteur;

        return $this;
    }

    /**
     * Get expediteur
     *
     * @return string 
     */
    public function getExpediteur()
    {
        return $this->expediteur;
    }
    
            /**
     * Set dateAjout
     *
     * @param \DateTime $dateAjout
     * @return Commentaire
     */
    public function setDateAjout($dateAjout)
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }
    
    /**
     * Get dateAjout
     *
     * @return \DateTime 
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }
}
