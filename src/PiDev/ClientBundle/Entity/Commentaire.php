<?php

namespace PiDev\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire
 *
 * @ORM\Entity
 */
class Commentaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Corps", type="string", length=255, nullable=false)
     */
    private $corps;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCom", type="datetime", nullable=false)
     */
    private $datecom;

    /**
     * @var \Produit
     *
     * @ORM\ManyToOne(targetEntity="Produit" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Produit_id", referencedColumnName="id",onDelete="CASCADE")
     * })
     */
    private $produit;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="User_id", referencedColumnName="id",onDelete="CASCADE")
     * })
     */
    private $user;
    
   public function __construct()
    {
        $this->datecom = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set corps
     *
     * @param string $corps
     * @return Commentaire
     */
    public function setCorps($corps)
    {
        $this->corps = $corps;

        return $this;
    }

    /**
     * Get corps
     *
     * @return string 
     */
    public function getCorps()
    {
        return $this->corps;
    }

    /**
     * Set datecom
     *
     * @param \DateTime $datecom
     * @return Commentaire
     */
    public function setDatecom($datecom)
    {
        $this->datecom = $datecom;

        return $this;
    }

    /**
     * Get datecom
     *
     * @return \DateTime 
     */
    public function getDatecom()
    {
        return $this->datecom;
    }

    /**
     * Set produit
     *
     * @param \PiDev\ClientBundle\Entity\Produit $produit
     * @return Commentaire
     */
    public function setProduit(\PiDev\ClientBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \PiDev\ClientBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set user
     *
     * @param \PiDev\ClientBundle\Entity\User $user
     * @return Commentaire
     */
    public function setUser(\PiDev\ClientBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \PiDev\ClientBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
