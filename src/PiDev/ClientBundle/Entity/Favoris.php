<?php

namespace PiDev\ClientBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Favoris {
/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;


    
         /**
     * @var \Produit
     *
     * @ORM\ManyToMany(targetEntity="Produit" ,cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="Produit_id", referencedColumnName="id")
     * })
     */
    private $produit;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set produit
     *
     * @param \PiDev\ClientBundle\Entity\Produit $produit
     * @return Panier
     */
    
//    public function setProduit(\PiDev\ClientBundle\Entity\Produit $produit )
//    {
//        $this->produit[] = $produit;
//
//        return $this;
//    }
    function __construct() {
        $this->produit = new ArrayCollection();
    }

    /**
     * Get produit
     *
     * @return \PiDev\ClientBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }
    
    public function addProduit(\PiDev\ClientBundle\Entity\Produit $produit )
    {
     $this->produit[]=$produit;

    }
    
    

    /**
     * Remove produit
     *
     * @param \PiDev\ClientBundle\Entity\Produit $produit
     */
    public function removeProduit(\PiDev\ClientBundle\Entity\Produit $produit)
    {
        $this->produit->removeElement($produit);
    }
    
        public function __toString()
    {
        return (string ) $this->getId();
    }
}
