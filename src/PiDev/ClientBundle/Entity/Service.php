<?php
namespace PiDev\ClientBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class Service {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
   
        /**
     *@ORM\Column(type="string",length=255)
     */
    private $nom;
        /**
     *@ORM\Column(type="string",length=255)
     */
    private $Description;
    /**
     *@ORM\ManyToOne(targetEntity="PiDev\ClientBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="proprietaire_id", referencedColumnName="id",onDelete="CASCADE")
     * })
       */
    private $proprietaire;
         /**
     *@ORM\Column(type="integer", nullable=true)
     */
    private $valider;
         /**
     *@ORM\Column(type="integer")
     */
    private $estimation;
     /**
     *@ORM\ManyToOne(targetEntity="PiDev\AdminBundle\Entity\CategorieService")
     */
    private $Categorie;
      /**
     *@ORM\Column(type="string",length=255)
     */
    private $Mail;
    
     /**
     *@ORM\Column(type="integer")
     */
    private $Telephone;
    
     /**
     *@ORM\Column(type="string",length=255)
     */
    private $Adresse;
    
    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getDescription() {
        return $this->Description;
    }

    public function getProprietaire() {
        return $this->proprietaire;
    }

    public function getValider() {
        return $this->valider;
    }

    public function getEstimation() {
        return $this->estimation;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setDescription($Description) {
        $this->Description = $Description;
    }

    public function setProprietaire($proprietaire) {
        $this->proprietaire = $proprietaire;
    }

    public function setValider($valider) {
        $this->valider = $valider;
    }

    public function setEstimation($estimation) {
        $this->estimation = $estimation;
    }

    public function getCategorie() {
        return $this->Categorie;
    }

    public function setCategorie($Categorie) {
        $this->Categorie = $Categorie;
    }

    public function getMail() {
        return $this->Mail;
    }

    public function getTelephone() {
        return $this->Telephone;
    }

    public function getAdresse() {
        return $this->Adresse;
    }

    public function setMail($Mail) {
        $this->Mail = $Mail;
    }

    public function setTelephone($Telephone) {
        $this->Telephone = $Telephone;
    }

    public function setAdresse($Adresse) {
        $this->Adresse = $Adresse;
    }
    public function __toString() {
        return (string)$this->getId();
    }



}
