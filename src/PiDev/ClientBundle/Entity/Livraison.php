<?php

namespace PiDev\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Livraison
 *
 * @ORM\Table(name="livraison")
 * @ORM\Entity
 */
class Livraison
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="modeLivraison", type="string", length=45, nullable=true)
     */
    private $modelivraison;
//
//    /**
//     * @var \Commande
//     *
//     * @ORM\OneToOne(targetEntity="Commande")
//     * @ORM\JoinColumns({
//     * @ORM\JoinColumn(name="commande_id", referencedColumnName="id")
//     * })
//     */
//    private $commande;

    /**
     * @var \Adresse
     * @ORM\ManyToOne(targetEntity="\PiDev\ClientBundle\Entity\Adresse",cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="adresse_id", referencedColumnName="id")
     * })
     */
    private $adresse;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modelivraison
     *
     * @param string $modelivraison
     * @return Livraison
     */
    public function setModelivraison($modelivraison)
    {
        $this->modelivraison = $modelivraison;

        return $this;
    }

    /**
     * Get modelivraison
     *
     * @return string 
     */
    public function getModelivraison()
    {
        return $this->modelivraison;
    }
    function __construct() {
        
 
    }

        /**
     * Set adresse
     *
     * @param \PiDev\ClientBundle\Entity\Adresse $adresse
     * @return Livraison
     */
    public function setAdresse(\PiDev\ClientBundle\Entity\Adresse $adresse = null)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return \PiDev\ClientBundle\Entity\Adresse 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }
        
    public function __toString() {
        return $this->getAdresse();
    }
}
