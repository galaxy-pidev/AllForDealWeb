<?php

namespace PiDev\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentaireServiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commentaire','text',array('required' => true,'attr' => array('class' => 'form-control')))
         //   ->add('datecom')
          //  ->add('service')
         //   ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiDev\ClientBundle\Entity\CommentaireService'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'pidev_clientbundle_commentaireservice';
    }
}
