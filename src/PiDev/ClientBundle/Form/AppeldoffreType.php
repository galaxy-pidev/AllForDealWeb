<?php

namespace PiDev\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AppeldoffreType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('nom','text',array('required' => true,'attr' => array('class' => 'text')))
                ->add('Description','text',array('required' => true,'attr' => array('class' => 'text')))
                ->add('Mail','text',array('required' => true,'attr' => array('class' => 'text')))
                ->add('Telephone','integer',array('required' => true,'attr' => array('class' => 'text')))
                ->add('Adresse','text',array('required' => true,'attr' => array('class' => 'text')))
     ;
 
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiDev\ClientBundle\Entity\Appeldoffre'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'piDevClientBundle_appeldoffre';
    }
}