<?php

namespace PiDev\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProduitType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $builder
            ->add('nom','text',array('required' => true,'attr' => array('class' => 'text')))
            ->add('description','text',array('required' => true,'attr' => array('class' => 'text')))
            ->add('prix','integer',array('required' => true,'attr' => array('class' => 'text')))
            ->add('qantite','integer',array('required' => true,'attr' => array('class' => 'text')))
            ->add('promotion','integer',array('required' => true,'attr' => array('class' => 'text')))
            ->add('marque','text',array('required' => true,'attr' => array('class' => 'text')))
            //->add('nbvente')
          //  ->add('nbpointbase','integer',array('required' => true,'attr' => array('class' => 'text')))
            ->add('categorie','entity' , array('class'=> 'PiDev\AdminBundle\Entity\Categorie',
                'property'=>'nom'))

                 ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiDev\ClientBundle\Entity\Produit'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'pidev_clientbundle_produit';
    }
}
