<?php

namespace PiDev\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdresseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rue','text',array('required' => true,'attr' => array('class' => 'text')))
            ->add('ville','text',array('required' => true,'attr' => array('class' => 'text')))
            ->add('codepostal','integer',array('required' => true,'attr' => array('class' => 'text')))
            ->add('pays','text',array('required' => true,'attr' => array('class' => 'text')))
          //  ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiDev\ClientBundle\Entity\Adresse'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'pidev_clientbundle_adresse';
    }
}
