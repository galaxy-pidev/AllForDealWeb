<?php

namespace PiDev\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServiceType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array('required' => true, 'attr' => array('class' => 'text')))
                ->add('Description', 'text', array('required' => true, 'attr' => array('class' => 'text')))
                ->add('estimation', 'integer', array('required' => true, 'attr' => array('class' => 'text')))
                ->add('Categorie')
                ->add('Mail', 'text', array('required' => true, 'attr' => array('class' => 'text')))
                ->add('Telephone', 'integer', array('required' => true, 'attr' => array('class' => 'text')))
                ->add('Adresse', 'text', array('required' => true, 'attr' => array('class' => 'text')))
                ->add('Ajouter', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'PiDev\ClientBundle\Entity\Service'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'pidev_clientbundle_service';
    }

}
