<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel {

    public function registerBundles() {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Ob\HighchartsBundle\ObHighchartsBundle(),
          
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Nomaya\SocialBundle\NomayaSocialBundle(),
//            new FOS\UserBundle\FOSUserBundle(),
//            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
//            new Sonata\IntlBundle\SonataIntlBundle(),
//            new Sonata\NotificationBundle\SonataNotificationBundle(),
            //     new Application\Sonata\UserBundle\SonataUserBundle(),
//            new Application\Sonata\CustomerBundle\ApplicationSonataCustomerBundle(),
//            new Application\Sonata\DeliveryBundle\ApplicationSonataDeliveryBundle(),
//            new Application\Sonata\BasketBundle\ApplicationSonataBasketBundle(),
//            new Application\Sonata\InvoiceBundle\ApplicationSonataInvoiceBundle(),
//            new Application\Sonata\MediaBundle\ApplicationSonataMediaBundle(),
//            new Application\Sonata\OrderBundle\ApplicationSonataOrderBundle(),
//            new Application\Sonata\PaymentBundle\ApplicationSonataPaymentBundle(),
//            new Application\Sonata\ProductBundle\ApplicationSonataProductBundle(),
//            new Sonata\CustomerBundle\SonataCustomerBundle(),
//            new Sonata\ProductBundle\SonataProductBundle(),
//            new Sonata\BasketBundle\SonataBasketBundle(),
//            new Sonata\OrderBundle\SonataOrderBundle(),
//            new Sonata\InvoiceBundle\SonataInvoiceBundle(),
//            new Sonata\MediaBundle\SonataMediaBundle(),
//            new Sonata\DeliveryBundle\SonataDeliveryBundle(),
//            new Sonata\PaymentBundle\SonataPaymentBundle(),
//            new Sonata\PriceBundle\SonataPriceBundle(),
            new PiDev\UserBundle\PiDevUserBundle(),
            new PiDev\ClientBundle\PiDevClientBundle(),
            new PiDev\AdminBundle\PiDevAdminBundle(),
//          new FOS\RestBundle\FOSRestBundle(),
//          new FOS\CommentBundle\FOSCommentBundle(),
//          new JMS\SerializerBundle\JMSSerializerBundle($this),
            //   new Aimeos\ShopBundle\AimeosShopBundle(),
            new blackknight467\StarRatingBundle\StarRatingBundle(),
            new Ideato\StarRatingBundle\IdeatoStarRatingBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader) {
        $loader->load(__DIR__ . '/config/config_' . $this->getEnvironment() . '.yml');
    }

}
