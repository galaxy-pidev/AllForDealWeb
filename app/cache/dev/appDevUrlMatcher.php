<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/js/8bb605b')) {
            // _assetic_8bb605b
            if ($pathinfo === '/js/8bb605b.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '8bb605b',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_8bb605b',);
            }

            // _assetic_8bb605b_0
            if ($pathinfo === '/js/8bb605b_rating_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '8bb605b',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_8bb605b_0',);
            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // pi_dev_user_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pi_dev_user_homepage')), array (  '_controller' => 'PiDev\\UserBundle\\Controller\\DefaultController::indexAction',));
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

            // fos_user_resetting_request
            if ($pathinfo === '/profile/request') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_request;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_send_email
            if ($pathinfo === '/profile/send-email') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
            }

            // fos_user_resetting_check_email
            if ($pathinfo === '/profile/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_check_email;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
            }
            not_fos_user_resetting_check_email:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/profile/reset') && preg_match('#^/profile/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_resetting_reset;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
            }
            not_fos_user_resetting_reset:

            // fos_user_change_password
            if ($pathinfo === '/profile/change-password') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_change_password;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
            }
            not_fos_user_change_password:

        }

        if (0 === strpos($pathinfo, '/admin')) {
            // admin_supprimer_utilisateur
            if (preg_match('#^/admin/(?P<id>[^/]++)/supprimerUtilisateur$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_supprimer_utilisateur')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\AdminController::SupprimerUtilisateurAction',));
            }

            // admin_liste_utilisateurs
            if ($pathinfo === '/admin/listeUser') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\AdminController::ShowAllUtilisateurAction',  '_route' => 'admin_liste_utilisateurs',);
            }

            // admin_modifier_utilisateur
            if (0 === strpos($pathinfo, '/admin/ModifierUtilisateur') && preg_match('#^/admin/ModifierUtilisateur/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_modifier_utilisateur')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\AdminController::ModifierUtilisateurAction',));
            }

            // admin_services_nonvalidés
            if ($pathinfo === '/admin/ListeServicesNonValides') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\AdminController::ListeServicesNonValidésAction',  '_route' => 'admin_services_nonvalidés',);
            }

            // admin_valider_service
            if (0 === strpos($pathinfo, '/admin/ValiderService') && preg_match('#^/admin/ValiderService/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_valider_service')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\AdminController::ValiderServiceAction',));
            }

            // admin_produits_nonvalidés
            if ($pathinfo === '/admin/ListeProduitsNonValidés') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\AdminController::ListeProduitsNonValidésAction',  '_route' => 'admin_produits_nonvalidés',);
            }

            // admin_valider_produit
            if (0 === strpos($pathinfo, '/admin/ValiderProduit') && preg_match('#^/admin/ValiderProduit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_valider_produit')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\AdminController::ValiderProduitAction',));
            }

        }

        if (0 === strpos($pathinfo, '/categorie')) {
            // categorie
            if (rtrim($pathinfo, '/') === '/categorie') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'categorie');
                }

                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieController::indexAction',  '_route' => 'categorie',);
            }

            // categorie_show
            if (preg_match('#^/categorie/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorie_show')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieController::showAction',));
            }

            // categorie_new
            if ($pathinfo === '/categorie/new') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieController::newAction',  '_route' => 'categorie_new',);
            }

            // categorie_create
            if ($pathinfo === '/categorie/create') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieController::createAction',  '_route' => 'categorie_create',);
            }

            // categorie_edit
            if (preg_match('#^/categorie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorie_edit')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieController::editAction',));
            }

            // categorie_update
            if (preg_match('#^/categorie/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorie_update')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieController::updateAction',));
            }

            // categorie_delete
            if (preg_match('#^/categorie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorie_delete')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieController::deleteAction',));
            }

            // categorie_recherche
            if ($pathinfo === '/categorie/rechercher') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieController::RechercheAction',  '_route' => 'categorie_recherche',);
            }

            // categorie_list
            if ($pathinfo === '/categorie/list') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieController::listAction',  '_route' => 'categorie_list',);
            }

            if (0 === strpos($pathinfo, '/categorieservice')) {
                // categorieservice
                if (rtrim($pathinfo, '/') === '/categorieservice') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'categorieservice');
                    }

                    return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieServiceController::indexAction',  '_route' => 'categorieservice',);
                }

                // categorieservice_show
                if (preg_match('#^/categorieservice/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorieservice_show')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieServiceController::showAction',));
                }

                // categorieservice_new
                if ($pathinfo === '/categorieservice/new') {
                    return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieServiceController::newAction',  '_route' => 'categorieservice_new',);
                }

                // categorieservice_create
                if ($pathinfo === '/categorieservice/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_categorieservice_create;
                    }

                    return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieServiceController::createAction',  '_route' => 'categorieservice_create',);
                }
                not_categorieservice_create:

                // categorieservice_edit
                if (preg_match('#^/categorieservice/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorieservice_edit')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieServiceController::editAction',));
                }

                // categorieservice_update
                if (preg_match('#^/categorieservice/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorieservice_update')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieServiceController::updateAction',));
                }

                // categorieservice_delete
                if (preg_match('#^/categorieservice/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorieservice_delete')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieServiceController::deleteAction',));
                }

                // categorieservice_showAll
                if ($pathinfo === '/categorieservice/showAll') {
                    return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\CategorieServiceController::showAllAction',  '_route' => 'categorieservice_showAll',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/publicite')) {
            // publicite
            if (rtrim($pathinfo, '/') === '/publicite') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'publicite');
                }

                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\PubliciteController::indexAction',  '_route' => 'publicite',);
            }

            // publicite_show
            if (preg_match('#^/publicite/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'publicite_show')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\PubliciteController::showAction',));
            }

            // publicite_create
            if ($pathinfo === '/publicite/new') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\PubliciteController::createAction',  '_route' => 'publicite_create',);
            }

            // publicite_image
            if (preg_match('#^/publicite/(?P<id>[^/]++)/image$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'publicite_image')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\PubliciteController::AfficheImageAction',));
            }

            // publicite_edit
            if (preg_match('#^/publicite/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'publicite_edit')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\PubliciteController::editAction',));
            }

            // publicite_update
            if (preg_match('#^/publicite/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_publicite_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'publicite_update')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\PubliciteController::updateAction',));
            }
            not_publicite_update:

            // publicite_delete
            if (preg_match('#^/publicite/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'publicite_delete')), array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\PubliciteController::deleteAction',));
            }

            // publicite_showAll
            if ($pathinfo === '/publicite/showAll') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\PubliciteController::showAllAction',  '_route' => 'publicite_showAll',);
            }

        }

        if (0 === strpos($pathinfo, '/statistique/s')) {
            // statistique
            if ($pathinfo === '/statistique/stat') {
                return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\StatistiqueController::chartLineAction',  '_route' => 'statistique',);
            }

            // show
            if ($pathinfo === '/statistique/show') {
                return array (  '_controller' => 'PiDevAdminBundle:Valider:show',  '_route' => 'show',);
            }

            // supprimer
            if (0 === strpos($pathinfo, '/statistique/supp') && preg_match('#^/statistique/supp/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'supprimer')), array (  '_controller' => 'PiDevAdminBundle:Valider:supprimer',));
            }

        }

        // pi_dev_admin_homepage
        if ($pathinfo === '/hello') {
            return array (  '_controller' => 'PiDev\\AdminBundle\\Controller\\DefaultController::indexAction',  '_route' => 'pi_dev_admin_homepage',);
        }

        if (0 === strpos($pathinfo, '/lignecommande')) {
            // lignecommande
            if (rtrim($pathinfo, '/') === '/lignecommande') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'lignecommande');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\LigneCommandeController::indexAction',  '_route' => 'lignecommande',);
            }

            // lignecommande_show
            if (preg_match('#^/lignecommande/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'lignecommande_show')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\LigneCommandeController::showAction',));
            }

            // lignecommande_create
            if (preg_match('#^/lignecommande/(?P<produit>[^/]++)/create$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'lignecommande_create')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\LigneCommandeController::createAction',));
            }

            // lignecommande_edit
            if (preg_match('#^/lignecommande/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'lignecommande_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\LigneCommandeController::editAction',));
            }

            // lignecommande_update
            if (preg_match('#^/lignecommande/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_lignecommande_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'lignecommande_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\LigneCommandeController::updateAction',));
            }
            not_lignecommande_update:

            // lignecommande_delete
            if (preg_match('#^/lignecommande/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_lignecommande_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'lignecommande_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\LigneCommandeController::deleteAction',));
            }
            not_lignecommande_delete:

        }

        if (0 === strpos($pathinfo, '/image')) {
            // my_app_esprit_upload
            if ($pathinfo === '/image/upload') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ImageController::uploadAction',  '_route' => 'my_app_esprit_upload',);
            }

            // my_app_esprit_list
            if ($pathinfo === '/image/list') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ImageController::listAction',  '_route' => 'my_app_esprit_list',);
            }

            // my_app_esprit_aff_article
            if (0 === strpos($pathinfo, '/image/aff') && preg_match('#^/image/aff/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_esprit_aff_article')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ImageController::afficheAction',));
            }

            // my_image_route
            if (0 === strpos($pathinfo, '/image/images') && preg_match('#^/image/images/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_image_route')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ImageController::photoAction',));
            }

        }

        if (0 === strpos($pathinfo, '/client')) {
            // client
            if (rtrim($pathinfo, '/') === '/client') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'client');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::homeAction',  '_route' => 'client',);
            }

            // Client_Desactiver_compte
            if ($pathinfo === '/client/desactiver') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::DesactiverCompteAction',  '_route' => 'Client_Desactiver_compte',);
            }

            // image
            if ($pathinfo === '/client/image') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::afficheImageAction',  '_route' => 'image',);
            }

            // client_compte_parametres
            if ($pathinfo === '/client/parametres') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::ParametresAction',  '_route' => 'client_compte_parametres',);
            }

            if (0 === strpos($pathinfo, '/client/mes')) {
                // client_list_produit
                if ($pathinfo === '/client/mesproduits') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::listerProduitsAction',  '_route' => 'client_list_produit',);
                }

                // client_list_services
                if ($pathinfo === '/client/messervices') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::listerServicesAction',  '_route' => 'client_list_services',);
                }

                // client_list_Forums
                if ($pathinfo === '/client/mesforums') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::listerForumsAction',  '_route' => 'client_list_Forums',);
                }

                // client_list_appeloffres
                if ($pathinfo === '/client/mesappeloffres') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::listerAppelOffresAction',  '_route' => 'client_list_appeloffres',);
                }

            }

            if (0 === strpos($pathinfo, '/client/list')) {
                // listPanier
                if ($pathinfo === '/client/listPanier') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::listerPanierAction',  '_route' => 'listPanier',);
                }

                // listCommande
                if ($pathinfo === '/client/listCommande') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::listerCommandeAction',  '_route' => 'listCommande',);
                }

            }

            // Inbox
            if ($pathinfo === '/client/inbox') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::InboxAction',  '_route' => 'Inbox',);
            }

            // user_image
            if (0 === strpos($pathinfo, '/client/ImageUser') && preg_match('#^/client/ImageUser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_image')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::afficheImageUserAction',));
            }

            // Profile
            if ($pathinfo === '/client/monprofile') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ClientController::MyprofileAction',  '_route' => 'Profile',);
            }

        }

        if (0 === strpos($pathinfo, '/evaluationservice')) {
            // evaluationservice
            if (rtrim($pathinfo, '/') === '/evaluationservice') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'evaluationservice');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationServiceController::indexAction',  '_route' => 'evaluationservice',);
            }

            // evaluationservice_show
            if (preg_match('#^/evaluationservice/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationservice_show')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationServiceController::showAction',));
            }

            // evaluationservice_new
            if ($pathinfo === '/evaluationservice/new') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationServiceController::newAction',  '_route' => 'evaluationservice_new',);
            }

            // evaluationservice_create
            if (0 === strpos($pathinfo, '/evaluationservice/create') && preg_match('#^/evaluationservice/create/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationservice_create')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationServiceController::createAction',));
            }

            // evaluation_create
            if (0 === strpos($pathinfo, '/evaluationservice/ajout') && preg_match('#^/evaluationservice/ajout/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluation_create')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationServiceController::createAction',));
            }

            // evaluationservice_edit
            if (preg_match('#^/evaluationservice/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationservice_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationServiceController::editAction',));
            }

            // evaluationservice_update
            if (preg_match('#^/evaluationservice/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationservice_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationServiceController::updateAction',));
            }

            // evaluationservice_delete
            if (preg_match('#^/evaluationservice/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_evaluationservice_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationservice_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationServiceController::deleteAction',));
            }
            not_evaluationservice_delete:

        }

        if (0 === strpos($pathinfo, '/commentaires')) {
            if (0 === strpos($pathinfo, '/commentaireservice')) {
                // commentaireservice
                if (rtrim($pathinfo, '/') === '/commentaireservice') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'commentaireservice');
                    }

                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentaireServiceController::indexAction',  '_route' => 'commentaireservice',);
                }

                // commentaireservice_show
                if (preg_match('#^/commentaireservice/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentaireservice_show')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentaireServiceController::showAction',));
                }

                // commentaireservice_edit
                if (preg_match('#^/commentaireservice/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentaireservice_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentaireServiceController::editAction',));
                }

                // commentaireservice_update
                if (preg_match('#^/commentaireservice/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_commentaireservice_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentaireservice_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentaireServiceController::updateAction',));
                }
                not_commentaireservice_update:

                // commentaireservice_delete
                if (preg_match('#^/commentaireservice/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentaireservice_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentaireServiceController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/commentairesujet')) {
                // commentairesujet
                if (rtrim($pathinfo, '/') === '/commentairesujet') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'commentairesujet');
                    }

                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentairesujetController::indexAction',  '_route' => 'commentairesujet',);
                }

                // commentairesujet_show
                if (preg_match('#^/commentairesujet/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentairesujet_show')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentairesujetController::showAction',));
                }

                // commentairesujet_edit
                if (preg_match('#^/commentairesujet/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentairesujet_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentairesujetController::editAction',));
                }

                // commentairesujet_update
                if (preg_match('#^/commentairesujet/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_commentairesujet_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentairesujet_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentairesujetController::updateAction',));
                }
                not_commentairesujet_update:

                // commentairesujet_delete
                if (preg_match('#^/commentairesujet/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentairesujet_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentairesujetController::deleteAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/sujet')) {
            // sujet
            if (rtrim($pathinfo, '/') === '/sujet') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'sujet');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\SujetController::indexAction',  '_route' => 'sujet',);
            }

            // detail_sujet
            if (preg_match('#^/sujet/(?P<sujet>[^/]++)/detail$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'detail_sujet')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\SujetController::DetailSujetAction',));
            }

            // sujet_show-All
            if ($pathinfo === '/sujet/showAll') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\SujetController::showAllAction',  '_route' => 'sujet_show-All',);
            }

            // sujet_create
            if ($pathinfo === '/sujet/new') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\SujetController::createAction',  '_route' => 'sujet_create',);
            }

            // sujet_edit
            if (preg_match('#^/sujet/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sujet_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\SujetController::editAction',));
            }

            // sujet_image
            if (preg_match('#^/sujet/(?P<id>[^/]++)/image$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sujet_image')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\SujetController::AfficheImageAction',));
            }

            // sujet_update
            if (preg_match('#^/sujet/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_sujet_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sujet_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\SujetController::updateAction',));
            }
            not_sujet_update:

            // sujet_delete
            if (preg_match('#^/sujet/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sujet_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\SujetController::deleteAction',));
            }

        }

        if (0 === strpos($pathinfo, '/adresse')) {
            // adresse
            if (rtrim($pathinfo, '/') === '/adresse') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'adresse');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AdresseController::indexAction',  '_route' => 'adresse',);
            }

            // adresse_show
            if (preg_match('#^/adresse/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adresse_show')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AdresseController::showAction',));
            }

            if (0 === strpos($pathinfo, '/adresse/c')) {
                // adresse_create
                if ($pathinfo === '/adresse/create') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AdresseController::createAction',  '_route' => 'adresse_create',);
                }

                // choisir_adresse
                if ($pathinfo === '/adresse/choisir') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AdresseController::ChoisirAdresseAction',  '_route' => 'choisir_adresse',);
                }

            }

            // adresse_edit
            if (preg_match('#^/adresse/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adresse_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AdresseController::editAction',));
            }

            // adresse_update
            if (preg_match('#^/adresse/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_adresse_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adresse_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AdresseController::updateAction',));
            }
            not_adresse_update:

            // adresse_delete
            if (preg_match('#^/adresse/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_adresse_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'adresse_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AdresseController::deleteAction',));
            }
            not_adresse_delete:

            // localiser_adresse
            if ($pathinfo === '/adresse/map') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AdresseController::LocaliserAdresseAction',  '_route' => 'localiser_adresse',);
            }

        }

        if (0 === strpos($pathinfo, '/evaluationproduit')) {
            // evaluationproduit
            if (rtrim($pathinfo, '/') === '/evaluationproduit') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'evaluationproduit');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationProduitController::indexAction',  '_route' => 'evaluationproduit',);
            }

            // evaluationproduit_show
            if (preg_match('#^/evaluationproduit/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationproduit_show')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationProduitController::showAction',));
            }

            // evaluationproduit_create
            if (0 === strpos($pathinfo, '/evaluationproduit/create') && preg_match('#^/evaluationproduit/create/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationproduit_create')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationProduitController::createAction',));
            }

            // AjoutEva
            if (0 === strpos($pathinfo, '/evaluationproduit/ajout') && preg_match('#^/evaluationproduit/ajout/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'AjoutEva')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationProduitController::createAction',));
            }

            // evaluationproduit_edit
            if (preg_match('#^/evaluationproduit/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationproduit_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationProduitController::editAction',));
            }

            // evaluationproduit_update
            if (preg_match('#^/evaluationproduit/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_evaluationproduit_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationproduit_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationProduitController::updateAction',));
            }
            not_evaluationproduit_update:

            // evaluationproduit_delete
            if (preg_match('#^/evaluationproduit/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_evaluationproduit_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluationproduit_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\EvaluationProduitController::deleteAction',));
            }
            not_evaluationproduit_delete:

        }

        if (0 === strpos($pathinfo, '/commande')) {
            // facture_commande
            if (0 === strpos($pathinfo, '/commande/facturation') && preg_match('#^/commande/facturation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'facture_commande')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommandeController::FactureCommandeAction',));
            }

            // confirmer_commande
            if (0 === strpos($pathinfo, '/commande/confirmer') && preg_match('#^/commande/confirmer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'confirmer_commande')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommandeController::ConfirmerCommandeAction',));
            }

            // reponse_commande
            if (0 === strpos($pathinfo, '/commande/reponse') && preg_match('#^/commande/reponse/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reponse_commande')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommandeController::ReponseCommandeAction',));
            }

            // payer_commande
            if (0 === strpos($pathinfo, '/commande/payer') && preg_match('#^/commande/payer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'payer_commande')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommandeController::PayerCommandeAction',));
            }

        }

        if (0 === strpos($pathinfo, '/appelOffre')) {
            // new_appel_offre
            if ($pathinfo === '/appelOffre/new') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AppeldoffreController::AjouterAction',  '_route' => 'new_appel_offre',);
            }

            // recherche_appel_offre
            if ($pathinfo === '/appelOffre/rechercher') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AppeldoffreController::RechercheAction',  '_route' => 'recherche_appel_offre',);
            }

            // detail_appel_offre
            if (0 === strpos($pathinfo, '/appelOffre/details') && preg_match('#^/appelOffre/details/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'detail_appel_offre')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AppeldoffreController::DetailsAction',));
            }

            // supprimer_appel_offre
            if (0 === strpos($pathinfo, '/appelOffre/supprimer') && preg_match('#^/appelOffre/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'supprimer_appel_offre')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AppeldoffreController::supprimerAction',));
            }

            // modifier_appel_offre
            if (0 === strpos($pathinfo, '/appelOffre/modifier') && preg_match('#^/appelOffre/modifier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'modifier_appel_offre')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AppeldoffreController::ModifierAction',));
            }

        }

        if (0 === strpos($pathinfo, '/service')) {
            // service_supprimer
            if (0 === strpos($pathinfo, '/service/supprimer') && preg_match('#^/service/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'service_supprimer')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ServiceController::supprimerAction',));
            }

            // service_modifier
            if (0 === strpos($pathinfo, '/service/modifier') && preg_match('#^/service/modifier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'service_modifier')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ServiceController::ModifierAction',));
            }

            // service_ajouter
            if ($pathinfo === '/service/ajout') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ServiceController::AjouterAction',  '_route' => 'service_ajouter',);
            }

            // recherchre
            if ($pathinfo === '/service/recherche') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ServiceController::RechercheAction',  '_route' => 'recherchre',);
            }

            // service_details
            if (preg_match('#^/service/(?P<service>[^/]++)/details$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'service_details')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ServiceController::DetailServiceAction',));
            }

            // service_show_all
            if ($pathinfo === '/service/showAll') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ServiceController::FindAllAction',  '_route' => 'service_show_all',);
            }

        }

        if (0 === strpos($pathinfo, '/mail')) {
            // mail
            if (rtrim($pathinfo, '/') === '/mail') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'mail');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\MailController::indexAction',  '_route' => 'mail',);
            }

            // mail_show
            if (preg_match('#^/mail/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'mail_show')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\MailController::showAction',));
            }

            // my_app_mail_succ
            if ($pathinfo === '/mail/succ') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\MailController::indexAction',  '_route' => 'my_app_mail_succ',);
            }

            // mail_new
            if ($pathinfo === '/mail/mail') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\MailController::newAction',  '_route' => 'mail_new',);
            }

            // my_app_mail_sendpage
            if ($pathinfo === '/mail/sendmail') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\MailController::sendMailAction',  '_route' => 'my_app_mail_sendpage',);
            }

        }

        // AccueilClient
        if ($pathinfo === '/accueilclient') {
            return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AccueilController::AccueilAction',  '_route' => 'AccueilClient',);
        }

        if (0 === strpos($pathinfo, '/favoris')) {
            // favoris
            if (rtrim($pathinfo, '/') === '/favoris') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'favoris');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\FavorisController::indexAction',  '_route' => 'favoris',);
            }

            // favoris_show
            if ($pathinfo === '/favoris/show') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\FavorisController::showAction',  '_route' => 'favoris_show',);
            }

            // favoris_create
            if (preg_match('#^/favoris/(?P<id>[^/]++)/(?P<produit>[^/]++)/create$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_create')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\FavorisController::createAction',));
            }

            // favoris_remove
            if (preg_match('#^/favoris/(?P<id>[^/]++)/(?P<produit>[^/]++)/remove$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_remove')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\FavorisController::removeAction',));
            }

            // favoris_edit
            if (preg_match('#^/favoris/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\FavorisController::editAction',));
            }

            // favoris_update
            if (preg_match('#^/favoris/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\FavorisController::updateAction',));
            }

            // favoris_delete
            if (preg_match('#^/favoris/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\FavorisController::deleteAction',));
            }

        }

        if (0 === strpos($pathinfo, '/produit')) {
            // produit
            if ($pathinfo === '/produit/showAll') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ProduitController::showAllAction',  '_route' => 'produit',);
            }

            // produit_detail
            if (preg_match('#^/produit/(?P<id>[^/]++)/detail$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'produit_detail')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ProduitController::ProduitDetailAction',));
            }

            // produit_create
            if ($pathinfo === '/produit/new') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ProduitController::AjouterAction',  '_route' => 'produit_create',);
            }

            // produit_edit
            if (preg_match('#^/produit/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'produit_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ProduitController::editAction',));
            }

            // produit_update
            if (preg_match('#^/produit/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'PUT', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'PUT', 'HEAD'));
                    goto not_produit_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'produit_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ProduitController::updateAction',));
            }
            not_produit_update:

            // produit_delete
            if (0 === strpos($pathinfo, '/produit/delete') && preg_match('#^/produit/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'produit_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ProduitController::deleteAction',));
            }

            // produit_recherche
            if ($pathinfo === '/produit/recherche') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ProduitController::rechercheAction',  '_route' => 'produit_recherche',);
            }

        }

        if (0 === strpos($pathinfo, '/commentaire')) {
            // commentaire
            if (rtrim($pathinfo, '/') === '/commentaire') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'commentaire');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentaireController::indexAction',  '_route' => 'commentaire',);
            }

            // commentaire_edit
            if (preg_match('#^/commentaire/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentaire_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentaireController::editAction',));
            }

            // commentaire_update
            if (preg_match('#^/commentaire/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_commentaire_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentaire_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentaireController::updateAction',));
            }
            not_commentaire_update:

            // commentaire_delete
            if (preg_match('#^/commentaire/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentaire_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\CommentaireController::deleteAction',));
            }

        }

        if (0 === strpos($pathinfo, '/panier')) {
            // panier
            if (rtrim($pathinfo, '/') === '/panier') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'panier');
                }

                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\PanierController::indexAction',  '_route' => 'panier',);
            }

            // panier_show
            if ($pathinfo === '/panier/show') {
                return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\PanierController::showAction',  '_route' => 'panier_show',);
            }

            // panier_remove
            if (preg_match('#^/panier/(?P<id>[^/]++)/(?P<produit>[^/]++)/remove$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'panier_remove')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\PanierController::removeAction',));
            }

            // panier_create
            if (preg_match('#^/panier/(?P<id>[^/]++)/(?P<produit>[^/]++)/create$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'panier_create')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\PanierController::createAction',));
            }

            // panier_edit
            if (preg_match('#^/panier/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'panier_edit')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\PanierController::editAction',));
            }

            // panier_update
            if (preg_match('#^/panier/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_panier_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'panier_update')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\PanierController::updateAction',));
            }
            not_panier_update:

            // panier_delete
            if (preg_match('#^/panier/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_panier_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'panier_delete')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\PanierController::deleteAction',));
            }
            not_panier_delete:

        }

        // pi_dev_client_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pi_dev_client_homepage');
            }

            return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\AccueilController::accueilAction',  '_route' => 'pi_dev_client_homepage',);
        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_format' => 'html',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_format' => 'html',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',  '_format' => 'html',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_format' => 'html',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/reclamation')) {
                // reclamation
                if (rtrim($pathinfo, '/') === '/reclamation') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'reclamation');
                    }

                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ReclamationController::indexAction',  '_route' => 'reclamation',);
                }

                // reclamation_show
                if (preg_match('#^/reclamation/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'reclamation_show')), array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ReclamationController::showAction',));
                }

                // my_app_reclamation_succ
                if ($pathinfo === '/reclamation/succ') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ReclamationController::indexAction',  '_route' => 'my_app_reclamation_succ',);
                }

                // my_app_reclamation_form
                if ($pathinfo === '/reclamation/reclamation') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ReclamationController::newAction',  '_route' => 'my_app_reclamation_form',);
                }

                // my_app_reclamation_sendpage
                if ($pathinfo === '/reclamation/sendreclamation') {
                    return array (  '_controller' => 'PiDev\\ClientBundle\\Controller\\ReclamationController::sendReclamationAction',  '_route' => 'my_app_reclamation_sendpage',);
                }

            }

        }

        // homepage
        if ($pathinfo === '/app/example') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        // ideato_star_rating_rate
        if ($pathinfo === '/isr-rate') {
            return array (  '_controller' => 'Ideato\\StarRatingBundle\\Controller\\StarRatingController::rateAction',  '_route' => 'ideato_star_rating_rate',);
        }

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _demo_security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_demo_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
