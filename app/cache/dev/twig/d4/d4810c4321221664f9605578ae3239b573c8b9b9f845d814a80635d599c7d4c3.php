<?php

/* PiDevClientBundle:EvaluationProduit:new.html.twig */
class __TwigTemplate_a92218e329759adf7b45e23c0693978760f563bd7dd947918ff06d23e3eee2ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:EvaluationProduit:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/style.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/rating.css"), "html", null, true);
        echo "\" />

    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js\"></script>
    ";
        // line 8
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "8bb605b_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8bb605b_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/8bb605b_rating_1.js");
            // line 10
            echo "      <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/js/rating.js"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "8bb605b"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8bb605b") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/8bb605b.js");
            echo "      <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/js/rating.js"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        // line 12
        echo "
    <h1>Evaluation d'un produit </h1>
    
  
      <fieldset>  <legend>Produit ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "nom", array()), "html", null, true);
        echo " </legend>


    <table class=\"record_properties\">
        <tbody>
 
            </tr>
            <tr>
                <th>Marque</th>
                <td>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "marque", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "description", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Prix</th>
                <td>";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "prix", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Quantite</th>
                <td>";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "qantite", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Promotion</th>
                <td>";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "promotion", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nombre de point bonus</th>
                <td>";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "nbPointBase", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>

    </table>
               <form method=\"post\" >  
                   
                    
                 
<div class=\"stars\">
\t\t<input type=\"radio\" name=\"adresse\" class=\"star-1\" id=\"star-1\" value=\"1\" />
\t\t<label class=\"star-1\" for=\"star-1\">1</label>
\t\t<input type=\"radio\" name=\"adresse\" class=\"star-2\" id=\"star-2\" value=\"2\"/>
\t\t<label class=\"star-2\" for=\"star-2\">2</label>
\t\t<input type=\"radio\" name=\"adresse\" class=\"star-3\" id=\"star-3\" value=\"3\"/>
\t\t<label class=\"star-3\" for=\"star-3\">3</label>
\t\t<input type=\"radio\" name=\"adresse\" class=\"star-4\" id=\"star-4\" value=\"4\"/>
\t\t<label class=\"star-4\" for=\"star-4\">4</label>
\t\t<input type=\"radio\" name=\"adresse\" class=\"star-5\" id=\"star-5\" value=\"5\"/>
\t\t<label class=\"star-5\" for=\"star-5\">5</label>               
\t\t<span></span>              
\t</div>                   
                     <input type=\"submit\" value=\"valider\"/>
               </form>
        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 71
        echo $this->env->getExtension('routing')->getPath("evaluationproduit");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:EvaluationProduit:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 71,  115 => 45,  108 => 41,  101 => 37,  94 => 33,  87 => 29,  80 => 25,  68 => 16,  62 => 12,  48 => 10,  44 => 8,  38 => 5,  34 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* */
/* <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/style.css') }}" />*/
/* <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/rating.css') }}" />*/
/* */
/*     <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>*/
/*     {% javascripts*/
/*       '@StarRatingBundle/Resources/public/js/rating.js' %}*/
/*       <script src="{{ asset('bundles/starrating/js/rating.js') }}"></script>*/
/*     {% endjavascripts %}*/
/* */
/*     <h1>Evaluation d'un produit </h1>*/
/*     */
/*   */
/*       <fieldset>  <legend>Produit {{ produit.nom }} </legend>*/
/* */
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*  */
/*             </tr>*/
/*             <tr>*/
/*                 <th>Marque</th>*/
/*                 <td>{{ produit.marque }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Description</th>*/
/*                 <td>{{ produit.description }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Prix</th>*/
/*                 <td>{{ produit.prix }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Quantite</th>*/
/*                 <td>{{ produit.qantite }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Promotion</th>*/
/*                 <td>{{ produit.promotion }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Nombre de point bonus</th>*/
/*                 <td>{{ produit.nbPointBase }}</td>*/
/*             </tr>*/
/*             <tr>*/
/* */
/*     </table>*/
/*                <form method="post" >  */
/*                    */
/*                     */
/*                  */
/* <div class="stars">*/
/* 		<input type="radio" name="adresse" class="star-1" id="star-1" value="1" />*/
/* 		<label class="star-1" for="star-1">1</label>*/
/* 		<input type="radio" name="adresse" class="star-2" id="star-2" value="2"/>*/
/* 		<label class="star-2" for="star-2">2</label>*/
/* 		<input type="radio" name="adresse" class="star-3" id="star-3" value="3"/>*/
/* 		<label class="star-3" for="star-3">3</label>*/
/* 		<input type="radio" name="adresse" class="star-4" id="star-4" value="4"/>*/
/* 		<label class="star-4" for="star-4">4</label>*/
/* 		<input type="radio" name="adresse" class="star-5" id="star-5" value="5"/>*/
/* 		<label class="star-5" for="star-5">5</label>               */
/* 		<span></span>              */
/* 	</div>                   */
/*                      <input type="submit" value="valider"/>*/
/*                </form>*/
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('evaluationproduit') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/* </ul>*/
/* {% endblock %}*/
