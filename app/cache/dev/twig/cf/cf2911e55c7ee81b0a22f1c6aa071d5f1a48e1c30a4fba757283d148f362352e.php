<?php

/* PiDevAdminBundle:Admin:liste.html.twig */
class __TwigTemplate_7e9bf400da5f54873db72763281f69f7054a035172dd3c7c195c125abf15465a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:Admin:liste.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h1>Listes  des Services a valider </h1>

    
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modeles"]) ? $context["modeles"] : $this->getContext($context, "modeles")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 7
            echo "        <div>
            <br>   Services:<th>";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</th></br>
            <br>    Description:<th>";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</th></br>
    ";
            // line 11
            echo "       <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_valider_service", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">Valider se service</a>
        </div>
         
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "  ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Admin:liste.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 15,  51 => 11,  47 => 9,  43 => 8,  40 => 7,  36 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::baseAdmin.html.twig' %}*/
/* {% block content %}*/
/* <h1>Listes  des Services a valider </h1>*/
/* */
/*     */
/*     {% for i in modeles %}*/
/*         <div>*/
/*             <br>   Services:<th>{{i.nom}}</th></br>*/
/*             <br>    Description:<th>{{i.description}}</th></br>*/
/*     {#  <a href="{{path("details",{'id':i.id})}}">En savoir Plus!</a> #}*/
/*        <a href="{{path("admin_valider_service",{'id':i.id})}}">Valider se service</a>*/
/*         </div>*/
/*          */
/*     {% endfor %}*/
/*   {% endblock %}*/
