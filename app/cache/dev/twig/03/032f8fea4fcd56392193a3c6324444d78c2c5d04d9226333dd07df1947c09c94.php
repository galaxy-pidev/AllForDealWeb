<?php

/* PiDevClientBundle:Panier:affiche.html.twig */
class __TwigTemplate_4252890a8019c62c97e34063423d82a57f1c8823818bd2f18ad68319d2b987aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <html>
        <head>  
<!-- styles -->
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- theme stylesheet -->
    <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/style.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-stylesheet\">

    <!-- your stylesheet with modifications -->
    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/custom.css\" rel=\"stylesheet"), "html", null, true);
        echo "\">

    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/respond.min.js"), "html", null, true);
        echo "\"></script>

    <link rel=\"shortcut icon\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/img/favicon.png"), "html", null, true);
        echo "\">



</head>

<body>";
        // line 26
        echo "    
    
    
    
  <div id=\"content\">
            <div class=\"container\">


                <div class=\"col-md-9\" id=\"basket\">

                    <div class=\"box\">

                        <form method=\"post\">

                            <h1>Shopping cart</h1>
                            <p class=\"text-muted\">You currently have 3 item(s) in your cart.</p>
                            <div class=\"table-responsive\">
                                <table class=\"table\">
                                    <thead>
                                        <tr>
                                            <th colspan=\"2\">Product</th>
                                            <th>Quantity</th>
                                            <th>Unit price</th>
                                            <th>Discount</th>
                                            <th colspan=\"2\">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody
                                        ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            echo " 
                            ";
            // line 55
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["j"]);
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo "  
                                        <tr>
                                            <td>
                                                <a href=\"#\">
                                                    <img src=\"";
                // line 59
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
                echo "\" alt=\"White Blouse Armani\">
                                                </a>
                                            </td>
                                            <td><a href=\"#\">";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
                echo "</a>
                                            </td>
                                            <td>
                                                ";
                // line 65
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["cm"]) ? $context["cm"] : $this->getContext($context, "cm")));
                foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
                    // line 66
                    echo "                                       ";
                    if (($this->getAttribute($this->getAttribute($context["m"], "produit", array()), "id", array()) == $this->getAttribute($context["i"], "id", array()))) {
                        // line 67
                        echo "                                                <input type=\"number\" value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "qte", array()), "html", null, true);
                        echo "\" class=\"form-control\">
                                                    ";
                    }
                    // line 69
                    echo "                                              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " 
                                            </td>
                                            <td>";
                // line 71
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
                echo " DT</td>
                                            <td>";
                // line 72
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
                echo " DT</td>
                                            <td>";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
                echo " DT</td>
                                            <td><a href=\"";
                // line 74
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_remove", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "panier", array()), "id", array()), "produit" => $this->getAttribute(                // line 75
$context["i"], "id", array()))), "html", null, true);
                echo "\"><i class=\"fa fa-trash-o\"></i></a>
                                            </td>
                                        </tr>
                                        
                                        ";
                // line 79
                $context["prixSP"] = ((isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")) + $this->getAttribute($context["i"], "prix", array()));
                // line 80
                echo "                                    ";
                $context["prixP"] = ((isset($context["prixP"]) ? $context["prixP"] : $this->getContext($context, "prixP")) + ($this->getAttribute($context["i"], "prix", array()) - (($this->getAttribute($context["i"], "prix", array()) * $this->getAttribute($context["i"], "promotion", array())) / 100)));
                // line 81
                echo "                                              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 82
            echo "                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 83
        echo "                                     
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan=\"5\">Total</th>
                                            <th colspan=\"2\">";
        // line 88
        echo twig_escape_filter($this->env, (isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")), "html", null, true);
        echo " DT</th>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                            <!-- /.table-responsive -->

                            <div class=\"box-footer\">
                                <div class=\"pull-left\">
                                    <a href=\"category.html\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i> Continue shopping</a>
                                </div>
                                <div class=\"pull-right\">
                                    <a class=\"btn btn-default\"><i class=\"fa fa-refresh\"></i> Update basket</a>
                                    <a href=\"";
        // line 102
        echo $this->env->getExtension('routing')->getPath("choisir_adresse");
        echo "\" class=\"btn btn-primary\">Proceed to checkout <i class=\"fa fa-chevron-right\"></i>
                                    </a>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- /.box -->


                    <div class=\"row same-height-row\">
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"box same-height\">
                                <h3>You may also like these products</h3>
                            </div>
                        </div>
                        ";
        // line 119
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, (isset($context["prods"]) ? $context["prods"] : $this->getContext($context, "prods")), 0, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 120
            echo "                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"product same-height\">
                                <div class=\"flip-container\">
                                    <div class=\"flipper\">
                                        <div class=\"front\">
                                            <a href=\"";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "\">
                                                <img src=\"";
            // line 126
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\"  width=\"182px\" height=\"200px\" alt=\"\">
                                            </a>
                                        </div>
                                        <div class=\"back\">
                                            <a href=\"";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "\">
                                                <img src=\"";
            // line 131
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\"  width=\"182px\" height=\"200px\" alt=\"\" >
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href=\"";
            // line 136
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "\" class=\"invisible\">
                                    <img src=\"";
            // line 137
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\" width=\"182px\" height=\"200px\" alt=\"\" >
                                </a>
                                <div class=\"text\">
                                    <h3>";
            // line 140
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</h3>
                                    <p class=\"price\">";
            // line 141
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
            echo "</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "
          

                    </div>


                </div>
                <!-- /.col-md-9 -->

                <div class=\"col-md-3\">
                    <div class=\"box\" id=\"order-summary\">
                        <div class=\"box-header\">
                            <h3>Order summary</h3>
                        </div>
                        <p class=\"text-muted\">Shipping and additional costs are calculated based on the values you have entered.</p>

                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>Order subtotal</td>
                                        <th>";
        // line 168
        echo twig_escape_filter($this->env, (isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")), "html", null, true);
        echo " DT</th>
                                    </tr>
                                    <tr>
                                        <td>Shipping and handling</td>
                                        <th>10.00 DT</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>0.00 DT</th>
                                    </tr>
                                    <tr class=\"total\">
                                        <td>Total</td>
                                        <th>";
        // line 180
        echo twig_escape_filter($this->env, ((isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")) + 10), "html", null, true);
        echo "DT</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>


                    <div class=\"box\">
                        <div class=\"box-header\">
                            <h4>Coupon code</h4>
                        </div>
                        <p class=\"text-muted\">If you have a coupon code, please enter it in the box below.</p>
                        <form>
                            <div class=\"input-group\">

                                <input type=\"text\" class=\"form-control\">

                                <span class=\"input-group-btn\">

\t\t\t\t\t<button class=\"btn btn-primary\" type=\"button\"><i class=\"fa fa-gift\"></i></button>

\t\t\t\t    </span>
                            </div>
                            <!-- /input-group -->
                        </form>
                    </div>

                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
  <script src=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/waypoints.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/modernizr.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap-hover-dropdown.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/front.js"), "html", null, true);
        echo "\"></script>   
</body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Panier:affiche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  396 => 222,  392 => 221,  388 => 220,  384 => 219,  380 => 218,  376 => 217,  372 => 216,  368 => 215,  330 => 180,  315 => 168,  292 => 147,  280 => 141,  276 => 140,  270 => 137,  266 => 136,  258 => 131,  254 => 130,  247 => 126,  243 => 125,  236 => 120,  232 => 119,  212 => 102,  195 => 88,  188 => 83,  182 => 82,  176 => 81,  173 => 80,  171 => 79,  164 => 75,  163 => 74,  159 => 73,  155 => 72,  151 => 71,  142 => 69,  136 => 67,  133 => 66,  129 => 65,  123 => 62,  117 => 59,  108 => 55,  102 => 54,  72 => 26,  63 => 19,  58 => 17,  53 => 15,  47 => 12,  41 => 9,  37 => 8,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* */
/*     <html>*/
/*         <head>  */
/* <!-- styles -->*/
/*     <link href="{{asset('bundles/panier/css/font-awesome.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/animate.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.carousel.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.theme.css')}}" rel="stylesheet">*/
/* */
/*     <!-- theme stylesheet -->*/
/*     <link href="{{asset('bundles/panier/css/style.default.css')}}" rel="stylesheet" id="theme-stylesheet">*/
/* */
/*     <!-- your stylesheet with modifications -->*/
/*     <link href="{{asset('bundles/panier/css/custom.css" rel="stylesheet')}}">*/
/* */
/*     <script src="{{asset('bundles/panier/js/respond.min.js')}}"></script>*/
/* */
/*     <link rel="shortcut icon" href="{{asset('bundles/panier/img/favicon.png')}}">*/
/* */
/* */
/* */
/* </head>*/
/* */
/* <body>{# empty Twig template #}*/
/*     */
/*     */
/*     */
/*     */
/*   <div id="content">*/
/*             <div class="container">*/
/* */
/* */
/*                 <div class="col-md-9" id="basket">*/
/* */
/*                     <div class="box">*/
/* */
/*                         <form method="post">*/
/* */
/*                             <h1>Shopping cart</h1>*/
/*                             <p class="text-muted">You currently have 3 item(s) in your cart.</p>*/
/*                             <div class="table-responsive">*/
/*                                 <table class="table">*/
/*                                     <thead>*/
/*                                         <tr>*/
/*                                             <th colspan="2">Product</th>*/
/*                                             <th>Quantity</th>*/
/*                                             <th>Unit price</th>*/
/*                                             <th>Discount</th>*/
/*                                             <th colspan="2">Total</th>*/
/*                                         </tr>*/
/*                                     </thead>*/
/*                                     <tbody*/
/*                                         {% for j in produits %} */
/*                             {% for i in j %}  */
/*                                         <tr>*/
/*                                             <td>*/
/*                                                 <a href="#">*/
/*                                                     <img src="{{ asset(path('my_image_route', {'id': i.id})) }}" alt="White Blouse Armani">*/
/*                                                 </a>*/
/*                                             </td>*/
/*                                             <td><a href="#">{{i.nom}}</a>*/
/*                                             </td>*/
/*                                             <td>*/
/*                                                 {% for m in cm %}*/
/*                                        {% if m.produit.id == i.id %}*/
/*                                                 <input type="number" value="{{m.qte}}" class="form-control">*/
/*                                                     {% endif %}*/
/*                                               {% endfor %} */
/*                                             </td>*/
/*                                             <td>{{ i.prix}} DT</td>*/
/*                                             <td>{{ i.prix}} DT</td>*/
/*                                             <td>{{ i.prix}} DT</td>*/
/*                                             <td><a href="{{ path('panier_remove', { 'id': entity.panier.id ,*/
/*                                    'produit': i.id }  ) }}"><i class="fa fa-trash-o"></i></a>*/
/*                                             </td>*/
/*                                         </tr>*/
/*                                         */
/*                                         {% set prixSP = prixSP + i.prix  %}*/
/*                                     {% set prixP = prixP + (i.prix - (i.prix *i.promotion)/100) %}*/
/*                                               {% endfor %}*/
/*                                             {% endfor %}*/
/*                                      */
/*                                     </tbody>*/
/*                                     <tfoot>*/
/*                                         <tr>*/
/*                                             <th colspan="5">Total</th>*/
/*                                             <th colspan="2">{{prixSP}} DT</th>*/
/*                                         </tr>*/
/*                                     </tfoot>*/
/*                                 </table>*/
/* */
/*                             </div>*/
/*                             <!-- /.table-responsive -->*/
/* */
/*                             <div class="box-footer">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="category.html" class="btn btn-default"><i class="fa fa-chevron-left"></i> Continue shopping</a>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <a class="btn btn-default"><i class="fa fa-refresh"></i> Update basket</a>*/
/*                                     <a href="{{ path('choisir_adresse') }}" class="btn btn-primary">Proceed to checkout <i class="fa fa-chevron-right"></i>*/
/*                                     </a>*/
/*                                 </div>*/
/*                             </div>*/
/* */
/*                         </form>*/
/* */
/*                     </div>*/
/*                     <!-- /.box -->*/
/* */
/* */
/*                     <div class="row same-height-row">*/
/*                         <div class="col-md-3 col-sm-6">*/
/*                             <div class="box same-height">*/
/*                                 <h3>You may also like these products</h3>*/
/*                             </div>*/
/*                         </div>*/
/*                         {% for i in prods|slice(0, 3) %}*/
/*                         <div class="col-md-3 col-sm-6">*/
/*                             <div class="product same-height">*/
/*                                 <div class="flip-container">*/
/*                                     <div class="flipper">*/
/*                                         <div class="front">*/
/*                                             <a href="{{i.nom}}">*/
/*                                                 <img src="{{ asset(path('my_image_route', {'id': i.id})) }}"  width="182px" height="200px" alt="">*/
/*                                             </a>*/
/*                                         </div>*/
/*                                         <div class="back">*/
/*                                             <a href="{{i.nom}}">*/
/*                                                 <img src="{{ asset(path('my_image_route', {'id': i.id})) }}"  width="182px" height="200px" alt="" >*/
/*                                             </a>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <a href="{{i.nom}}" class="invisible">*/
/*                                     <img src="{{ asset(path('my_image_route', {'id': i.id})) }}" width="182px" height="200px" alt="" >*/
/*                                 </a>*/
/*                                 <div class="text">*/
/*                                     <h3>{{i.nom}}</h3>*/
/*                                     <p class="price">{{i.prix}}</p>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <!-- /.product -->*/
/*                         </div>*/
/*                         {% endfor %}*/
/* */
/*           */
/* */
/*                     </div>*/
/* */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-9 -->*/
/* */
/*                 <div class="col-md-3">*/
/*                     <div class="box" id="order-summary">*/
/*                         <div class="box-header">*/
/*                             <h3>Order summary</h3>*/
/*                         </div>*/
/*                         <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>*/
/* */
/*                         <div class="table-responsive">*/
/*                             <table class="table">*/
/*                                 <tbody>*/
/*                                     <tr>*/
/*                                         <td>Order subtotal</td>*/
/*                                         <th>{{prixSP}} DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Shipping and handling</td>*/
/*                                         <th>10.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Tax</td>*/
/*                                         <th>0.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr class="total">*/
/*                                         <td>Total</td>*/
/*                                         <th>{{prixSP +10 }}DT</th>*/
/*                                     </tr>*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/* */
/*                     </div>*/
/* */
/* */
/*                     <div class="box">*/
/*                         <div class="box-header">*/
/*                             <h4>Coupon code</h4>*/
/*                         </div>*/
/*                         <p class="text-muted">If you have a coupon code, please enter it in the box below.</p>*/
/*                         <form>*/
/*                             <div class="input-group">*/
/* */
/*                                 <input type="text" class="form-control">*/
/* */
/*                                 <span class="input-group-btn">*/
/* */
/* 					<button class="btn btn-primary" type="button"><i class="fa fa-gift"></i></button>*/
/* */
/* 				    </span>*/
/*                             </div>*/
/*                             <!-- /input-group -->*/
/*                         </form>*/
/*                     </div>*/
/* */
/*                 </div>*/
/*                 <!-- /.col-md-3 -->*/
/* */
/*             </div>*/
/*             <!-- /.container -->*/
/*         </div>*/
/*   <script src="{{asset('bundles/panier/js/jquery-1.11.0.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/jquery.cookie.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/waypoints.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/modernizr.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap-hover-dropdown.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/owl.carousel.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/front.js')}}"></script>   */
/* </body>*/
/*     </html>*/
/* */
