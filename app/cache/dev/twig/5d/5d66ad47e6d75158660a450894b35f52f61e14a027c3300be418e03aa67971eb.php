<?php

/* PiDevClientBundle:Service:proprietaire.html.twig */
class __TwigTemplate_a7d840f3e527de13c5fee9b6b776911f07ca515f793bc436c86a09597d0655f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Service:proprietaire.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h1>Mes Services </h1>
        <div>
            ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["mod"]);
        foreach ($context['_seq'] as $context["_key"] => $context["mod"]) {
            // line 6
            echo "            <br> <th>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["mod"], "nom", array()), "html", null, true);
            echo "</th></br>
            <br> <th>";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute($context["mod"], "description", array()), "html", null, true);
            echo "</th></br>
            <br> <a href=\"";
            // line 8
            echo $this->env->getExtension('routing')->getPath("ajouter");
            echo "\">Proposer un nouveau service</a></br>
            <br>   <a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modifier", array("id" => $this->getAttribute($context["mod"], "id", array()))), "html", null, true);
            echo "\">Modifier mon service</a></br>
             <br>   <a href=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supprimer", array("id" => $this->getAttribute($context["mod"], "id", array()))), "html", null, true);
            echo "\">Supprimer mon service</a></br>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mod'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "        </div>
       ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Service:proprietaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 12,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  39 => 6,  35 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* <h1>Mes Services </h1>*/
/*         <div>*/
/*             {% for mod in mod %}*/
/*             <br> <th>{{mod.nom}}</th></br>*/
/*             <br> <th>{{mod.description}}</th></br>*/
/*             <br> <a href="{{path("ajouter")}}">Proposer un nouveau service</a></br>*/
/*             <br>   <a href="{{path("modifier",{'id':mod.id})}}">Modifier mon service</a></br>*/
/*              <br>   <a href="{{path("supprimer",{'id':mod.id})}}">Supprimer mon service</a></br>*/
/*             {% endfor %}*/
/*         </div>*/
/*        {% endblock %}  */
