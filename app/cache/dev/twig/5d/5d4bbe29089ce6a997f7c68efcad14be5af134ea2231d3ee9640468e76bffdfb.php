<?php

/* PiDevClientBundle:Panier:test.html.twig */
class __TwigTemplate_00d10a95ffe367f23f4ff16db17f736aed86be13df80b9633eedaabe0ca6a75f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Panier:test.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    </br>
    </br>
    

    ";
        // line 8
        if (twig_test_empty((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")))) {
            // line 9
            echo "          </div>
         <div class=\"register_account\">
           <div class=\"wrap\">
  <h4 class=\"title\">votre panier est vide</h4>
  <p class=\"cart\">vous n'avez aucun produit dans votre panier.<br>Cliquer<a href=\"";
            // line 13
            echo $this->env->getExtension('routing')->getPath("produit");
            echo "\"> ici</a> 
      pour continuer le shopping</p>
    \t   </div>
    \t</div>
       
          
    ";
        } else {
            // line 20
            echo " <ul class=\"record_actions \">
    <li>
          <a href=\"";
            // line 22
            echo $this->env->getExtension('routing')->getPath("produit");
            echo "\"class=\"btn btn-success \">
            Continuer vos achats
        </a>    
    </li>
    </ul>




<div class=\"panel panel-info\">
  <div class=\"panel-heading\">
      
    <h3 class=\"panel-title\">Panier</h3>
  </div>
    <!-- Table -->
    <form method=\"POST\" > 
  <table class=\"table table-striped panel panel-info\">
      <thead>
       <th class=\"info\"></th>
      <th class=\"info\">Nom</th>
      <th class=\"info\">Prix</th>
      <th class=\"info\">Marque</th>
      <th class=\"info\">Quantité</th>
      <th class=\"info\">Détail</th>
      <th class=\"info\">Retirer</th>
      </thead>
        
      
      <tbody> 
           
          ";
            // line 52
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")));
            foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                echo " 
              ";
                // line 53
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["j"]);
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    echo " 
                
                 
          <tr>
              <td>
                  <img src=";
                    // line 58
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/pic.jpg"), "html", null, true);
                    echo " alt=\"\" width=\"150px\"/>
              </td>
             
              <td>";
                    // line 61
                    echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
                    echo "</td>
              <td>";
                    // line 62
                    echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
                    echo " DT</td>
              <td>";
                    // line 63
                    echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "marque", array()), "html", null, true);
                    echo "</td>
                      <td><select name=\"";
                    // line 64
                    echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
                    echo "\" id=\"qte\">
                              <option value=\"1\">1</option>
                              <option value=\"2\">2</option>
                              <option value=\"3\">3</option>
                              <option value=\"4\">4</option>
                              <option value=\"5\">5</option>
                              <option value=\"6\">6</option>
                              <option value=\"7\">7</option>
                              <option value=\"8\">8</option>
                              <option value=\"9\">9</option>
                              <option value=\"10\">10</option>
                  </select></td>
              <td>  <a href=";
                    // line 76
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_show", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
                    echo ">Détail</a></td>
              <td><a href=\"";
                    // line 77
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_remove", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "panier", array()), "id", array()), "produit" => $this->getAttribute(                    // line 78
$context["i"], "id", array()))), "html", null, true);
                    echo "\">retirer</a></td>
          </tr>
           ";
                    // line 80
                    $context["prixSP"] = ((isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")) + $this->getAttribute($context["i"], "prix", array()));
                    // line 81
                    echo "           ";
                    $context["prixP"] = ((isset($context["prixP"]) ? $context["prixP"] : $this->getContext($context, "prixP")) + ($this->getAttribute($context["i"], "prix", array()) - (($this->getAttribute($context["i"], "prix", array()) * $this->getAttribute($context["i"], "promotion", array())) / 100)));
                    // line 82
                    echo "             
           ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 84
                echo "               ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 85
            echo "          
      </tbody>
  </table>
          
</div>
               </br> prix sans promotion : ";
            // line 90
            echo twig_escape_filter($this->env, (isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")), "html", null, true);
            echo " DT</br>
               </br> prix apres promotion : ";
            // line 91
            echo twig_escape_filter($this->env, (isset($context["prixP"]) ? $context["prixP"] : $this->getContext($context, "prixP")), "html", null, true);
            echo " DT</br> </br> 
           
           <ul class=\"record_actions \">
    <li>
        <input type=\"submit\" value=\"ok\">
        <a href=\"";
            // line 96
            echo $this->env->getExtension('routing')->getPath("choisir_adresse");
            echo "\"class=\"btn btn-success \">
            Passer la commande
        </a>  
             </form>
    </li>
    </ul>
    ";
        }
        // line 102
        echo "      
          
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Panier:test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 102,  188 => 96,  180 => 91,  176 => 90,  169 => 85,  163 => 84,  156 => 82,  153 => 81,  151 => 80,  146 => 78,  145 => 77,  141 => 76,  126 => 64,  122 => 63,  118 => 62,  114 => 61,  108 => 58,  98 => 53,  92 => 52,  59 => 22,  55 => 20,  45 => 13,  39 => 9,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     </br>*/
/*     </br>*/
/*     */
/* */
/*     {% if produits is empty  %}*/
/*           </div>*/
/*          <div class="register_account">*/
/*            <div class="wrap">*/
/*   <h4 class="title">votre panier est vide</h4>*/
/*   <p class="cart">vous n'avez aucun produit dans votre panier.<br>Cliquer<a href="{{ path('produit') }}"> ici</a> */
/*       pour continuer le shopping</p>*/
/*     	   </div>*/
/*     	</div>*/
/*        */
/*           */
/*     {% else %}*/
/*  <ul class="record_actions ">*/
/*     <li>*/
/*           <a href="{{ path('produit') }}"class="btn btn-success ">*/
/*             Continuer vos achats*/
/*         </a>    */
/*     </li>*/
/*     </ul>*/
/* */
/* */
/* */
/* */
/* <div class="panel panel-info">*/
/*   <div class="panel-heading">*/
/*       */
/*     <h3 class="panel-title">Panier</h3>*/
/*   </div>*/
/*     <!-- Table -->*/
/*     <form method="POST" > */
/*   <table class="table table-striped panel panel-info">*/
/*       <thead>*/
/*        <th class="info"></th>*/
/*       <th class="info">Nom</th>*/
/*       <th class="info">Prix</th>*/
/*       <th class="info">Marque</th>*/
/*       <th class="info">Quantité</th>*/
/*       <th class="info">Détail</th>*/
/*       <th class="info">Retirer</th>*/
/*       </thead>*/
/*         */
/*       */
/*       <tbody> */
/*            */
/*           {% for j in produits %} */
/*               {% for i in j %} */
/*                 */
/*                  */
/*           <tr>*/
/*               <td>*/
/*                   <img src={{asset('images/pic.jpg')}} alt="" width="150px"/>*/
/*               </td>*/
/*              */
/*               <td>{{ i.id}}</td>*/
/*               <td>{{ i.prix}} DT</td>*/
/*               <td>{{ i.marque}}</td>*/
/*                       <td><select name="{{i.id}}" id="qte">*/
/*                               <option value="1">1</option>*/
/*                               <option value="2">2</option>*/
/*                               <option value="3">3</option>*/
/*                               <option value="4">4</option>*/
/*                               <option value="5">5</option>*/
/*                               <option value="6">6</option>*/
/*                               <option value="7">7</option>*/
/*                               <option value="8">8</option>*/
/*                               <option value="9">9</option>*/
/*                               <option value="10">10</option>*/
/*                   </select></td>*/
/*               <td>  <a href={{ path('produit_show', { 'id': i.id }) }}>Détail</a></td>*/
/*               <td><a href="{{ path('panier_remove', { 'id': entity.panier.id ,*/
/*                                    'produit': i.id }  ) }}">retirer</a></td>*/
/*           </tr>*/
/*            {% set prixSP = prixSP + i.prix  %}*/
/*            {% set prixP = prixP + (i.prix - (i.prix *i.promotion)/100) %}*/
/*              */
/*            {% endfor %}*/
/*                {% endfor %}*/
/*           */
/*       </tbody>*/
/*   </table>*/
/*           */
/* </div>*/
/*                </br> prix sans promotion : {{prixSP}} DT</br>*/
/*                </br> prix apres promotion : {{prixP}} DT</br> </br> */
/*            */
/*            <ul class="record_actions ">*/
/*     <li>*/
/*         <input type="submit" value="ok">*/
/*         <a href="{{ path('choisir_adresse') }}"class="btn btn-success ">*/
/*             Passer la commande*/
/*         </a>  */
/*              </form>*/
/*     </li>*/
/*     </ul>*/
/*     {% endif %}      */
/*           */
/* {% endblock %}*/
