<?php

/* PiDevClientBundle:Adresse:localiser.html.twig */
class __TwigTemplate_e1498b9666bbf0087ee2003eb36bffec4a1a80c9eaddfb87b82fa3d83f987299 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<html>
    <head>
        <style type=\"text/css\">html,body,#map-canvas { height: 100% ; margin: 0 ;padding: 0 ;}</style>
        <script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyD0F3qXLy34KWVC4MG6NtGLUIny2PcTnHs\">

        </script>

        <script type=\"text/javascript\">
    var myCenter =new google.maps.LatLng(51.508742,-0.120850);
    var marker;        
    
  function initialize(){
  var mapProp = {
    center: myCenter,
    zoom:5,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById(\"googleMap\"),mapProp);
  
  var marker=new google.maps.Marker({
    position: myCenter,
    animation: google.maps.Animation.BOUNCE
   });
   marker.setMap(map);
}
                google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>

<body>
";
        // line 32
        $this->displayBlock('container', $context, $blocks);
        // line 35
        echo "</body>
</html>
";
    }

    // line 32
    public function block_container($context, array $blocks = array())
    {
        // line 33
        echo "
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Adresse:localiser.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  64 => 33,  61 => 32,  55 => 35,  53 => 32,  20 => 1,);
    }
}
/* */
/* <html>*/
/*     <head>*/
/*         <style type="text/css">html,body,#map-canvas { height: 100% ; margin: 0 ;padding: 0 ;}</style>*/
/*         <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD0F3qXLy34KWVC4MG6NtGLUIny2PcTnHs">*/
/* */
/*         </script>*/
/* */
/*         <script type="text/javascript">*/
/*     var myCenter =new google.maps.LatLng(51.508742,-0.120850);*/
/*     var marker;        */
/*     */
/*   function initialize(){*/
/*   var mapProp = {*/
/*     center: myCenter,*/
/*     zoom:5,*/
/*     mapTypeId:google.maps.MapTypeId.ROADMAP*/
/*   };*/
/*   var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);*/
/*   */
/*   var marker=new google.maps.Marker({*/
/*     position: myCenter,*/
/*     animation: google.maps.Animation.BOUNCE*/
/*    });*/
/*    marker.setMap(map);*/
/* }*/
/*                 google.maps.event.addDomListener(window, 'load', initialize);*/
/*     </script>*/
/* </head>*/
/* */
/* <body>*/
/* {% block container %}*/
/* */
/* {% endblock %}*/
/* </body>*/
/* </html>*/
/* */
