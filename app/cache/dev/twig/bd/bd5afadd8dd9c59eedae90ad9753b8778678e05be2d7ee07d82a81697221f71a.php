<?php

/* PiDevClientBundle:Sujet:show.html.twig */
class __TwigTemplate_8018a992e2622e36bf68bac88afc41cbd935ac00645beecc9a8074bcc67d9066 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Sujet:show.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <head>
        <script src=";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/ckeditor/ckeditor.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>
        <link href=";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/ckeditor/contents.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
    </head>
    <h1>Sujet</h1>
    

    <table class=\"record_properties\">
        <tbody>
         
            <tr>
                <th>Titre</th>
                <td>";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "titre", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Sujet</th>
                
                <td class=\"ckedito\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sujet", array()), "html", null, true);
        echo "</td>
          
            </tr>
            <div class=\"my-container\">
";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sujet", array()), "html", null, true);
        echo "
   
</div>
        </tbody>
    </table>

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("sujet");
        echo "\">
            Back to the list
        </a>
    </li>
    <li>
        <a href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("sujet_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">
            Edit
        </a>
    </li>
    <li>";
        // line 42
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Sujet:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 42,  85 => 38,  77 => 33,  66 => 25,  59 => 21,  51 => 16,  38 => 6,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <head>*/
/*         <script src={{asset('bundles/ckeditor/ckeditor.js')}} type="text/javascript"></script>*/
/*         <link href={{asset('bundles/ckeditor/contents.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*     </head>*/
/*     <h1>Sujet</h1>*/
/*     */
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*          */
/*             <tr>*/
/*                 <th>Titre</th>*/
/*                 <td>{{ entity.titre }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Sujet</th>*/
/*                 */
/*                 <td class="ckedito">{{ entity.sujet }}</td>*/
/*           */
/*             </tr>*/
/*             <div class="my-container">*/
/* {{ entity.sujet }}*/
/*    */
/* </div>*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('sujet') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/*     <li>*/
/*         <a href="{{ path('sujet_edit', { 'id': entity.id }) }}">*/
/*             Edit*/
/*         </a>*/
/*     </li>*/
/*     <li>{{ form(delete_form) }}</li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
