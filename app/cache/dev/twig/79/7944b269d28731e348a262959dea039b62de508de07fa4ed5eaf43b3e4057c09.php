<?php

/* PiDevAdminBundle:Categorie:search.html.twig */
class __TwigTemplate_1415e3939e30b1dd63a9d02e0eebe2d1dfcb275b5cca27aa393b43aeff41064c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:Categorie:search.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    
<h1>Listes  des categorie </h1>
<form method=\"POST\"  action=\"\">
    Rechercher categorie! : <select name=\"search\">
";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
            // line 8
            echo "<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cat"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cat"], "id", array()), "html", null, true);
            echo "</option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "</select>
    <input type=\"submit\" value=\"chercher\"/>
</form>

    
    ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modeles"]) ? $context["modeles"] : $this->getContext($context, "modeles")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 16
            echo "        <div>
            <br>   Produit:<th>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</th></br>
            <br>    Description:<th>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</th></br>
      <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("categorie_show", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">En savoir Plus!</a>
        </div>
         
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "        ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Categorie:search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 23,  74 => 19,  70 => 18,  66 => 17,  63 => 16,  59 => 15,  52 => 10,  41 => 8,  37 => 7,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {%extends "::baseAdmin.html.twig"%}*/
/* {% block content %}*/
/*     */
/* <h1>Listes  des categorie </h1>*/
/* <form method="POST"  action="">*/
/*     Rechercher categorie! : <select name="search">*/
/* {% for cat in categories %}*/
/* <option value="{{ cat.id }}">{{ cat.id }}</option>*/
/* {% endfor %}*/
/* </select>*/
/*     <input type="submit" value="chercher"/>*/
/* </form>*/
/* */
/*     */
/*     {% for i in modeles %}*/
/*         <div>*/
/*             <br>   Produit:<th>{{i.nom}}</th></br>*/
/*             <br>    Description:<th>{{i.description}}</th></br>*/
/*       <a href="{{path("categorie_show",{'id':i.id})}}">En savoir Plus!</a>*/
/*         </div>*/
/*          */
/*     {% endfor %}*/
/*         {% endblock %}*/
