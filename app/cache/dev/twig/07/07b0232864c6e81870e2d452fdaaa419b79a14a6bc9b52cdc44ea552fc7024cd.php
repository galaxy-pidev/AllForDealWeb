<?php

/* PiDevClientBundle:Default:index.html.twig */
class __TwigTemplate_51e4bae369a35cbe3a25d6adaecf6e0a4607a6300ce203f80a0ec237ca21836f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PiDevClientBundle::base.html.twig", "PiDevClientBundle:Default:index.html.twig", 1);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "PiDevClientBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 1,);
    }
}
/* {% extends "PiDevClientBundle::base.html.twig" %}*/
/* */
