<?php

/* PiDevClientBundle:Client:listProduits.html.twig */
class __TwigTemplate_5aa337e0ec7c774a12ee75bf018f9ed594054390f44a84c19b36156adc4a14f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Client:listProduits.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/datatables/dataTables.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/datatables/dataTables.tableTools.css"), "html", null, true);
        echo "\">
            <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-switch/bootstrap-switch.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-progressbar/bootstrap-progressbar.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/datatables/dynamic/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/datatables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/datatables/dataTables.tableTools.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/table_editable.js"), "html", null, true);
        echo "\"></script>
    <head>

    </head>
    <body>
        <!-- BEGAIN PRELOADER -->   
        <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Produits</strong></h3>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading bg-red\">
                            <h3 class=\"panel-title\"><strong>Liste des </strong> Produits</h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 m-b-20\">

                                    <div class=\"btn-group pull-right\">
                                        <button class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">Tools <i class=\"fa fa-angle-down\"></i>
                                        </button>
                                        <ul class=\"dropdown-menu pull-right\">
                                            <li><a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("produit_create");
        echo "\">Ajouter</a>
                                            </li>
                                            <li><a href=\"#\">Save as PDF</a>
                                            </li>
                                            <li><a href=\"#\">Export to Excel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive table-red\">
                                    <table class=\"table table-striped table-hover dataTable\" id=\"table-editable\">

                                        <tbody>
                                        <thead>
                                            <tr role=\"row\">
                                                <th>Nom</th>
                                                <th>Prix</th>
                                                <th>Quantité</th>
                                                <th>Marque</th>
                                                <th>Prodmotion</th>
                                                <th>Description</th>
                                                <th class=\"text-center\">Action</th>
                                            </tr>
                                        </thead>
                                        ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 59
            echo "                                            <tr>
                                                <td>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</td>
                                                <td>";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
            echo "</td> 
                                                <td>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "qantite", array()), "html", null, true);
            echo "</td>  
                                                <td>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "marque", array()), "html", null, true);
            echo "</td> 
                                                <td>";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "promotion", array()), "html", null, true);
            echo "</td>  
                                                <td>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</td> 
                                                <td class=\"text-center\">
                                                    <a class=\"edit btn btn-dark\" href=\"javascript:;\"><i class=\"fa fa-pencil-square-o\"></i>Modifier</a> 
                                                    <a class=\"delete btn btn-danger\" href=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_delete", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-times-circle\"></i> Supprimer</a>
                                                </td>

                                            </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "                                                                                                                               
                                        </tbody>
                                    </table>

                                    <!-- nom  -->                                  

                                    <div id=\"modal\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>

                                        <section class=\"popupBodny\">
                                            <!-- Username & Password Login form -->
                                            <div class=\"user_login\">
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>




    </body>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:listProduits.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 72,  147 => 68,  141 => 65,  137 => 64,  133 => 63,  129 => 62,  125 => 61,  121 => 60,  118 => 59,  114 => 58,  87 => 34,  60 => 10,  56 => 9,  52 => 8,  48 => 7,  44 => 6,  40 => 5,  36 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content  %}*/
/*         <link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.css')}}">*/
/*     <link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.tableTools.css')}}">*/
/*             <script src="{{asset('plugins/bootstrap-switch/bootstrap-switch.js')}}"></script>*/
/*         <script src="{{asset('plugins/bootstrap-progressbar/bootstrap-progressbar.js')}}"></script>*/
/*         <script src="{{asset('plugins/datatables/dynamic/jquery.dataTables.min.js')}}"></script>*/
/*         <script src="{{asset('plugins/datatables/dataTables.bootstrap.js')}}"></script>*/
/*         <script src="{{asset('plugins/datatables/dataTables.tableTools.js')}}"></script>*/
/*         <script src="{{asset('bundles/backOffice/js/table_editable.js')}}"></script>*/
/*     <head>*/
/* */
/*     </head>*/
/*     <body>*/
/*         <!-- BEGAIN PRELOADER -->   */
/*         <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Produits</strong></h3>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading bg-red">*/
/*                             <h3 class="panel-title"><strong>Liste des </strong> Produits</h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 m-b-20">*/
/* */
/*                                     <div class="btn-group pull-right">*/
/*                                         <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>*/
/*                                         </button>*/
/*                                         <ul class="dropdown-menu pull-right">*/
/*                                             <li><a href="{{path('produit_create')}}">Ajouter</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Save as PDF</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Export to Excel</a>*/
/*                                             </li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">*/
/*                                     <table class="table table-striped table-hover dataTable" id="table-editable">*/
/* */
/*                                         <tbody>*/
/*                                         <thead>*/
/*                                             <tr role="row">*/
/*                                                 <th>Nom</th>*/
/*                                                 <th>Prix</th>*/
/*                                                 <th>Quantité</th>*/
/*                                                 <th>Marque</th>*/
/*                                                 <th>Prodmotion</th>*/
/*                                                 <th>Description</th>*/
/*                                                 <th class="text-center">Action</th>*/
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         {% for i in produits %}*/
/*                                             <tr>*/
/*                                                 <td>{{i.nom}}</td>*/
/*                                                 <td>{{i.prix}}</td> */
/*                                                 <td>{{i.qantite}}</td>  */
/*                                                 <td>{{i.marque}}</td> */
/*                                                 <td>{{i.promotion}}</td>  */
/*                                                 <td>{{i.description}}</td> */
/*                                                 <td class="text-center">*/
/*                                                     <a class="edit btn btn-dark" href="javascript:;"><i class="fa fa-pencil-square-o"></i>Modifier</a> */
/*                                                     <a class="delete btn btn-danger" href="{{path('produit_delete',{'id':i.id})}}"><i class="fa fa-times-circle"></i> Supprimer</a>*/
/*                                                 </td>*/
/* */
/*                                             </tr>*/
/*                                         {% endfor %}                                                                                                                               */
/*                                         </tbody>*/
/*                                     </table>*/
/* */
/*                                     <!-- nom  -->                                  */
/* */
/*                                     <div id="modal" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/* */
/*                                         <section class="popupBodny">*/
/*                                             <!-- Username & Password Login form -->*/
/*                                             <div class="user_login">*/
/*                                             </div>*/
/*                                         </section>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- END MAIN CONTENT -->*/
/*         </div>*/
/* */
/* */
/* */
/* */
/*     </body>*/
/* {% endblock %}*/
/* */
