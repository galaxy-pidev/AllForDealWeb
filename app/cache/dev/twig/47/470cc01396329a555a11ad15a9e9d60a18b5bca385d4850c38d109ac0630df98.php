<?php

/* IdeatoStarRatingBundle:StarRating:rate.html.twig */
class __TwigTemplate_4a5070e6a9b9bdefb6b2748c1f7af6b5d89f07b991e1366a35f57641f3934d14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"isr-rate\" data-contentid=\"";
        echo twig_escape_filter($this->env, (isset($context["contentId"]) ? $context["contentId"] : $this->getContext($context, "contentId")), "html", null, true);
        echo "\" data-score=\"";
        echo twig_escape_filter($this->env, (isset($context["score"]) ? $context["score"] : $this->getContext($context, "score")), "html", null, true);
        echo "\" data-route=\"";
        echo $this->env->getExtension('routing')->getPath("ideato_star_rating_rate");
        echo "\"></div>";
    }

    public function getTemplateName()
    {
        return "IdeatoStarRatingBundle:StarRating:rate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div class="isr-rate" data-contentid="{{ contentId }}" data-score="{{ score }}" data-route="{{ path('ideato_star_rating_rate') }}"></div>*/
