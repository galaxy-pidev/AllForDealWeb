<?php

/* PiDevClientBundle:Service:details.html.twig */
class __TwigTemplate_6b4f52d113012348caee3d74e3118c2560a493731df3e54738f18ee453326d02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Service:details.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h1>Details du Services ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "nom", array()), "html", null, true);
        echo " </h1>

   

     
        <div>
      
       <br>  Description: <th>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "description", array()), "html", null, true);
        echo "</th></br>
       <br> Mail: <th>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "Mail", array()), "html", null, true);
        echo "</th></br>
       <br> Estimation:<th>";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "estimation", array()), "html", null, true);
        echo "</th></br>
       <br>Adresse:<th>";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "Adresse", array()), "html", null, true);
        echo "</th></br>
       <br>Telephone:<th>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "Telephone", array()), "html", null, true);
        echo "</th></br>
        </div>
  ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Service:details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 14,  54 => 13,  50 => 12,  46 => 11,  42 => 10,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* <h1>Details du Services {{mod.nom}} </h1>*/
/* */
/*    */
/* */
/*      */
/*         <div>*/
/*       */
/*        <br>  Description: <th>{{mod.description}}</th></br>*/
/*        <br> Mail: <th>{{mod.Mail}}</th></br>*/
/*        <br> Estimation:<th>{{mod.estimation}}</th></br>*/
/*        <br>Adresse:<th>{{mod.Adresse}}</th></br>*/
/*        <br>Telephone:<th>{{mod.Telephone}}</th></br>*/
/*         </div>*/
/*   {% endblock %}       */
