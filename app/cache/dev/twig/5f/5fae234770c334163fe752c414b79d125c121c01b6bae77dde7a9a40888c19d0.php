<?php

/* PiDevClientBundle:Mail:In.html.twig */
class __TwigTemplate_27ba0823cbeda7893eff82677fe665f75348736e6ef0f4d5c2dbdf91f1bda42e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js sidebar-large lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js sidebar-large lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js sidebar-large lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js sidebar-large\"> <!--<![endif]-->

<!-- Added by HTTrack --><meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" /><!-- /Added by HTTrack -->
<head>
    <!-- BEGIN META SECTION -->
    <meta charset=\"utf-8\">
    <title>Pixit - Responsive Boostrap3 Admin</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta content=\"\" name=\"description\" />
    <meta content=\"themes-lab\" name=\"author\" />
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    
    
    <link href=";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    <link href=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    <link href=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/style.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    
    
    <!-- END  MANDATORY STYLE -->
    <script src=\"assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js\"></script>
</head>

<body data-page=\"mailbox\">
    <!-- BEGIN TOP MENU -->
    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"container-fluid\">
            <div class=\"navbar-center\">Mailbox</div>

        </div>
    </nav>
    <!-- END TOP MENU -->
    <!-- BEGIN WRAPPER -->
    <div id=\"wrapper\">
        <!-- BEGIN MAIN SIDEBAR -->
        <nav id=\"sidebar\">
            <div id=\"main-menu\">
                <ul class=\"sidebar-nav\">
                    <li>
                        <a href=\"index.html\"><i class=\"fa fa-dashboard\"></i><span class=\"sidebar-text\">Votre profile</span></a>
                    </li>
                    <li>
                        <a href=\"widgets.html\"><i class=\"glyph-icon flaticon-widgets\"></i><span class=\"sidebar-text\">Boite de reception <span class=\"label label-danger pull-right\">New</span></span></a>
                    </li>
                    <li>
                        <a href=\"charts.html\"><i class=\"glyph-icon flaticon-charts2\"></i><span class=\"sidebar-text\">Nouveau Message</span><span class=\"pull-right badge badge-primary\">2</span></a>
                    </li>
                    <li>
                        <a href=\"http://themes-lab.com/pixit/frontend/index.html\" target=\"_blank\"><i class=\"glyph-icon flaticon-frontend\"></i><span class=\"sidebar-text\">Consulter votre panier</span></a>
                    </li>
                    <li class=\"current active hasSub\">
                        <a href=\"#\"><i class=\"glyph-icon flaticon-email\"></i><span class=\"sidebar-text\">Panier</span><span class=\"fa arrow\"></span></a>
                      
                    </li>
                    <li>
                        <a href=\"#\"><i class=\"glyph-icon flaticon-forms\"></i><span class=\"sidebar-text\">Produit</span><span class=\"fa arrow\"></span></a>
                      
                    </li>
                    <li>
                        <a href=\"#\"><i class=\"glyph-icon flaticon-ui-elements2\"></i><span class=\"sidebar-text\">Forums</span><span class=\"fa arrow\"></span></a>
                       
                    </li>
                    <li>
                        <a href=\"#\"><i class=\"glyph-icon flaticon-pages\"></i><span class=\"sidebar-text\">Services</span><span class=\"fa arrow\"></span></a>
                      
                    </li>
                    <li>
                        <a href=\"maps.html\"><i class=\"glyph-icon flaticon-world\"></i><span class=\"sidebar-text\">Apples d'offre</span></a>
                    </li>
                    <li>
                        <a href=\"#\"><i class=\"glyph-icon flaticon-panels\"></i><span class=\"sidebar-text\">Commandes</span><span class=\"fa arrow\"></span></a>
                        
                    </li>
                    <li>
                        <a href=\"#\"><i class=\"fa fa-heart\"></i><span class=\"sidebar-text\">Favoris</span><span class=\"fa arrow\"></span></a>
                       
                    </li>
                    <li>
                        <a href=\"#\"><i class=\"glyph-icon flaticon-account\"></i><span class=\"sidebar-text\">Account</span><span class=\"fa arrow\"></span></a>
                        
                    </li>
                    <li>
                        <a href=\"#\"><i class=\"glyph-icon flaticon-gallery\"></i><span class=\"sidebar-text\">Images Manager</span><span class=\"fa arrow\"></span></a>
                       
                    </li>
                    <li class=\"m-b-245\">
                        <a href=\"calendar.html\"><i class=\"glyph-icon flaticon-calendar53\"></i><span class=\"sidebar-text\">Calendar</span></a>
                    </li>
                </ul>
            </div>
            <div class=\"footer-widget\">
                <img src=";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/gradient.png"), "html", null, true);
        echo " alt=\"gradient effet\" class=\"sidebar-gradient-img\" />
                <div id=\"sidebar-charts\">
 
                <div class=\"sidebar-footer clearfix\">
                    <a class=\"pull-left\" href=\"profil.html\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Settings\"><i class=\"glyph-icon flaticon-settings21\"></i></a>
                    <a class=\"pull-left toggle_fullscreen\" href=\"#\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Fullscreen\"><i class=\"glyph-icon flaticon-fullscreen3\"></i></a>
                    <a class=\"pull-left\" href=\"lockscreen.html\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Lockscreen\"><i class=\"glyph-icon flaticon-padlock23\"></i></a>
                    <a class=\"pull-left\" href=\"login.html\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Logout\"><i class=\"fa fa-power-off\"></i></a>
                </div> 
            </div>
        </nav>
        <!-- END MAIN SIDEBAR -->
        <div id=\"main-content\" class=\"page-mailbox container-fluid\">
            <div class=\"row\" data-equal-height=\"true\">
                <div class=\"col-md-4 list-messages\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-body messages\">
                            <div class=\"input-group input-group-lg border-bottom\">
                                <span class=\"input-group-btn\">
                                    <a href=\"#\" class=\"btn\"><i class=\"fa fa-search\"></i></a>
                                  </span>
                                <input type=\"text\" class=\"form-control bd-0 bd-white\" placeholder=\"Search\">
                            </div>
                            <div id=\"messages-list\" class=\"panel panel-default withScroll\" data-height=\"window\" data-padding=\"90\">
                                  ";
        // line 121
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mail"]) ? $context["mail"] : $this->getContext($context, "mail")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "   
                                <a class=\"message-item hire-me-btn media\" href=\"#message\" >
                                    <div class=\"pull-left text-center\">
                                        <div class=\"pos-rel message-checkbox\">
                                            <input type=\"checkbox\" data-style=\"flat-red\">
                                        </div>
                                        <div>
                                            <strong><i class=\"fa fa-paperclip\"></i> 2</strong>
                                        </div>
                                    </div>
   
       
 <div class=\"message-item-right\">
    
                                        <div class=\"media\">
                                            <img src=";
            // line 136
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
            echo " alt=\"avatar 3\" width=\"50\" class=\"pull-left\">
                                            <div class=\"media-body\">
                                                <small class=\"pull-right\">23 Sept</small>
                                                <h5 class=\"c-dark\"><strong>";
            // line 139
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
            echo "</strong></h5>
                                                <h4 class=\"c-dark\">";
            // line 140
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</h4>
                                            </div>
</div>
                                        <p class=\"f-14 c-gray\">";
            // line 143
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "Text", array()), "html", null, true);
            echo "</p>
                                          
          </div>       
                                        </a>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END WRAPPER -->
    <!-- BEGIN CHAT MENU -->
    
    <!-- END CHAT MENU -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src=";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
    <!-- END MANDATORY SCRIPTS -->
    <script src=";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>
</body>

</html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Mail:In.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  276 => 176,  271 => 174,  267 => 173,  263 => 172,  259 => 171,  255 => 170,  251 => 169,  247 => 168,  243 => 167,  239 => 166,  235 => 165,  231 => 164,  227 => 163,  223 => 162,  219 => 161,  203 => 147,  192 => 143,  186 => 140,  182 => 139,  176 => 136,  156 => 121,  129 => 97,  51 => 22,  47 => 21,  43 => 20,  39 => 19,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->*/
/* <!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->*/
/* <!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->*/
/* <!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->*/
/* */
/* <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->*/
/* <head>*/
/*     <!-- BEGIN META SECTION -->*/
/*     <meta charset="utf-8">*/
/*     <title>Pixit - Responsive Boostrap3 Admin</title>*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <meta content="" name="description" />*/
/*     <meta content="themes-lab" name="author" />*/
/*     <!-- END META SECTION -->*/
/*     <!-- BEGIN MANDATORY STYLE -->*/
/*     */
/*     */
/*     <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*     <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*     <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*     <link href={{asset('bundles/backOffice/css/style.min.css')}} rel="stylesheet">*/
/*     */
/*     */
/*     <!-- END  MANDATORY STYLE -->*/
/*     <script src="assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>*/
/* </head>*/
/* */
/* <body data-page="mailbox">*/
/*     <!-- BEGIN TOP MENU -->*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*         <div class="container-fluid">*/
/*             <div class="navbar-center">Mailbox</div>*/
/* */
/*         </div>*/
/*     </nav>*/
/*     <!-- END TOP MENU -->*/
/*     <!-- BEGIN WRAPPER -->*/
/*     <div id="wrapper">*/
/*         <!-- BEGIN MAIN SIDEBAR -->*/
/*         <nav id="sidebar">*/
/*             <div id="main-menu">*/
/*                 <ul class="sidebar-nav">*/
/*                     <li>*/
/*                         <a href="index.html"><i class="fa fa-dashboard"></i><span class="sidebar-text">Votre profile</span></a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="widgets.html"><i class="glyph-icon flaticon-widgets"></i><span class="sidebar-text">Boite de reception <span class="label label-danger pull-right">New</span></span></a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="charts.html"><i class="glyph-icon flaticon-charts2"></i><span class="sidebar-text">Nouveau Message</span><span class="pull-right badge badge-primary">2</span></a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="http://themes-lab.com/pixit/frontend/index.html" target="_blank"><i class="glyph-icon flaticon-frontend"></i><span class="sidebar-text">Consulter votre panier</span></a>*/
/*                     </li>*/
/*                     <li class="current active hasSub">*/
/*                         <a href="#"><i class="glyph-icon flaticon-email"></i><span class="sidebar-text">Panier</span><span class="fa arrow"></span></a>*/
/*                       */
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#"><i class="glyph-icon flaticon-forms"></i><span class="sidebar-text">Produit</span><span class="fa arrow"></span></a>*/
/*                       */
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#"><i class="glyph-icon flaticon-ui-elements2"></i><span class="sidebar-text">Forums</span><span class="fa arrow"></span></a>*/
/*                        */
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#"><i class="glyph-icon flaticon-pages"></i><span class="sidebar-text">Services</span><span class="fa arrow"></span></a>*/
/*                       */
/*                     </li>*/
/*                     <li>*/
/*                         <a href="maps.html"><i class="glyph-icon flaticon-world"></i><span class="sidebar-text">Apples d'offre</span></a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#"><i class="glyph-icon flaticon-panels"></i><span class="sidebar-text">Commandes</span><span class="fa arrow"></span></a>*/
/*                         */
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#"><i class="fa fa-heart"></i><span class="sidebar-text">Favoris</span><span class="fa arrow"></span></a>*/
/*                        */
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#"><i class="glyph-icon flaticon-account"></i><span class="sidebar-text">Account</span><span class="fa arrow"></span></a>*/
/*                         */
/*                     </li>*/
/*                     <li>*/
/*                         <a href="#"><i class="glyph-icon flaticon-gallery"></i><span class="sidebar-text">Images Manager</span><span class="fa arrow"></span></a>*/
/*                        */
/*                     </li>*/
/*                     <li class="m-b-245">*/
/*                         <a href="calendar.html"><i class="glyph-icon flaticon-calendar53"></i><span class="sidebar-text">Calendar</span></a>*/
/*                     </li>*/
/*                 </ul>*/
/*             </div>*/
/*             <div class="footer-widget">*/
/*                 <img src={{asset('img/gradient.png')}} alt="gradient effet" class="sidebar-gradient-img" />*/
/*                 <div id="sidebar-charts">*/
/*  */
/*                 <div class="sidebar-footer clearfix">*/
/*                     <a class="pull-left" href="profil.html" rel="tooltip" data-placement="top" data-original-title="Settings"><i class="glyph-icon flaticon-settings21"></i></a>*/
/*                     <a class="pull-left toggle_fullscreen" href="#" rel="tooltip" data-placement="top" data-original-title="Fullscreen"><i class="glyph-icon flaticon-fullscreen3"></i></a>*/
/*                     <a class="pull-left" href="lockscreen.html" rel="tooltip" data-placement="top" data-original-title="Lockscreen"><i class="glyph-icon flaticon-padlock23"></i></a>*/
/*                     <a class="pull-left" href="login.html" rel="tooltip" data-placement="top" data-original-title="Logout"><i class="fa fa-power-off"></i></a>*/
/*                 </div> */
/*             </div>*/
/*         </nav>*/
/*         <!-- END MAIN SIDEBAR -->*/
/*         <div id="main-content" class="page-mailbox container-fluid">*/
/*             <div class="row" data-equal-height="true">*/
/*                 <div class="col-md-4 list-messages">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-body messages">*/
/*                             <div class="input-group input-group-lg border-bottom">*/
/*                                 <span class="input-group-btn">*/
/*                                     <a href="#" class="btn"><i class="fa fa-search"></i></a>*/
/*                                   </span>*/
/*                                 <input type="text" class="form-control bd-0 bd-white" placeholder="Search">*/
/*                             </div>*/
/*                             <div id="messages-list" class="panel panel-default withScroll" data-height="window" data-padding="90">*/
/*                                   {% for i in mail %}   */
/*                                 <a class="message-item hire-me-btn media" href="#message" >*/
/*                                     <div class="pull-left text-center">*/
/*                                         <div class="pos-rel message-checkbox">*/
/*                                             <input type="checkbox" data-style="flat-red">*/
/*                                         </div>*/
/*                                         <div>*/
/*                                             <strong><i class="fa fa-paperclip"></i> 2</strong>*/
/*                                         </div>*/
/*                                     </div>*/
/*    */
/*        */
/*  <div class="message-item-right">*/
/*     */
/*                                         <div class="media">*/
/*                                             <img src={{asset(path('image'))}} alt="avatar 3" width="50" class="pull-left">*/
/*                                             <div class="media-body">*/
/*                                                 <small class="pull-right">23 Sept</small>*/
/*                                                 <h5 class="c-dark"><strong>{{user.username}}</strong></h5>*/
/*                                                 <h4 class="c-dark">{{i.nom}}</h4>*/
/*                                             </div>*/
/* </div>*/
/*                                         <p class="f-14 c-gray">{{i.Text}}</p>*/
/*                                           */
/*           </div>       */
/*                                         </a>*/
/*                                 {% endfor %}  */
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <!-- END MAIN CONTENT -->*/
/*     </div>*/
/*     <!-- END WRAPPER -->*/
/*     <!-- BEGIN CHAT MENU -->*/
/*     */
/*     <!-- END CHAT MENU -->*/
/*     <!-- BEGIN MANDATORY SCRIPTS -->*/
/*     <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*     <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*     <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*     <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*     <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*     <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*     <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*     <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*     <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*     <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*     <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*     <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*     <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*     <script src={{asset('js/mailbox.js')}}></script>*/
/*     <!-- END MANDATORY SCRIPTS -->*/
/*     <script src={{asset('js/application.js')}}></script>*/
/* </body>*/
/* */
/* </html>*/
/* */
