<?php

/* PiDevClientBundle:adresse:choisir.html.twig */
class __TwigTemplate_f2df4c6ae7c51a50cab32c1aca16abe59e358bac7c4b5778bf94250e2cf9b81b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <html>
        <head>  
<!-- styles -->
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- theme stylesheet -->
    <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/style.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-stylesheet\">

    <!-- your stylesheet with modifications -->
    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/custom.css\" rel=\"stylesheet"), "html", null, true);
        echo "\">

    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/respond.min.js"), "html", null, true);
        echo "\"></script>

    <link rel=\"shortcut icon\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/img/favicon.png"), "html", null, true);
        echo "\">



</head>

<body>";
        // line 26
        echo "    
   <div id=\"content\">
            <div class=\"container\">

                <div class=\"col-md-12\">
                    <ul class=\"breadcrumb\">
                        <li><a href=\"#\">Home</a>
                        </li>
                        <li>Checkout - Address</li>
                    </ul>
                </div>

                <div class=\"col-md-9\" id=\"checkout\">

                    <div class=\"box\">
                          <form  method=\"post\" action=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath("choisir_adresse");
        echo "\">
                            <h1>Checkout</h1>
                            <ul class=\"nav nav-pills nav-justified\">
                                <li class=\"active\"><a href=\"checkout1.html\"><i class=\"fa fa fa-truck\"></i><br>Livraison</a>
                                </li>
                                <li><a href=\"checkout2.html\"><i class=\"fa fa-money\"></i><br>Payment</a>
                                </li>
                                <li><a href=\"checkout3.html\"><i class=\"fa fa-eye\"></i><br>Confirmation</a>
                                </li>
                                <li ><a href=\"#\"><i class=\"fa fa-archive\"></i><br>Facture</a></li>
                            </ul>
<div class=\"content\">
                                <div class=\"row\">
                                           ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["adresse"]) ? $context["adresse"] : $this->getContext($context, "adresse")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 55
            echo "                                    <div class=\"col-sm-6\">
                                        <div class=\"box shipping-method\">

                                            <h4>Adresse : </h4>

                                            <p>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "rue", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "ville", array()), "html", null, true);
            echo ".</p>

                                            <div class=\"box-footer text-center\">

                                                <input type=\"radio\" name=\"adresse\" value=\"";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
            echo "\">
                                            </div>
                                        </div>
                                    </div>
                                            
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "                   
                      

                                </div>
                                      <a href=\"";
        // line 74
        echo $this->env->getExtension('routing')->getPath("adresse_create");
        echo "\">ajouter une nouvelle adresse</a><br>
                                            
                                                  <hr>
                                                                    <label for=\"name\"> mode de livraison<span class=\"typo-1\">*</span></label>
                                                                        <select  class=\"drop1\" name=\"modeLivraison\" >
                                                                            <option value=\"0\">Par poste</option>
                                                                            <option value=\"1\">Livraison à domicile</option>
                                                                        </select>
                                                                 
                                <!-- /.row -->

                            </div>

                            <div class=\"box-footer\">
                                <div class=\"pull-left\">
                                    <a href=\"basket.html\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Back to basket</a>
                                </div>
                                <div class=\"pull-right\">
                                    <button type=\"submit\" class=\"btn btn-primary\">Continue to Delivery Method<i class=\"fa fa-chevron-right\"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class=\"col-md-3\">

                    <div class=\"box\" id=\"order-summary\">
                        <div class=\"box-header\">
                            <h3>Order summary</h3>
                        </div>
                        <p class=\"text-muted\">Shipping and additional costs are calculated based on the values you have entered.</p>

                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>Order subtotal</td>
                                        <th>\$446.00</th>
                                    </tr>
                                    <tr>
                                        <td>Shipping and handling</td>
                                        <th>\$10.00</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>\$0.00</th>
                                    </tr>
                                    <tr class=\"total\">
                                        <td>Total</td>
                                        <th>\$456.00</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
    
    
    
 <script src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/waypoints.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/modernizr.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap-hover-dropdown.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/front.js"), "html", null, true);
        echo "\"></script>   
</body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:adresse:choisir.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  246 => 153,  242 => 152,  238 => 151,  234 => 150,  230 => 149,  226 => 148,  222 => 147,  218 => 146,  143 => 74,  137 => 70,  125 => 64,  116 => 60,  109 => 55,  105 => 54,  89 => 41,  72 => 26,  63 => 19,  58 => 17,  53 => 15,  47 => 12,  41 => 9,  37 => 8,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* */
/*     <html>*/
/*         <head>  */
/* <!-- styles -->*/
/*     <link href="{{asset('bundles/panier/css/font-awesome.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/animate.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.carousel.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.theme.css')}}" rel="stylesheet">*/
/* */
/*     <!-- theme stylesheet -->*/
/*     <link href="{{asset('bundles/panier/css/style.default.css')}}" rel="stylesheet" id="theme-stylesheet">*/
/* */
/*     <!-- your stylesheet with modifications -->*/
/*     <link href="{{asset('bundles/panier/css/custom.css" rel="stylesheet')}}">*/
/* */
/*     <script src="{{asset('bundles/panier/js/respond.min.js')}}"></script>*/
/* */
/*     <link rel="shortcut icon" href="{{asset('bundles/panier/img/favicon.png')}}">*/
/* */
/* */
/* */
/* </head>*/
/* */
/* <body>{# empty Twig template #}*/
/*     */
/*    <div id="content">*/
/*             <div class="container">*/
/* */
/*                 <div class="col-md-12">*/
/*                     <ul class="breadcrumb">*/
/*                         <li><a href="#">Home</a>*/
/*                         </li>*/
/*                         <li>Checkout - Address</li>*/
/*                     </ul>*/
/*                 </div>*/
/* */
/*                 <div class="col-md-9" id="checkout">*/
/* */
/*                     <div class="box">*/
/*                           <form  method="post" action="{{path ('choisir_adresse')}}">*/
/*                             <h1>Checkout</h1>*/
/*                             <ul class="nav nav-pills nav-justified">*/
/*                                 <li class="active"><a href="checkout1.html"><i class="fa fa fa-truck"></i><br>Livraison</a>*/
/*                                 </li>*/
/*                                 <li><a href="checkout2.html"><i class="fa fa-money"></i><br>Payment</a>*/
/*                                 </li>*/
/*                                 <li><a href="checkout3.html"><i class="fa fa-eye"></i><br>Confirmation</a>*/
/*                                 </li>*/
/*                                 <li ><a href="#"><i class="fa fa-archive"></i><br>Facture</a></li>*/
/*                             </ul>*/
/* <div class="content">*/
/*                                 <div class="row">*/
/*                                            {% for i in adresse %}*/
/*                                     <div class="col-sm-6">*/
/*                                         <div class="box shipping-method">*/
/* */
/*                                             <h4>Adresse : </h4>*/
/* */
/*                                             <p>{{ i.rue}} {{i.ville}}.</p>*/
/* */
/*                                             <div class="box-footer text-center">*/
/* */
/*                                                 <input type="radio" name="adresse" value="{{i.id}}">*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                             */
/*                                     {% endfor %}*/
/*                    */
/*                       */
/* */
/*                                 </div>*/
/*                                       <a href="{{path('adresse_create')}}">ajouter une nouvelle adresse</a><br>*/
/*                                             */
/*                                                   <hr>*/
/*                                                                     <label for="name"> mode de livraison<span class="typo-1">*</span></label>*/
/*                                                                         <select  class="drop1" name="modeLivraison" >*/
/*                                                                             <option value="0">Par poste</option>*/
/*                                                                             <option value="1">Livraison à domicile</option>*/
/*                                                                         </select>*/
/*                                                                  */
/*                                 <!-- /.row -->*/
/* */
/*                             </div>*/
/* */
/*                             <div class="box-footer">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="basket.html" class="btn btn-default"><i class="fa fa-chevron-left"></i>Back to basket</a>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <button type="submit" class="btn btn-primary">Continue to Delivery Method<i class="fa fa-chevron-right"></i>*/
/*                                     </button>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </form>*/
/*                     </div>*/
/*                     <!-- /.box -->*/
/* */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-9 -->*/
/* */
/*                 <div class="col-md-3">*/
/* */
/*                     <div class="box" id="order-summary">*/
/*                         <div class="box-header">*/
/*                             <h3>Order summary</h3>*/
/*                         </div>*/
/*                         <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>*/
/* */
/*                         <div class="table-responsive">*/
/*                             <table class="table">*/
/*                                 <tbody>*/
/*                                     <tr>*/
/*                                         <td>Order subtotal</td>*/
/*                                         <th>$446.00</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Shipping and handling</td>*/
/*                                         <th>$10.00</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Tax</td>*/
/*                                         <th>$0.00</th>*/
/*                                     </tr>*/
/*                                     <tr class="total">*/
/*                                         <td>Total</td>*/
/*                                         <th>$456.00</th>*/
/*                                     </tr>*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/* */
/*                     </div>*/
/* */
/*                 </div>*/
/*                 <!-- /.col-md-3 -->*/
/* */
/*             </div>*/
/*             <!-- /.container -->*/
/*         </div>*/
/*     */
/*     */
/*     */
/*  <script src="{{asset('bundles/panier/js/jquery-1.11.0.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/jquery.cookie.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/waypoints.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/modernizr.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap-hover-dropdown.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/owl.carousel.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/front.js')}}"></script>   */
/* </body>*/
/*     </html>*/
/* */
