<?php

/* PiDevAdminBundle:Default:index.html.twig */
class __TwigTemplate_60e94a71d89142b34f6555cbd5ff702cb261e01e4ded28a0c1fdfabf4f49d5fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PiDevAdminBundle::baseAdmin.html.twig", "PiDevAdminBundle:Default:index.html.twig", 1);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "PiDevAdminBundle::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 1,);
    }
}
/* {% extends "PiDevAdminBundle::baseAdmin.html.twig" %}*/
/* */
