<?php

/* PiDevClientBundle:CommentaireService:ajout.html.twig */
class __TwigTemplate_ca57cfc5e34583a183335840ce740244c3984fb592d4720f0d60133021cf2ded extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:CommentaireService:ajout.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "  <html>
    <head>  
      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1.0\"/>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <link href=";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
        <script type=\"text/javascript\" src=";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
        <!-- start menu -->
        <link href=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <script type=\"text/javascript\" src=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
        <script  src=";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
        <!--start slider -->
        <link rel=\"stylesheet\" href=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
        <script src=";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
        <script src=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
        <!--end slider -->
        <script src=";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>

       <!-- CSS  -->      
      <link href=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/materialize.css"), "html", null, true);
        echo " type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>
      <!-- Font Awesome -->
      <link href=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/font-awesome.css"), "html", null, true);
        echo " rel=\"stylesheet\">
      <!-- Skill Progress Bar -->
      <link href=";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/pro-bars.css\" rel=\"stylesheet"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" />
      <!-- Owl Carousel -->
      <link rel=\"stylesheet\" href=";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/owl.carousel.css"), "html", null, true);
        echo ">
      <!-- Default Theme CSS File-->
      <link id=\"switcher\" href=";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/themes/blue-theme.css"), "html", null, true);
        echo " type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>     
      <!-- Main css File -->
      <link href=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/style.css"), "html", null, true);
        echo " type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>
      <!-- Font -->
      <link href=\"http://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
    </head>

    <body>


     
        <main role=\"main\">
          <!-- START HOME SECTION -->
          <section id=\"banner\">
            <div class=\"parallax-container\">
              <div class=\"parallax\">
                <img src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/img/blog-header-bg.jpg"), "html", null, true);
        echo "\">
              </div>
              <div class=\"overlay-header\">       
              </div>
              <div class=\"overlay-content blog-head\">
                <div class=\"container\">
                  <h1 class=\"header-title\">Services</h1>                  
                </div>
              </div>
            </div>
          </section>


          <!-- START ABOUT SECTION -->


          <!-- Start Facts -->
          <section id=\"footer\">
            <div class=\"facts-overlay\">
              <div class=\"container\">
              <div class=\"row\">
                <div class=\"col s12\">
                  <div class=\"facts-inner\">
                    <div class=\"row\">
                        
                      <div class=\"col s12 m4 l4\">
                        <div class=\"single-facts waves-effect waves-block waves-light\">
                         <i class=\"fa fa-users\"></i>
                          <span class=\"counter\">329</span>
                          <span class=\"counter-text\"><a href=\"";
        // line 79
        echo $this->env->getExtension('routing')->getPath("listPanier");
        echo "\" >Votre panier</a></span>
                        </div>
                      </div>
                      
                      <div class=\"col s12 m4 l4\">
                        <div class=\"single-facts waves-effect waves-block waves-light\">
                          <i class=\"material-icons\">supervisor_account</i>
                          <span class=\"counter\">250</span>
                          <span class=\"counter-text\">Vos Forums</span>
                        </div>
                      </div>
                      <div class=\"col s12 m4 l4\">
                        <div class=\"single-facts waves-effect waves-block waves-light\">
                          <i class=\"material-icons\">redeem</i>
                          <span class=\"counter\">69</span>
                          <span class=\"counter-text\"><a href=\"";
        // line 94
        echo $this->env->getExtension('routing')->getPath("listCommande");
        echo "\" >Vos Commandes</a></span>
                        </div>
                      </div>
                    </div>
                        
                  </div>
                </div>
              </div>
                        
            </div>
            </div>
          </section>


          <!-- Start Footer -->
        
      <!-- jQuery Library -->
      <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-2.1.1.min.js\"></script>
      <!-- Materialize js -->
      <script type=\"text/javascript\" src=";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/materialize.min.js"), "html", null, true);
        echo "></script>
      <!-- Skill Progress Bar -->
      <script src=";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/appear.min.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>
      <script src=";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/pro-bars.min.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>
      <!-- Owl slider -->      
      <script src=";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/owl.carousel.min.js"), "html", null, true);
        echo "></script>    
      <!-- Mixit slider  -->
      <script src=\"http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js\"></script>
      <!-- counter -->
      <script src=";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/waypoints.min.js"), "html", null, true);
        echo "></script>
      <script src=";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/jquery.counterup.min.js"), "html", null, true);
        echo "></script>     

      <!-- Custom Js -->
      <script src=";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/custom.js"), "html", null, true);
        echo "></script>      
    </body>
  </html>
  ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:CommentaireService:ajout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 126,  228 => 123,  224 => 122,  217 => 118,  212 => 116,  208 => 115,  203 => 113,  181 => 94,  163 => 79,  131 => 50,  114 => 36,  109 => 34,  104 => 32,  99 => 30,  94 => 28,  89 => 26,  83 => 23,  78 => 21,  74 => 20,  70 => 19,  66 => 18,  61 => 16,  57 => 15,  53 => 14,  48 => 12,  43 => 10,  39 => 9,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/*  {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/*   <html>*/
/*     <head>  */
/*       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>*/
/*       <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*         <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*         <!-- start menu -->*/
/*         <link href={{asset ('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*         <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*         <!--start slider -->*/
/*         <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*         <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*         <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*         <script src={{asset('js/fwslider.js')}}></script>*/
/*         <!--end slider -->*/
/*         <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/* */
/*        <!-- CSS  -->      */
/*       <link href={{asset('bundles/profile/css/materialize.css')}} type="text/css" rel="stylesheet" media="screen,projection"/>*/
/*       <!-- Font Awesome -->*/
/*       <link href={{asset('bundles/profile/css/font-awesome.css')}} rel="stylesheet">*/
/*       <!-- Skill Progress Bar -->*/
/*       <link href={{asset('bundles/profile/css/pro-bars.css" rel="stylesheet')}} type="text/css" media="all" />*/
/*       <!-- Owl Carousel -->*/
/*       <link rel="stylesheet" href={{asset('bundles/profile/css/owl.carousel.css')}}>*/
/*       <!-- Default Theme CSS File-->*/
/*       <link id="switcher" href={{asset('bundles/profile/css/themes/blue-theme.css')}} type="text/css" rel="stylesheet" media="screen,projection"/>     */
/*       <!-- Main css File -->*/
/*       <link href={{asset('bundles/profile/style.css')}} type="text/css" rel="stylesheet" media="screen,projection"/>*/
/*       <!-- Font -->*/
/*       <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">*/
/*     </head>*/
/* */
/*     <body>*/
/* */
/* */
/*      */
/*         <main role="main">*/
/*           <!-- START HOME SECTION -->*/
/*           <section id="banner">*/
/*             <div class="parallax-container">*/
/*               <div class="parallax">*/
/*                 <img src="{{asset('bundles/profile/img/blog-header-bg.jpg')}}">*/
/*               </div>*/
/*               <div class="overlay-header">       */
/*               </div>*/
/*               <div class="overlay-content blog-head">*/
/*                 <div class="container">*/
/*                   <h1 class="header-title">Services</h1>                  */
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*           </section>*/
/* */
/* */
/*           <!-- START ABOUT SECTION -->*/
/* */
/* */
/*           <!-- Start Facts -->*/
/*           <section id="footer">*/
/*             <div class="facts-overlay">*/
/*               <div class="container">*/
/*               <div class="row">*/
/*                 <div class="col s12">*/
/*                   <div class="facts-inner">*/
/*                     <div class="row">*/
/*                         */
/*                       <div class="col s12 m4 l4">*/
/*                         <div class="single-facts waves-effect waves-block waves-light">*/
/*                          <i class="fa fa-users"></i>*/
/*                           <span class="counter">329</span>*/
/*                           <span class="counter-text"><a href="{{path('listPanier')}}" >Votre panier</a></span>*/
/*                         </div>*/
/*                       </div>*/
/*                       */
/*                       <div class="col s12 m4 l4">*/
/*                         <div class="single-facts waves-effect waves-block waves-light">*/
/*                           <i class="material-icons">supervisor_account</i>*/
/*                           <span class="counter">250</span>*/
/*                           <span class="counter-text">Vos Forums</span>*/
/*                         </div>*/
/*                       </div>*/
/*                       <div class="col s12 m4 l4">*/
/*                         <div class="single-facts waves-effect waves-block waves-light">*/
/*                           <i class="material-icons">redeem</i>*/
/*                           <span class="counter">69</span>*/
/*                           <span class="counter-text"><a href="{{path('listCommande')}}" >Vos Commandes</a></span>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/*                         */
/*                   </div>*/
/*                 </div>*/
/*               </div>*/
/*                         */
/*             </div>*/
/*             </div>*/
/*           </section>*/
/* */
/* */
/*           <!-- Start Footer -->*/
/*         */
/*       <!-- jQuery Library -->*/
/*       <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>*/
/*       <!-- Materialize js -->*/
/*       <script type="text/javascript" src={{asset('bundles/profile/js/materialize.min.js')}}></script>*/
/*       <!-- Skill Progress Bar -->*/
/*       <script src={{asset('bundles/profile/js/appear.min.js')}} type="text/javascript"></script>*/
/*       <script src={{asset('bundles/profile/js/pro-bars.min.js')}} type="text/javascript"></script>*/
/*       <!-- Owl slider -->      */
/*       <script src={{asset('bundles/profile/js/owl.carousel.min.js')}}></script>    */
/*       <!-- Mixit slider  -->*/
/*       <script src="http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>*/
/*       <!-- counter -->*/
/*       <script src={{asset('bundles/profile/js/waypoints.min.js')}}></script>*/
/*       <script src={{asset('bundles/profile/js/jquery.counterup.min.js')}}></script>     */
/* */
/*       <!-- Custom Js -->*/
/*       <script src={{asset('bundles/profile/js/custom.js')}}></script>      */
/*     </body>*/
/*   </html>*/
/*   {% endblock %}*/
