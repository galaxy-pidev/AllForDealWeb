<?php

/* PiDevAdminBundle:Admin:ListeUtilisateurs.html.twig */
class __TwigTemplate_59d6cebb06d9bf78e586b92e5cdb1f596f394705d6a8b4a10a58e3ec830da59d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:Admin:ListeUtilisateurs.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        echo " 
 
  <html>
    <head>  
        <link href=";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
       


        <!-- END  MANDATORY STYLE -->
   
                <script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/popup/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/popup/js/jquery.leanModal.min.js"), "html", null, true);
        echo "\"></script>
        <link rel=\"stylesheet\" href=\"http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" />
        <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/popup/css/style.css"), "html", null, true);
        echo "\" />
    
    </head>

    <body>
      <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Liste </strong> des utilisateurs </h3>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\"><strong>toutes  </strong>les informations des utiisateurs </h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive\">
                                    <table class=\"table table-striped table-hover\">
                                        <thead class=\"no-bd\">
                                            <tr>
                                                <th style=\"width:30px;\" class=\"div_checkbox\">
                                                    <div class=\"div_checkbox\">
                                                    
                                                    </div>
                                                </th>
                                                <th><strong>Nom</strong>
                                                </th>
                                        
                                                </th>
                                                <th><strong>Etat du compte</strong>
                                                </th>
                                             
                                                <th><strong>Nombre des produits</strong>
                                                </th>
                                                <th><strong>Nombre des services</strong>
                                                </th>
                                                <th ><strong>Nombre des appels d'offre</strong>
                                                </th>
                                                 <th ><strong>Nombre des sujets</strong>
                                                </th>
                                                 <th ><strong>Roles</strong>
                                                </th>
                                                <th class=\"text-center\"><strong>Actions</strong>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class=\"no-bd-y\">
                                          ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mods"]) ? $context["mods"] : $this->getContext($context, "mods")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            echo " 
                                              
                                            <tr>
                                                <td>
                                                    <div class=\"div_checkbox\">
                                                   
                                                    </div>
                                                </td>
                                                <td>";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "username", array()), "html", null, true);
            echo "</td>
                                              
                                                ";
            // line 75
            if (($this->getAttribute($context["j"], "enabled", array()) == 0)) {
                // line 76
                echo "                                                <td class=\"text-left color-success\">compte desactivé <br>depuis ";
                echo twig_escape_filter($this->env, $this->env->getExtension('date')->diff($this->env, $this->getAttribute($context["j"], "lastlogin", array())), "html", null, true);
                echo "</td>
                                                ";
            } else {
                // line 78
                echo "                                            <td class=\"text-left color-success\">compte activé </td>
                                            ";
            }
            // line 80
            echo "                                                ";
            $context["nbprod"] = 0;
            // line 81
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["prods"]) ? $context["prods"] : $this->getContext($context, "prods")));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 82
                echo "                                                ";
                if (($this->getAttribute($this->getAttribute($context["p"], "user", array()), "id", array()) == $this->getAttribute($context["j"], "id", array()))) {
                    // line 83
                    echo "                                                ";
                    $context["nbprod"] = ((isset($context["nbprod"]) ? $context["nbprod"] : $this->getContext($context, "nbprod")) + 1);
                    // line 84
                    echo "                                                ";
                }
                echo " 
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 86
            echo "                                               
                                               <td class=\"text-center\">";
            // line 87
            echo twig_escape_filter($this->env, (isset($context["nbprod"]) ? $context["nbprod"] : $this->getContext($context, "nbprod")), "html", null, true);
            echo "</td>
                                                ";
            // line 88
            $context["nbservice"] = 0;
            // line 89
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["services"]) ? $context["services"] : $this->getContext($context, "services")));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 90
                echo "                                                ";
                if (($this->getAttribute($this->getAttribute($context["p"], "proprietaire", array()), "id", array()) == $this->getAttribute($context["j"], "id", array()))) {
                    // line 91
                    echo "                                                ";
                    $context["nbservice"] = ((isset($context["nbservice"]) ? $context["nbservice"] : $this->getContext($context, "nbservice")) + 1);
                    // line 92
                    echo "                                                ";
                }
                echo " 
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo "                                                <td class=\"text-center\">";
            echo twig_escape_filter($this->env, (isset($context["nbservice"]) ? $context["nbservice"] : $this->getContext($context, "nbservice")), "html", null, true);
            echo "</td>
                                                ";
            // line 95
            $context["nbappeloffre"] = 0;
            // line 96
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["appeloffre"]) ? $context["appeloffre"] : $this->getContext($context, "appeloffre")));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 97
                echo "                                                ";
                if (($this->getAttribute($this->getAttribute($context["p"], "proprietaire", array()), "id", array()) == $this->getAttribute($context["j"], "id", array()))) {
                    // line 98
                    echo "                                                ";
                    $context["nbappeloffre"] = ((isset($context["nbappeloffre"]) ? $context["nbappeloffre"] : $this->getContext($context, "nbappeloffre")) + 1);
                    // line 99
                    echo "                                                ";
                }
                echo " 
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 101
            echo "                                                <td class=\"text-center\"> ";
            echo twig_escape_filter($this->env, (isset($context["nbappeloffre"]) ? $context["nbappeloffre"] : $this->getContext($context, "nbappeloffre")), "html", null, true);
            echo "</td>
                                                ";
            // line 102
            $context["nbsujet"] = 0;
            // line 103
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["sujet"]) ? $context["sujet"] : $this->getContext($context, "sujet")));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 104
                echo "                                                ";
                if (($this->getAttribute($this->getAttribute($context["p"], "user", array()), "id", array()) == $this->getAttribute($context["j"], "id", array()))) {
                    // line 105
                    echo "                                                ";
                    $context["nbsujet"] = ((isset($context["nbsujet"]) ? $context["nbsujet"] : $this->getContext($context, "nbsujet")) + 1);
                    // line 106
                    echo "                                                ";
                }
                echo " 
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 108
            echo "                                                <td class=\"text-center\">";
            echo twig_escape_filter($this->env, (isset($context["nbsujet"]) ? $context["nbsujet"] : $this->getContext($context, "nbsujet")), "html", null, true);
            echo " </td>
                                               
                                                <td >
                                                     ";
            // line 111
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["j"], "roles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 112
                echo "                                                         ";
                echo twig_escape_filter($this->env, $context["r"], "html", null, true);
                echo "<br>
                                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 114
            echo "                                                </td>
                                             
                                                <td class=\"text-center\">
                                                 <div class=\"onoffswitch\">
            <a href=\"";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("lignecommande", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>
            <a href=\"#";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", array()), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "username", array()), "html", null, true);
            echo "\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>
                                                </td>
                                            </tr>
                                            
                                          
         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 125
        echo "
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    </div>
                                      
                    ";
        // line 136
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mods"]) ? $context["mods"] : $this->getContext($context, "mods")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            echo " 
                                        <div id=\"";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", array()), "html", null, true);
            echo "\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>

                                        <section class=\"popupBodny\">
                                            <!-- Register Form -->
                                            <div class=\"user_register\">
                                                                       
    <form method='post' action=\"";
            // line 147
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_modifier_utilisateur", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
            echo "\">
   
        <label>    roles :</label>
                 <td> 
             <lable name=\"";
            // line 151
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", array()), "html", null, true);
            echo "\"></lable>
             <div class=\"styled-select\">
   <select name=\"role\">
      <option value=\"ROLE_ADMIN\">Administrateur</option>
      <option value=\"ROLE_USER\"> Utilisateur</option>
   </select>
</div>
             \t<div class=\"checkbox\">
\t\t\t\t\t
             <input type=\"radio\" name=\"role\" value=\"ROLE_ADMIN\" checked> Administrateur<br>
               <input type=\"radio\" name=\"role\" value=\"ROLE_USER\"> Utilisateur <br>
    </div>
                 
            
     


<input type=\"submit\" value=\"modifier\" >

</form>                                  </div>
                                        </section>
                                    </div>    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 174
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mods"]) ? $context["mods"] : $this->getContext($context, "mods")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            echo " 
           <script type=\"text/javascript\">
            \$(\"#";
            // line 176
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "username", array()), "html", null, true);
            echo "\").leanModal({top: 200, overlay: 0.6, closeButton: \".modal_close\"});

     
        </script>
        <script>
            \$(function() {
    \$(\"#selection\").change(function() {
        var val = \$(this).val();

 
            </script>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 188
        echo "        
        <script src=";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 190
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
        <!-- END MANDATORY SCRIPTS -->
        <script src=";
        // line 204
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>  
  </body>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Admin:ListeUtilisateurs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  447 => 204,  442 => 202,  438 => 201,  434 => 200,  430 => 199,  426 => 198,  422 => 197,  418 => 196,  414 => 195,  410 => 194,  406 => 193,  402 => 192,  398 => 191,  394 => 190,  390 => 189,  387 => 188,  369 => 176,  361 => 174,  332 => 151,  325 => 147,  312 => 137,  306 => 136,  293 => 125,  279 => 119,  275 => 118,  269 => 114,  260 => 112,  256 => 111,  249 => 108,  240 => 106,  237 => 105,  234 => 104,  229 => 103,  227 => 102,  222 => 101,  213 => 99,  210 => 98,  207 => 97,  202 => 96,  200 => 95,  195 => 94,  186 => 92,  183 => 91,  180 => 90,  175 => 89,  173 => 88,  169 => 87,  166 => 86,  157 => 84,  154 => 83,  151 => 82,  146 => 81,  143 => 80,  139 => 78,  133 => 76,  131 => 75,  126 => 73,  113 => 65,  62 => 17,  57 => 15,  53 => 14,  44 => 8,  40 => 7,  36 => 6,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::baseAdmin.html.twig' %}*/
/* {% block content%} */
/*  */
/*   <html>*/
/*     <head>  */
/*         <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*        */
/* */
/* */
/*         <!-- END  MANDATORY STYLE -->*/
/*    */
/*                 <script type="text/javascript" src="{{asset('bundles/popup/js/jquery-1.11.0.min.js')}}"></script>*/
/*         <script type="text/javascript" src="{{asset('bundles/popup/js/jquery.leanModal.min.js')}}"></script>*/
/*         <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />*/
/*         <link type="text/css" rel="stylesheet" href="{{asset('bundles/popup/css/style.css')}}" />*/
/*     */
/*     </head>*/
/* */
/*     <body>*/
/*       <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Liste </strong> des utilisateurs </h3>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading">*/
/*                             <h3 class="panel-title"><strong>toutes  </strong>les informations des utiisateurs </h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">*/
/*                                     <table class="table table-striped table-hover">*/
/*                                         <thead class="no-bd">*/
/*                                             <tr>*/
/*                                                 <th style="width:30px;" class="div_checkbox">*/
/*                                                     <div class="div_checkbox">*/
/*                                                     */
/*                                                     </div>*/
/*                                                 </th>*/
/*                                                 <th><strong>Nom</strong>*/
/*                                                 </th>*/
/*                                         */
/*                                                 </th>*/
/*                                                 <th><strong>Etat du compte</strong>*/
/*                                                 </th>*/
/*                                              */
/*                                                 <th><strong>Nombre des produits</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Nombre des services</strong>*/
/*                                                 </th>*/
/*                                                 <th ><strong>Nombre des appels d'offre</strong>*/
/*                                                 </th>*/
/*                                                  <th ><strong>Nombre des sujets</strong>*/
/*                                                 </th>*/
/*                                                  <th ><strong>Roles</strong>*/
/*                                                 </th>*/
/*                                                 <th class="text-center"><strong>Actions</strong>*/
/*                                                 </th>*/
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         <tbody class="no-bd-y">*/
/*                                           {% for j in mods %} */
/*                                               */
/*                                             <tr>*/
/*                                                 <td>*/
/*                                                     <div class="div_checkbox">*/
/*                                                    */
/*                                                     </div>*/
/*                                                 </td>*/
/*                                                 <td>{{j.username}}</td>*/
/*                                               */
/*                                                 {% if j.enabled==0 %}*/
/*                                                 <td class="text-left color-success">compte desactivé <br>depuis {{j.lastlogin|time_diff }}</td>*/
/*                                                 {% else %}*/
/*                                             <td class="text-left color-success">compte activé </td>*/
/*                                             {% endif %}*/
/*                                                 {% set nbprod =0 %}*/
/*                                                 {% for p in prods %}*/
/*                                                 {% if p.user.id == j.id %}*/
/*                                                 {% set nbprod =nbprod+1 %}*/
/*                                                 {% endif %} */
/*                                                 {% endfor %}*/
/*                                                */
/*                                                <td class="text-center">{{nbprod}}</td>*/
/*                                                 {% set nbservice =0 %}*/
/*                                                 {% for p in services %}*/
/*                                                 {% if p.proprietaire.id == j.id %}*/
/*                                                 {% set nbservice =nbservice+1 %}*/
/*                                                 {% endif %} */
/*                                                 {% endfor %}*/
/*                                                 <td class="text-center">{{nbservice}}</td>*/
/*                                                 {% set nbappeloffre =0 %}*/
/*                                                 {% for p in appeloffre %}*/
/*                                                 {% if p.proprietaire.id == j.id %}*/
/*                                                 {% set nbappeloffre =nbappeloffre+1 %}*/
/*                                                 {% endif %} */
/*                                                 {% endfor %}*/
/*                                                 <td class="text-center"> {{nbappeloffre}}</td>*/
/*                                                 {% set nbsujet =0 %}*/
/*                                                 {% for p in sujet %}*/
/*                                                 {% if p.user.id == j.id %}*/
/*                                                 {% set nbsujet =nbsujet+1 %}*/
/*                                                 {% endif %} */
/*                                                 {% endfor %}*/
/*                                                 <td class="text-center">{{nbsujet}} </td>*/
/*                                                */
/*                                                 <td >*/
/*                                                      {% for r in j.roles %}*/
/*                                                          {{r}}<br>*/
/*                                                         {% endfor %}*/
/*                                                 </td>*/
/*                                              */
/*                                                 <td class="text-center">*/
/*                                                  <div class="onoffswitch">*/
/*             <a href="{{path('lignecommande',{'id':j.id})}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>*/
/*             <a href="#{{j.id}}" id="{{j.username}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>*/
/*                                                 </td>*/
/*                                             </tr>*/
/*                                             */
/*                                           */
/*          {% endfor %}*/
/* */
/*                                         </tbody>*/
/*                                     </table>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*                     </div>*/
/*                                       */
/*                     {% for j in mods %} */
/*                                         <div id="{{j.id}}" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/* */
/*                                         <section class="popupBodny">*/
/*                                             <!-- Register Form -->*/
/*                                             <div class="user_register">*/
/*                                                                        */
/*     <form method='post' action="{{path('admin_modifier_utilisateur',{'id':j.id})}}">*/
/*    */
/*         <label>    roles :</label>*/
/*                  <td> */
/*              <lable name="{{j.id}}"></lable>*/
/*              <div class="styled-select">*/
/*    <select name="role">*/
/*       <option value="ROLE_ADMIN">Administrateur</option>*/
/*       <option value="ROLE_USER"> Utilisateur</option>*/
/*    </select>*/
/* </div>*/
/*              	<div class="checkbox">*/
/* 					*/
/*              <input type="radio" name="role" value="ROLE_ADMIN" checked> Administrateur<br>*/
/*                <input type="radio" name="role" value="ROLE_USER"> Utilisateur <br>*/
/*     </div>*/
/*                  */
/*             */
/*      */
/* */
/* */
/* <input type="submit" value="modifier" >*/
/* */
/* </form>                                  </div>*/
/*                                         </section>*/
/*                                     </div>    */
/*     {% endfor %}*/
/*         {% for j in mods %} */
/*            <script type="text/javascript">*/
/*             $("#{{j.username}}").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});*/
/* */
/*      */
/*         </script>*/
/*         <script>*/
/*             $(function() {*/
/*     $("#selection").change(function() {*/
/*         var val = $(this).val();*/
/* */
/*  */
/*             </script>*/
/*           {% endfor %}*/
/*         */
/*         <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*         <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*         <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*         <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*         <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*         <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*         <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*         <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*         <script src={{asset('js/mailbox.js')}}></script>*/
/*         <!-- END MANDATORY SCRIPTS -->*/
/*         <script src={{asset('js/application.js')}}></script>  */
/*   </body>*/
/*  {% endblock %}*/
