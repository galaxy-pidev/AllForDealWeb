<?php

/* PiDevClientBundle:Commande:Payment.html.twig */
class __TwigTemplate_57da79bba6464e979e97be2f63ef114b383d87f02150696982924c6f3d8a491b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <html>
        <head>  
<!-- styles -->
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- theme stylesheet -->
    <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/style.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-stylesheet\">

    <!-- your stylesheet with modifications -->
    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/custom.css\" rel=\"stylesheet"), "html", null, true);
        echo "\">

    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/respond.min.js"), "html", null, true);
        echo "\"></script>

    <link rel=\"shortcut icon\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/img/favicon.png"), "html", null, true);
        echo "\">



</head>

<body>";
        // line 26
        echo "   
    
    
    <div id=\"content\">
            <div class=\"container\">

                <div class=\"col-md-12\">
                    <ul class=\"breadcrumb\">
                        <li><a href=\"#\">Home</a>
                        </li>
                        <li>Checkout - Delivery method</li>
                    </ul>
                </div>

                <div class=\"col-md-9\" id=\"checkout\">

                    <div class=\"box\">
                        <form method=\"post\" action=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("payer_commande", array("id" => $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "id", array()))), "html", null, true);
        echo "\">
                            <h1>Checkout - Delivery method</h1>
                            <ul class=\"nav nav-pills nav-justified\">
                                <li><a href=\"checkout1.html\"><i class=\"fa fa-map-marker\"></i><br>Address</a>
                                </li>
                                <li class=\"active\"><a href=\"#\"><i class=\"fa fa-truck\"></i><br>Delivery Method</a>
                                </li>
                                <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-money\"></i><br>Payment Method</a>
                                </li>
                                <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-eye\"></i><br>Order Review</a>
                                </li>
                            </ul>

                            <div class=\"content\">
                                <div class=\"row\">
                                    <div class=\"col-sm-6\">
                                        <div class=\"box shipping-method\">

                                            <h4>point bonus</h4>

                                            <p>Get it right on next day - fastest option possible.</p>

                                            <div class=\"box-footer text-center\">

                                                <input type=\"radio\" name=\"type\" value=\"1\">
                                            </div>
                                        </div>
                                    </div>
                                    
                                                                        <div class=\"col-sm-6\">
                                        <div class=\"box shipping-method\">

                                            <h4>carte bancaire</h4>

                                            <p>Get it right on next day - fastest option possible.</p>

                                            <div class=\"box-footer text-center\">

                                                <input type=\"radio\" name=\"type\" value=\"2\">
                                            </div>
                                        </div>
                                    </div>
                                    
                                                                        <div class=\"col-sm-6\">
                                        <div class=\"box shipping-method\">

                                            <h4>à la livraison</h4>

                                            <p>Get it right on next day - fastest option possible.</p>

                                            <div class=\"box-footer text-center\">

                                                <input type=\"radio\" name=\"type\" value=\"2\">
                                            </div>
                                        </div>
                                    </div>
       

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.content -->

                            <div class=\"box-footer\">
                                <div class=\"pull-left\">
                                    <a href=\"basket.html\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Back to Addresses</a>
                                </div>
                                <div class=\"pull-right\">
                                    <button type=\"submit\" class=\"btn btn-primary\">Continue to Payment Method<i class=\"fa fa-chevron-right\"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class=\"col-md-3\">

                    <div class=\"box\" id=\"order-summary\">
                        <div class=\"box-header\">
                            <h3>Order summary</h3>
                        </div>
                        <p class=\"text-muted\">Shipping and additional costs are calculated based on the values you have entered.</p>

                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>Order subtotal</td>
                                        <th>\$446.00</th>
                                    </tr>
                                    <tr>
                                        <td>Shipping and handling</td>
                                        <th>\$10.00</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>\$0.00</th>
                                    </tr>
                                    <tr class=\"total\">
                                        <td>Total</td>
                                        <th>\$456.00</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
    
 <script src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/waypoints.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/modernizr.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap-hover-dropdown.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/front.js"), "html", null, true);
        echo "\"></script>   
</body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commande:Payment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 172,  240 => 171,  236 => 170,  232 => 169,  228 => 168,  224 => 167,  220 => 166,  216 => 165,  91 => 43,  72 => 26,  63 => 19,  58 => 17,  53 => 15,  47 => 12,  41 => 9,  37 => 8,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* */
/*     <html>*/
/*         <head>  */
/* <!-- styles -->*/
/*     <link href="{{asset('bundles/panier/css/font-awesome.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/animate.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.carousel.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.theme.css')}}" rel="stylesheet">*/
/* */
/*     <!-- theme stylesheet -->*/
/*     <link href="{{asset('bundles/panier/css/style.default.css')}}" rel="stylesheet" id="theme-stylesheet">*/
/* */
/*     <!-- your stylesheet with modifications -->*/
/*     <link href="{{asset('bundles/panier/css/custom.css" rel="stylesheet')}}">*/
/* */
/*     <script src="{{asset('bundles/panier/js/respond.min.js')}}"></script>*/
/* */
/*     <link rel="shortcut icon" href="{{asset('bundles/panier/img/favicon.png')}}">*/
/* */
/* */
/* */
/* </head>*/
/* */
/* <body>{# empty Twig template #}{# empty Twig template #}*/
/*    */
/*     */
/*     */
/*     <div id="content">*/
/*             <div class="container">*/
/* */
/*                 <div class="col-md-12">*/
/*                     <ul class="breadcrumb">*/
/*                         <li><a href="#">Home</a>*/
/*                         </li>*/
/*                         <li>Checkout - Delivery method</li>*/
/*                     </ul>*/
/*                 </div>*/
/* */
/*                 <div class="col-md-9" id="checkout">*/
/* */
/*                     <div class="box">*/
/*                         <form method="post" action="{{path('payer_commande',{'id':commande.id})}}">*/
/*                             <h1>Checkout - Delivery method</h1>*/
/*                             <ul class="nav nav-pills nav-justified">*/
/*                                 <li><a href="checkout1.html"><i class="fa fa-map-marker"></i><br>Address</a>*/
/*                                 </li>*/
/*                                 <li class="active"><a href="#"><i class="fa fa-truck"></i><br>Delivery Method</a>*/
/*                                 </li>*/
/*                                 <li class="disabled"><a href="#"><i class="fa fa-money"></i><br>Payment Method</a>*/
/*                                 </li>*/
/*                                 <li class="disabled"><a href="#"><i class="fa fa-eye"></i><br>Order Review</a>*/
/*                                 </li>*/
/*                             </ul>*/
/* */
/*                             <div class="content">*/
/*                                 <div class="row">*/
/*                                     <div class="col-sm-6">*/
/*                                         <div class="box shipping-method">*/
/* */
/*                                             <h4>point bonus</h4>*/
/* */
/*                                             <p>Get it right on next day - fastest option possible.</p>*/
/* */
/*                                             <div class="box-footer text-center">*/
/* */
/*                                                 <input type="radio" name="type" value="1">*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     */
/*                                                                         <div class="col-sm-6">*/
/*                                         <div class="box shipping-method">*/
/* */
/*                                             <h4>carte bancaire</h4>*/
/* */
/*                                             <p>Get it right on next day - fastest option possible.</p>*/
/* */
/*                                             <div class="box-footer text-center">*/
/* */
/*                                                 <input type="radio" name="type" value="2">*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     */
/*                                                                         <div class="col-sm-6">*/
/*                                         <div class="box shipping-method">*/
/* */
/*                                             <h4>à la livraison</h4>*/
/* */
/*                                             <p>Get it right on next day - fastest option possible.</p>*/
/* */
/*                                             <div class="box-footer text-center">*/
/* */
/*                                                 <input type="radio" name="type" value="2">*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*        */
/* */
/*                                 </div>*/
/*                                 <!-- /.row -->*/
/* */
/*                             </div>*/
/*                             <!-- /.content -->*/
/* */
/*                             <div class="box-footer">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="basket.html" class="btn btn-default"><i class="fa fa-chevron-left"></i>Back to Addresses</a>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <button type="submit" class="btn btn-primary">Continue to Payment Method<i class="fa fa-chevron-right"></i>*/
/*                                     </button>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </form>*/
/*                     </div>*/
/*                     <!-- /.box -->*/
/* */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-9 -->*/
/* */
/*                 <div class="col-md-3">*/
/* */
/*                     <div class="box" id="order-summary">*/
/*                         <div class="box-header">*/
/*                             <h3>Order summary</h3>*/
/*                         </div>*/
/*                         <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>*/
/* */
/*                         <div class="table-responsive">*/
/*                             <table class="table">*/
/*                                 <tbody>*/
/*                                     <tr>*/
/*                                         <td>Order subtotal</td>*/
/*                                         <th>$446.00</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Shipping and handling</td>*/
/*                                         <th>$10.00</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Tax</td>*/
/*                                         <th>$0.00</th>*/
/*                                     </tr>*/
/*                                     <tr class="total">*/
/*                                         <td>Total</td>*/
/*                                         <th>$456.00</th>*/
/*                                     </tr>*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/* */
/*                     </div>*/
/* */
/*                 </div>*/
/*                 <!-- /.col-md-3 -->*/
/* */
/*             </div>*/
/*             <!-- /.container -->*/
/*         </div>*/
/*         <!-- /#content -->*/
/*     */
/*  <script src="{{asset('bundles/panier/js/jquery-1.11.0.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/jquery.cookie.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/waypoints.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/modernizr.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap-hover-dropdown.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/owl.carousel.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/front.js')}}"></script>   */
/* </body>*/
/*     </html>*/
/* */
