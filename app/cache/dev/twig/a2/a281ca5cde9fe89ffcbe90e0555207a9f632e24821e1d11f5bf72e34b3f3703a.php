<?php

/* PiDevClientBundle:Produit:listuser.html.twig */
class __TwigTemplate_e65c3eeab9ff845c186131c30f0d13a4e83613d10db2d9c944c0c0b3e285839f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:listuser.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <center>  <title> Liste des Produit All for deal: </title><br> </center>
    <forum>
       <table border=\"1px\" style=\"width:100%\">
           <tr > <td>id</td>
               <td> Nom  </td>
               <td> Description </td>
               <td> Prix  </td>
               <td> Quantité  </td>
               <td> Promotion  </td> 
               <td> Marque </td> 
               <td> NBvente  </td>
               <td> dateAjout  </td>
               <td> Nbpointbase </td>  
               <td> Categorie </td> 
               <td> supprimer </td>
               <td> modifier </td>


       </tr>
             ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            // line 23
            echo "         

      
        <tr>
            
            <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nom", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "description", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "prix", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "qantite", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "promotion", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "marque", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nbvente", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["j"], "dateajout", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "</td>
            <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nbpointbase", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "categorie", array()), "html", null, true);
            echo "</td>
            <td><a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_delete", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
            echo "\"> Supprimer </a></td>
 <td><a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_update", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
            echo "\"> Modifier </a></td>

        </tr>
        
         
       
 
         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "   

       </table>
    </forum>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:listuser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 48,  111 => 40,  107 => 39,  103 => 38,  99 => 37,  95 => 36,  91 => 35,  87 => 34,  83 => 33,  79 => 32,  75 => 31,  71 => 30,  67 => 29,  63 => 28,  56 => 23,  52 => 22,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/*     <center>  <title> Liste des Produit All for deal: </title><br> </center>*/
/*     <forum>*/
/*        <table border="1px" style="width:100%">*/
/*            <tr > <td>id</td>*/
/*                <td> Nom  </td>*/
/*                <td> Description </td>*/
/*                <td> Prix  </td>*/
/*                <td> Quantité  </td>*/
/*                <td> Promotion  </td> */
/*                <td> Marque </td> */
/*                <td> NBvente  </td>*/
/*                <td> dateAjout  </td>*/
/*                <td> Nbpointbase </td>  */
/*                <td> Categorie </td> */
/*                <td> supprimer </td>*/
/*                <td> modifier </td>*/
/* */
/* */
/*        </tr>*/
/*              {% for j in entities %}*/
/*          */
/* */
/*       */
/*         <tr>*/
/*             */
/*             <td>{{j.id}}</td>*/
/*             <td>{{j.nom}}</td>*/
/*             <td>{{j.description}}</td>*/
/*             <td>{{j.prix}}</td>*/
/*             <td>{{j.qantite}}</td>*/
/*             <td>{{j.promotion}}</td>*/
/*             <td>{{j.marque}}</td>*/
/*             <td>{{j.nbvente}}</td>*/
/*             <td>{{j.dateajout.format('Y-m-d')}}</td>*/
/*             <td>{{j.nbpointbase}}</td>*/
/*             <td>{{j.categorie}}</td>*/
/*             <td><a href="{{path('produit_delete',{'id':j.id})}}"> Supprimer </a></td>*/
/*  <td><a href="{{path('produit_update',{'id':j.id})}}"> Modifier </a></td>*/
/* */
/*         </tr>*/
/*         */
/*          */
/*        */
/*  */
/*          {% endfor %}*/
/*    */
/* */
/*        </table>*/
/*     </forum>*/
/* {% endblock %}*/
