<?php

/* PiDevClientBundle:Client:parametre.html.twig */
class __TwigTemplate_63167a376e1f7f5a30b8dc34a7846b261c6a7b0895ca080aa5898e160f175e1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Client:parametre.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <head>

        <script type=\"text/javascript\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/popup/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/popup/js/jquery.leanModal.min.js"), "html", null, true);
        echo "\"></script>
        <link rel=\"stylesheet\" href=\"http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" />
        <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/popup/css/style.css"), "html", null, true);
        echo "\" />
    </head>
    <body>
        <!-- BEGAIN PRELOADER -->   
        <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Paramétres</strong></h3>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading bg-red\">
                            <h3 class=\"panel-title\"><strong>Paramétres du </strong> Compte</h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 m-b-20\">

                                    <div class=\"btn-group pull-right\">
                                        <button class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">Tools <i class=\"fa fa-angle-down\"></i>
                                        </button>
                                        <ul class=\"dropdown-menu pull-right\">
                                            <li><a href=\"#\">Print</a>
                                            </li>
                                            <li><a href=\"#\">Save as PDF</a>
                                            </li>
                                            <li><a href=\"#\">Export to Excel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive table-red\">
                                    <table class=\"table table-striped table-hover dataTable\" id=\"table-editable\">

                                        <tbody>
                                            <tr>
                                                <td>Nom</td>
                                                <td>";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</td>                                          
                                                <td class=\"text-center\"><a class=\"edit btn btn-dark\" href=\"#modal\" id=\"modal_trigger\"><i class=\"fa fa-pencil-square-o\"></i>Edit</a> </td>

                                            </tr>
                                            <tr>
                                                <td>Mot de Passe</td>
                                                <td>********</td>                                          
                                                <td class=\"text-center\"><a class=\"edit btn btn-dark\" href=\"#ok\" id=\"test\"><i class=\"fa fa-pencil-square-o\"></i>Edit</a> </td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td>";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</td>                                          
                                                <td class=\"text-center\"><a class=\"edit btn btn-dark\" href=\"#modal\" id=\"test2\"><i class=\"fa fa-pencil-square-o\"></i>Edit</a> </td>
                                            </tr>
                                            <tr>
                                                <td><a href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("Client_Desactiver_compte");
        echo "\">Desactiver Compte</a></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- nom  -->                                  
                                    <div id=\"modal\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>
                                        <section class=\"popupBodny\">
                                            <!-- Username & Password Login form -->
                                            <div class=\"user_login\">
                                                ";
        // line 75
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "PiDevClientBundle:Client:parametre.html.twig", 75)->display($context);
        // line 76
        echo "                                            </div>
                                        </section>
                                    </div>
                                    <!-- mot de passe -->      
                                    <div id=\"oki\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>

                                        <section class=\"popupBodny\">
                                            <!-- Username & Password Login form -->
                                            <div class=\"user_login\">
                                                ";
        // line 89
        $this->loadTemplate("FOSUserBundle:ChangePassword:changePassword_content.html.twig", "PiDevClientBundle:Client:parametre.html.twig", 89)->display($context);
        // line 90
        echo "
                                            </div>
                                        </section>
                                    </div>
                                    <!--email -->      
                                    <div id=\"ok\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>

                                        <section class=\"popupBodny\">
                                            <!-- Register Form -->
                                            <div class=\"user_register\">
                                                ";
        // line 104
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "PiDevClientBundle:Client:parametre.html.twig", 104)->display($context);
        // line 105
        echo "                                            </div>
                                        </section>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>

        <script type=\"text/javascript\">
            \$(\"#test\").leanModal({top: 200, overlay: 0.6, closeButton: \".modal_close\"});

           
        </script>

        <script type=\"text/javascript\">
            \$(\"#modal_trigger\").leanModal({top: 200, overlay: 0.6, closeButton: \".modal_close\"});           
              </script>
        <script type=\"text/javascript\">
            \$(\"#test2\").leanModal({top: 200, overlay: 0.6, closeButton: \".modal_close\"});
        </script>

    </body>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:parametre.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 105,  158 => 104,  142 => 90,  140 => 89,  125 => 76,  123 => 75,  105 => 60,  98 => 56,  84 => 45,  44 => 8,  39 => 6,  35 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content  %}*/
/*     <head>*/
/* */
/*         <script type="text/javascript" src="{{asset('bundles/popup/js/jquery-1.11.0.min.js')}}"></script>*/
/*         <script type="text/javascript" src="{{asset('bundles/popup/js/jquery.leanModal.min.js')}}"></script>*/
/*         <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />*/
/*         <link type="text/css" rel="stylesheet" href="{{asset('bundles/popup/css/style.css')}}" />*/
/*     </head>*/
/*     <body>*/
/*         <!-- BEGAIN PRELOADER -->   */
/*         <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Paramétres</strong></h3>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading bg-red">*/
/*                             <h3 class="panel-title"><strong>Paramétres du </strong> Compte</h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 m-b-20">*/
/* */
/*                                     <div class="btn-group pull-right">*/
/*                                         <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>*/
/*                                         </button>*/
/*                                         <ul class="dropdown-menu pull-right">*/
/*                                             <li><a href="#">Print</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Save as PDF</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Export to Excel</a>*/
/*                                             </li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">*/
/*                                     <table class="table table-striped table-hover dataTable" id="table-editable">*/
/* */
/*                                         <tbody>*/
/*                                             <tr>*/
/*                                                 <td>Nom</td>*/
/*                                                 <td>{{user.username}}</td>                                          */
/*                                                 <td class="text-center"><a class="edit btn btn-dark" href="#modal" id="modal_trigger"><i class="fa fa-pencil-square-o"></i>Edit</a> </td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td>Mot de Passe</td>*/
/*                                                 <td>********</td>                                          */
/*                                                 <td class="text-center"><a class="edit btn btn-dark" href="#ok" id="test"><i class="fa fa-pencil-square-o"></i>Edit</a> </td>*/
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td>Email</td>*/
/*                                                 <td>{{user.email}}</td>                                          */
/*                                                 <td class="text-center"><a class="edit btn btn-dark" href="#modal" id="test2"><i class="fa fa-pencil-square-o"></i>Edit</a> </td>*/
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td><a href="{{path('Client_Desactiver_compte')}}">Desactiver Compte</a></td>*/
/*                                                 <td></td>*/
/*                                                 <td></td>*/
/*                                             </tr>*/
/*                                         </tbody>*/
/*                                     </table>*/
/*                                     <!-- nom  -->                                  */
/*                                     <div id="modal" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/*                                         <section class="popupBodny">*/
/*                                             <!-- Username & Password Login form -->*/
/*                                             <div class="user_login">*/
/*                                                 {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/*                                             </div>*/
/*                                         </section>*/
/*                                     </div>*/
/*                                     <!-- mot de passe -->      */
/*                                     <div id="oki" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/* */
/*                                         <section class="popupBodny">*/
/*                                             <!-- Username & Password Login form -->*/
/*                                             <div class="user_login">*/
/*                                                 {% include "FOSUserBundle:ChangePassword:changePassword_content.html.twig" %}*/
/* */
/*                                             </div>*/
/*                                         </section>*/
/*                                     </div>*/
/*                                     <!--email -->      */
/*                                     <div id="ok" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/* */
/*                                         <section class="popupBodny">*/
/*                                             <!-- Register Form -->*/
/*                                             <div class="user_register">*/
/*                                                 {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/*                                             </div>*/
/*                                         </section>*/
/*                                     </div>*/
/* */
/* */
/* */
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- END MAIN CONTENT -->*/
/*         </div>*/
/* */
/*         <script type="text/javascript">*/
/*             $("#test").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});*/
/* */
/*            */
/*         </script>*/
/* */
/*         <script type="text/javascript">*/
/*             $("#modal_trigger").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});           */
/*               </script>*/
/*         <script type="text/javascript">*/
/*             $("#test2").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});*/
/*         </script>*/
/* */
/*     </body>*/
/* {% endblock %}*/
