<?php

/* PiDevClientBundle:Appeldoffre:details.html.twig */
class __TwigTemplate_f114b39ad198d2c052ca5826e23395bb368d53594cc95b0fb50f452c48a2afd0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Appeldoffre:details.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<h1>details de l'Appels d'offre ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "nom", array()), "html", null, true);
        echo " </h1>

   

     
        <div>
        <th>Nom:";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "nom", array()), "html", null, true);
        echo "</th>
        <br>Description: <th>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "description", array()), "html", null, true);
        echo "</th</br>
        <br>Mail de contact: <th>";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "mail", array()), "html", null, true);
        echo "</th></br>
        <br>  Telephone:<th>";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "telephone", array()), "html", null, true);
        echo "</th</br>
        <br>  Adresse:<th>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")), "adresse", array()), "html", null, true);
        echo "</th</br>
        </div>
         ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Appeldoffre:details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 14,  53 => 13,  49 => 12,  45 => 11,  41 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* <h1>details de l'Appels d'offre {{mod.nom}} </h1>*/
/* */
/*    */
/* */
/*      */
/*         <div>*/
/*         <th>Nom:{{mod.nom}}</th>*/
/*         <br>Description: <th>{{mod.description}}</th</br>*/
/*         <br>Mail de contact: <th>{{mod.mail}}</th></br>*/
/*         <br>  Telephone:<th>{{mod.telephone}}</th</br>*/
/*         <br>  Adresse:<th>{{mod.adresse}}</th</br>*/
/*         </div>*/
/*          {% endblock %}*/
/*          */
/* */
