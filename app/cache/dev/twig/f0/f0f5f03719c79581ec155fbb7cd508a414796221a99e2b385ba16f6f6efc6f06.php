<?php

/* PiDevClientBundle:Produit:showall.html.twig */
class __TwigTemplate_4968600fb2bc54ce18158899c20e6ca5ef335c731490a3210b78af8f377b0bf1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:showall.html.twig", 1);
        $this->blocks = array(
            'slider' => array($this, 'block_slider'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_slider($context, array $blocks = array())
    {
        echo "   
<script src=";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/login.js"), "html", null, true);
        echo " ></script>
    <div id=\"fwslider\">
        <div class=\"slider_container\">
            <div class=\"slide\"> 
                <!-- Slide image -->
                <img src=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/banner.jpg"), "html", null, true);
        echo " alt=\"\"/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class=\"slide_content\">
                    <div class=\"slide_content_wrap\">
                        <!-- Text title -->
                        <h4 class=\"title\">Aluminium Club</h4>
                        <!-- /Text title -->

                        <!-- Text description -->
                        <p class=\"description\">Experiance ray ban</p>
                        <!-- /Text description -->
                    </div>
                </div>
                <!-- /Texts container -->
            </div>
            <!-- /Duplicate to create more slides -->
            <div class=\"slide\">
                <img src=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/banner1.jpg"), "html", null, true);
        echo " alt=\"\"/>
                <div class=\"slide_content\">
                    <div class=\"slide_content_wrap\">
                        <h4 class=\"title\">consectetuer adipiscing </h4>
                        <p class=\"description\">diam nonummy nibh euismod</p>
                    </div>
                </div>
            </div>
            <!--/slide -->
        </div>
        <div class=\"timers\"></div>
        <div class=\"slidePrev\"><span></span></div>
        <div class=\"slideNext\"><span></span></div>
    </div>

";
    }

    // line 42
    public function block_content($context, array $blocks = array())
    {
        // line 43
        echo "


    <div class=\"cont span_2_of_3\">
        <h2 class=\"head\">Liste des Produits</h2>
        <div>
            ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 50
            echo "                <tr ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                echo "class=\"color\"";
            }
            echo ">
                <div class=\"col_1_of_3 span_1_of_3\"> 
                    <a href=";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo ">
                        <div class=\"inner_content clearfix\">
                            <div class=\"product_image\">
                                <img class=\"etalage_thumb_image\" src=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\" alt=\"\" width=\"200px\" height=\"200px\">
                                                                  
                            </div>
                            <div class=\"sale-box\"><span class=\"on_sale title_shop\">New</span></div>\t
                            <div class=\"price\">
                                <div class=\"cart-left\">
                                    <p class=\"title\">";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nom", array()), "html", null, true);
            echo "</p>
                                    <div class=\"price1\">
                                        <span class=\"actual\">";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "prix", array()), "html", null, true);
            echo "</span>

                                    </div>
                                </div>
                            </div>\t
                        </div>
                    </a>
                    <div class=\"cart-right\">
                        <a href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("lignecommande_create", array("produit" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">.P.</a>

                    </div>  
                    <div class=\"cart-left\">
                        <a href=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_create", array("id" => $this->getAttribute((isset($context["favoris"]) ? $context["favoris"] : $this->getContext($context, "favoris")), "id", array()), "produit" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">
                            <span class=\"glyphicon glyphicon-star\" aria-hidden=\"true\"></span> .F. </a>                        

                    </div>  
                    <div class=\"clear\"></div>  
 
                </div>            
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo " 

        </div>     

    </div>

    <div class=\"clear\"></div>
    <div class=\"navigation\" align=\"center\">
        ";
        // line 90
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        echo " </div>
    <div class=\"clear\"></div>























    ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:showall.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 90,  179 => 82,  157 => 75,  150 => 71,  139 => 63,  134 => 61,  125 => 55,  119 => 52,  111 => 50,  94 => 49,  86 => 43,  83 => 42,  63 => 26,  42 => 8,  34 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block slider %}   */
/* <script src={{asset('js/login.js')}} ></script>*/
/*     <div id="fwslider">*/
/*         <div class="slider_container">*/
/*             <div class="slide"> */
/*                 <!-- Slide image -->*/
/*                 <img src={{asset('images/banner.jpg')}} alt=""/>*/
/*                 <!-- /Slide image -->*/
/*                 <!-- Texts container -->*/
/*                 <div class="slide_content">*/
/*                     <div class="slide_content_wrap">*/
/*                         <!-- Text title -->*/
/*                         <h4 class="title">Aluminium Club</h4>*/
/*                         <!-- /Text title -->*/
/* */
/*                         <!-- Text description -->*/
/*                         <p class="description">Experiance ray ban</p>*/
/*                         <!-- /Text description -->*/
/*                     </div>*/
/*                 </div>*/
/*                 <!-- /Texts container -->*/
/*             </div>*/
/*             <!-- /Duplicate to create more slides -->*/
/*             <div class="slide">*/
/*                 <img src={{asset('images/banner1.jpg')}} alt=""/>*/
/*                 <div class="slide_content">*/
/*                     <div class="slide_content_wrap">*/
/*                         <h4 class="title">consectetuer adipiscing </h4>*/
/*                         <p class="description">diam nonummy nibh euismod</p>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!--/slide -->*/
/*         </div>*/
/*         <div class="timers"></div>*/
/*         <div class="slidePrev"><span></span></div>*/
/*         <div class="slideNext"><span></span></div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
/* {% block content %}*/
/* */
/* */
/* */
/*     <div class="cont span_2_of_3">*/
/*         <h2 class="head">Liste des Produits</h2>*/
/*         <div>*/
/*             {% for entity in pagination %}*/
/*                 <tr {% if loop.index is odd %}class="color"{% endif %}>*/
/*                 <div class="col_1_of_3 span_1_of_3"> */
/*                     <a href={{ path('produit_detail', { 'id': entity.id }) }}>*/
/*                         <div class="inner_content clearfix">*/
/*                             <div class="product_image">*/
/*                                 <img class="etalage_thumb_image" src="{{path('my_image_route',{'id':entity.id})}}" alt="" width="200px" height="200px">*/
/*                                                                   */
/*                             </div>*/
/*                             <div class="sale-box"><span class="on_sale title_shop">New</span></div>	*/
/*                             <div class="price">*/
/*                                 <div class="cart-left">*/
/*                                     <p class="title">{{entity.nom}}</p>*/
/*                                     <div class="price1">*/
/*                                         <span class="actual">{{ entity.prix }}</span>*/
/* */
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>	*/
/*                         </div>*/
/*                     </a>*/
/*                     <div class="cart-right">*/
/*                         <a href="{{path('lignecommande_create',{'produit':entity.id})}}">.P.</a>*/
/* */
/*                     </div>  */
/*                     <div class="cart-left">*/
/*                         <a href="{{path('favoris_create',{'id':favoris.id,'produit':entity.id})}}">*/
/*                             <span class="glyphicon glyphicon-star" aria-hidden="true"></span> .F. </a>                        */
/* */
/*                     </div>  */
/*                     <div class="clear"></div>  */
/*  */
/*                 </div>            */
/*             {% endfor %} */
/* */
/*         </div>     */
/* */
/*     </div>*/
/* */
/*     <div class="clear"></div>*/
/*     <div class="navigation" align="center">*/
/*         {{ knp_pagination_render(pagination) }} </div>*/
/*     <div class="clear"></div>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*     {#*/
/*             <tbody>*/
/*                 <tr>*/
/*                       <th>Nom</th>*/
/*                       <th>Marque</th>*/
/*                       <th>Description</th>*/
/*                       <th>Prix</th>*/
/*                       <th>Quantite</th>*/
/*                       <th>Promotion</th>*/
/*                       <th>Points bonus</th>*/
/*                       */
/*                       {% for entity in entities %}*/
/*     */
/*                 <tr>*/
/*                   */
/*                     <td>{{ entity.nom }}</td>*/
/*                 */
/*                 */
/*                     */
/*                     <td>{{ entity.marque }}</td>*/
/*                */
/*                     */
/*                     <td>{{ entity.description }}</td>*/
/*                */
/*                     */
/*                     <td>{{ entity.prix }}</td>*/
/*                */
/*                    */
/*                     <td>{{ entity.quantite }}</td>*/
/*                */
/*                     */
/*                     <td>{{ entity.promotion }} % </td>*/
/*                */
/*                     */
/*                     <td>{{ entity.nbrpoint }}</td>*/
/*                    */
/*                 */
/*                     <td><form action="{{path('favoris_create',{'id':favoris.id,'produit':entity.id})}}" method="post" >*/
/*         <input type="submit" value="Favoris"/>*/
/*                         </form></td>*/
/*     <td><form action="{{path('panier_create',{'id':panier.id,'produit':entity.id})}}" method="post" >*/
/*         <input type="submit" value="Panier"/>*/
/*     </form>*/
/*                        */
/*     */
/*                     </td>*/
/*     */
/*                      */
/*             </tr>*/
/*                  {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     #}*/
/* {% endblock %}*/
/* */
