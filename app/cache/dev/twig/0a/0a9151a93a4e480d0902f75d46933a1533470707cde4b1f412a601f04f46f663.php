<?php

/* PiDevClientBundle:image:upload.html.twig */
class __TwigTemplate_85e86acff9325ad84cabe2d2889fc02de3d3a7b5995f1b6d538cca5e84bffc7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<h1>Upload File</h1>

<form action=\"#\" method=\"post\" ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo ">

 ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "

 <input type=\"submit\" value=\"Upload Document\" />
 

</form>";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:image:upload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 6,  23 => 4,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <h1>Upload File</h1>*/
/* */
/* <form action="#" method="post" {{ form_enctype(form) }}>*/
/* */
/*  {{ form_widget(form) }}*/
/* */
/*  <input type="submit" value="Upload Document" />*/
/*  */
/* */
/* </form>*/
