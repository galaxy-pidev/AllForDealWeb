<?php

/* PiDevAdminBundle:Default:index.html.twig */
class __TwigTemplate_f5098bfae1afa4cc7debb9fd2fb01f5681a866eb4b6601e27db985a41b5465dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PiDevAdminBundle::baseAdmin.html.twig", "PiDevAdminBundle:Default:index.html.twig", 1);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "PiDevAdminBundle::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 1,);
    }
}
/* {% extends "PiDevAdminBundle::baseAdmin.html.twig" %}*/
/* */
