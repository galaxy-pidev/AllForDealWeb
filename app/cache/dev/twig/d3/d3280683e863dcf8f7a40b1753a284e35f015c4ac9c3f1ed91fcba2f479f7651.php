<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_51d86a08d754c2560045dcfda85a04713bea50573fd901f3a5350a936fd9ca41 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en-IN\">
<head>
<meta charset=\"utf-8\">
<title></title>
<link href=";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<link href='http://fonts.googleapis.com/css?family=Ropa+Sans' rel='stylesheet'>
<link href=\"http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel='stylesheet'>
<link href=";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/login/css/login.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
</head>
<body>
<div id=\"login-form\">

<input type=\"radio\" checked id=\"login\" name=\"switch\" class=\"hide\">
<input type=\"radio\" id=\"signup\" name=\"switch\" class=\"hide\">

<div>
<ul class=\"form-header\">
<li><label for=\"login\"><i class=\"fa fa-lock\"></i> LOGIN<label for=\"login\"></li>
<li><label for=\"signup\"><i class=\"fa fa-credit-card\"></i> REGISTER</label></li>
</ul>
</div>

<div class=\"section-out\">
<section class=\"login-section\">
<div class=\"login\">
<form action=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
<ul class=\"ul-list\">

<li><input type=\"text\" class=\"input\" id=\"username\" name=\"_username\" value=\"\" required=\"required\" /><span class=\"icon\"><i class=\"fa fa-user\"></i></span></li>
<li><input class=\"input\" type=\"password\" id=\"modlgn_passwd\" name=\"_password\" required=\"required\" value=\"Password\"/><span class=\"icon\"><i class=\"fa fa-lock\"></i></span></li></li>
\t<li><span class=\"remember\"><input type=\"checkbox\" id=\"check\"> <label for=\"check\">Remember Me</label></span><span class=\"remember\"><a href=\"\">Forget Password</a></span></li>
<li><input type=\"submit\" value=\"SIGN IN\" class=\"btn\"></li>
</ul>
</form>
</div>

<div class=\"social-login\">Or sign in with &nbsp;
<a href=\"\" class=\"fb\"><i class=\"fa fa-facebook\"></i></a>
<a href=\"\" class=\"tw\"><i class=\"fa fa-twitter\"></i></a>
<a href=\"\" class=\"gp\"><i class=\"fa fa-google-plus\"></i></a>
<a href=\"\" class=\"in\"><i class=\"fa fa-linkedin\"></i></a>
</div>
</section>

<section class=\"signup-section\">
<div class=\"login\">
<form action=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " method=\"POST\" >
<ul class=\"ul-list\">
     <li>   ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "input", "placeholder" => $this->env->getExtension('translator')->trans("form.email"))));
        echo "<span class=\"icon\"><i class=\"fa fa-envelope\"></i></span></li>
        ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "

     <li>   ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("class" => "input", "placeholder" => $this->env->getExtension('translator')->trans("form.username"))));
        echo "<span class=\"icon\"><i class=\"fa fa-user\"></i></span></li>
        ";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'errors');
        echo "

      <li>  ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget', array("attr" => array("class" => "input", "placeholder" => $this->env->getExtension('translator')->trans("form.password"))));
        echo "<span class=\"icon\"><i class=\"fa fa-lock\"></i></span></li>
        ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'errors');
        echo "

     <li>   ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget', array("attr" => array("class" => "input", "placeholder" => $this->env->getExtension('translator')->trans("form.password_confirmation"))));
        echo "<span class=\"icon\"><i class=\"fa fa-lock\"></i></span></li>
        ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'errors');
        echo "
     <li>   ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'widget');
        echo "</li>
        ";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'errors');
        echo "
     
     ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
        
<li><input type=\"checkbox\" id=\"check1\"> <label for=\"check1\">I accept terms and conditions</label></li>
<li>
      <input type=\"submit\" value=\"SIGN UP\" class=\"btn\"></li>
</ul>
</form>
</div>

<div class=\"social-login\">Or sign up with &nbsp;
<a href=\"\" class=\"fb\"><i class=\"fa fa-facebook\"></i></a>
<a href=\"\" class=\"tw\"><i class=\"fa fa-twitter\"></i></a>
<a href=\"\" class=\"gp\"><i class=\"fa fa-google-plus\"></i></a>
<a href=\"\" class=\"in\"><i class=\"fa fa-linkedin\"></i></a>
</div>
</section>
</div>

</div>

</body>
</html>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 64,  123 => 62,  119 => 61,  115 => 60,  111 => 59,  106 => 57,  102 => 56,  97 => 54,  93 => 53,  88 => 51,  84 => 50,  77 => 48,  53 => 27,  32 => 9,  26 => 6,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en-IN">*/
/* <head>*/
/* <meta charset="utf-8">*/
/* <title></title>*/
/* <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/* <link href='http://fonts.googleapis.com/css?family=Ropa+Sans' rel='stylesheet'>*/
/* <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel='stylesheet'>*/
/* <link href={{asset('bundles/login/css/login.css')}} rel="stylesheet" type="text/css" media="all" />*/
/* </head>*/
/* <body>*/
/* <div id="login-form">*/
/* */
/* <input type="radio" checked id="login" name="switch" class="hide">*/
/* <input type="radio" id="signup" name="switch" class="hide">*/
/* */
/* <div>*/
/* <ul class="form-header">*/
/* <li><label for="login"><i class="fa fa-lock"></i> LOGIN<label for="login"></li>*/
/* <li><label for="signup"><i class="fa fa-credit-card"></i> REGISTER</label></li>*/
/* </ul>*/
/* </div>*/
/* */
/* <div class="section-out">*/
/* <section class="login-section">*/
/* <div class="login">*/
/* <form action="{{ path("fos_user_security_check") }}" method="post">*/
/* <ul class="ul-list">*/
/* */
/* <li><input type="text" class="input" id="username" name="_username" value="" required="required" /><span class="icon"><i class="fa fa-user"></i></span></li>*/
/* <li><input class="input" type="password" id="modlgn_passwd" name="_password" required="required" value="Password"/><span class="icon"><i class="fa fa-lock"></i></span></li></li>*/
/* 	<li><span class="remember"><input type="checkbox" id="check"> <label for="check">Remember Me</label></span><span class="remember"><a href="">Forget Password</a></span></li>*/
/* <li><input type="submit" value="SIGN IN" class="btn"></li>*/
/* </ul>*/
/* </form>*/
/* </div>*/
/* */
/* <div class="social-login">Or sign in with &nbsp;*/
/* <a href="" class="fb"><i class="fa fa-facebook"></i></a>*/
/* <a href="" class="tw"><i class="fa fa-twitter"></i></a>*/
/* <a href="" class="gp"><i class="fa fa-google-plus"></i></a>*/
/* <a href="" class="in"><i class="fa fa-linkedin"></i></a>*/
/* </div>*/
/* </section>*/
/* */
/* <section class="signup-section">*/
/* <div class="login">*/
/* <form action="{{ path('fos_user_registration_register') }}" {{ form_enctype(form) }} method="POST" >*/
/* <ul class="ul-list">*/
/*      <li>   {{ form_widget(form.email, { 'attr': {'class': 'input', 'placeholder': 'form.email'|trans } }) }}<span class="icon"><i class="fa fa-envelope"></i></span></li>*/
/*         {{ form_errors(form.email) }}*/
/* */
/*      <li>   {{ form_widget(form.username, { 'attr': {'class': 'input', 'placeholder': 'form.username'|trans } }) }}<span class="icon"><i class="fa fa-user"></i></span></li>*/
/*         {{ form_errors(form.username) }}*/
/* */
/*       <li>  {{ form_widget(form.plainPassword.first, { 'attr': {'class': 'input', 'placeholder': 'form.password'|trans } }) }}<span class="icon"><i class="fa fa-lock"></i></span></li>*/
/*         {{ form_errors(form.plainPassword.first) }}*/
/* */
/*      <li>   {{ form_widget(form.plainPassword.second, { 'attr': {'class': 'input', 'placeholder': 'form.password_confirmation'|trans } }) }}<span class="icon"><i class="fa fa-lock"></i></span></li>*/
/*         {{ form_errors(form.plainPassword.second) }}*/
/*      <li>   {{ form_widget(form.file)}}</li>*/
/*         {{ form_errors(form.file) }}*/
/*      */
/*      {{ form_rest(form) }}*/
/*         */
/* <li><input type="checkbox" id="check1"> <label for="check1">I accept terms and conditions</label></li>*/
/* <li>*/
/*       <input type="submit" value="SIGN UP" class="btn"></li>*/
/* </ul>*/
/* </form>*/
/* </div>*/
/* */
/* <div class="social-login">Or sign up with &nbsp;*/
/* <a href="" class="fb"><i class="fa fa-facebook"></i></a>*/
/* <a href="" class="tw"><i class="fa fa-twitter"></i></a>*/
/* <a href="" class="gp"><i class="fa fa-google-plus"></i></a>*/
/* <a href="" class="in"><i class="fa fa-linkedin"></i></a>*/
/* </div>*/
/* </section>*/
/* </div>*/
/* */
/* </div>*/
/* */
/* </body>*/
/* </html>*/
