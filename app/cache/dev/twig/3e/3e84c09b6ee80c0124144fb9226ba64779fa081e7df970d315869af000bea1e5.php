<?php

/* PiDevClientBundle:Appeldoffre:modifier.html.twig */
class __TwigTemplate_0fe6ec50ad4d47dedbc8900f6f4eeb965f8d3de96835634040f434c016d9fd3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Appeldoffre:modifier.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<h1>Modifier Mon Appel d'offre </h1>
<form action=\"\" method=\"post\" >
    <table> 
    <tr>
        <td>nom: </td>
        <td>";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget');
        echo "</td>  
    </tr>
    <tr>  
    <td>   Description:</td>
    <td>";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Description", array()), 'widget');
        echo "</td>
    </tr>
    <tr>  
    <td>   Mail:</td>
    <td>";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Mail", array()), 'widget');
        echo "</td>
    </tr>
    <tr>  
    <td>   Telephone:</td>
    <td>";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Telephone", array()), 'widget');
        echo "</td>
    </tr>
    <tr>  
    <td>   Description:</td>
    <td>";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Adresse", array()), 'widget');
        echo "</td>
    </tr>
</table>
    <br>
  
";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
</form>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Appeldoffre:modifier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 31,  67 => 26,  60 => 22,  53 => 18,  46 => 14,  39 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* */
/* <h1>Modifier Mon Appel d'offre </h1>*/
/* <form action="" method="post" >*/
/*     <table> */
/*     <tr>*/
/*         <td>nom: </td>*/
/*         <td>{{ form_widget(form.nom)}}</td>  */
/*     </tr>*/
/*     <tr>  */
/*     <td>   Description:</td>*/
/*     <td>{{ form_widget(form.Description)}}</td>*/
/*     </tr>*/
/*     <tr>  */
/*     <td>   Mail:</td>*/
/*     <td>{{ form_widget(form.Mail)}}</td>*/
/*     </tr>*/
/*     <tr>  */
/*     <td>   Telephone:</td>*/
/*     <td>{{ form_widget(form.Telephone)}}</td>*/
/*     </tr>*/
/*     <tr>  */
/*     <td>   Description:</td>*/
/*     <td>{{ form_widget(form.Adresse)}}</td>*/
/*     </tr>*/
/* </table>*/
/*     <br>*/
/*   */
/* {{form_rest(form)}}*/
/* </form>*/
/*  {% endblock %}*/
/* */
