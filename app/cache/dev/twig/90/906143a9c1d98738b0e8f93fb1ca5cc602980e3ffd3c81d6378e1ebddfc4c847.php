<?php

/* PiDevAdminBundle:Categorie:ajout.html_1.twig */
class __TwigTemplate_b8d8c08ec49a4abc49da1de3fa67bd0683985bc85e2033ff27a1b89398b6f425 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        echo "<form method='post'>
<table>
    
     <tr><td>Type:</td><td>";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "id", array()), 'widget');
        echo "</td></tr>
    <tr><td>Description:</td><td>";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Description", array()), 'widget');
        echo "</td></tr>
</table>
      ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo "
</form>";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Categorie:ajout.html_1.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 9,  28 => 7,  24 => 6,  19 => 3,);
    }
}
/* {# empty Twig template #}*/
/* {# empty Twig template #}*/
/* <form method='post'>*/
/* <table>*/
/*     */
/*      <tr><td>Type:</td><td>{{form_widget(f.id)}}</td></tr>*/
/*     <tr><td>Description:</td><td>{{form_widget(f.Description)}}</td></tr>*/
/* </table>*/
/*       {{form_rest(f)}}*/
/* </form>*/
