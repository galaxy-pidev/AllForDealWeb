<?php

/* PiDevClientBundle:Adresse:map.html.twig */
class __TwigTemplate_e8c2bd4595161d86129da80b32718fb60cee25ea1b7fe54ce22de9e4125aae67 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PiDevClientBundle:Adresse:localiser.html.twig", "PiDevClientBundle:Adresse:map.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PiDevClientBundle:Adresse:localiser.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        // line 3
        echo "
<div id=\"googleMap\" style=\"width:500px; height:380px;\"></div>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Adresse:map.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "PiDevClientBundle:Adresse:localiser.html.twig" %}*/
/* {% block container %}*/
/* */
/* <div id="googleMap" style="width:500px; height:380px;"></div>*/
/* {% endblock %}*/
