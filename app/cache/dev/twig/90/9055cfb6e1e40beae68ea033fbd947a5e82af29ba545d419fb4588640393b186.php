<?php

/* PiDevClientBundle:Commande:showCommandelist.html.twig */
class __TwigTemplate_7f9e935fdf4997a415bc71b66a99f1f49bbbc7801ca7b0c2afacdbe7c67a5ebd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Commande:showCommandelist.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
    <h1>Vos Commande </h1>

    <table class=\"record_properties\">
        <tbody>
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 9
            echo "                <tr>
                    <th>Commande</th>
                    <td>";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "id", array()), "html", null, true);
            echo "</td>


                    <td>   


                        <a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("facture_commande", array("id" => $this->getAttribute($context["m"], "id", array()))), "html", null, true);
            echo "\"> generer facture </a>

                    </td>    
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "            </tr>
        </tbody>
    </table>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commande:showCommandelist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 21,  55 => 17,  46 => 11,  42 => 9,  38 => 8,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* */
/*     <h1>Vos Commande </h1>*/
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*             {% for m in entities %}*/
/*                 <tr>*/
/*                     <th>Commande</th>*/
/*                     <td>{{ m.id}}</td>*/
/* */
/* */
/*                     <td>   */
/* */
/* */
/*                         <a href="{{path('facture_commande',{'id':m.id})}}"> generer facture </a>*/
/* */
/*                     </td>    */
/*                 {% endfor %}*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* {% endblock %}*/
/* */
/* */
