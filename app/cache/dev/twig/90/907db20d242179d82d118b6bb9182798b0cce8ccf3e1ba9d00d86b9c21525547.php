<?php

/* PiDevClientBundle:Appeldoffre:recherche.html.twig */
class __TwigTemplate_f60f8623995c745367870fa72a7af51a946a2726f5b638548efb77e23ce48a5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Appeldoffre:recherche.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Listes des Appels d'offre </h1>
<form method=\"POST\"  action=\"\">
    Rechercher Un Appel d'offre! : <input type=\"text\" name=\"search\"/>
    <input type=\"submit\" value=\"chercher\"/>
</form>

    
    ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modeles"]) ? $context["modeles"] : $this->getContext($context, "modeles")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 12
            echo "        <div>
        <th>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</th>
        
      <a href=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("a", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">Details</a>
        </div>
         
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "     ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Appeldoffre:recherche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 20,  56 => 16,  51 => 14,  47 => 13,  44 => 12,  40 => 11,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* <h1>Listes des Appels d'offre </h1>*/
/* <form method="POST"  action="">*/
/*     Rechercher Un Appel d'offre! : <input type="text" name="search"/>*/
/*     <input type="submit" value="chercher"/>*/
/* </form>*/
/* */
/*     */
/*     {% for i in modeles %}*/
/*         <div>*/
/*         <th>{{i.nom}}</th>*/
/*         <th>{{i.description}}</th>*/
/*         */
/*       <a href="{{path("a",{'id':i.id})}}">Details</a>*/
/*         </div>*/
/*          */
/*     {% endfor %}*/
/*      {% endblock %}*/
/* */
