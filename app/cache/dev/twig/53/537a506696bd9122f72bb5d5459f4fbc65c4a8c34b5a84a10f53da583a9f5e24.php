<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_5d5308b69ba278895d6e942b585bd3eaa1cb722e1dd039d3cb8f5dad0e2cb7fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 4
        echo "<html lang=\"en-IN\">
<head>
<meta charset=\"utf-8\">
<title></title>
<link href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<link href='http://fonts.googleapis.com/css?family=Ropa+Sans' rel='stylesheet'>
<link href=\"http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel='stylesheet'>
<link href=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/login/css/login.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
</head>
<body>
<div id=\"login-form\">

<input type=\"radio\" checked id=\"login\" name=\"switch\" class=\"hide\">
<input type=\"radio\" id=\"signup\" name=\"switch\" class=\"hide\">

<div>
<ul class=\"form-header\">
<li><label for=\"login\"><i class=\"fa fa-lock\"></i> LOGIN<label for=\"login\"></li>
<li><label for=\"signup\"><i class=\"fa fa-credit-card\"></i> REGISTER</label></li>
</ul>
</div>

<div class=\"section-out\">
<section class=\"login-section\">
<div class=\"login\">
<form action=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
<ul class=\"ul-list\">

<li><input type=\"text\" class=\"input\" id=\"username\" name=\"_username\" value=\"\" required=\"required\" /><span class=\"icon\"><i class=\"fa fa-user\"></i></span></li>
<li><input class=\"input\" type=\"password\" id=\"modlgn_passwd\" name=\"_password\" required=\"required\" value=\"Password\"/><span class=\"icon\"><i class=\"fa fa-lock\"></i></span></li></li>
\t<li><span class=\"remember\"><input type=\"checkbox\" id=\"check\"> <label for=\"check\">Remember Me</label></span><span class=\"remember\"><a href=\"\">Forget Password</a></span></li>
<li><input type=\"submit\" value=\"SIGN IN\" class=\"btn\"></li>
</ul>
</form>
</div>

<div class=\"social-login\">Or sign in with &nbsp;
<a href=\"\" class=\"fb\"><i class=\"fa fa-facebook\"></i></a>
<a href=\"\" class=\"tw\"><i class=\"fa fa-twitter\"></i></a>
<a href=\"\" class=\"gp\"><i class=\"fa fa-google-plus\"></i></a>
<a href=\"\" class=\"in\"><i class=\"fa fa-linkedin\"></i></a>
</div>
</section>

<section class=\"signup-section\">
<div class=\"login\">
<form action=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " method=\"POST\" >
<ul class=\"ul-list\">
     <li>   ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "input", "placeholder" => $this->env->getExtension('translator')->trans("form.email"))));
        echo "<span class=\"icon\"><i class=\"fa fa-envelope\"></i></span></li>
        ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "

     <li>   ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("class" => "input", "placeholder" => $this->env->getExtension('translator')->trans("form.username"))));
        echo "<span class=\"icon\"><i class=\"fa fa-user\"></i></span></li>
        ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'errors');
        echo "

      <li>  ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget', array("attr" => array("class" => "input", "placeholder" => $this->env->getExtension('translator')->trans("form.password"))));
        echo "<span class=\"icon\"><i class=\"fa fa-lock\"></i></span></li>
        ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'errors');
        echo "

     <li>   ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget', array("attr" => array("class" => "input", "placeholder" => $this->env->getExtension('translator')->trans("form.password_confirmation"))));
        echo "<span class=\"icon\"><i class=\"fa fa-lock\"></i></span></li>
        ";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'errors');
        echo "
     <li>   ";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'widget');
        echo "</li>
        ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'errors');
        echo "
     
     ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
        
<li><input type=\"checkbox\" id=\"check1\"> <label for=\"check1\">I accept terms and conditions</label></li>
<li>
      <input type=\"submit\" value=\"SIGN UP\" class=\"btn\"></li>
</ul>
</form>
</div>

<div class=\"social-login\">Or sign up with &nbsp;
<a href=\"\" class=\"fb\"><i class=\"fa fa-facebook\"></i></a>
<a href=\"\" class=\"tw\"><i class=\"fa fa-twitter\"></i></a>
<a href=\"\" class=\"gp\"><i class=\"fa fa-google-plus\"></i></a>
<a href=\"\" class=\"in\"><i class=\"fa fa-linkedin\"></i></a>
</div>
</section>
</div>

</div>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 66,  134 => 64,  130 => 63,  126 => 62,  122 => 61,  117 => 59,  113 => 58,  108 => 56,  104 => 55,  99 => 53,  95 => 52,  88 => 50,  64 => 29,  43 => 11,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* <html lang="en-IN">*/
/* <head>*/
/* <meta charset="utf-8">*/
/* <title></title>*/
/* <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/* <link href='http://fonts.googleapis.com/css?family=Ropa+Sans' rel='stylesheet'>*/
/* <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel='stylesheet'>*/
/* <link href={{asset('bundles/login/css/login.css')}} rel="stylesheet" type="text/css" media="all" />*/
/* </head>*/
/* <body>*/
/* <div id="login-form">*/
/* */
/* <input type="radio" checked id="login" name="switch" class="hide">*/
/* <input type="radio" id="signup" name="switch" class="hide">*/
/* */
/* <div>*/
/* <ul class="form-header">*/
/* <li><label for="login"><i class="fa fa-lock"></i> LOGIN<label for="login"></li>*/
/* <li><label for="signup"><i class="fa fa-credit-card"></i> REGISTER</label></li>*/
/* </ul>*/
/* </div>*/
/* */
/* <div class="section-out">*/
/* <section class="login-section">*/
/* <div class="login">*/
/* <form action="{{ path("fos_user_security_check") }}" method="post">*/
/* <ul class="ul-list">*/
/* */
/* <li><input type="text" class="input" id="username" name="_username" value="" required="required" /><span class="icon"><i class="fa fa-user"></i></span></li>*/
/* <li><input class="input" type="password" id="modlgn_passwd" name="_password" required="required" value="Password"/><span class="icon"><i class="fa fa-lock"></i></span></li></li>*/
/* 	<li><span class="remember"><input type="checkbox" id="check"> <label for="check">Remember Me</label></span><span class="remember"><a href="">Forget Password</a></span></li>*/
/* <li><input type="submit" value="SIGN IN" class="btn"></li>*/
/* </ul>*/
/* </form>*/
/* </div>*/
/* */
/* <div class="social-login">Or sign in with &nbsp;*/
/* <a href="" class="fb"><i class="fa fa-facebook"></i></a>*/
/* <a href="" class="tw"><i class="fa fa-twitter"></i></a>*/
/* <a href="" class="gp"><i class="fa fa-google-plus"></i></a>*/
/* <a href="" class="in"><i class="fa fa-linkedin"></i></a>*/
/* </div>*/
/* </section>*/
/* */
/* <section class="signup-section">*/
/* <div class="login">*/
/* <form action="{{ path('fos_user_registration_register') }}" {{ form_enctype(form) }} method="POST" >*/
/* <ul class="ul-list">*/
/*      <li>   {{ form_widget(form.email, { 'attr': {'class': 'input', 'placeholder': 'form.email'|trans } }) }}<span class="icon"><i class="fa fa-envelope"></i></span></li>*/
/*         {{ form_errors(form.email) }}*/
/* */
/*      <li>   {{ form_widget(form.username, { 'attr': {'class': 'input', 'placeholder': 'form.username'|trans } }) }}<span class="icon"><i class="fa fa-user"></i></span></li>*/
/*         {{ form_errors(form.username) }}*/
/* */
/*       <li>  {{ form_widget(form.plainPassword.first, { 'attr': {'class': 'input', 'placeholder': 'form.password'|trans } }) }}<span class="icon"><i class="fa fa-lock"></i></span></li>*/
/*         {{ form_errors(form.plainPassword.first) }}*/
/* */
/*      <li>   {{ form_widget(form.plainPassword.second, { 'attr': {'class': 'input', 'placeholder': 'form.password_confirmation'|trans } }) }}<span class="icon"><i class="fa fa-lock"></i></span></li>*/
/*         {{ form_errors(form.plainPassword.second) }}*/
/*      <li>   {{ form_widget(form.file)}}</li>*/
/*         {{ form_errors(form.file) }}*/
/*      */
/*      {{ form_rest(form) }}*/
/*         */
/* <li><input type="checkbox" id="check1"> <label for="check1">I accept terms and conditions</label></li>*/
/* <li>*/
/*       <input type="submit" value="SIGN UP" class="btn"></li>*/
/* </ul>*/
/* </form>*/
/* </div>*/
/* */
/* <div class="social-login">Or sign up with &nbsp;*/
/* <a href="" class="fb"><i class="fa fa-facebook"></i></a>*/
/* <a href="" class="tw"><i class="fa fa-twitter"></i></a>*/
/* <a href="" class="gp"><i class="fa fa-google-plus"></i></a>*/
/* <a href="" class="in"><i class="fa fa-linkedin"></i></a>*/
/* </div>*/
/* </section>*/
/* </div>*/
/* */
/* </div>*/
/* */
/* </body>*/
/* </html>*/
/* {% endblock %}*/
