<?php

/* PiDevClientBundle:Sujet:new.html.twig */
class __TwigTemplate_711d04b7aa44af6041674bfc8e4fe0dd344f1471ff4f993f916e982e107d09aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Sujet:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <script src=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/ckeditor/ckeditor.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>
</head>
<div id=\"main-content\" class=\"send-message\">
    <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
        <h3><strong>Nouveau</strong> Sujet</h3>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <div class=\"border-bottom\">
                                <div class=\"pull-left\">
                                    <a href=\"#\" class=\" btn btn-default\"><i class=\"fa fa-arrow-left fa-fw\"></i> back</a> 
                                </div>

                                <div class=\"clearfix m-b-20\"></div>
                            </div>

                            <form class=\"form-horizontal p-t-20\" method=\"POST\" action='";
        // line 23
        echo $this->env->getExtension('routing')->getPath("sujet_create");
        echo "'  ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo ">

                                <div class=\"form-group\">
                                    <label for=\"to\" class=\"col-sm-2 control-label\">Sujet:</label>
                                    <div class=\"col-sm-10\">
                                        <div class=\"input-group\">
                                            ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre", array()), 'widget');
        echo "
                                            <span class=\"input-group-addon bg-white cursor-pointer\" data-toggle=\"collapse\" data-target=\"#cc\">     
                                                Subject <span class=\"caret\"></span>

                                        </div>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label for=\"to\" class=\"col-sm-2 control-label\">Image:</label>
                                    <div class=\"col-sm-10\">
                                        <div class=\"input-group\">
                                            ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'widget');
        echo "                     
                                        </div>
                                    </div>
                                </div>


                                <div class=\"clearfix\"></div>


                                <div>
                                    ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "sujet", array()), 'widget');
        echo "
                                </div>
                                <div class=\"text-center footer-message\">

                                    <a class=\"btn btn-default\"><i class=\"fa fa-times-circle\"></i> Cancel</a>

                                    ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'widget');
        echo "
                                    <input type=\"submit\" class=\"btn btn-primary fa fa-times-circle\" value=\"Enregistrer\" action=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("sujet_create");
        echo "\">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Sujet:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 57,  102 => 56,  93 => 50,  80 => 40,  66 => 29,  55 => 23,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/*     <script src={{asset('bundles/ckeditor/ckeditor.js')}} type="text/javascript"></script>*/
/* </head>*/
/* <div id="main-content" class="send-message">*/
/*     <div class="page-title"> <i class="icon-custom-left"></i>*/
/*         <h3><strong>Nouveau</strong> Sujet</h3>*/
/*     </div>*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-body">*/
/*                     <div class="row">*/
/*                         <div class="col-md-12">*/
/*                             <div class="border-bottom">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="#" class=" btn btn-default"><i class="fa fa-arrow-left fa-fw"></i> back</a> */
/*                                 </div>*/
/* */
/*                                 <div class="clearfix m-b-20"></div>*/
/*                             </div>*/
/* */
/*                             <form class="form-horizontal p-t-20" method="POST" action='{{path('sujet_create')}}'  {{ form_enctype(form) }}>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label for="to" class="col-sm-2 control-label">Sujet:</label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <div class="input-group">*/
/*                                             {{ form_widget(form.titre)}}*/
/*                                             <span class="input-group-addon bg-white cursor-pointer" data-toggle="collapse" data-target="#cc">     */
/*                                                 Subject <span class="caret"></span>*/
/* */
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label for="to" class="col-sm-2 control-label">Image:</label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <div class="input-group">*/
/*                                             {{ form_widget(form.file)}}                     */
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/* */
/*                                 <div class="clearfix"></div>*/
/* */
/* */
/*                                 <div>*/
/*                                     {{ form_widget(form.sujet)}}*/
/*                                 </div>*/
/*                                 <div class="text-center footer-message">*/
/* */
/*                                     <a class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>*/
/* */
/*                                     {{ form_widget(form._token) }}*/
/*                                     <input type="submit" class="btn btn-primary fa fa-times-circle" value="Enregistrer" action="{{path('sujet_create')}}">*/
/*                                 </div>*/
/*                             </form>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* {% endblock %}*/
/* */
