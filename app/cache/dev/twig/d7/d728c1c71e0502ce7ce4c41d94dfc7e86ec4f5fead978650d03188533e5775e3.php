<?php

/* PiDevClientBundle:Client:affiche.html.twig */
class __TwigTemplate_70d8a2c31e708e5273ee43c17de5cec7da35b09f064f0ff0dbba5e6bda026ede extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<div>

<img src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
        echo "\"/>

</div>";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:affiche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 4,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <div>*/
/* */
/* <img src="{{asset(path('image'))}}"/>*/
/* */
/* </div>*/
