<?php

/* PiDevClientBundle:Produit:edit.html.twig */
class __TwigTemplate_82e855695a4b6b0c8d9e68be6a5896f6ac5ea2ab0585a9cdcf8108a7df0c11c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 8
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:edit.html.twig", 8);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "            <h1>Produit</h1>

            ";
        // line 12
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form');
        echo "

            <ul class=\"record_actions\">
                <li>
                    <a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("produit");
        echo "\">
                        Back to the list
                    </a>
                </li>
                <li>";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</li>
            </ul>
        ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 20,  42 => 16,  35 => 12,  31 => 10,  28 => 9,  11 => 8,);
    }
}
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*         {% extends '::base.html.twig' %}*/
/*         {% block content %}*/
/*             <h1>Produit</h1>*/
/* */
/*             {{ form(edit_form) }}*/
/* */
/*             <ul class="record_actions">*/
/*                 <li>*/
/*                     <a href="{{ path('produit') }}">*/
/*                         Back to the list*/
/*                     </a>*/
/*                 </li>*/
/*                 <li>{{ form(delete_form) }}</li>*/
/*             </ul>*/
/*         {% endblock %}*/
