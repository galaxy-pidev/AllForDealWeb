<?php

/* PiDevClientBundle::base.html.twig */
class __TwigTemplate_d72888aaf606f5f6dc795a87d510706836e53092f96ee0f12e6209d6344fb1a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'slider' => array($this, 'block_slider'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>
    <head>
        <title>All For Deal | Accueil </title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <link href=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
        <script type=\"text/javascript\" src=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
        <!-- start menu -->
        <link href=";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <script type=\"text/javascript\" src=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
        <script  src=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
        <!--start slider -->
        <link rel=\"stylesheet\" href=";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
        <script src=";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
        <script src=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
        <!--end slider -->
        <script src=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>
    </head>
    ";
        // line 23
        $this->displayBlock('header', $context, $blocks);
        // line 24
        echo "        <form method=\"post\"> 
        <div class=\"header-top\">
            <div class=\"wrap\"> 
                <div class=\"header-top-left\">
                 
                    <div class=\"clear\"></div>
                </div>
                <div class=\"cssmenu\">    
                    <ul>
                        ";
        // line 33
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            echo "  
                        <li class=\"color4\"><a href=\"";
            // line 34
            echo $this->env->getExtension('routing')->getPath("Profile");
            echo "\">Compte</a> </li> |                         
                        <li><a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_show", array("id" => $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "favoris", array()), "id", array()))), "html", null, true);
            echo "\">Favoris</a></li> |
                        <li><a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_show", array("id" => $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "panier", array()), "id", array()))), "html", null, true);
            echo "\">Panier</a></li> |
                        <li><a href=\"";
            // line 37
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">Se déconnecter</a></li> |
                       
                        ";
        } else {
            // line 40
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\">Se connecter</a></li> |
                        <li><a href=\"";
            // line 41
            echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
            echo "\">S'inscrire</a></li>
                        ";
        }
        // line 43
        echo "                        
                    </ul>
                </div>
                <div class=\"clear\"></div>
            </div>
        </div>
        <div class=\"header-bottom\">
            <div class=\"wrap\">
                <div class=\"header-bottom-left\">
                    <div class=\"logo\">
                        <a href=\"index.html\"><img src=";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo.png"), "html", null, true);
        echo " alt=\"\"/></a>
                    </div>
                    <div class=\"menu\">
                          ";
        // line 56
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            echo "  
                        <ul class=\"megamenu skyblue\">
                            <li class=\"active grid\"><a href=\"";
            // line 58
            echo $this->env->getExtension('routing')->getPath("produit");
            echo "\">Accueil</a></li>
                            <li><a class=\"color4\" href=\"";
            // line 59
            echo $this->env->getExtension('routing')->getPath("produit");
            echo "\">Produits</a>
                                <div class=\"megapanel\">
                                    <div class=\"row\">
                                        <div class=\"col1\">
                                            <div class=\"h_nav\">
                                                <h4>Catégorie 1</h4>
                                                <ul>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 1</a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 2</a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 3 </a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 4</a></li>
                                                </ul>\t
                                            </div>\t\t\t\t\t\t\t
                                        </div>
                                        <div class=\"col1\">
                                            <div class=\"h_nav\">
                                                <h4>Categorie 2</h4>
                                                <ul>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 1</a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 2</a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 3</a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 4</a></li>
                                                </ul>\t
                                            </div>\t\t\t\t\t\t\t
                                        </div>
                                        <div class=\"col1\">
                                            <div class=\"h_nav\">
                                                <h4>Categorie 3</h4>
                                                <ul>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 1</a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 2</a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 3</a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 4</a></li>
                                                    <li><a href=\"womens.html\">Sou-Catégorie 5</a></li>
                                                </ul>\t
                                            </div>\t\t\t\t\t\t\t\t\t\t\t\t
                                        </div>
                                    </div>
                                </div>
                            </li>\t\t\t\t
                            <li><a class=\"color5\" href=\"";
            // line 99
            echo $this->env->getExtension('routing')->getPath("recherchre");
            echo "\">Services</a>
                                <div class=\"megapanel\">
                                    <div class=\"col1\">
                                        <div class=\"h_nav\">
                                            <h4>Categorie 1</h4>
                                            <ul>
                                                <li><img <a href=\"mens.html\">sou categorie 1</a></li>
                                                <li><a href=\"mens.html\">sou categorie 2</a></li>
                                                <li><a href=\"mens.html\">sou categorie 3</a></li>
                                                <li><a href=\"mens.html\">sou categorie 4</a></li>
                                            </ul>\t
                                        </div>\t\t\t\t\t\t\t
                                    </div>
                                    <div class=\"col1\">
                                        <div class=\"h_nav\">
                                            <h4>Categorie 2</h4>
                                            <ul>
                                                <li><a href=\"mens.html\">sou categorie 1</a></li>
                                                <li><a href=\"mens.html\">sou categorie 2 </a></li>
                                                <li><a href=\"mens.html\">sou categorie 3 </a></li>
                                                <li><a href=\"mens.html\">sou categorie 4 </a></li>
                                            </ul>\t
                                        </div>\t\t\t\t\t\t\t
                                    </div>
                                    <div class=\"col1\">
                                        <div class=\"h_nav\">
                                            <h4>categorie 3 </h4>
                                            <ul>
                                                <li><a href=\"mens.html\">sou categorie 1</a></li>
                                                <li><a href=\"mens.html\">sou categorie 2 </a></li>
                                                <li><a href=\"mens.html\">sou categorie 3 </a></li>
                                                <li><a href=\"mens.html\">sou categorie 4 </a></li>
                                            </ul>\t
                                        </div>\t\t\t\t\t\t\t\t\t\t\t\t
                                    </div>
                                </div>
                            </li>
                            
                            
                            <li><a class=\"color7\" href=\"";
            // line 138
            echo $this->env->getExtension('routing')->getPath("recherche_appel_offre");
            echo "\">Appels d'offre</a></li>
                            <li><a class=\"color7\" href=\"";
            // line 139
            echo $this->env->getExtension('routing')->getPath("sujet_show-All");
            echo "\">Forum</a></li>
                             
                        </ul>
                            ";
        }
        // line 143
        echo "                    </div>
                </div>
                <div class=\"header-bottom-right\">
                    <div class=\"search\">\t  
                        <input type=\"text\" name=\"s\" class=\"textbox\" value=\"rechercher\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {
                                            this.value = 'rechercher';
                                        }\">
                        <input type=\"submit\" value=\"Subscribe\" id=\"submit\" name=\"submit\">
                        <div id=\"response\"> </div>
                    </div>
                    <div class=\"tag-list\">
                        <ul class=\"icon1 sub-icon1 profile_img\">
                            <li><a class=\"active-icon c1\" href=\"#\"> </a>
                                <ul class=\"sub-icon1 list\">
                                    <li><h3>sed diam nonummy</h3><a href=\"\"></a></li>
                                    <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href=\"\">adipiscing elit, sed diam</a></p></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class=\"icon1 sub-icon1 profile_img\">
                            <li><a class=\"active-icon c2\" href=\"#\"> </a>
                                <ul class=\"sub-icon1 list\">
                                    <li><h3>No Products</h3><a href=\"\"></a></li>
                                    <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href=\"\">adipiscing elit, sed diam</a></p></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class=\"last\"><li><a href=\"#\">Panier(0)</a></li></ul>
                    </div>
                </div>
                <div class=\"clear\"></div>
            </div>
        </div>
        <!-- start slider -->
        ";
        // line 177
        $this->displayBlock('slider', $context, $blocks);
        // line 216
        echo " ";
        $this->displayBlock('content', $context, $blocks);
        // line 220
        echo "        
        
        <div class=\"footer\">
            <div class=\"footer-top\">
                <div class=\"wrap\">
                    <div class=\"section group example\">
                        <div class=\"col_1_of_2 span_1_of_2\">
                            <ul class=\"f-list\">
                                <li><img src=";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/2.png"), "html", null, true);
        echo "><span class=\"f-text\">Free Shipping on orders over \$ 100</span><div class=\"clear\"></div></li>
                            </ul>
                        </div>
                        <div class=\"col_1_of_2 span_1_of_2\">
                            <ul class=\"f-list\">
                                <li><img src=";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/3.png"), "html", null, true);
        echo "><span class=\"f-text\">Call us! toll free-222-555-6666 </span><div class=\"clear\"></div></li>
                            </ul>
                        </div>
                        <div class=\"clear\"></div>
                    </div>
                </div>
            </div>
            <div class=\"footer-middle\">
                <div class=\"wrap\">
                    <!-- <div class=\"section group\">
                           <div class=\"f_10\">
                                   <div class=\"col_1_of_4 span_1_of_4\">
                                           <h3>Facebook</h3>
                                           <script>(function(d, s, id) {
                                             var js, fjs = d.getElementsByTagName(s)[0];
                                             if (d.getElementById(id)) return;
                                             js = d.createElement(s); js.id = id;
                                             js.src = \"//connect.facebook.net/en_US/all.js#xfbml=1\";
                                             fjs.parentNode.insertBefore(js, fjs);
                                           }(document, 'script', 'facebook-jssdk'));</script>
                                           <div class=\"like_box\">\t
                                                   <div class=\"fb-like-box\" data-href=\"http://www.facebook.com/w3layouts\" data-colorscheme=\"light\" data-show-faces=\"true\" data-header=\"true\" data-stream=\"false\" data-show-border=\"true\"></div>
                                           </div>
                                   </div>
                                   <div class=\"col_1_of_4 span_1_of_4\">
                                           <h3>From Twitter</h3>
                                           <div class=\"recent-tweet\">
                                                   <div class=\"recent-tweet-icon\">
                                                           <span> </span>
                                                   </div>
                                                   <div class=\"recent-tweet-info\">
                                                           <p>Ds which don't look even slightly believable. If you are <a href=\"#\">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
                                                   </div>
                                                   <div class=\"clear\"> </div>
                                           </div>
                                           <div class=\"recent-tweet\">
                                                   <div class=\"recent-tweet-icon\">
                                                           <span> </span>
                                                   </div>
                                                   <div class=\"recent-tweet-info\">
                                                           <p>Ds which don't look even slightly believable. If you are <a href=\"#\">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
                                                   </div>
                                                   <div class=\"clear\"> </div>
                                           </div>
                                   </div>
                           </div>
                           <div class=\"f_10\">
                                   <div class=\"col_1_of_4 span_1_of_4\">
                                       <h3>Information</h3>
                                           <ul class=\"f-list1\">
                                               <li><a href=\"#\">Duis autem vel eum iriure </a></li>
                                       <li><a href=\"#\">anteposuerit litterarum formas </a></li>
                                       <li><a href=\"#\">Tduis dolore te feugait nulla</a></li>
                                        <li><a href=\"#\">Duis autem vel eum iriure </a></li>
                                       <li><a href=\"#\">anteposuerit litterarum formas </a></li>
                                       <li><a href=\"#\">Tduis dolore te feugait nulla</a></li>
                                   </ul>
                                   </div>
                                   <div class=\"col_1_of_4 span_1_of_4\">
                                           <h3>Contact us</h3>
                                           <div class=\"company_address\">
                                                   <p>500 Lorem Ipsum Dolor Sit,</p>
                                                                   <p>22-56-2-9 Sit Amet, Lorem,</p>
                                                                   <p>USA</p>
                                                   <p>Phone:(00) 222 666 444</p>
                                                   <p>Fax: (000) 000 00 00 0</p>
                                                   <p>Email: <span>mail[at]leoshop.com</span></p>
                                                   
                                      </div>
                                      <div class=\"social-media\">
                                                <ul>
                                                   <li> <span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Google\"><a href=\"#\" target=\"_blank\"> </a></span></li>
                                                   <li><span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Linked in\"><a href=\"#\" target=\"_blank\"> </a> </span></li>
                                                   <li><span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Rss\"><a href=\"#\" target=\"_blank\"> </a></span></li>
                                                   <li><span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Facebook\"><a href=\"#\" target=\"_blank\"> </a></span></li>
                                               </ul>
                                      </div>
                                   </div>
                           <div class=\"clear\"></div>
                   </div>
                   <div class=\"clear\"></div>
             </div>-->





                    <div class=\"section group example\">
                        <div class=\"col_1_of_f_1 span_1_of_f_1\">
                            <div class=\"section group example\">
                                <div class=\"col_1_of_f_2 span_1_of_f_2\">
                                    <h3>Facebook</h3>
                                   <script src=";
        // line 325
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Facebook.js"), "html", null, true);
        echo "></script>
                                    <div class=\"like_box\">\t
                                        <div class=\"fb-like-box\" data-href=\"http://www.facebook.com/w3layouts\" data-colorscheme=\"light\" data-show-faces=\"true\" data-header=\"true\" data-stream=\"false\" data-show-border=\"true\"></div>
                                    </div>
                                </div>
                                <div class=\"col_1_of_f_2 span_1_of_f_2\">
                                    <h3>Twitter</h3>
                                    <div class=\"recent-tweet\">
                                        <div class=\"recent-tweet-icon\">
                                            <span> </span>
                                        </div>
                                        <div class=\"recent-tweet-info\">
                                            <p>Ds which don't look even slightly believable. If you are <a href=\"#\">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
                                        </div>
                                        <div class=\"clear\"> </div>
                                    </div>
                                    <div class=\"recent-tweet\">
                                        <div class=\"recent-tweet-icon\">
                                            <span> </span>
                                        </div>
                                        <div class=\"recent-tweet-info\">
                                            <p>Ds which don't look even slightly believable. If you are <a href=\"#\">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
                                        </div>
                                        <div class=\"clear\"> </div>
                                    </div>
                                </div>
                                <div class=\"clear\"></div>
                            </div>
                        </div>
                        <div class=\"col_1_of_f_1 span_1_of_f_1\">
                            <div class=\"section group example\">
                                <div class=\"col_1_of_f_2 span_1_of_f_2\">
                                    <h3>Information</h3>
                                    <ul class=\"f-list1\">
                                        <li><a href=\"#\">Duis autem vel eum iriure </a></li>
                                        <li><a href=\"#\">anteposuerit litterarum formas </a></li>
                                        <li><a href=\"#\">Tduis dolore te feugait nulla</a></li>
                                        <li><a href=\"#\">Duis autem vel eum iriure </a></li>
                                        <li><a href=\"#\">anteposuerit litterarum formas </a></li>
                                        <li><a href=\"#\">Tduis dolore te feugait nulla</a></li>
                                    </ul>
                                </div>
                                <div class=\"col_1_of_f_2 span_1_of_f_2\">
                                    <h3>Contacter nous</h3>
                                    <div class=\"company_address\">
                                        <p>Ariana , Tunis</p>
                                        <p>Chotrana , Jaafar 2,</p>
                                        <p>Tunisia</p>
                                        <p>Phone:(+216) 22 666 444</p>
                                        <p>Fax: (+216) 71 444 555</p>
                                        <p>Email: <span>AllForDeal@gmail.com</span></p>

                                    </div>
                                    <div class=\"social-media\">
                                        <ul>
                                            <li> <span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Google\"><a href=\"#\" target=\"_blank\"> </a></span></li>
                                            <li><span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Linked in\"><a href=\"#\" target=\"_blank\"> </a> </span></li>
                                            <li><span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Rss\"><a href=\"#\" target=\"_blank\"> </a></span></li>
                                            <li><span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Facebook\"><a href=\"#\" target=\"_blank\"> </a></span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class=\"clear\"></div>
                            </div>
                        </div>
                        <div class=\"clear\"></div>
                    </div>
                </div>
            </div>
            <div class=\"footer-bottom\">
                <div class=\"wrap\">
                    <div class=\"copy\">
                        <p>© 2016 Copyrights <a href='#'> AllForDeal </a> </p>
                    </div>
                    <div class=\"f-list2\">
                          ";
        // line 400
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            echo "  
                        <ul>
                            <li class=\"active\"><a href=\"about.html\">A propos</a></li> |
                            <li><a href=\"delivery.html\">Livraison</a></li> |
                            <li><a href=\"\">Informations légales</a></li> |
                            <li><a href=\"";
            // line 405
            echo $this->env->getExtension('routing')->getPath("mail_new");
            echo "\">Contacter nous</a></li> 
                            <li><a href=\"";
            // line 406
            echo $this->env->getExtension('routing')->getPath("my_app_reclamation_form");
            echo "\">Reclamation</a></li> 
                        </ul>
                        ";
        }
        // line 409
        echo "                    </div>
                    <div class=\"clear\"></div>
                </div>
            </div>
        </div>
    
</html>
  ";
    }

    // line 23
    public function block_header($context, array $blocks = array())
    {
        echo " ";
    }

    // line 177
    public function block_slider($context, array $blocks = array())
    {
        // line 178
        echo "        <div id=\"fwslider\">
            <div class=\"slider_container\">
                <div class=\"slide\"> 
                    <!-- Slide image -->
                    <img src=";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/banner.jpg"), "html", null, true);
        echo " alt=\"\"/>
                    <!-- /Slide image -->
                    <!-- Texts container -->
                    <div class=\"slide_content\">
                        <div class=\"slide_content_wrap\">
                            <!-- Text title -->
                            <h4 class=\"title\">Aluminium Club</h4>
                            <!-- /Text title -->

                            <!-- Text description -->
                            <p class=\"description\">Experiance ray ban</p>
                            <!-- /Text description -->
                        </div>
                    </div>
                    <!-- /Texts container -->
                </div>
                <!-- /Duplicate to create more slides -->
                <div class=\"slide\">
                    <img src=";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/banner1.jpg"), "html", null, true);
        echo " alt=\"\"/>
                    <div class=\"slide_content\">
                        <div class=\"slide_content_wrap\">
                            <h4 class=\"title\">consectetuer adipiscing </h4>
                            <p class=\"description\">diam nonummy nibh euismod</p>
                        </div>
                    </div>
                </div>
                <!--/slide -->
            </div>
            <div class=\"timers\"></div>
            <div class=\"slidePrev\"><span></span></div>
            <div class=\"slideNext\"><span></span></div>
        </div>
        <!--/slider -->
        ";
    }

    // line 216
    public function block_content($context, array $blocks = array())
    {
        // line 217
        echo "     

";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  567 => 217,  564 => 216,  544 => 200,  523 => 182,  517 => 178,  514 => 177,  508 => 23,  497 => 409,  491 => 406,  487 => 405,  479 => 400,  401 => 325,  306 => 233,  298 => 228,  288 => 220,  285 => 216,  283 => 177,  247 => 143,  240 => 139,  236 => 138,  194 => 99,  151 => 59,  147 => 58,  142 => 56,  136 => 53,  124 => 43,  119 => 41,  114 => 40,  108 => 37,  104 => 36,  100 => 35,  96 => 34,  92 => 33,  81 => 24,  79 => 23,  74 => 21,  69 => 19,  65 => 18,  61 => 17,  57 => 16,  52 => 14,  48 => 13,  44 => 12,  39 => 10,  34 => 8,  30 => 7,  22 => 1,);
    }
}
/* <!DOCTYPE HTML>*/
/* <html>*/
/*     <head>*/
/*         <title>All For Deal | Accueil </title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*         <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*         <!-- start menu -->*/
/*         <link href={{asset ('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*         <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*         <!--start slider -->*/
/*         <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*         <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*         <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*         <script src={{asset('js/fwslider.js')}}></script>*/
/*         <!--end slider -->*/
/*         <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/*     </head>*/
/*     {% block header %} {% endblock %}*/
/*         <form method="post"> */
/*         <div class="header-top">*/
/*             <div class="wrap"> */
/*                 <div class="header-top-left">*/
/*                  */
/*                     <div class="clear"></div>*/
/*                 </div>*/
/*                 <div class="cssmenu">    */
/*                     <ul>*/
/*                         {% if is_granted("ROLE_USER") %}  */
/*                         <li class="color4"><a href="{{path('Profile')}}">Compte</a> </li> |                         */
/*                         <li><a href="{{ path('favoris_show', { 'id': user.favoris.id }) }}">Favoris</a></li> |*/
/*                         <li><a href="{{ path('panier_show', { 'id': user.panier.id }) }}">Panier</a></li> |*/
/*                         <li><a href="{{path('fos_user_security_logout')}}">Se déconnecter</a></li> |*/
/*                        */
/*                         {% else %}*/
/*                         <li><a href="{{path('fos_user_security_login')}}">Se connecter</a></li> |*/
/*                         <li><a href="{{path('fos_user_registration_register')}}">S'inscrire</a></li>*/
/*                         {% endif %}*/
/*                         */
/*                     </ul>*/
/*                 </div>*/
/*                 <div class="clear"></div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="header-bottom">*/
/*             <div class="wrap">*/
/*                 <div class="header-bottom-left">*/
/*                     <div class="logo">*/
/*                         <a href="index.html"><img src={{asset('images/logo.png')}} alt=""/></a>*/
/*                     </div>*/
/*                     <div class="menu">*/
/*                           {% if is_granted("ROLE_USER") %}  */
/*                         <ul class="megamenu skyblue">*/
/*                             <li class="active grid"><a href="{{path('produit')}}">Accueil</a></li>*/
/*                             <li><a class="color4" href="{{path('produit')}}">Produits</a>*/
/*                                 <div class="megapanel">*/
/*                                     <div class="row">*/
/*                                         <div class="col1">*/
/*                                             <div class="h_nav">*/
/*                                                 <h4>Catégorie 1</h4>*/
/*                                                 <ul>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 1</a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 2</a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 3 </a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 4</a></li>*/
/*                                                 </ul>	*/
/*                                             </div>							*/
/*                                         </div>*/
/*                                         <div class="col1">*/
/*                                             <div class="h_nav">*/
/*                                                 <h4>Categorie 2</h4>*/
/*                                                 <ul>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 1</a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 2</a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 3</a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 4</a></li>*/
/*                                                 </ul>	*/
/*                                             </div>							*/
/*                                         </div>*/
/*                                         <div class="col1">*/
/*                                             <div class="h_nav">*/
/*                                                 <h4>Categorie 3</h4>*/
/*                                                 <ul>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 1</a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 2</a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 3</a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 4</a></li>*/
/*                                                     <li><a href="womens.html">Sou-Catégorie 5</a></li>*/
/*                                                 </ul>	*/
/*                                             </div>												*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </li>				*/
/*                             <li><a class="color5" href="{{path('recherchre')}}">Services</a>*/
/*                                 <div class="megapanel">*/
/*                                     <div class="col1">*/
/*                                         <div class="h_nav">*/
/*                                             <h4>Categorie 1</h4>*/
/*                                             <ul>*/
/*                                                 <li><img <a href="mens.html">sou categorie 1</a></li>*/
/*                                                 <li><a href="mens.html">sou categorie 2</a></li>*/
/*                                                 <li><a href="mens.html">sou categorie 3</a></li>*/
/*                                                 <li><a href="mens.html">sou categorie 4</a></li>*/
/*                                             </ul>	*/
/*                                         </div>							*/
/*                                     </div>*/
/*                                     <div class="col1">*/
/*                                         <div class="h_nav">*/
/*                                             <h4>Categorie 2</h4>*/
/*                                             <ul>*/
/*                                                 <li><a href="mens.html">sou categorie 1</a></li>*/
/*                                                 <li><a href="mens.html">sou categorie 2 </a></li>*/
/*                                                 <li><a href="mens.html">sou categorie 3 </a></li>*/
/*                                                 <li><a href="mens.html">sou categorie 4 </a></li>*/
/*                                             </ul>	*/
/*                                         </div>							*/
/*                                     </div>*/
/*                                     <div class="col1">*/
/*                                         <div class="h_nav">*/
/*                                             <h4>categorie 3 </h4>*/
/*                                             <ul>*/
/*                                                 <li><a href="mens.html">sou categorie 1</a></li>*/
/*                                                 <li><a href="mens.html">sou categorie 2 </a></li>*/
/*                                                 <li><a href="mens.html">sou categorie 3 </a></li>*/
/*                                                 <li><a href="mens.html">sou categorie 4 </a></li>*/
/*                                             </ul>	*/
/*                                         </div>												*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </li>*/
/*                             */
/*                             */
/*                             <li><a class="color7" href="{{ path('recherche_appel_offre')}}">Appels d'offre</a></li>*/
/*                             <li><a class="color7" href="{{ path('sujet_show-All')}}">Forum</a></li>*/
/*                              */
/*                         </ul>*/
/*                             {% endif %}*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="header-bottom-right">*/
/*                     <div class="search">	  */
/*                         <input type="text" name="s" class="textbox" value="rechercher" onfocus="this.value = '';" onblur="if (this.value == '') {*/
/*                                             this.value = 'rechercher';*/
/*                                         }">*/
/*                         <input type="submit" value="Subscribe" id="submit" name="submit">*/
/*                         <div id="response"> </div>*/
/*                     </div>*/
/*                     <div class="tag-list">*/
/*                         <ul class="icon1 sub-icon1 profile_img">*/
/*                             <li><a class="active-icon c1" href="#"> </a>*/
/*                                 <ul class="sub-icon1 list">*/
/*                                     <li><h3>sed diam nonummy</h3><a href=""></a></li>*/
/*                                     <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                         </ul>*/
/*                         <ul class="icon1 sub-icon1 profile_img">*/
/*                             <li><a class="active-icon c2" href="#"> </a>*/
/*                                 <ul class="sub-icon1 list">*/
/*                                     <li><h3>No Products</h3><a href=""></a></li>*/
/*                                     <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                         </ul>*/
/*                         <ul class="last"><li><a href="#">Panier(0)</a></li></ul>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="clear"></div>*/
/*             </div>*/
/*         </div>*/
/*         <!-- start slider -->*/
/*         {% block slider %}*/
/*         <div id="fwslider">*/
/*             <div class="slider_container">*/
/*                 <div class="slide"> */
/*                     <!-- Slide image -->*/
/*                     <img src={{asset('images/banner.jpg')}} alt=""/>*/
/*                     <!-- /Slide image -->*/
/*                     <!-- Texts container -->*/
/*                     <div class="slide_content">*/
/*                         <div class="slide_content_wrap">*/
/*                             <!-- Text title -->*/
/*                             <h4 class="title">Aluminium Club</h4>*/
/*                             <!-- /Text title -->*/
/* */
/*                             <!-- Text description -->*/
/*                             <p class="description">Experiance ray ban</p>*/
/*                             <!-- /Text description -->*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- /Texts container -->*/
/*                 </div>*/
/*                 <!-- /Duplicate to create more slides -->*/
/*                 <div class="slide">*/
/*                     <img src={{asset('images/banner1.jpg')}} alt=""/>*/
/*                     <div class="slide_content">*/
/*                         <div class="slide_content_wrap">*/
/*                             <h4 class="title">consectetuer adipiscing </h4>*/
/*                             <p class="description">diam nonummy nibh euismod</p>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <!--/slide -->*/
/*             </div>*/
/*             <div class="timers"></div>*/
/*             <div class="slidePrev"><span></span></div>*/
/*             <div class="slideNext"><span></span></div>*/
/*         </div>*/
/*         <!--/slider -->*/
/*         {% endblock %}*/
/*  {% block content %}*/
/*      */
/* */
/* {% endblock %}*/
/*         */
/*         */
/*         <div class="footer">*/
/*             <div class="footer-top">*/
/*                 <div class="wrap">*/
/*                     <div class="section group example">*/
/*                         <div class="col_1_of_2 span_1_of_2">*/
/*                             <ul class="f-list">*/
/*                                 <li><img src={{asset('images/2.png')}}><span class="f-text">Free Shipping on orders over $ 100</span><div class="clear"></div></li>*/
/*                             </ul>*/
/*                         </div>*/
/*                         <div class="col_1_of_2 span_1_of_2">*/
/*                             <ul class="f-list">*/
/*                                 <li><img src={{asset('images/3.png')}}><span class="f-text">Call us! toll free-222-555-6666 </span><div class="clear"></div></li>*/
/*                             </ul>*/
/*                         </div>*/
/*                         <div class="clear"></div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="footer-middle">*/
/*                 <div class="wrap">*/
/*                     <!-- <div class="section group">*/
/*                            <div class="f_10">*/
/*                                    <div class="col_1_of_4 span_1_of_4">*/
/*                                            <h3>Facebook</h3>*/
/*                                            <script>(function(d, s, id) {*/
/*                                              var js, fjs = d.getElementsByTagName(s)[0];*/
/*                                              if (d.getElementById(id)) return;*/
/*                                              js = d.createElement(s); js.id = id;*/
/*                                              js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";*/
/*                                              fjs.parentNode.insertBefore(js, fjs);*/
/*                                            }(document, 'script', 'facebook-jssdk'));</script>*/
/*                                            <div class="like_box">	*/
/*                                                    <div class="fb-like-box" data-href="http://www.facebook.com/w3layouts" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>*/
/*                                            </div>*/
/*                                    </div>*/
/*                                    <div class="col_1_of_4 span_1_of_4">*/
/*                                            <h3>From Twitter</h3>*/
/*                                            <div class="recent-tweet">*/
/*                                                    <div class="recent-tweet-icon">*/
/*                                                            <span> </span>*/
/*                                                    </div>*/
/*                                                    <div class="recent-tweet-info">*/
/*                                                            <p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>*/
/*                                                    </div>*/
/*                                                    <div class="clear"> </div>*/
/*                                            </div>*/
/*                                            <div class="recent-tweet">*/
/*                                                    <div class="recent-tweet-icon">*/
/*                                                            <span> </span>*/
/*                                                    </div>*/
/*                                                    <div class="recent-tweet-info">*/
/*                                                            <p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>*/
/*                                                    </div>*/
/*                                                    <div class="clear"> </div>*/
/*                                            </div>*/
/*                                    </div>*/
/*                            </div>*/
/*                            <div class="f_10">*/
/*                                    <div class="col_1_of_4 span_1_of_4">*/
/*                                        <h3>Information</h3>*/
/*                                            <ul class="f-list1">*/
/*                                                <li><a href="#">Duis autem vel eum iriure </a></li>*/
/*                                        <li><a href="#">anteposuerit litterarum formas </a></li>*/
/*                                        <li><a href="#">Tduis dolore te feugait nulla</a></li>*/
/*                                         <li><a href="#">Duis autem vel eum iriure </a></li>*/
/*                                        <li><a href="#">anteposuerit litterarum formas </a></li>*/
/*                                        <li><a href="#">Tduis dolore te feugait nulla</a></li>*/
/*                                    </ul>*/
/*                                    </div>*/
/*                                    <div class="col_1_of_4 span_1_of_4">*/
/*                                            <h3>Contact us</h3>*/
/*                                            <div class="company_address">*/
/*                                                    <p>500 Lorem Ipsum Dolor Sit,</p>*/
/*                                                                    <p>22-56-2-9 Sit Amet, Lorem,</p>*/
/*                                                                    <p>USA</p>*/
/*                                                    <p>Phone:(00) 222 666 444</p>*/
/*                                                    <p>Fax: (000) 000 00 00 0</p>*/
/*                                                    <p>Email: <span>mail[at]leoshop.com</span></p>*/
/*                                                    */
/*                                       </div>*/
/*                                       <div class="social-media">*/
/*                                                 <ul>*/
/*                                                    <li> <span class="simptip-position-bottom simptip-movable" data-tooltip="Google"><a href="#" target="_blank"> </a></span></li>*/
/*                                                    <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Linked in"><a href="#" target="_blank"> </a> </span></li>*/
/*                                                    <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Rss"><a href="#" target="_blank"> </a></span></li>*/
/*                                                    <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Facebook"><a href="#" target="_blank"> </a></span></li>*/
/*                                                </ul>*/
/*                                       </div>*/
/*                                    </div>*/
/*                            <div class="clear"></div>*/
/*                    </div>*/
/*                    <div class="clear"></div>*/
/*              </div>-->*/
/* */
/* */
/* */
/* */
/* */
/*                     <div class="section group example">*/
/*                         <div class="col_1_of_f_1 span_1_of_f_1">*/
/*                             <div class="section group example">*/
/*                                 <div class="col_1_of_f_2 span_1_of_f_2">*/
/*                                     <h3>Facebook</h3>*/
/*                                    <script src={{asset('js/Facebook.js')}}></script>*/
/*                                     <div class="like_box">	*/
/*                                         <div class="fb-like-box" data-href="http://www.facebook.com/w3layouts" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="col_1_of_f_2 span_1_of_f_2">*/
/*                                     <h3>Twitter</h3>*/
/*                                     <div class="recent-tweet">*/
/*                                         <div class="recent-tweet-icon">*/
/*                                             <span> </span>*/
/*                                         </div>*/
/*                                         <div class="recent-tweet-info">*/
/*                                             <p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>*/
/*                                         </div>*/
/*                                         <div class="clear"> </div>*/
/*                                     </div>*/
/*                                     <div class="recent-tweet">*/
/*                                         <div class="recent-tweet-icon">*/
/*                                             <span> </span>*/
/*                                         </div>*/
/*                                         <div class="recent-tweet-info">*/
/*                                             <p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>*/
/*                                         </div>*/
/*                                         <div class="clear"> </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="clear"></div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col_1_of_f_1 span_1_of_f_1">*/
/*                             <div class="section group example">*/
/*                                 <div class="col_1_of_f_2 span_1_of_f_2">*/
/*                                     <h3>Information</h3>*/
/*                                     <ul class="f-list1">*/
/*                                         <li><a href="#">Duis autem vel eum iriure </a></li>*/
/*                                         <li><a href="#">anteposuerit litterarum formas </a></li>*/
/*                                         <li><a href="#">Tduis dolore te feugait nulla</a></li>*/
/*                                         <li><a href="#">Duis autem vel eum iriure </a></li>*/
/*                                         <li><a href="#">anteposuerit litterarum formas </a></li>*/
/*                                         <li><a href="#">Tduis dolore te feugait nulla</a></li>*/
/*                                     </ul>*/
/*                                 </div>*/
/*                                 <div class="col_1_of_f_2 span_1_of_f_2">*/
/*                                     <h3>Contacter nous</h3>*/
/*                                     <div class="company_address">*/
/*                                         <p>Ariana , Tunis</p>*/
/*                                         <p>Chotrana , Jaafar 2,</p>*/
/*                                         <p>Tunisia</p>*/
/*                                         <p>Phone:(+216) 22 666 444</p>*/
/*                                         <p>Fax: (+216) 71 444 555</p>*/
/*                                         <p>Email: <span>AllForDeal@gmail.com</span></p>*/
/* */
/*                                     </div>*/
/*                                     <div class="social-media">*/
/*                                         <ul>*/
/*                                             <li> <span class="simptip-position-bottom simptip-movable" data-tooltip="Google"><a href="#" target="_blank"> </a></span></li>*/
/*                                             <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Linked in"><a href="#" target="_blank"> </a> </span></li>*/
/*                                             <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Rss"><a href="#" target="_blank"> </a></span></li>*/
/*                                             <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Facebook"><a href="#" target="_blank"> </a></span></li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="clear"></div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="clear"></div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="footer-bottom">*/
/*                 <div class="wrap">*/
/*                     <div class="copy">*/
/*                         <p>© 2016 Copyrights <a href='#'> AllForDeal </a> </p>*/
/*                     </div>*/
/*                     <div class="f-list2">*/
/*                           {% if is_granted("ROLE_USER") %}  */
/*                         <ul>*/
/*                             <li class="active"><a href="about.html">A propos</a></li> |*/
/*                             <li><a href="delivery.html">Livraison</a></li> |*/
/*                             <li><a href="">Informations légales</a></li> |*/
/*                             <li><a href="{{ path('mail_new')}}">Contacter nous</a></li> */
/*                             <li><a href="{{ path('my_app_reclamation_form')}}">Reclamation</a></li> */
/*                         </ul>*/
/*                         {% endif %}*/
/*                     </div>*/
/*                     <div class="clear"></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     */
/* </html>*/
/*   */
