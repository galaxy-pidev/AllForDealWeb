<?php

/* PiDevClientBundle:Client:profile.html.twig */
class __TwigTemplate_6d7d9fbf4f176e0c2aa2bc7f3517e9bf0fff7c130005a499b4ec4c0f9604c737 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Client:profile.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "



    <body>
        <!-- BEGAIN PRELOADER -->   
        <div id=\"main-content\" class=\"page-profil\">
            <div class=\"row\">

                <!-- BEGIN PROFIL SIDEBAR -->

                <!-- END PROFIL SIDEBAR -->

                <!-- BEGIN PROFIL CONTENT -->
                <div class=\"col-md-9 profil-content m-t-20\">

                    <div class=\"row\">
                        <div class=\"mix category-1 col-lg-3 col-md-6 col-sm-6 col-xs-12\" data-value=\"1\">
                            <div class=\"thumbnail\">
                                <div class=\"overlay\">
                                    <div class=\"thumbnail-actions\">
                                        <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
        echo "\" class=\"btn btn-default btn-icon btn-rounded magnific\" title=\"Photo de profil\"><i class=\"fa fa-search\"></i></a>
                                        <a href=\"#\" class=\"btn btn-default btn-icon btn-rounded favorite\" title=\"love this picture\"><i class=\"fa fa-heart\"></i></a>
                                        <a href=\"#\" class=\"btn btn-blue btn-icon btn-rounded\" title=\"download this picture\"><i class=\"fa fa-cloud-download\"></i></a>
                                    </div>
                                </div>
                                <img src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"Photo de profil\" />
                                <div class=\"thumbnail-meta\">
                                    <h5>
                                        Photo de profil<br>

                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                                       
                    <div class=\"row profil-review\">
                        <div class=\"col-md-6\">
                            <div class=\"p-20\">
                                <div class=\"row\">

                                    <div class=\"col-xs-9 col-md-9 section-box\">
                                        <h2><font COLOR=#0E133F >
                                            <u>   Mes informations </u> <a href=\"#\" target=\"_blank\"></a>
                                            </font>  </h2>
                                        <b> <p>Nom : ";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo " </p></b>
                                        <hr />
                                        <b>  <p>Email : ";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo " </p> </b>
                                        <hr />
                                        <b>   <p>Crédit : ";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "credit", array()), "html", null, true);
        echo " </p> </b>
                                          <hr />
                                          <b>  <p> Point Bonus : ";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "ptBonus", array()), "html", null, true);
        echo "</p></b>
                                          <hr /> <b><p>  <a ";
        // line 57
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
        echo ">Modifier</a></p></b>
                                                                     
                            <hr /> <b><p>     <a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\">Modifier Mot de passe</a></p></b>
                            <hr /> <b><p>     <a href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
        echo "\">Modifier Coordonnées</a></p></b>
                                        <hr />
                                        

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"col-md-12 border-bottom p-t-20\"></div>
                    </div>
                    </div>


                </div>

                <!-- END PROFIL CONTENT -->

            </div>
        </div>
        <!-- END MAIN CONTENT -->

    ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 60,  110 => 59,  105 => 57,  101 => 56,  96 => 54,  91 => 52,  86 => 50,  62 => 29,  54 => 24,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content  %}*/
/* */
/* */
/* */
/* */
/*     <body>*/
/*         <!-- BEGAIN PRELOADER -->   */
/*         <div id="main-content" class="page-profil">*/
/*             <div class="row">*/
/* */
/*                 <!-- BEGIN PROFIL SIDEBAR -->*/
/* */
/*                 <!-- END PROFIL SIDEBAR -->*/
/* */
/*                 <!-- BEGIN PROFIL CONTENT -->*/
/*                 <div class="col-md-9 profil-content m-t-20">*/
/* */
/*                     <div class="row">*/
/*                         <div class="mix category-1 col-lg-3 col-md-6 col-sm-6 col-xs-12" data-value="1">*/
/*                             <div class="thumbnail">*/
/*                                 <div class="overlay">*/
/*                                     <div class="thumbnail-actions">*/
/*                                         <a href="{{asset(path('image'))}}" class="btn btn-default btn-icon btn-rounded magnific" title="Photo de profil"><i class="fa fa-search"></i></a>*/
/*                                         <a href="#" class="btn btn-default btn-icon btn-rounded favorite" title="love this picture"><i class="fa fa-heart"></i></a>*/
/*                                         <a href="#" class="btn btn-blue btn-icon btn-rounded" title="download this picture"><i class="fa fa-cloud-download"></i></a>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <img src="{{asset(path('image'))}}" class="img-responsive" alt="Photo de profil" />*/
/*                                 <div class="thumbnail-meta">*/
/*                                     <h5>*/
/*                                         Photo de profil<br>*/
/* */
/*                                     </h5>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                             */
/*                                        */
/*                     <div class="row profil-review">*/
/*                         <div class="col-md-6">*/
/*                             <div class="p-20">*/
/*                                 <div class="row">*/
/* */
/*                                     <div class="col-xs-9 col-md-9 section-box">*/
/*                                         <h2><font COLOR=#0E133F >*/
/*                                             <u>   Mes informations </u> <a href="#" target="_blank"></a>*/
/*                                             </font>  </h2>*/
/*                                         <b> <p>Nom : {{user.username}} </p></b>*/
/*                                         <hr />*/
/*                                         <b>  <p>Email : {{user.email}} </p> </b>*/
/*                                         <hr />*/
/*                                         <b>   <p>Crédit : {{user.credit}} </p> </b>*/
/*                                           <hr />*/
/*                                           <b>  <p> Point Bonus : {{user.ptBonus}}</p></b>*/
/*                                           <hr /> <b><p>  <a {{path('fos_user_profile_edit')}}>Modifier</a></p></b>*/
/*                                                                      */
/*                             <hr /> <b><p>     <a href="{{path('fos_user_change_password')}}">Modifier Mot de passe</a></p></b>*/
/*                             <hr /> <b><p>     <a href="{{path('fos_user_profile_edit')}}">Modifier Coordonnées</a></p></b>*/
/*                                         <hr />*/
/*                                         */
/* */
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="col-md-12 border-bottom p-t-20"></div>*/
/*                     </div>*/
/*                     </div>*/
/* */
/* */
/*                 </div>*/
/* */
/*                 <!-- END PROFIL CONTENT -->*/
/* */
/*             </div>*/
/*         </div>*/
/*         <!-- END MAIN CONTENT -->*/
/* */
/*     {% endblock %}*/
