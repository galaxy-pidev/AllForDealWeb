<?php

/* PiDevClientBundle:image:list.html.twig */
class __TwigTemplate_a17c90f1c5f869357d3d5b264ad994501a7fc865d23f4b6dfea2b102ed9117bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<table border=\"2\">

 <tr>

 <th> Id </th>

 <th> Title</th>

 <th> Show picture </th>

 </tr>

 

 ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : $this->getContext($context, "images")));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 17
            echo "
 <tr>

 <th>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
            echo "</th>

 <th>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "titre", array()), "html", null, true);
            echo "</th>
 


 <th>

 ";
            // line 29
            echo "     <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["image"], "id", array()))), "html", null, true);
            echo "\">Show picture

 </a> 

</th>

 </tr>

 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "
</table>

";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:image:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 38,  58 => 29,  49 => 22,  44 => 20,  39 => 17,  35 => 16,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <table border="2">*/
/* */
/*  <tr>*/
/* */
/*  <th> Id </th>*/
/* */
/*  <th> Title</th>*/
/* */
/*  <th> Show picture </th>*/
/* */
/*  </tr>*/
/* */
/*  */
/* */
/*  {% for image in images %}*/
/* */
/*  <tr>*/
/* */
/*  <th>{{image.id}}</th>*/
/* */
/*  <th>{{image.titre}}</th>*/
/*  */
/* */
/* */
/*  <th>*/
/* */
/*  {# <a href="{{path("my_image_route", {'id':image.id })}}">Show picture #}*/
/*      <a href="{{path("my_image_route", {'id':image.id })}}">Show picture*/
/* */
/*  </a> */
/* */
/* </th>*/
/* */
/*  </tr>*/
/* */
/*  {% endfor %}*/
/* */
/* </table>*/
/* */
/* */
