<?php

/* PiDevAdminBundle:Admin:ListeUtilisateurs.html.twig */
class __TwigTemplate_422dcb58cd567ade691cc862720c9ee6e487b2a1d4b1571600e5124bf43a866c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:Admin:ListeUtilisateurs.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        echo " 
 
  <html>
    <head>  
        <link href=";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
       


        <!-- END  MANDATORY STYLE -->
        <script src=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
        echo "></script>
    </head>

    <body>
      <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Professional</strong> style of your data in tables</h3>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\"><strong>E-commerce </strong>possibilities example</h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive\">
                                    <table class=\"table table-striped table-hover\">
                                        <thead class=\"no-bd\">
                                            <tr>
                                                <th style=\"width:30px;\" class=\"div_checkbox\">
                                                    <div class=\"div_checkbox\">
                                                        <input class=\"toggle_checkbox\" type=\"checkbox\">
                                                    </div>
                                                </th>
                                                <th><strong>Nom</strong>
                                                </th>
                                                <th><strong>Email</strong>
                                                </th>
                                                <th><strong>Etat du compte</strong>
                                                </th>
                                                <th><strong>Nombre de produit</strong>
                                                </th>
                                                <th><strong>Nombre de service</strong>
                                                </th>
                                                <th class=\"text-center\"><strong>Nombre d'appel d'offre</strong>
                                                </th>
                                                 <th class=\"text-center\"><strong>Nombre de sujet</strong>
                                                </th>
                                                <th class=\"text-center\"><strong>Actions</strong>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class=\"no-bd-y\">
                                          ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mods"]) ? $context["mods"] : $this->getContext($context, "mods")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            echo " 
                                              
                                            <tr>
                                                <td>
                                                    <div class=\"div_checkbox\">
                                                        <input type=\"checkbox\">
                                                    </div>
                                                </td>
                                                <td>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "username", array()), "html", null, true);
            echo "</td>
                                                <td>";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "email", array()), "html", null, true);
            echo "</td>
                                                <td class=\"text-left color-success\"></td>
                                           
                                                <td>";
            // line 69
            echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y:m:j h:i:s") - twig_date_format_filter($this->env, $this->getAttribute($context["j"], "lastlogin", array()), "y:m:j h:i:s")), "html", null, true);
            echo "</td>
                                             
                                           
                                                <td class=\"color-success\"></td>
                                                <td class=\"text-center\"> </td>
                                                <td class=\"text-center\"> </td>
                                                <td class=\"text-center\">
                                                 <div class=\"onoffswitch\">
            <a href=\"";
            // line 77
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_supprimer_utilisateur", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>
            <a href=\"";
            // line 78
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_modifier_utilisateur", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>
                                                </td>
                                            </tr>
                                          
         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 83
        echo "                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    </div>
         
   
        <script src=";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
        <!-- END MANDATORY SCRIPTS -->
        <script src=";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>  
  </body>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Admin:ListeUtilisateurs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 110,  214 => 108,  210 => 107,  206 => 106,  202 => 105,  198 => 104,  194 => 103,  190 => 102,  186 => 101,  182 => 100,  178 => 99,  174 => 98,  170 => 97,  166 => 96,  162 => 95,  148 => 83,  137 => 78,  133 => 77,  122 => 69,  116 => 66,  112 => 65,  99 => 57,  52 => 13,  44 => 8,  40 => 7,  36 => 6,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::baseAdmin.html.twig' %}*/
/* {% block content%} */
/*  */
/*   <html>*/
/*     <head>  */
/*         <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*        */
/* */
/* */
/*         <!-- END  MANDATORY STYLE -->*/
/*         <script src={{asset('plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js')}}></script>*/
/*     </head>*/
/* */
/*     <body>*/
/*       <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Professional</strong> style of your data in tables</h3>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading">*/
/*                             <h3 class="panel-title"><strong>E-commerce </strong>possibilities example</h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">*/
/*                                     <table class="table table-striped table-hover">*/
/*                                         <thead class="no-bd">*/
/*                                             <tr>*/
/*                                                 <th style="width:30px;" class="div_checkbox">*/
/*                                                     <div class="div_checkbox">*/
/*                                                         <input class="toggle_checkbox" type="checkbox">*/
/*                                                     </div>*/
/*                                                 </th>*/
/*                                                 <th><strong>Nom</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Email</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Etat du compte</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Nombre de produit</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Nombre de service</strong>*/
/*                                                 </th>*/
/*                                                 <th class="text-center"><strong>Nombre d'appel d'offre</strong>*/
/*                                                 </th>*/
/*                                                  <th class="text-center"><strong>Nombre de sujet</strong>*/
/*                                                 </th>*/
/*                                                 <th class="text-center"><strong>Actions</strong>*/
/*                                                 </th>*/
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         <tbody class="no-bd-y">*/
/*                                           {% for j in mods %} */
/*                                               */
/*                                             <tr>*/
/*                                                 <td>*/
/*                                                     <div class="div_checkbox">*/
/*                                                         <input type="checkbox">*/
/*                                                     </div>*/
/*                                                 </td>*/
/*                                                 <td>{{j.username}}</td>*/
/*                                                 <td>{{j.email}}</td>*/
/*                                                 <td class="text-left color-success"></td>*/
/*                                            */
/*                                                 <td>{{"now"|date("y:m:j h:i:s") - j.lastlogin|date('y:m:j h:i:s') }}</td>*/
/*                                              */
/*                                            */
/*                                                 <td class="color-success"></td>*/
/*                                                 <td class="text-center"> </td>*/
/*                                                 <td class="text-center"> </td>*/
/*                                                 <td class="text-center">*/
/*                                                  <div class="onoffswitch">*/
/*             <a href="{{path('admin_supprimer_utilisateur',{'id':j.id})}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>*/
/*             <a href="{{path('admin_modifier_utilisateur',{'id':j.id})}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>*/
/*                                                 </td>*/
/*                                             </tr>*/
/*                                           */
/*          {% endfor %}*/
/*                                             */
/*                                         </tbody>*/
/*                                     </table>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*                     </div>*/
/*          */
/*    */
/*         <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*         <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*         <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*         <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*         <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*         <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*         <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*         <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*         <script src={{asset('js/mailbox.js')}}></script>*/
/*         <!-- END MANDATORY SCRIPTS -->*/
/*         <script src={{asset('js/application.js')}}></script>  */
/*   </body>*/
/*  {% endblock %}*/
