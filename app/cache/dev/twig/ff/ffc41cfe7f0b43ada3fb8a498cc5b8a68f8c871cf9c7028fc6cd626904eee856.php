<?php

/* PiDevAdminBundle:Admin:ListeProduitsNonValides.html.twig */
class __TwigTemplate_23fd15474f18aa319d686d6144429959d5a92467b1ba235318c719e1b0492696 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:Admin:ListeProduitsNonValides.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        echo " 
 
  <html>
    <head>  
        <link href=";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
       


        <!-- END  MANDATORY STYLE -->
        <script src=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
        echo "></script>
    </head>
    <style>.onoffswitch {
    position: relative; width: 90px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}
.onoffswitch-checkbox {
    display: none;
}
.onoffswitch-label {
    display: block; overflow: hidden; cursor: pointer;
    border: 2px solid #999999; border-radius: 20px;
}
.onoffswitch-inner {
    display: block; width: 200%; margin-left: -100%;
    transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before, .onoffswitch-inner:after {
    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
    box-sizing: border-box;
}
.onoffswitch-inner:before {
    content: \"ON\";
    padding-left: 10px;
    background-color: #34A7C1; color: #FFFFFF;
}
.onoffswitch-inner:after {
    content: \"OFF\";
    padding-right: 10px;
    background-color: #EEEEEE; color: #999999;
    text-align: right;
}
.onoffswitch-switch {
    display: block; width: 18px; margin: 6px;
    background: #FFFFFF;
    position: absolute; top: 0; bottom: 0;
    right: 56px;
    border: 2px solid #999999; border-radius: 20px;
    transition: all 0.3s ease-in 0s; 
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
    right: 0px; 
}</style>
    <body>
      <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Professional</strong> style of your data in tables</h3>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\"><strong>E-commerce </strong>possibilities example</h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive\">
                                    <table class=\"table table-striped table-hover\">
                                        <thead class=\"no-bd\">
                                            <tr>
                                                <th style=\"width:30px;\" class=\"div_checkbox\">
                                                    <div class=\"div_checkbox\">
                                                        <input class=\"toggle_checkbox\" type=\"checkbox\">
                                                    </div>
                                                </th>
                                                <th><strong>Nom</strong>
                                                </th>
                                                <th><strong>Description</strong>
                                                </th>
                                                <th><strong>Prix</strong>
                                                </th>
                                                <th><strong>Quantité</strong>
                                                </th>
                                                <th><strong>Promotion</strong>
                                                </th>
                                                <th class=\"text-center\"><strong>Marque</strong>
                                                </th>
                                                 <th class=\"text-center\"><strong>Categorie</strong>
                                                </th>
                                                <th class=\"text-center\"><strong>Activate</strong>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class=\"no-bd-y\">
                                          ";
        // line 101
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            echo " 
                                              ";
            // line 102
            if (($this->getAttribute($context["j"], "valider", array()) == 0)) {
                // line 103
                echo "                                            <tr>
                                                <td>
                                                    <div class=\"div_checkbox\">
                                                        <input type=\"checkbox\">
                                                    </div>
                                                </td>
                                                <td>";
                // line 109
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nom", array()), "html", null, true);
                echo "</td>
                                                <td>";
                // line 110
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "description", array()), "html", null, true);
                echo "</td>
                                                <td class=\"text-left color-success\">";
                // line 111
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "prix", array()), "html", null, true);
                echo "</td>
                                                <td>";
                // line 112
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "qantite", array()), "html", null, true);
                echo "</td>
                                                <td class=\"color-success\"> ";
                // line 113
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "promotion", array()), "html", null, true);
                echo "</td>
                                                <td class=\"text-center\"> ";
                // line 114
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "marque", array()), "html", null, true);
                echo " </td>
                                                <td class=\"text-center\"> ";
                // line 115
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["j"], "categorie", array()), "nom", array()), "html", null, true);
                echo " </td>
                                                <td class=\"text-center\">
                                                 <div class=\"onoffswitch\">
            <a href=\"";
                // line 118
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supprimer", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
                echo "\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>
            <a href=\"";
                // line 119
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_valider_produit", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
                echo "\"><i class=\"fa fa-check-square\" aria-hidden=\"true\"></i></a>
                                                </td>
                                            </tr>
                                             ";
            }
            // line 122
            echo "   
         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    </div>
         
   
        <script src=";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
        <!-- END MANDATORY SCRIPTS -->
        <script src=";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>  
  </body>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Admin:ListeProduitsNonValides.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  278 => 151,  273 => 149,  269 => 148,  265 => 147,  261 => 146,  257 => 145,  253 => 144,  249 => 143,  245 => 142,  241 => 141,  237 => 140,  233 => 139,  229 => 138,  225 => 137,  221 => 136,  207 => 124,  200 => 122,  193 => 119,  189 => 118,  183 => 115,  179 => 114,  175 => 113,  171 => 112,  167 => 111,  163 => 110,  159 => 109,  151 => 103,  149 => 102,  143 => 101,  52 => 13,  44 => 8,  40 => 7,  36 => 6,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::baseAdmin.html.twig' %}*/
/* {% block content%} */
/*  */
/*   <html>*/
/*     <head>  */
/*         <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*        */
/* */
/* */
/*         <!-- END  MANDATORY STYLE -->*/
/*         <script src={{asset('plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js')}}></script>*/
/*     </head>*/
/*     <style>.onoffswitch {*/
/*     position: relative; width: 90px;*/
/*     -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;*/
/* }*/
/* .onoffswitch-checkbox {*/
/*     display: none;*/
/* }*/
/* .onoffswitch-label {*/
/*     display: block; overflow: hidden; cursor: pointer;*/
/*     border: 2px solid #999999; border-radius: 20px;*/
/* }*/
/* .onoffswitch-inner {*/
/*     display: block; width: 200%; margin-left: -100%;*/
/*     transition: margin 0.3s ease-in 0s;*/
/* }*/
/* .onoffswitch-inner:before, .onoffswitch-inner:after {*/
/*     display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;*/
/*     font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;*/
/*     box-sizing: border-box;*/
/* }*/
/* .onoffswitch-inner:before {*/
/*     content: "ON";*/
/*     padding-left: 10px;*/
/*     background-color: #34A7C1; color: #FFFFFF;*/
/* }*/
/* .onoffswitch-inner:after {*/
/*     content: "OFF";*/
/*     padding-right: 10px;*/
/*     background-color: #EEEEEE; color: #999999;*/
/*     text-align: right;*/
/* }*/
/* .onoffswitch-switch {*/
/*     display: block; width: 18px; margin: 6px;*/
/*     background: #FFFFFF;*/
/*     position: absolute; top: 0; bottom: 0;*/
/*     right: 56px;*/
/*     border: 2px solid #999999; border-radius: 20px;*/
/*     transition: all 0.3s ease-in 0s; */
/* }*/
/* .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {*/
/*     margin-left: 0;*/
/* }*/
/* .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {*/
/*     right: 0px; */
/* }</style>*/
/*     <body>*/
/*       <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Professional</strong> style of your data in tables</h3>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading">*/
/*                             <h3 class="panel-title"><strong>E-commerce </strong>possibilities example</h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">*/
/*                                     <table class="table table-striped table-hover">*/
/*                                         <thead class="no-bd">*/
/*                                             <tr>*/
/*                                                 <th style="width:30px;" class="div_checkbox">*/
/*                                                     <div class="div_checkbox">*/
/*                                                         <input class="toggle_checkbox" type="checkbox">*/
/*                                                     </div>*/
/*                                                 </th>*/
/*                                                 <th><strong>Nom</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Description</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Prix</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Quantité</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Promotion</strong>*/
/*                                                 </th>*/
/*                                                 <th class="text-center"><strong>Marque</strong>*/
/*                                                 </th>*/
/*                                                  <th class="text-center"><strong>Categorie</strong>*/
/*                                                 </th>*/
/*                                                 <th class="text-center"><strong>Activate</strong>*/
/*                                                 </th>*/
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         <tbody class="no-bd-y">*/
/*                                           {% for j in m %} */
/*                                               {% if( j.valider == 0) %}*/
/*                                             <tr>*/
/*                                                 <td>*/
/*                                                     <div class="div_checkbox">*/
/*                                                         <input type="checkbox">*/
/*                                                     </div>*/
/*                                                 </td>*/
/*                                                 <td>{{j.nom}}</td>*/
/*                                                 <td>{{j.description}}</td>*/
/*                                                 <td class="text-left color-success">{{j.prix}}</td>*/
/*                                                 <td>{{j.qantite}}</td>*/
/*                                                 <td class="color-success"> {{j.promotion}}</td>*/
/*                                                 <td class="text-center"> {{j.marque}} </td>*/
/*                                                 <td class="text-center"> {{j.categorie.nom}} </td>*/
/*                                                 <td class="text-center">*/
/*                                                  <div class="onoffswitch">*/
/*             <a href="{{path('supprimer',{'id':j.id})}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>*/
/*             <a href="{{path('admin_valider_produit',{'id':j.id})}}"><i class="fa fa-check-square" aria-hidden="true"></i></a>*/
/*                                                 </td>*/
/*                                             </tr>*/
/*                                              {% endif %}   */
/*          {% endfor %}*/
/*                                             */
/*                                         </tbody>*/
/*                                     </table>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*                     </div>*/
/*          */
/*    */
/*         <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*         <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*         <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*         <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*         <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*         <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*         <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*         <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*         <script src={{asset('js/mailbox.js')}}></script>*/
/*         <!-- END MANDATORY SCRIPTS -->*/
/*         <script src={{asset('js/application.js')}}></script>  */
/*   </body>*/
/*  {% endblock %}*/
