<?php

/* PiDevClientBundle:Client:compte.html.twig */
class __TwigTemplate_62d9df31f24b028a31007a4b03b00a9e0d8f20639fba536489e1e3fe7cbd215b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'syrine' => array($this, 'block_syrine'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " 
  <html>
    <head>  
      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1.0\"/>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <link href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href=";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
        <script type=\"text/javascript\" src=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
        <!-- start menu -->
        <link href=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <script type=\"text/javascript\" src=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
        <script  src=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
        <!--start slider -->
        <link rel=\"stylesheet\" href=";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
        <script src=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
        <script src=";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
        <!--end slider -->
        <script src=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>

       <!-- CSS  -->      
      <link href=";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/materialize.css"), "html", null, true);
        echo " type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>
      <!-- Font Awesome -->
      <link href=";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/font-awesome.css"), "html", null, true);
        echo " rel=\"stylesheet\">
      <!-- Skill Progress Bar -->
      <link href=";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/pro-bars.css\" rel=\"stylesheet"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" />
      <!-- Owl Carousel -->
      <link rel=\"stylesheet\" href=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/owl.carousel.css"), "html", null, true);
        echo ">
      <!-- Default Theme CSS File-->
      <link id=\"switcher\" href=";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/themes/blue-theme.css"), "html", null, true);
        echo " type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>     
      <!-- Main css File -->
      <link href=";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/style.css"), "html", null, true);
        echo " type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>
      <!-- Font -->
      <link href=\"http://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
    </head>

    <body>
      <!-- BEGAIN PRELOADER -->         
      <div id=\"preloader\">        
        <div class=\"progress\">
          <div class=\"indeterminate\"></div>
        </div>        
      </div>
      <!-- END PRELOADER -->

     
        <main role=\"main\">
          <!-- START HOME SECTION -->
          <section id=\"home\">
            <div class=\"overlay-section\">
              <div class=\"container\">
                <div class=\"row\">
                  <div class=\"col s12\">
                    <div class=\"home-inner\">
                      <h1 class=\"home-title\">Ton profile <span>";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</span></h1>
                               <ul class=\"megamenu skyblue\">          
                              <li><a class=\"btn-floating waves-effect waves-light btn-large white\" href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("recherchre");
        echo "\"><img src=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/setting.png"), "html", null, true);
        echo "><i class=\"material-icons\">play_for_work</i></a></li>
                              <li><a class=\"btn-floating waves-effect waves-light btn-large white\" href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("recherchre");
        echo "\"><img src=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/email.png"), "html", null, true);
        echo "><i class=\"material-icons\">play_for_work</i></a></li>
                              <li><a class=\"btn-floating waves-effect waves-light btn-large white\" href=\"";
        // line 62
        echo $this->env->getExtension('routing')->getPath("recherchre");
        echo "\"><img src=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/forum.png"), "html", null, true);
        echo "><i class=\"material-icons\">play_for_work</i></a></li>
                              <li><a class=\"btn-floating waves-effect waves-light btn-large white\" href=\"";
        // line 63
        echo $this->env->getExtension('routing')->getPath("recherchre");
        echo "\"><img src=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/loked.png"), "html", null, true);
        echo "><i class=\"material-icons\">play_for_work</i></a></li>
                              
                        </ul> </div> 
                      <!-- Call to About Button -->
                </div>
                  </div>  
                </div>
              </div>
            </div>
                     
      
         ";
        // line 74
        $this->displayBlock('syrine', $context, $blocks);
    }

    public function block_syrine($context, array $blocks = array())
    {
        // line 75
        echo "
          <!-- START ABOUT SECTION -->
          <section id=\"about\">
            <div class=\"container\">
              <div class=\"row\">
                <div class=\"col s12\">
                  <div class=\"about-inner\">
                    <div class=\"row\">
                      <div class=\"col s12 m4 l3\">
                        <div class=\"about-inner-left\">
                          <img class=\"profile-img\" src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
        echo "\" alt=\"Profile Image\">
                    
                        </div>
                      </div>
                      <div class=\"col s12 m8 l9\">
                        <div class=\"about-inner-right\">
                          <h3>About Me</h3>

                          <div class=\"personal-information col s12 m12 l6\">
                            <h3>Personal Information</h3>
                            <ul>
                              <li><span>Name : </span> ";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo " </li>
                              <li><span>Credit : </span>";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "credit", array()), "html", null, true);
        echo " dt </li>
                              <li><span>Email : </span>";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</li>
                              <li><a href=\"";
        // line 99
        echo $this->env->getExtension('routing')->getPath("Desactiver");
        echo "\">Desactiver compte</a></li>
                              <li><a href=\"";
        // line 100
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\">Modifier Mot de passe</a></li>
                              <li><a href=\"";
        // line 101
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
        echo "\">Modifier Coordonnées</a></li>
                            </ul>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <!-- Start Facts -->
          <section id=\"footer\">
            <div class=\"facts-overlay\">
              <div class=\"container\">
              <div class=\"row\">
                <div class=\"col s12\">
                  <div class=\"facts-inner\">
                    <div class=\"row\">
                      <div class=\"col s12 m4 l4\">
                        <div class=\"single-facts waves-effect waves-block waves-light\">
                          <i class=\"material-icons\">work</i>
                          <span class=\"counter\">329</span>
                          <span class=\"counter-text\"><a href=\"";
        // line 126
        echo $this->env->getExtension('routing')->getPath("listPanier");
        echo "\" >Votre panier</a></span>
                        </div>
                      </div>
                      
                      <div class=\"col s12 m4 l4\">
                        <div class=\"single-facts waves-effect waves-block waves-light\">
                          <i class=\"material-icons\">supervisor_account</i>
                          <span class=\"counter\">250</span>
                          <span class=\"counter-text\">Vos Forums</span>
                        </div>
                      </div>
                      <div class=\"col s12 m4 l4\">
                        <div class=\"single-facts waves-effect waves-block waves-light\">
                          <i class=\"material-icons\">redeem</i>
                          <span class=\"counter\">69</span>
                          <span class=\"counter-text\"><a href=\"";
        // line 141
        echo $this->env->getExtension('routing')->getPath("listCommande");
        echo "\" >Vos Commandes</a></span>
                        </div>
                      </div>
                    </div>
                        
                  </div>
                </div>
              </div>
                        
            </div>
            </div>
          </section>

          <!-- Start Blog -->
          <section id=\"Mes Forums\">
            <div class=\"container\">
              <div class=\"row\">
               <div class=\"col s12\">
                 <div class=\"blog-inner\">
                   <h2 class=\"title\">Mes Forums</h2>
                  
                  <!-- Start Blog area -->
                  <div class=\"blog-area\">
                    <div class=\"row\">
                      <!-- Start single blog post -->
                      <div class=\"col s12 m4 l4\">
                        <div class=\"blog-post\">
                          <div class=\"card\">
                            <div class=\"card-image\">
                              <img src=";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/img/blog1.jpg"), "html", null, true);
        echo ">     
                            </div>
                            <div class=\"card-content blog-post-content\">
                              <h2><a href=\"blog-single.html\">Awesome Post Title</a></h2>
                              <div class=\"meta-media\">
                                <div class=\"single-meta\">
                                  Post By <a href=\"#\">Admin</a>
                                </div>
                                <div class=\"single-meta\">
                                  Category : <a href=\"#\">Web/Design</a>
                                </div>
                              </div>
                              <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, 
content here.</p>
                            </div>
                            <div class=\"card-action\">
                              <a class=\"post-comment\" href=\"#\"><i class=\"material-icons\">comment</i><span>15</span></a>
                              <a class=\"readmore-btn\" href=blog-single.html\">Read More</a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Start single blog post -->
                      <div class=\"col s12 m4 l4\">
                        <div class=\"blog-post\">
                          <div class=\"card\">
                            <div class=\"card-image\">
                              <img src=";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/img/blog2.jpg"), "html", null, true);
        echo ">     
                            </div>
                            <div class=\"card-content blog-post-content\">
                              <h2><a href=";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/blog-single.html"), "html", null, true);
        echo ">Awesome Post Title</a></h2>
                              <div class=\"meta-media\">
                                <div class=\"single-meta\">
                                  Post By <a href=\"#\">Admin</a>
                                </div>
                                <div class=\"single-meta\">
                                  Category : <a href=\"#\">Web/Design</a>
                                </div>
                              </div>
                              <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, 
content here.</p>
                            </div>
                            <div class=\"card-action\">
                              <a class=\"post-comment\" href=\"#\"><i class=\"material-icons\">comment</i><span>10</span></a>
                              <a class=\"readmore-btn\" href=";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/blog-single.html"), "html", null, true);
        echo ">Read More</a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Start single blog post -->
                      <div class=\"col s12 m4 l4\">
                        <div class=\"blog-post\">
                          <div class=\"card\">
                            <div class=\"card-image\">
                              <img src=";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/img/blog3.jpg"), "html", null, true);
        echo ">     
                            </div>
                            <div class=\"card-content blog-post-content\">
                              <h2><a href=";
        // line 227
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/blog-single.html"), "html", null, true);
        echo ">Awesome Post Title</a></h2>
                              <div class=\"meta-media\">
                                <div class=\"single-meta\">
                                  Post By <a href=\"#\">Admin</a>
                                </div>
                                <div class=\"single-meta\">
                                  Category : <a href=\"#\">Web/Design</a>
                                </div>
                              </div>
                              <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, 
content here.</p>
                            </div>
                            <div class=\"card-action\">
                              <a class=\"post-comment\" href=\"#\"><i class=\"material-icons\">comment</i><span>15</span></a>
                              <a class=\"readmore-btn\" href=\"blog-single.html\">Read More</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- All Post Button -->
                    <a class=\"waves-effect waves-light btn-large allpost-btn\" href=";
        // line 248
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("blog-archive.html"), "html", null, true);
        echo ">All Post</a>
                  </div>                    
                 </div>
                </div>
              </div> 
            </div>
          </section>
          <!-- Start Footer -->
        
      <!-- jQuery Library -->
      <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-2.1.1.min.js\"></script>
      <!-- Materialize js -->
      <script type=\"text/javascript\" src=";
        // line 260
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/materialize.min.js"), "html", null, true);
        echo "></script>
      <!-- Skill Progress Bar -->
      <script src=";
        // line 262
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/appear.min.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>
      <script src=";
        // line 263
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/pro-bars.min.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>
      <!-- Owl slider -->      
      <script src=";
        // line 265
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/owl.carousel.min.js"), "html", null, true);
        echo "></script>    
      <!-- Mixit slider  -->
      <script src=\"http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js\"></script>
      <!-- counter -->
      <script src=";
        // line 269
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/waypoints.min.js"), "html", null, true);
        echo "></script>
      <script src=";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/jquery.counterup.min.js"), "html", null, true);
        echo "></script>     

      <!-- Custom Js -->
      <script src=";
        // line 273
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/custom.js"), "html", null, true);
        echo "></script>      
    </body>
  </html>
  ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:compte.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  441 => 273,  435 => 270,  431 => 269,  424 => 265,  419 => 263,  415 => 262,  410 => 260,  395 => 248,  371 => 227,  365 => 224,  352 => 214,  335 => 200,  329 => 197,  299 => 170,  267 => 141,  249 => 126,  221 => 101,  217 => 100,  213 => 99,  209 => 98,  205 => 97,  201 => 96,  187 => 85,  175 => 75,  169 => 74,  153 => 63,  147 => 62,  141 => 61,  135 => 60,  130 => 58,  104 => 35,  99 => 33,  94 => 31,  89 => 29,  84 => 27,  79 => 25,  73 => 22,  68 => 20,  64 => 19,  60 => 18,  56 => 17,  51 => 15,  47 => 14,  43 => 13,  38 => 11,  33 => 9,  29 => 8,  20 => 1,);
    }
}
/*  */
/*   <html>*/
/*     <head>  */
/*       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>*/
/*       <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*         <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*         <!-- start menu -->*/
/*         <link href={{asset ('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*         <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*         <!--start slider -->*/
/*         <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*         <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*         <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*         <script src={{asset('js/fwslider.js')}}></script>*/
/*         <!--end slider -->*/
/*         <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/* */
/*        <!-- CSS  -->      */
/*       <link href={{asset('bundles/profile/css/materialize.css')}} type="text/css" rel="stylesheet" media="screen,projection"/>*/
/*       <!-- Font Awesome -->*/
/*       <link href={{asset('bundles/profile/css/font-awesome.css')}} rel="stylesheet">*/
/*       <!-- Skill Progress Bar -->*/
/*       <link href={{asset('bundles/profile/css/pro-bars.css" rel="stylesheet')}} type="text/css" media="all" />*/
/*       <!-- Owl Carousel -->*/
/*       <link rel="stylesheet" href={{asset('bundles/profile/css/owl.carousel.css')}}>*/
/*       <!-- Default Theme CSS File-->*/
/*       <link id="switcher" href={{asset('bundles/profile/css/themes/blue-theme.css')}} type="text/css" rel="stylesheet" media="screen,projection"/>     */
/*       <!-- Main css File -->*/
/*       <link href={{asset('bundles/profile/style.css')}} type="text/css" rel="stylesheet" media="screen,projection"/>*/
/*       <!-- Font -->*/
/*       <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">*/
/*     </head>*/
/* */
/*     <body>*/
/*       <!-- BEGAIN PRELOADER -->         */
/*       <div id="preloader">        */
/*         <div class="progress">*/
/*           <div class="indeterminate"></div>*/
/*         </div>        */
/*       </div>*/
/*       <!-- END PRELOADER -->*/
/* */
/*      */
/*         <main role="main">*/
/*           <!-- START HOME SECTION -->*/
/*           <section id="home">*/
/*             <div class="overlay-section">*/
/*               <div class="container">*/
/*                 <div class="row">*/
/*                   <div class="col s12">*/
/*                     <div class="home-inner">*/
/*                       <h1 class="home-title">Ton profile <span>{{user.username}}</span></h1>*/
/*                                <ul class="megamenu skyblue">          */
/*                               <li><a class="btn-floating waves-effect waves-light btn-large white" href="{{path('recherchre')}}"><img src={{asset('images/setting.png')}}><i class="material-icons">play_for_work</i></a></li>*/
/*                               <li><a class="btn-floating waves-effect waves-light btn-large white" href="{{path('recherchre')}}"><img src={{asset('images/email.png')}}><i class="material-icons">play_for_work</i></a></li>*/
/*                               <li><a class="btn-floating waves-effect waves-light btn-large white" href="{{path('recherchre')}}"><img src={{asset('images/forum.png')}}><i class="material-icons">play_for_work</i></a></li>*/
/*                               <li><a class="btn-floating waves-effect waves-light btn-large white" href="{{path('recherchre')}}"><img src={{asset('images/loked.png')}}><i class="material-icons">play_for_work</i></a></li>*/
/*                               */
/*                         </ul> </div> */
/*                       <!-- Call to About Button -->*/
/*                 </div>*/
/*                   </div>  */
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*                      */
/*       */
/*          {% block syrine %}*/
/* */
/*           <!-- START ABOUT SECTION -->*/
/*           <section id="about">*/
/*             <div class="container">*/
/*               <div class="row">*/
/*                 <div class="col s12">*/
/*                   <div class="about-inner">*/
/*                     <div class="row">*/
/*                       <div class="col s12 m4 l3">*/
/*                         <div class="about-inner-left">*/
/*                           <img class="profile-img" src="{{asset(path('image'))}}" alt="Profile Image">*/
/*                     */
/*                         </div>*/
/*                       </div>*/
/*                       <div class="col s12 m8 l9">*/
/*                         <div class="about-inner-right">*/
/*                           <h3>About Me</h3>*/
/* */
/*                           <div class="personal-information col s12 m12 l6">*/
/*                             <h3>Personal Information</h3>*/
/*                             <ul>*/
/*                               <li><span>Name : </span> {{user.username}} </li>*/
/*                               <li><span>Credit : </span>{{user.credit}} dt </li>*/
/*                               <li><span>Email : </span>{{user.email}}</li>*/
/*                               <li><a href="{{path('Desactiver')}}">Desactiver compte</a></li>*/
/*                               <li><a href="{{path('fos_user_change_password')}}">Modifier Mot de passe</a></li>*/
/*                               <li><a href="{{path('fos_user_profile_edit')}}">Modifier Coordonnées</a></li>*/
/*                             </ul>*/
/*                           </div>*/
/* */
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*           </section>*/
/* */
/*           <!-- Start Facts -->*/
/*           <section id="footer">*/
/*             <div class="facts-overlay">*/
/*               <div class="container">*/
/*               <div class="row">*/
/*                 <div class="col s12">*/
/*                   <div class="facts-inner">*/
/*                     <div class="row">*/
/*                       <div class="col s12 m4 l4">*/
/*                         <div class="single-facts waves-effect waves-block waves-light">*/
/*                           <i class="material-icons">work</i>*/
/*                           <span class="counter">329</span>*/
/*                           <span class="counter-text"><a href="{{path('listPanier')}}" >Votre panier</a></span>*/
/*                         </div>*/
/*                       </div>*/
/*                       */
/*                       <div class="col s12 m4 l4">*/
/*                         <div class="single-facts waves-effect waves-block waves-light">*/
/*                           <i class="material-icons">supervisor_account</i>*/
/*                           <span class="counter">250</span>*/
/*                           <span class="counter-text">Vos Forums</span>*/
/*                         </div>*/
/*                       </div>*/
/*                       <div class="col s12 m4 l4">*/
/*                         <div class="single-facts waves-effect waves-block waves-light">*/
/*                           <i class="material-icons">redeem</i>*/
/*                           <span class="counter">69</span>*/
/*                           <span class="counter-text"><a href="{{path('listCommande')}}" >Vos Commandes</a></span>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/*                         */
/*                   </div>*/
/*                 </div>*/
/*               </div>*/
/*                         */
/*             </div>*/
/*             </div>*/
/*           </section>*/
/* */
/*           <!-- Start Blog -->*/
/*           <section id="Mes Forums">*/
/*             <div class="container">*/
/*               <div class="row">*/
/*                <div class="col s12">*/
/*                  <div class="blog-inner">*/
/*                    <h2 class="title">Mes Forums</h2>*/
/*                   */
/*                   <!-- Start Blog area -->*/
/*                   <div class="blog-area">*/
/*                     <div class="row">*/
/*                       <!-- Start single blog post -->*/
/*                       <div class="col s12 m4 l4">*/
/*                         <div class="blog-post">*/
/*                           <div class="card">*/
/*                             <div class="card-image">*/
/*                               <img src={{asset('bundles/profile/img/blog1.jpg')}}>     */
/*                             </div>*/
/*                             <div class="card-content blog-post-content">*/
/*                               <h2><a href="blog-single.html">Awesome Post Title</a></h2>*/
/*                               <div class="meta-media">*/
/*                                 <div class="single-meta">*/
/*                                   Post By <a href="#">Admin</a>*/
/*                                 </div>*/
/*                                 <div class="single-meta">*/
/*                                   Category : <a href="#">Web/Design</a>*/
/*                                 </div>*/
/*                               </div>*/
/*                               <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, */
/* content here.</p>*/
/*                             </div>*/
/*                             <div class="card-action">*/
/*                               <a class="post-comment" href="#"><i class="material-icons">comment</i><span>15</span></a>*/
/*                               <a class="readmore-btn" href=blog-single.html">Read More</a>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                       </div>*/
/*                       <!-- Start single blog post -->*/
/*                       <div class="col s12 m4 l4">*/
/*                         <div class="blog-post">*/
/*                           <div class="card">*/
/*                             <div class="card-image">*/
/*                               <img src={{asset('bundles/profile/img/blog2.jpg')}}>     */
/*                             </div>*/
/*                             <div class="card-content blog-post-content">*/
/*                               <h2><a href={{asset('bundles/profile/blog-single.html')}}>Awesome Post Title</a></h2>*/
/*                               <div class="meta-media">*/
/*                                 <div class="single-meta">*/
/*                                   Post By <a href="#">Admin</a>*/
/*                                 </div>*/
/*                                 <div class="single-meta">*/
/*                                   Category : <a href="#">Web/Design</a>*/
/*                                 </div>*/
/*                               </div>*/
/*                               <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, */
/* content here.</p>*/
/*                             </div>*/
/*                             <div class="card-action">*/
/*                               <a class="post-comment" href="#"><i class="material-icons">comment</i><span>10</span></a>*/
/*                               <a class="readmore-btn" href={{asset('bundles/profile/blog-single.html')}}>Read More</a>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                       </div>*/
/*                       <!-- Start single blog post -->*/
/*                       <div class="col s12 m4 l4">*/
/*                         <div class="blog-post">*/
/*                           <div class="card">*/
/*                             <div class="card-image">*/
/*                               <img src={{asset('bundles/profile/img/blog3.jpg')}}>     */
/*                             </div>*/
/*                             <div class="card-content blog-post-content">*/
/*                               <h2><a href={{asset('bundles/profile/blog-single.html')}}>Awesome Post Title</a></h2>*/
/*                               <div class="meta-media">*/
/*                                 <div class="single-meta">*/
/*                                   Post By <a href="#">Admin</a>*/
/*                                 </div>*/
/*                                 <div class="single-meta">*/
/*                                   Category : <a href="#">Web/Design</a>*/
/*                                 </div>*/
/*                               </div>*/
/*                               <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, */
/* content here.</p>*/
/*                             </div>*/
/*                             <div class="card-action">*/
/*                               <a class="post-comment" href="#"><i class="material-icons">comment</i><span>15</span></a>*/
/*                               <a class="readmore-btn" href="blog-single.html">Read More</a>*/
/*                             </div>*/
/*                           </div>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/*                     <!-- All Post Button -->*/
/*                     <a class="waves-effect waves-light btn-large allpost-btn" href={{asset('blog-archive.html')}}>All Post</a>*/
/*                   </div>                    */
/*                  </div>*/
/*                 </div>*/
/*               </div> */
/*             </div>*/
/*           </section>*/
/*           <!-- Start Footer -->*/
/*         */
/*       <!-- jQuery Library -->*/
/*       <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>*/
/*       <!-- Materialize js -->*/
/*       <script type="text/javascript" src={{asset('bundles/profile/js/materialize.min.js')}}></script>*/
/*       <!-- Skill Progress Bar -->*/
/*       <script src={{asset('bundles/profile/js/appear.min.js')}} type="text/javascript"></script>*/
/*       <script src={{asset('bundles/profile/js/pro-bars.min.js')}} type="text/javascript"></script>*/
/*       <!-- Owl slider -->      */
/*       <script src={{asset('bundles/profile/js/owl.carousel.min.js')}}></script>    */
/*       <!-- Mixit slider  -->*/
/*       <script src="http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>*/
/*       <!-- counter -->*/
/*       <script src={{asset('bundles/profile/js/waypoints.min.js')}}></script>*/
/*       <script src={{asset('bundles/profile/js/jquery.counterup.min.js')}}></script>     */
/* */
/*       <!-- Custom Js -->*/
/*       <script src={{asset('bundles/profile/js/custom.js')}}></script>      */
/*     </body>*/
/*   </html>*/
/*   {% endblock %}*/
