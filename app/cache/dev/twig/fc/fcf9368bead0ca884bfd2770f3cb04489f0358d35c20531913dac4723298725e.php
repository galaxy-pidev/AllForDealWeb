<?php

/* PiDevAdminBundle::baseAdmin.html.twig */
class __TwigTemplate_333890c3b5d01fbf787ff37b9b25542aa9d391861981727c88b070662323570c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 66
        echo "</head>

<body>

\t<!-- Fixed top -->
\t<div id=\"top\">
\t\t<div class=\"fixed\">
                    
\t\t\t<a href=\"\" alt=\"\" /></a>
\t\t\t<ul class=\"top-menu\">
\t\t\t\t<li><a class=\"fullview\"></a></li>
\t\t\t\t<li><a class=\"showmenu\"></a></li>
\t\t\t\t<li><a href=\"#\" title=\"\" class=\"messages\"></a></li>
\t\t\t\t<li class=\"dropdown\">
                                    <a class=\"user-menu\" data-toggle=\"dropdown\"><img src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/"), "html", null, true);
        echo "\" style=\"width: 20px; height: 20px\" alt=\"\" /><span>Bonjour <b class=\"caret\"></b></span></a>
\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t<li><a href=\"\" title=\"\"><i class=\"icon-user\"></i>Profil</a></li>
\t\t\t\t\t\t<li><a href=\"#\" title=\"\"><i class=\"icon-inbox\"></i>Messages<span class=\"badge badge-info\">9</span></a></li>
\t\t\t\t\t\t<li><a href=\"#\" title=\"\"><i class=\"icon-cog\"></i>Settings</a></li>
\t\t\t\t\t\t<li><a href=\"#\" title=\"\"><i class=\"icon-remove\"></i>Logout</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<!-- /fixed top -->


\t<!-- Content container -->
\t<div id=\"container\">

\t\t<!-- Sidebar -->
\t\t<div id=\"sidebar\">

\t\t\t<div class=\"sidebar-tabs\">
\t\t        <ul class=\"tabs-nav two-items\">
\t\t            <li><a href=\"#general\" title=\"\"><i class=\"icon-reorder\"></i></a></li>
\t\t            <li><a href=\"#stuff\" title=\"\"><i class=\"icon-cogs\"></i></a></li>
\t\t        </ul>

\t\t        <div id=\"general\">

\t\t\t        <!-- Sidebar user -->
\t\t\t        <div class=\"sidebar-user widget\">
\t\t\t\t\t\t<div class=\"navbar\"><div class=\"navbar-inner\"><h6>Bonjour, !</h6></div></div>
\t\t\t            <a href=\"#\" title=\"\" class=\"user\"><img src=\"\" alt=\"\" /></a>
\t\t\t            
\t\t\t        </div>
\t\t\t        <!-- /sidebar user -->

\t\t\t        

\t\t\t\t    <!-- Main navigation -->
\t\t\t        <ul class=\"navigation widget\">
\t\t\t            <li class=\"active\"><a href=\"\" title=\"\"><i class=\"icon-home\"></i>Dashboard</a></li>
\t\t\t            <li><a href=\"\" title=\"\" class=\"expand\"><i class=\"icon-reorder\"></i>Mes Articles</a>
                                        <ul>
\t\t\t                    <li><a href=\"\" title=\"\">Mes articles</a></li>
\t\t\t                    <li><a href=\"\" title=\"\">La liste des articles</a></li>
                                        </ul>
\t\t\t            </li>
\t\t\t            <li><a href=\"\" title=\"\"><i class=\"icon-signal\"></i>Statistiques</a></li>
\t\t\t            <li><a href=\"calendar.html\" title=\"\"><i class=\"icon-calendar\"></i>Calendrier</a></li>
\t\t\t            
\t\t\t            
\t\t\t        </ul>
\t\t\t        <!-- /main navigation -->

\t\t        </div>

\t\t        <div id=\"stuff\">

\t\t\t        <!-- Social stats -->
\t\t\t        <div class=\"widget\">
\t\t\t        \t<h6 class=\"widget-name\"><i class=\"icon-twitter\"></i>Social statistics</h6>
\t\t\t        \t<ul class=\"social-stats\">
\t\t\t        \t\t<li>
\t\t\t        \t\t\t<a href=\"\" title=\"\" class=\"orange-square\"><i class=\"icon-rss\"></i></a>
\t\t\t        \t\t\t
\t\t\t        \t\t</li>
\t\t\t        \t\t<li>
\t\t\t        \t\t\t<a href=\"\" title=\"\" class=\"blue-square\"><i class=\"icon-twitter\"></i></a>
\t\t\t        \t\t\t
\t\t\t        \t\t</li>
\t\t\t        \t\t<li>
\t\t\t        \t\t\t<a href=\"\" title=\"\" class=\"dark-blue-square\"><i class=\"icon-facebook\"></i></a>
\t\t\t        \t\t\t
\t\t\t        \t\t</li>
\t\t\t        \t</ul>
\t\t\t        </div>
\t\t\t        <!-- /social stats -->


                    <!-- Datepicker -->
\t\t 


\t\t\t\t   
\t\t        \t<!-- Action buttons -->
\t                
\t                <!-- /action buttons -->

\t\t\t        <!-- Tags input -->
\t\t\t\t\t
\t\t\t\t\t<!-- /tags input -->

\t\t        </div>

\t\t    </div>
\t\t</div>
\t\t<!-- /sidebar -->


\t\t<!-- Content -->
                <div id=\"content\">

\t\t    <!-- Content wrapper -->
\t\t    <div class=\"wrapper\">

\t\t\t    <!-- Breadcrumbs line -->
\t\t\t    <div class=\"crumbs\">
\t\t            <ul id=\"breadcrumbs\" class=\"breadcrumb\"> 
\t\t                <li><a href=\"\">Dashboard</a></li>
\t\t                
\t\t            </ul>
\t\t\t        
\t\t            <ul class=\"alt-buttons\">
\t\t                <li><a href=\"\" title=\"\"><i class=\"icon-signal\"></i><span>Statistiques</span></a></li>
\t\t                <li><a href=\"#\" title=\"\"><i class=\"icon-comments\"></i><span>Messages</span></a></li>
\t\t                
\t\t            </ul>
\t\t\t    </div>
\t\t\t    <!-- /breadcrumbs line -->

\t\t\t    <!-- Page header -->
\t\t\t    <div class=\"page-header\">
\t\t\t    \t<div class=\"page-title\">
\t\t\t\t    \t<h5>Dashboard</h5>
\t\t\t\t    \t<span>Bienvenue!</span>
\t\t\t    \t</div>

\t\t\t    \t<ul class=\"page-stats\">
\t\t\t    \t\t<li>
\t\t\t    \t\t\t<div class=\"showcase\">
\t\t\t    \t\t\t\t<span>Nouvelles visites</span>
\t\t\t    \t\t\t</div>
\t\t\t    \t\t\t<div id=\"total-visits\" class=\"chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
\t\t\t    \t\t</li>
\t\t\t    \t\t<li>
\t\t\t    \t\t\t<div class=\"showcase\">
\t\t\t    \t\t\t\t<span>Ventes produits</span>
\t\t\t    \t\t\t</div>
\t\t\t    \t\t\t<div id=\"balance\" class=\"chart\">20,10,8,30,42,23,41,10,11,19,16,18, 21,20,40</div>
\t\t\t    \t\t</li>
\t\t\t    \t</ul>
\t\t\t    </div>
\t\t\t    <!-- /page header -->
                
\t\t";
        // line 224
        $this->displayBlock('content', $context, $blocks);
        // line 226
        echo "                
                <!-- /content -->
\t</div>
\t<!-- /content container -->
                </div>
        </div>


\t<!-- Footer -->
\t<div id=\"footer\">
\t\t<div class=\"copyrights\">&copy;  Brought to you by AllForDeal.</div>
\t\t<ul class=\"footer-links\">
\t\t\t<li><a href=\"\" title=\"\"><i class=\"icon-cogs\"></i>Contacter l'admin</a></li>
\t\t\t<li><a href=\"\" title=\"\"><i class=\"icon-screenshot\"></i>Reporter un bug</a></li>
\t\t</ul>
\t</div>
\t<!-- /footer -->

</body>
</html>
";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "       
       
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />
<title>All for Deal |Espace Prestataire</title>
<link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
<!--[if IE 8]><link href=\"css/ie8.css\" rel=\"stylesheet\" type=\"text/css\" /><![endif]-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"></script>
<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js\"></script>
<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&amp;sensor=false\"></script>

<script type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/excanvas.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/jquery.flot.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/jquery.sparkline.min.js"), "html", null, true);
        echo "\"></script>

<script type=\"text/javascript\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.easytabs.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.collapsible.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.mousewheel.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/prettify.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.bootbox.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.colorpicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.timepicker.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.jgrowl.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.fancybox.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.elfinder.js"), "html", null, true);
        echo "\"></script>

<script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/plupload.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/plupload.html4.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/plupload.html5.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/jquery.plupload.queue.js"), "html", null, true);
        echo "\"></script>

<script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.autosize.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.inputlimiter.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.tagsinput.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.inputmask.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.listbox.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.validation.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.validationEngine-en.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.form.wizard.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.form.js"), "html", null, true);
        echo "\"></script>

<script type=\"text/javascript\" src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/tables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>

<script type=\"text/javascript\" src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/files/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

<script type=\"text/javascript\" src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/files/functions.js"), "html", null, true);
        echo "\"></script>

<script type=\"text/javascript\" src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/graph.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/chart1.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/chart2.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/chart3.js"), "html", null, true);
        echo "\"></script>

<link href=";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/zoom.css"), "html", null, true);
        echo " rel=\"stylesheet\">

";
    }

    // line 224
    public function block_content($context, array $blocks = array())
    {
        // line 225
        echo "                ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle::baseAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  404 => 225,  401 => 224,  394 => 63,  389 => 61,  385 => 60,  381 => 59,  377 => 58,  372 => 56,  367 => 54,  362 => 52,  357 => 50,  353 => 49,  349 => 48,  345 => 47,  341 => 46,  337 => 45,  333 => 44,  329 => 43,  325 => 42,  321 => 41,  317 => 40,  312 => 38,  308 => 37,  304 => 36,  300 => 35,  295 => 33,  291 => 32,  287 => 31,  283 => 30,  279 => 29,  275 => 28,  271 => 27,  267 => 26,  263 => 25,  259 => 24,  255 => 23,  250 => 21,  246 => 20,  242 => 19,  238 => 18,  227 => 10,  220 => 5,  217 => 4,  193 => 226,  191 => 224,  44 => 80,  28 => 66,  26 => 4,  21 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*     {% block head %}*/
/*        */
/*        */
/* <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />*/
/* <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />*/
/* <title>All for Deal |Espace Prestataire</title>*/
/* <link href="{{asset('bundles/backOffice/css/main.css')}}" rel="stylesheet" type="text/css" />*/
/* <!--[if IE 8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->*/
/* <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>*/
/* */
/* <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>*/
/* <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>*/
/* <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&amp;sensor=false"></script>*/
/* */
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/excanvas.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/jquery.flot.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/jquery.flot.resize.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/jquery.sparkline.min.js')}}"></script>*/
/* */
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.easytabs.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.collapsible.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.mousewheel.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/prettify.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.bootbox.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.colorpicker.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.timepicker.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.jgrowl.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.fancybox.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.fullcalendar.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.elfinder.js')}}"></script>*/
/* */
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/plupload.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/plupload.html4.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/plupload.html5.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/jquery.plupload.queue.js')}}"></script>*/
/* */
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.uniform.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.autosize.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.inputlimiter.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.tagsinput.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.inputmask.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.select2.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.listbox.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.validation.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.validationEngine-en.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.form.wizard.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.form.js')}}"></script>*/
/* */
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/tables/jquery.dataTables.min.js')}}"></script>*/
/* */
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/files/bootstrap.min.js')}}"></script>*/
/* */
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/files/functions.js')}}"></script>*/
/* */
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/graph.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/chart1.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/chart2.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/chart3.js')}}"></script>*/
/* */
/* <link href={{asset('bundles/backOffice/zoom.css')}} rel="stylesheet">*/
/* */
/* {% endblock %}*/
/* </head>*/
/* */
/* <body>*/
/* */
/* 	<!-- Fixed top -->*/
/* 	<div id="top">*/
/* 		<div class="fixed">*/
/*                     */
/* 			<a href="" alt="" /></a>*/
/* 			<ul class="top-menu">*/
/* 				<li><a class="fullview"></a></li>*/
/* 				<li><a class="showmenu"></a></li>*/
/* 				<li><a href="#" title="" class="messages"></a></li>*/
/* 				<li class="dropdown">*/
/*                                     <a class="user-menu" data-toggle="dropdown"><img src="{{asset('bundles/backOffice/')}}" style="width: 20px; height: 20px" alt="" /><span>Bonjour <b class="caret"></b></span></a>*/
/* 					<ul class="dropdown-menu">*/
/* 						<li><a href="" title=""><i class="icon-user"></i>Profil</a></li>*/
/* 						<li><a href="#" title=""><i class="icon-inbox"></i>Messages<span class="badge badge-info">9</span></a></li>*/
/* 						<li><a href="#" title=""><i class="icon-cog"></i>Settings</a></li>*/
/* 						<li><a href="#" title=""><i class="icon-remove"></i>Logout</a></li>*/
/* 					</ul>*/
/* 				</li>*/
/* 			</ul>*/
/* 		</div>*/
/* 	</div>*/
/* 	<!-- /fixed top -->*/
/* */
/* */
/* 	<!-- Content container -->*/
/* 	<div id="container">*/
/* */
/* 		<!-- Sidebar -->*/
/* 		<div id="sidebar">*/
/* */
/* 			<div class="sidebar-tabs">*/
/* 		        <ul class="tabs-nav two-items">*/
/* 		            <li><a href="#general" title=""><i class="icon-reorder"></i></a></li>*/
/* 		            <li><a href="#stuff" title=""><i class="icon-cogs"></i></a></li>*/
/* 		        </ul>*/
/* */
/* 		        <div id="general">*/
/* */
/* 			        <!-- Sidebar user -->*/
/* 			        <div class="sidebar-user widget">*/
/* 						<div class="navbar"><div class="navbar-inner"><h6>Bonjour, !</h6></div></div>*/
/* 			            <a href="#" title="" class="user"><img src="" alt="" /></a>*/
/* 			            */
/* 			        </div>*/
/* 			        <!-- /sidebar user -->*/
/* */
/* 			        */
/* */
/* 				    <!-- Main navigation -->*/
/* 			        <ul class="navigation widget">*/
/* 			            <li class="active"><a href="" title=""><i class="icon-home"></i>Dashboard</a></li>*/
/* 			            <li><a href="" title="" class="expand"><i class="icon-reorder"></i>Mes Articles</a>*/
/*                                         <ul>*/
/* 			                    <li><a href="" title="">Mes articles</a></li>*/
/* 			                    <li><a href="" title="">La liste des articles</a></li>*/
/*                                         </ul>*/
/* 			            </li>*/
/* 			            <li><a href="" title=""><i class="icon-signal"></i>Statistiques</a></li>*/
/* 			            <li><a href="calendar.html" title=""><i class="icon-calendar"></i>Calendrier</a></li>*/
/* 			            */
/* 			            */
/* 			        </ul>*/
/* 			        <!-- /main navigation -->*/
/* */
/* 		        </div>*/
/* */
/* 		        <div id="stuff">*/
/* */
/* 			        <!-- Social stats -->*/
/* 			        <div class="widget">*/
/* 			        	<h6 class="widget-name"><i class="icon-twitter"></i>Social statistics</h6>*/
/* 			        	<ul class="social-stats">*/
/* 			        		<li>*/
/* 			        			<a href="" title="" class="orange-square"><i class="icon-rss"></i></a>*/
/* 			        			*/
/* 			        		</li>*/
/* 			        		<li>*/
/* 			        			<a href="" title="" class="blue-square"><i class="icon-twitter"></i></a>*/
/* 			        			*/
/* 			        		</li>*/
/* 			        		<li>*/
/* 			        			<a href="" title="" class="dark-blue-square"><i class="icon-facebook"></i></a>*/
/* 			        			*/
/* 			        		</li>*/
/* 			        	</ul>*/
/* 			        </div>*/
/* 			        <!-- /social stats -->*/
/* */
/* */
/*                     <!-- Datepicker -->*/
/* 		 */
/* */
/* */
/* 				   */
/* 		        	<!-- Action buttons -->*/
/* 	                */
/* 	                <!-- /action buttons -->*/
/* */
/* 			        <!-- Tags input -->*/
/* 					*/
/* 					<!-- /tags input -->*/
/* */
/* 		        </div>*/
/* */
/* 		    </div>*/
/* 		</div>*/
/* 		<!-- /sidebar -->*/
/* */
/* */
/* 		<!-- Content -->*/
/*                 <div id="content">*/
/* */
/* 		    <!-- Content wrapper -->*/
/* 		    <div class="wrapper">*/
/* */
/* 			    <!-- Breadcrumbs line -->*/
/* 			    <div class="crumbs">*/
/* 		            <ul id="breadcrumbs" class="breadcrumb"> */
/* 		                <li><a href="">Dashboard</a></li>*/
/* 		                */
/* 		            </ul>*/
/* 			        */
/* 		            <ul class="alt-buttons">*/
/* 		                <li><a href="" title=""><i class="icon-signal"></i><span>Statistiques</span></a></li>*/
/* 		                <li><a href="#" title=""><i class="icon-comments"></i><span>Messages</span></a></li>*/
/* 		                */
/* 		            </ul>*/
/* 			    </div>*/
/* 			    <!-- /breadcrumbs line -->*/
/* */
/* 			    <!-- Page header -->*/
/* 			    <div class="page-header">*/
/* 			    	<div class="page-title">*/
/* 				    	<h5>Dashboard</h5>*/
/* 				    	<span>Bienvenue!</span>*/
/* 			    	</div>*/
/* */
/* 			    	<ul class="page-stats">*/
/* 			    		<li>*/
/* 			    			<div class="showcase">*/
/* 			    				<span>Nouvelles visites</span>*/
/* 			    			</div>*/
/* 			    			<div id="total-visits" class="chart">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>*/
/* 			    		</li>*/
/* 			    		<li>*/
/* 			    			<div class="showcase">*/
/* 			    				<span>Ventes produits</span>*/
/* 			    			</div>*/
/* 			    			<div id="balance" class="chart">20,10,8,30,42,23,41,10,11,19,16,18, 21,20,40</div>*/
/* 			    		</li>*/
/* 			    	</ul>*/
/* 			    </div>*/
/* 			    <!-- /page header -->*/
/*                 */
/* 		{% block content %}*/
/*                 {% endblock content %}*/
/*                 */
/*                 <!-- /content -->*/
/* 	</div>*/
/* 	<!-- /content container -->*/
/*                 </div>*/
/*         </div>*/
/* */
/* */
/* 	<!-- Footer -->*/
/* 	<div id="footer">*/
/* 		<div class="copyrights">&copy;  Brought to you by AllForDeal.</div>*/
/* 		<ul class="footer-links">*/
/* 			<li><a href="" title=""><i class="icon-cogs"></i>Contacter l'admin</a></li>*/
/* 			<li><a href="" title=""><i class="icon-screenshot"></i>Reporter un bug</a></li>*/
/* 		</ul>*/
/* 	</div>*/
/* 	<!-- /footer -->*/
/* */
/* </body>*/
/* </html>*/
/* */
