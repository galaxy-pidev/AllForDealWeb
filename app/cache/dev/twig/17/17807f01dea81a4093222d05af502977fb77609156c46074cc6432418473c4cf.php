<?php

/* PiDevClientBundle:Service:Detail.html.twig */
class __TwigTemplate_cc97b04fb067f940c70aa46eac3e34ad480b6e488ef0770606243b02a53f159b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Service:Detail.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html>
        <head>
            <title>All For Deal | Accueil </title>

            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
            <link href=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.min.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/style.css"), "html", null, true);
        echo "\" />
            <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
            <script type=\"text/javascript\" src=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
            <!-- start menu -->
            <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <script type=\"text/javascript\" src=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
            <script  src=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
            <!--start slider -->
            <link rel=\"stylesheet\" href=";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
            <script src=";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
            <script src=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/bootstrap.min.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/customjs.js"), "html", null, true);
        echo "></script>
            <link rel=\"stylesheet\" href=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/bootstrap.min.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <link rel=\"stylesheet\" href=";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/custom.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <!--end slider -->
            <link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">

            <script src=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.load.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.ready.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.slides.js"), "html", null, true);
        echo "></script>
            <!-- start details -->
            <script src=";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/slides.min.jquery.js"), "html", null, true);
        echo "></script>

            <link rel=\"stylesheet\" href=";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/etalage.css"), "html", null, true);
        echo ">
            <script src=";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.etalage.min.js"), "html", null, true);
        echo "></script>
            <!-- CSS  -->      
        </head>
        <body>



            <div class=\"mens\">    
            <div id=\"main-content\">
                    <div class=\"wrap\">
                        <div class=\"cont span_2_of_3\">                        
                            
                            <div class=\"grid images_3_of_2\">                           
                    <h5 class=\"m_1\">Rechercher un service</h5>
                    <div class=\"input-group\">

                        <input type=\"text\" name=\"search\" >

                        <button class=\"btn btn-info\" type=\"button\">
                            <i class=\"fa fa-search\"></i>
                        </button>

                    </div> <hr>
                    <h5 class=\"m_1\">Ajouter un service</h5>
                    <div class=\"panel panel-default\">

                        <div class=\"panel-body\" >

                            Si vous desirez ajouter un sujet clique au-dessous<br>
                            <center>  <a href=\"";
        // line 72
        echo $this->env->getExtension('routing')->getPath("service_ajouter");
        echo "\" >  <button type=\"submit\" action=\"";
        echo $this->env->getExtension('routing')->getPath("service_ajouter");
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-plus-square\"></i>  Service</button></a>
                            </center>   

                        </div>
                    </div>
                            </div>

                            <div class=\"desc1 span_3_of_2\">
                                <h3 class=\"m_3\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nom", array()), "html", null, true);
        echo "</h3>
                                <p class=\"m_5\">Prix: ";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "estimation", array()), "html", null, true);
        echo "DT<span class=\"reducedfrom\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "estimation", array()), "html", null, true);
        echo " DT</span> </p>

                                <div class=\"availablelhg\">
                                    <table class=\"record_properties\">
                                        <tbody>


                                            <tr>
                                                <th>Email :</th>
                                                <td>";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "Mail", array()), "html", null, true);
        echo "</td>
                                            </tr>
                                            <tr>
                                                <th>Adresse :</th>
                                                <td>";
        // line 94
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "Adresse", array()), "html", null, true);
        echo "</td>
                                            </tr>
                                            <tr>
                                                <th>Telephone :</th>
                                                <td>";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "Telephone", array()), "html", null, true);
        echo "</td>
                                            </tr>                                
                                            <tr>
                                                <th>---------------------------</th>

                                            </tr>      
                                            <tr><th>Description :</th>
                                                <td>  <p class=\"m_text2\"> ";
        // line 105
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "description", array()), "html", null, true);
        echo "</p> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>


                                <div>";
        // line 113
        echo $this->env->getExtension('nomaya_social_bar')->getSocialButtons();
        echo "</div> 
                                <div class=\"btn_form\">
                                    <br>  <hr>


                                    <hr>
                                    <form name=\"rating\" action=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("service_details", array("service" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" method=\"post\"> 
                                        <font color=\"#110D58\" size=\"4px\"> <center>  <p> <b>Notez ce service</b></p> </center></font>
                                        <div class=\"stars\">
                                            <input type=\"radio\" name=\"adresse\" class=\"star-1\" id=\"star-1\" value=\"1\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-1\" for=\"star-1\">1</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-2\" id=\"star-2\" value=\"2\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-2\" for=\"star-2\">2</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-3\" id=\"star-3\" value=\"3\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-3\" for=\"star-3\">3</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-4\" id=\"star-4\" value=\"4\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-4\" for=\"star-4\">4</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-5\" id=\"star-5\" value=\"5\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-5\" for=\"star-5\">5</label>               
                                            <span></span>              
                                        </div>   

                                        ";
        // line 135
        if (((isset($context["vote"]) ? $context["vote"] : $this->getContext($context, "vote")) == true)) {
            // line 136
            echo "                                            <center>     <p>    vous avez donnez un note ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["evaluation"]) ? $context["evaluation"] : $this->getContext($context, "evaluation")), "rating", array()), "html", null, true);
            echo "/5 <p><br>
                                                <p> si vous desirez changer votre avis vous pouvez voter de nouveau </p>
                                            </center>
                                        ";
        }
        // line 140
        echo "
                                    </form>



                                    <hr>
                                </div> 
                            </div>

                            <div class=\"clear\"></div>\t
                            <div class=\"clients\">

                                </script>
                                <script type=\"text/javascript\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.flexisel.js"), "html", null, true);
        echo "\"></script>
                            </div>
                            <div class=\"toogle\">
                                <h3 class=\"m_3\">Commentaires</h3> 
                                <div class=\"well\">
                                    <h4>Leave a Comment:</h4>
                                    <form name=\"commentaire\" action=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("service_details", array("service" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" method=\"post\">
                                        <div class=\"form-group\">
                                            ";
        // line 161
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentaire", array()), 'widget');
        echo "
                                        </div>
                                        <button type=\"submit\" action=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("service_details", array("service" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-comment\"></i> Commenter</button>
                                    </form>
                                </div>
                                <hr>
                                <div>

                                    ";
        // line 169
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 170
            echo "
                                        <h4>  <span >  <img src=";
            // line 171
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
            echo " alt=\"\" width=\"50px\" height=\"50px\"/> </span> 
                                            ";
            // line 172
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "html", null, true);
            echo ": <small>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "DateCom", array()), "Y-m-d H:i:s"), "html", null, true);
            echo "</small>
                                            ";
            // line 173
            if (($this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()) == $this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "id", array()))) {
                // line 174
                echo "                                                <span class=\"popbtn\"><a class=\"arrow\"></a></span></h4>
                                                ";
            }
            // line 176
            echo "                                        <p>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "commentaire", array()), "html", null, true);
            echo " </p> 


                                        ";
            // line 179
            if (($this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()) == $this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "id", array()))) {
                // line 180
                echo "                                            <div id=\"popover\" style=\"display: none\">
                                                <a href=\"";
                // line 181
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaireservice_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-pencil\"></span></a>
                                                <a href=\"";
                // line 182
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaireservice_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-remove\"></span></a>\t\t\t

                                            </div>



                                        ";
            }
            // line 188
            echo "  
                                        <hr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 191
        echo "
                                </div>
                            </div>

                        </div>
                        <div class=\"row\">
                            <div class=\"rsingle span_1_of_single\">


                                <h5 class=\"m_1\">Ajouter un produit</h5>
                                <div class=\"panel panel-default\">

                                    <div class=\"panel-body\" >

                                        Si vous desirez ajouter un service clique au-dessous<br>
                                        <center>  <a href=\"";
        // line 206
        echo $this->env->getExtension('routing')->getPath("service_ajouter");
        echo "\" >  <button type=\"submit\" action=\"";
        echo $this->env->getExtension('routing')->getPath("produit_create");
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-plus-square\"></i>  Produit</button></a>
                                        </center>   
                                    </div>

                                </div><hr>
                                <h5 class=\"m_1\">Avis sur ce service</h5>
                                <div>
                                    ";
        // line 213
        $context["exelent"] = 0;
        // line 214
        echo "                                        ";
        $context["bien"] = 0;
        // line 215
        echo "                                            ";
        $context["assezbien"] = 0;
        // line 216
        echo "                                                ";
        $context["moyen"] = 0;
        // line 217
        echo "                                                    ";
        $context["mauvais"] = 0;
        // line 218
        echo "                                                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["evaluations"]) ? $context["evaluations"] : $this->getContext($context, "evaluations")));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 219
            echo "
                                                            ";
            // line 220
            if (($this->getAttribute($context["e"], "rating", array()) == 5)) {
                // line 221
                echo "                                                                ";
                $context["exelent"] = ((isset($context["exelent"]) ? $context["exelent"] : $this->getContext($context, "exelent")) + 1);
                // line 222
                echo "                                                                    ";
            }
            // line 223
            echo "                                                                        ";
            if (($this->getAttribute($context["e"], "rating", array()) == 4)) {
                // line 224
                echo "                                                                            ";
                $context["bien"] = ((isset($context["bien"]) ? $context["bien"] : $this->getContext($context, "bien")) + 1);
                // line 225
                echo "                                                                                ";
            }
            // line 226
            echo "                                                                                    ";
            if (($this->getAttribute($context["e"], "rating", array()) == 3)) {
                // line 227
                echo "                                                                                        ";
                $context["assezbien"] = ((isset($context["assezbien"]) ? $context["assezbien"] : $this->getContext($context, "assezbien")) + 1);
                // line 228
                echo "                                                                                            ";
            }
            // line 229
            echo "                                                                                                ";
            if (($this->getAttribute($context["e"], "rating", array()) == 2)) {
                // line 230
                echo "                                                                                                    ";
                $context["moyen"] = ((isset($context["moyen"]) ? $context["moyen"] : $this->getContext($context, "moyen")) + 1);
                // line 231
                echo "                                                                                                        ";
            }
            // line 232
            echo "                                                                                                            ";
            if (($this->getAttribute($context["e"], "rating", array()) == 1)) {
                // line 233
                echo "                                                                                                                ";
                $context["mauvais"] = ((isset($context["mauvais"]) ? $context["mauvais"] : $this->getContext($context, "mauvais")) + 1);
                // line 234
                echo "                                                                                                                    ";
            }
            // line 235
            echo "                                                                                                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 236
        echo "
                                                                                                                           
                                                                                                                           
                                                                                                                            
                                                                                                                      
                                                                                                                     

                                                                                                                            <br>
                                                                                                                              <p> Mauvais : ";
        // line 244
        echo twig_escape_filter($this->env, (isset($context["mauvais"]) ? $context["mauvais"] : $this->getContext($context, "mauvais")), "html", null, true);
        echo " Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 20%\">
                                                                                                                                   
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                                   <p>Moyen : ";
        // line 250
        echo twig_escape_filter($this->env, (isset($context["moyen"]) ? $context["moyen"] : $this->getContext($context, "moyen")), "html", null, true);
        echo " Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">                               
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%\">
                                                                                                                                    <span class=\"sr-only\">40% Complete (success)</span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <p>Assez bien :";
        // line 256
        echo twig_escape_filter($this->env, (isset($context["assezbien"]) ? $context["assezbien"] : $this->getContext($context, "assezbien")), "html", null, true);
        echo "Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%\">
                                                                                                                                    <span class=\"sr-only\">40% Complete (success)</span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                             <p>Bien :";
        // line 262
        echo twig_escape_filter($this->env, (isset($context["bien"]) ? $context["bien"] : $this->getContext($context, "bien")), "html", null, true);
        echo " Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"80\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">
                                                                                                                                    <span class=\"sr-only\">40% Complete (success)</span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                             <p>Excelent : ";
        // line 268
        echo twig_escape_filter($this->env, (isset($context["exelent"]) ? $context["exelent"] : $this->getContext($context, "exelent")), "html", null, true);
        echo " Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%\">
                                                                                                                                    <span class=\"sr-only\">40% Complete (success)</span>
                                                                                                                                </div>
                                                                                                                            </div>

                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                        <div class=\"clear\"></div>
                    </div>
              
        


        
        </div>


        </div>
    </form>


";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Service:Detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  533 => 268,  524 => 262,  515 => 256,  506 => 250,  497 => 244,  487 => 236,  481 => 235,  478 => 234,  475 => 233,  472 => 232,  469 => 231,  466 => 230,  463 => 229,  460 => 228,  457 => 227,  454 => 226,  451 => 225,  448 => 224,  445 => 223,  442 => 222,  439 => 221,  437 => 220,  434 => 219,  429 => 218,  426 => 217,  423 => 216,  420 => 215,  417 => 214,  415 => 213,  403 => 206,  386 => 191,  378 => 188,  368 => 182,  364 => 181,  361 => 180,  359 => 179,  352 => 176,  348 => 174,  346 => 173,  340 => 172,  336 => 171,  333 => 170,  329 => 169,  320 => 163,  315 => 161,  310 => 159,  301 => 153,  286 => 140,  278 => 136,  276 => 135,  257 => 119,  248 => 113,  237 => 105,  227 => 98,  220 => 94,  213 => 90,  199 => 81,  195 => 80,  182 => 72,  150 => 43,  146 => 42,  141 => 40,  136 => 38,  132 => 37,  128 => 36,  123 => 34,  118 => 32,  114 => 31,  110 => 30,  106 => 29,  102 => 28,  98 => 27,  94 => 26,  90 => 25,  86 => 24,  81 => 22,  77 => 21,  73 => 20,  68 => 18,  63 => 16,  59 => 15,  55 => 14,  51 => 13,  47 => 12,  43 => 11,  39 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html>*/
/*         <head>*/
/*             <title>All For Deal | Accueil </title>*/
/* */
/*             <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.min.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/style.css') }}" />*/
/*             <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*             <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*             <!-- start menu -->*/
/*             <link href={{asset('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*             <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*             <!--start slider -->*/
/*             <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*             <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*             <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*             <script src={{asset('js/fwslider.js')}}></script>*/
/*             <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/bootstrap.min.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/customjs.js')}}></script>*/
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/bootstrap.min.css')}} type="text/css" media="all" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/custom.css')}} type="text/css" media="all" /> */
/*             <!--end slider -->*/
/*             <link rel="stylesheet" href="{{asset('css/style.css')}}">*/
/* */
/*             <script src={{asset('js/jquery.load.js')}}></script>*/
/*             <script src={{asset('js/jquery.ready.js')}}></script>*/
/*             <script src={{asset('js/jquery.slides.js')}}></script>*/
/*             <!-- start details -->*/
/*             <script src={{asset('js/slides.min.jquery.js')}}></script>*/
/* */
/*             <link rel="stylesheet" href={{asset('css/etalage.css')}}>*/
/*             <script src={{asset('js/jquery.etalage.min.js')}}></script>*/
/*             <!-- CSS  -->      */
/*         </head>*/
/*         <body>*/
/* */
/* */
/* */
/*             <div class="mens">    */
/*             <div id="main-content">*/
/*                     <div class="wrap">*/
/*                         <div class="cont span_2_of_3">                        */
/*                             */
/*                             <div class="grid images_3_of_2">                           */
/*                     <h5 class="m_1">Rechercher un service</h5>*/
/*                     <div class="input-group">*/
/* */
/*                         <input type="text" name="search" >*/
/* */
/*                         <button class="btn btn-info" type="button">*/
/*                             <i class="fa fa-search"></i>*/
/*                         </button>*/
/* */
/*                     </div> <hr>*/
/*                     <h5 class="m_1">Ajouter un service</h5>*/
/*                     <div class="panel panel-default">*/
/* */
/*                         <div class="panel-body" >*/
/* */
/*                             Si vous desirez ajouter un sujet clique au-dessous<br>*/
/*                             <center>  <a href="{{path('service_ajouter')}}" >  <button type="submit" action="{{path('service_ajouter')}}" class="btn btn-info"><i class="fa fa-plus-square"></i>  Service</button></a>*/
/*                             </center>   */
/* */
/*                         </div>*/
/*                     </div>*/
/*                             </div>*/
/* */
/*                             <div class="desc1 span_3_of_2">*/
/*                                 <h3 class="m_3">{{ entity.nom }}</h3>*/
/*                                 <p class="m_5">Prix: {{entity.estimation}}DT<span class="reducedfrom">{{ entity.estimation }} DT</span> </p>*/
/* */
/*                                 <div class="availablelhg">*/
/*                                     <table class="record_properties">*/
/*                                         <tbody>*/
/* */
/* */
/*                                             <tr>*/
/*                                                 <th>Email :</th>*/
/*                                                 <td>{{entity.Mail}}</td>*/
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <th>Adresse :</th>*/
/*                                                 <td>{{entity.Adresse}}</td>*/
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <th>Telephone :</th>*/
/*                                                 <td>{{entity.Telephone}}</td>*/
/*                                             </tr>                                */
/*                                             <tr>*/
/*                                                 <th>---------------------------</th>*/
/* */
/*                                             </tr>      */
/*                                             <tr><th>Description :</th>*/
/*                                                 <td>  <p class="m_text2"> {{entity.description }}</p> </td>*/
/*                                             </tr>*/
/*                                         </tbody>*/
/*                                     </table>*/
/*                                     <hr>*/
/*                                 </div>*/
/* */
/* */
/*                                 <div>{{socialButtons()}}</div> */
/*                                 <div class="btn_form">*/
/*                                     <br>  <hr>*/
/* */
/* */
/*                                     <hr>*/
/*                                     <form name="rating" action="{{path('service_details',{'service':entity.id})}}" method="post"> */
/*                                         <font color="#110D58" size="4px"> <center>  <p> <b>Notez ce service</b></p> </center></font>*/
/*                                         <div class="stars">*/
/*                                             <input type="radio" name="adresse" class="star-1" id="star-1" value="1" onclick="this.form.submit()"/>*/
/*                                             <label class="star-1" for="star-1">1</label>*/
/*                                             <input type="radio" name="adresse" class="star-2" id="star-2" value="2" onclick="this.form.submit()"/>*/
/*                                             <label class="star-2" for="star-2">2</label>*/
/*                                             <input type="radio" name="adresse" class="star-3" id="star-3" value="3" onclick="this.form.submit()"/>*/
/*                                             <label class="star-3" for="star-3">3</label>*/
/*                                             <input type="radio" name="adresse" class="star-4" id="star-4" value="4" onclick="this.form.submit()"/>*/
/*                                             <label class="star-4" for="star-4">4</label>*/
/*                                             <input type="radio" name="adresse" class="star-5" id="star-5" value="5" onclick="this.form.submit()"/>*/
/*                                             <label class="star-5" for="star-5">5</label>               */
/*                                             <span></span>              */
/*                                         </div>   */
/* */
/*                                         {% if vote==true %}*/
/*                                             <center>     <p>    vous avez donnez un note {{evaluation.rating}}/5 <p><br>*/
/*                                                 <p> si vous desirez changer votre avis vous pouvez voter de nouveau </p>*/
/*                                             </center>*/
/*                                         {% endif %}*/
/* */
/*                                     </form>*/
/* */
/* */
/* */
/*                                     <hr>*/
/*                                 </div> */
/*                             </div>*/
/* */
/*                             <div class="clear"></div>	*/
/*                             <div class="clients">*/
/* */
/*                                 </script>*/
/*                                 <script type="text/javascript" src="{{asset('js/jquery.flexisel.js')}}"></script>*/
/*                             </div>*/
/*                             <div class="toogle">*/
/*                                 <h3 class="m_3">Commentaires</h3> */
/*                                 <div class="well">*/
/*                                     <h4>Leave a Comment:</h4>*/
/*                                     <form name="commentaire" action="{{path('service_details',{'service':entity.id})}}" method="post">*/
/*                                         <div class="form-group">*/
/*                                             {{ form_widget(form.commentaire)}}*/
/*                                         </div>*/
/*                                         <button type="submit" action="{{path('service_details',{'service':entity.id})}}" class="btn btn-info"><i class="fa fa-comment"></i> Commenter</button>*/
/*                                     </form>*/
/*                                 </div>*/
/*                                 <hr>*/
/*                                 <div>*/
/* */
/*                                     {% for entity in entities %}*/
/* */
/*                                         <h4>  <span >  <img src={{asset(path('image'))}} alt="" width="50px" height="50px"/> </span> */
/*                                             {{ entity.user.username }}: <small>{{entity.DateCom|date('Y-m-d H:i:s')}}</small>*/
/*                                             {% if entity.user.id == utilisateur.id %}*/
/*                                                 <span class="popbtn"><a class="arrow"></a></span></h4>*/
/*                                                 {% endif %}*/
/*                                         <p>{{ entity.commentaire }} </p> */
/* */
/* */
/*                                         {% if entity.user.id == utilisateur.id %}*/
/*                                             <div id="popover" style="display: none">*/
/*                                                 <a href="{{path('commentaireservice_edit',{'id':entity.id})}}"><span class="glyphicon glyphicon-pencil"></span></a>*/
/*                                                 <a href="{{path('commentaireservice_delete',{'id':entity.id})}}"><span class="glyphicon glyphicon-remove"></span></a>			*/
/* */
/*                                             </div>*/
/* */
/* */
/* */
/*                                         {% endif %}  */
/*                                         <hr>*/
/*                                     {% endfor %}*/
/* */
/*                                 </div>*/
/*                             </div>*/
/* */
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="rsingle span_1_of_single">*/
/* */
/* */
/*                                 <h5 class="m_1">Ajouter un produit</h5>*/
/*                                 <div class="panel panel-default">*/
/* */
/*                                     <div class="panel-body" >*/
/* */
/*                                         Si vous desirez ajouter un service clique au-dessous<br>*/
/*                                         <center>  <a href="{{path('service_ajouter')}}" >  <button type="submit" action="{{path('produit_create')}}" class="btn btn-info"><i class="fa fa-plus-square"></i>  Produit</button></a>*/
/*                                         </center>   */
/*                                     </div>*/
/* */
/*                                 </div><hr>*/
/*                                 <h5 class="m_1">Avis sur ce service</h5>*/
/*                                 <div>*/
/*                                     {% set exelent = 0 %}*/
/*                                         {% set bien = 0 %}*/
/*                                             {% set assezbien = 0 %}*/
/*                                                 {% set  moyen= 0 %}*/
/*                                                     {% set mauvais = 0 %}*/
/*                                                         {% for e in evaluations %}*/
/* */
/*                                                             {% if e.rating==5 %}*/
/*                                                                 {% set exelent=exelent+1 %}*/
/*                                                                     {% endif %}*/
/*                                                                         {% if e.rating==4 %}*/
/*                                                                             {% set bien=bien+1 %}*/
/*                                                                                 {% endif %}*/
/*                                                                                     {% if e.rating==3 %}*/
/*                                                                                         {% set assezbien=assezbien+1 %}*/
/*                                                                                             {% endif %}*/
/*                                                                                                 {% if e.rating==2 %}*/
/*                                                                                                     {% set moyen=moyen+1 %}*/
/*                                                                                                         {% endif %}*/
/*                                                                                                             {% if e.rating==1 %}*/
/*                                                                                                                 {% set mauvais=mauvais+1 %}*/
/*                                                                                                                     {% endif %}*/
/*                                                                                                                         {% endfor %}*/
/* */
/*                                                                                                                            */
/*                                                                                                                            */
/*                                                                                                                             */
/*                                                                                                                       */
/*                                                                                                                      */
/* */
/*                                                                                                                             <br>*/
/*                                                                                                                               <p> Mauvais : {{ mauvais }} Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">*/
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">*/
/*                                                                                                                                    */
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/*                                                                                                                                    <p>Moyen : {{ moyen }} Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">                               */
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">*/
/*                                                                                                                                     <span class="sr-only">40% Complete (success)</span>*/
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/*                                                                                                                             <p>Assez bien :{{ assezbien }}Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">*/
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">*/
/*                                                                                                                                     <span class="sr-only">40% Complete (success)</span>*/
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/*                                                                                                                              <p>Bien :{{ bien }} Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">*/
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">*/
/*                                                                                                                                     <span class="sr-only">40% Complete (success)</span>*/
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/*                                                                                                                              <p>Excelent : {{ exelent }} Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">*/
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">*/
/*                                                                                                                                     <span class="sr-only">40% Complete (success)</span>*/
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/* */
/*                                                                                                                         </div>*/
/*                                                                                                                     </div>*/
/*                                                                                                                 </div>*/
/*                         <div class="clear"></div>*/
/*                     </div>*/
/*               */
/*         */
/* */
/* */
/*         */
/*         </div>*/
/* */
/* */
/*         </div>*/
/*     </form>*/
/* */
/* */
/* {% endblock %}*/
