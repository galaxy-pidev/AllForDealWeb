<?php

/* PiDevClientBundle:EvaluationProduit:easy.html.twig */
class __TwigTemplate_68b91e6fedd6a618291e8125138150aaafcadc533b177ae104ef5a5f2e8c6194 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-latest.min.js\"></script>
<script src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/tuto/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/tuto/js/precise-star-rating.js"), "html", null, true);
        echo "\"></script>
<script>
    var loader = \"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/tuto/images/loader-small.gif"), "html", null, true);
        echo "\"; //link to the animated loader-small.gif
    var ROOT_URL = \"";
        // line 6
        echo $this->env->getExtension('routing')->getUrl("tuto_homepage");
        echo "\"; //your root URL, used in autocomplete-countries.js file
</script>";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:EvaluationProduit:easy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 6,  31 => 5,  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>*/
/* <script src="{{ asset('bundles/tuto/js/jquery.cookie.js') }}"></script>*/
/* <script src="{{ asset('bundles/tuto/js/precise-star-rating.js') }}"></script>*/
/* <script>*/
/*     var loader = "{{ asset('bundles/tuto/images/loader-small.gif') }}"; //link to the animated loader-small.gif*/
/*     var ROOT_URL = "{{ url('tuto_homepage')}}"; //your root URL, used in autocomplete-countries.js file*/
/* </script>*/
