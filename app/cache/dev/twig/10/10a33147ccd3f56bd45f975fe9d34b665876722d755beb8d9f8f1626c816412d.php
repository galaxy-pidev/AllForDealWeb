<?php

/* PiDevClientBundle:image:affiche.html.twig */
class __TwigTemplate_6c44fd2a55e8524febb5b3237608ca481c057c384a5dd9e68e76f06b841a676e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<div>
";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : $this->getContext($context, "images")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 4
            echo "<img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\"/>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:image:affiche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 6,  26 => 4,  22 => 3,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <div>*/
/* {% for i in images %}*/
/* <img src="{{ asset(path('my_image_route', {'id': i.id})) }}"/>*/
/* {% endfor %}*/
/* */
/* </div>*/
