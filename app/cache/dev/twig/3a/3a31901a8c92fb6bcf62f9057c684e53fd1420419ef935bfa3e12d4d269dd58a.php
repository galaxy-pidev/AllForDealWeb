<?php

/* ::baseAdmin.html.twig */
class __TwigTemplate_567f48236dae001d606a3473e230ccab0f4449e13a6bd786d0fa52edcb5be1f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
    <head>
        ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 63
        echo "    </head>

    <body>  

        <!-- Fixed top -->
        <div id=\"top\">
            <div class=\"fixed\">

                <a href=\"\" alt=\"\" /></a>
                <ul class=\"top-menu\">
                    <li><a class=\"fullview\"></a></li>
                    <li><a class=\"showmenu\"></a></li>
                    <li><a href=\"#\" title=\"\" class=\"messages\"></a></li>
                    <li class=\"dropdown\">
                        <a class=\"user-menu\" data-toggle=\"dropdown\"><img src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/"), "html", null, true);
        echo "\" style=\"width: 20px; height: 20px\" alt=\"\" /><span>Bonjour <b class=\"caret\"></b></span></a>
                        <ul class=\"dropdown-menu\">
                            
                            <li><a href=\"";
        // line 80
        echo $this->env->getExtension('routing')->getPath("AccueilClient");
        echo "\" title=\"\"><i class=\"icon-user\"></i>Profil</a></li>
                            <li><a href=\"\" title=\"\"><i class=\"icon-user\"></i>Profil</a></li>
                            <li><a href=\"#\" title=\"\"><i class=\"icon-inbox\"></i>Messages<span class=\"badge badge-info\">9</span></a></li>
                            <li><a href=\"#\" title=\"\"><i class=\"icon-cog\"></i>Settings</a></li>
                            <li><a href=\"";
        // line 84
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\" title=\"\"><i class=\"icon-remove\"></i>Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /fixed top -->


        <!-- Content container -->
        <div id=\"container\">

            <!-- Sidebar -->
            <div id=\"sidebar\">

                <div class=\"sidebar-tabs\">
                    <ul class=\"tabs-nav two-items\">
                        <li><a href=\"#general\" title=\"\"><i class=\"icon-reorder\"></i></a></li>
                        <li><a href=\"#stuff\" title=\"\"><i class=\"icon-cogs\"></i></a></li>
                    </ul>

                    <div id=\"general\">

                        <!-- Sidebar user -->
                        <div class=\"sidebar-user widget\">
                            <div class=\"navbar\"><div class=\"navbar-inner\"><h6>Bonjour, !</h6></div></div>
                            <a href=\"#\" title=\"\" class=\"user\"><img src=\"\" alt=\"\" /></a>

                        </div>
                        <!-- /sidebar user -->
                        <!-- Main navigation -->
                        <ul class=\"navigation widget\">
                            <li class=\"active\"><a href=\"\" title=\"\"><i class=\"icon-home\"></i>Dashboard</a></li>
                            <li><a href=\"";
        // line 117
        echo $this->env->getExtension('routing')->getPath("admin_produits_nonvalidés");
        echo "\" title=\"\" class=\"expand\"><i class=\"icon-reorder\"></i>Produits non validés</a>

                            </li>
                            <li><a href=\"";
        // line 120
        echo $this->env->getExtension('routing')->getPath("admin_services_nonvalidés");
        echo "\" title=\"\" class=\"expand\"><i class=\"icon-reorder\"></i>Services non validés</a>

                            </li>
                            <li><a title=\"\" class=\"expand\"><i class=\"icon-reorder\"></i>Catégories</a>
                                <ul>
                                    <li><a href=\"";
        // line 125
        echo $this->env->getExtension('routing')->getPath("categorie_list");
        echo "\" title=\"\">Catégories produits</a></li>
                                    <li><a href=\"";
        // line 126
        echo $this->env->getExtension('routing')->getPath("categorieservice_showAll");
        echo "\" title=\"\">Catégories services</a></li>
                                </ul>
                            </li>
                            <li><a href=\"";
        // line 129
        echo $this->env->getExtension('routing')->getPath("admin_liste_utilisateurs");
        echo "\" title=\"\"><i class=\"fa fa-users\" aria-hidden=\"true\"></i>Liste des utilisateurs</a></li>
                            <li><a href=\"";
        // line 130
        echo $this->env->getExtension('routing')->getPath("publicite_showAll");
        echo "\" title=\"\"><i class=\"fa fa-volume-down\" aria-hidden=\"true\"></i>Publicite</a></li>
                            <li><a href=\"\" title=\"\"><i class=\"icon-comments\"></i>les réclamations</a></li>
                            <li><a href=\"";
        // line 132
        echo $this->env->getExtension('routing')->getPath("statistique");
        echo "\" title=\"\"><i class=\"icon-signal\"></i>Statistiques</a></li>

                        </ul>
                        <!-- /main navigation -->

                    </div>

                    <div id=\"stuff\">

                        <!-- Social stats -->
                        <div class=\"widget\">
                            <h6 class=\"widget-name\"><i class=\"icon-twitter\"></i>Social statistics</h6>
                            <ul class=\"social-stats\">
                                <li>
                                    <a href=\"\" title=\"\" class=\"orange-square\"><i class=\"icon-rss\"></i></a>

                                </li>
                                <li>
                                    <a href=\"\" title=\"\" class=\"blue-square\"><i class=\"icon-twitter\"></i></a>

                                </li>
                                <li>
                                    <a href=\"\" title=\"\" class=\"dark-blue-square\"><i class=\"icon-facebook\"></i></a>

                                </li>
                            </ul>
                        </div>
                        <!-- /social stats -->


                        <!-- Datepicker -->




                        <!-- Action buttons -->

                        <!-- /action buttons -->

                        <!-- Tags input -->

                        <!-- /tags input -->

                    </div>

                </div>
            </div>
            <!-- /sidebar -->


            <!-- Content -->
            <div id=\"content\">

                <!-- Content wrapper -->
                <div class=\"wrapper\">

                    <!-- Breadcrumbs line -->
                    <div class=\"crumbs\">
                        <ul id=\"breadcrumbs\" class=\"breadcrumb\"> 
                            <li><a href=\"\">Dashboard</a></li>

                        </ul>

                        <ul class=\"alt-buttons\">
                            <li><a href=\"\" title=\"\"><i class=\"icon-signal\"></i><span>Statistiques</span></a></li>
                            <li><a href=\"#\" title=\"\"><i class=\"icon-comments\"></i><span>Messages</span></a></li>

                        </ul>
                    </div>
                    <!-- /breadcrumbs line -->

                    <!-- Page header -->
                    <div class=\"page-header\">
                        <div class=\"page-title\">
                            <h5>Dashboard</h5>
                            <span>Bienvenue!</span>
                        </div>




                    </div>
                    <!-- /page header -->

                    ";
        // line 216
        $this->displayBlock('content', $context, $blocks);
        // line 220
        echo "

                    <!-- /content -->
                </div>
                <!-- /content container -->
            </div>
        </div>

                <!-- Footer -->
                <div id=\"footer\">
                    <div class=\"copyrights\">&copy;  Brought to you by AllForDeal.</div>
                    <ul class=\"footer-links\">
                        <li><a href=\"\" title=\"\"><i class=\"icon-cogs\"></i>Contacter l'admin</a></li>
                        <li><a href=\"\" title=\"\"><i class=\"icon-screenshot\"></i>Reporter un bug</a></li>
                    </ul>
                </div>
                <!-- /footer -->

                </body>
                </html>
";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />
            <title>All for Deal |Espace Prestataire</title>
            <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
            <!--[if IE 8]><link href=\"css/ie8.css\" rel=\"stylesheet\" type=\"text/css\" /><![endif]-->
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

            <script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"></script>
            <script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js\"></script>
            <script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&amp;sensor=false\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/excanvas.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/jquery.flot.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/jquery.sparkline.min.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.easytabs.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.collapsible.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.mousewheel.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/prettify.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.bootbox.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.colorpicker.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.timepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.jgrowl.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.fancybox.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.elfinder.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/plupload.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/plupload.html4.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/plupload.html5.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/jquery.plupload.queue.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.autosize.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.inputlimiter.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.inputmask.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.select2.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.listbox.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.validation.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.validationEngine-en.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.form.wizard.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.form.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/tables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/files/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/files/functions.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/graph.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/chart1.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/chart2.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/chart3.js"), "html", null, true);
        echo "\"></script>
    <link href=";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/zoom.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        ";
    }

    // line 216
    public function block_content($context, array $blocks = array())
    {
        // line 217
        echo "

                    ";
    }

    public function getTemplateName()
    {
        return "::baseAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  423 => 217,  420 => 216,  414 => 61,  410 => 60,  406 => 59,  402 => 58,  398 => 57,  393 => 55,  388 => 53,  383 => 51,  378 => 49,  374 => 48,  370 => 47,  366 => 46,  362 => 45,  358 => 44,  354 => 43,  350 => 42,  346 => 41,  342 => 40,  338 => 39,  333 => 37,  329 => 36,  325 => 35,  321 => 34,  316 => 32,  312 => 31,  308 => 30,  304 => 29,  300 => 28,  296 => 27,  292 => 26,  288 => 25,  284 => 24,  280 => 23,  276 => 22,  271 => 20,  267 => 19,  263 => 18,  259 => 17,  248 => 9,  242 => 5,  239 => 4,  215 => 220,  213 => 216,  126 => 132,  121 => 130,  117 => 129,  111 => 126,  107 => 125,  99 => 120,  93 => 117,  57 => 84,  50 => 80,  44 => 77,  28 => 63,  26 => 4,  21 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="fr">*/
/*     <head>*/
/*         {% block head %}*/
/* */
/*             <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />*/
/*             <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />*/
/*             <title>All for Deal |Espace Prestataire</title>*/
/*             <link href="{{asset('bundles/backOffice/css/main.css')}}" rel="stylesheet" type="text/css" />*/
/*             <!--[if IE 8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->*/
/*             <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>*/
/* */
/*             <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>*/
/*             <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>*/
/*             <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&amp;sensor=false"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/excanvas.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/jquery.flot.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/jquery.flot.resize.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/jquery.sparkline.min.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.easytabs.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.collapsible.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.mousewheel.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/prettify.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.bootbox.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.colorpicker.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.timepicker.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.jgrowl.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.fancybox.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.fullcalendar.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.elfinder.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/plupload.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/plupload.html4.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/plupload.html5.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/jquery.plupload.queue.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.uniform.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.autosize.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.inputlimiter.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.tagsinput.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.inputmask.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.select2.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.listbox.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.validation.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.validationEngine-en.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.form.wizard.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.form.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/tables/jquery.dataTables.min.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/files/bootstrap.min.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/files/functions.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/graph.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/chart1.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/chart2.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/chart3.js')}}"></script>*/
/*     <link href={{asset('bundles/backOffice/zoom.css')}} rel="stylesheet">*/
/*         {% endblock %}*/
/*     </head>*/
/* */
/*     <body>  */
/* */
/*         <!-- Fixed top -->*/
/*         <div id="top">*/
/*             <div class="fixed">*/
/* */
/*                 <a href="" alt="" /></a>*/
/*                 <ul class="top-menu">*/
/*                     <li><a class="fullview"></a></li>*/
/*                     <li><a class="showmenu"></a></li>*/
/*                     <li><a href="#" title="" class="messages"></a></li>*/
/*                     <li class="dropdown">*/
/*                         <a class="user-menu" data-toggle="dropdown"><img src="{{asset('bundles/backOffice/')}}" style="width: 20px; height: 20px" alt="" /><span>Bonjour <b class="caret"></b></span></a>*/
/*                         <ul class="dropdown-menu">*/
/*                             */
/*                             <li><a href="{{path('AccueilClient')}}" title=""><i class="icon-user"></i>Profil</a></li>*/
/*                             <li><a href="" title=""><i class="icon-user"></i>Profil</a></li>*/
/*                             <li><a href="#" title=""><i class="icon-inbox"></i>Messages<span class="badge badge-info">9</span></a></li>*/
/*                             <li><a href="#" title=""><i class="icon-cog"></i>Settings</a></li>*/
/*                             <li><a href="{{path('fos_user_security_logout')}}" title=""><i class="icon-remove"></i>Logout</a></li>*/
/*                         </ul>*/
/*                     </li>*/
/*                 </ul>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /fixed top -->*/
/* */
/* */
/*         <!-- Content container -->*/
/*         <div id="container">*/
/* */
/*             <!-- Sidebar -->*/
/*             <div id="sidebar">*/
/* */
/*                 <div class="sidebar-tabs">*/
/*                     <ul class="tabs-nav two-items">*/
/*                         <li><a href="#general" title=""><i class="icon-reorder"></i></a></li>*/
/*                         <li><a href="#stuff" title=""><i class="icon-cogs"></i></a></li>*/
/*                     </ul>*/
/* */
/*                     <div id="general">*/
/* */
/*                         <!-- Sidebar user -->*/
/*                         <div class="sidebar-user widget">*/
/*                             <div class="navbar"><div class="navbar-inner"><h6>Bonjour, !</h6></div></div>*/
/*                             <a href="#" title="" class="user"><img src="" alt="" /></a>*/
/* */
/*                         </div>*/
/*                         <!-- /sidebar user -->*/
/*                         <!-- Main navigation -->*/
/*                         <ul class="navigation widget">*/
/*                             <li class="active"><a href="" title=""><i class="icon-home"></i>Dashboard</a></li>*/
/*                             <li><a href="{{path('admin_produits_nonvalidés')}}" title="" class="expand"><i class="icon-reorder"></i>Produits non validés</a>*/
/* */
/*                             </li>*/
/*                             <li><a href="{{path('admin_services_nonvalidés')}}" title="" class="expand"><i class="icon-reorder"></i>Services non validés</a>*/
/* */
/*                             </li>*/
/*                             <li><a title="" class="expand"><i class="icon-reorder"></i>Catégories</a>*/
/*                                 <ul>*/
/*                                     <li><a href="{{path('categorie_list')}}" title="">Catégories produits</a></li>*/
/*                                     <li><a href="{{path('categorieservice_showAll')}}" title="">Catégories services</a></li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                             <li><a href="{{path('admin_liste_utilisateurs')}}" title=""><i class="fa fa-users" aria-hidden="true"></i>Liste des utilisateurs</a></li>*/
/*                             <li><a href="{{path('publicite_showAll')}}" title=""><i class="fa fa-volume-down" aria-hidden="true"></i>Publicite</a></li>*/
/*                             <li><a href="" title=""><i class="icon-comments"></i>les réclamations</a></li>*/
/*                             <li><a href="{{path('statistique')}}" title=""><i class="icon-signal"></i>Statistiques</a></li>*/
/* */
/*                         </ul>*/
/*                         <!-- /main navigation -->*/
/* */
/*                     </div>*/
/* */
/*                     <div id="stuff">*/
/* */
/*                         <!-- Social stats -->*/
/*                         <div class="widget">*/
/*                             <h6 class="widget-name"><i class="icon-twitter"></i>Social statistics</h6>*/
/*                             <ul class="social-stats">*/
/*                                 <li>*/
/*                                     <a href="" title="" class="orange-square"><i class="icon-rss"></i></a>*/
/* */
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="" title="" class="blue-square"><i class="icon-twitter"></i></a>*/
/* */
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="" title="" class="dark-blue-square"><i class="icon-facebook"></i></a>*/
/* */
/*                                 </li>*/
/*                             </ul>*/
/*                         </div>*/
/*                         <!-- /social stats -->*/
/* */
/* */
/*                         <!-- Datepicker -->*/
/* */
/* */
/* */
/* */
/*                         <!-- Action buttons -->*/
/* */
/*                         <!-- /action buttons -->*/
/* */
/*                         <!-- Tags input -->*/
/* */
/*                         <!-- /tags input -->*/
/* */
/*                     </div>*/
/* */
/*                 </div>*/
/*             </div>*/
/*             <!-- /sidebar -->*/
/* */
/* */
/*             <!-- Content -->*/
/*             <div id="content">*/
/* */
/*                 <!-- Content wrapper -->*/
/*                 <div class="wrapper">*/
/* */
/*                     <!-- Breadcrumbs line -->*/
/*                     <div class="crumbs">*/
/*                         <ul id="breadcrumbs" class="breadcrumb"> */
/*                             <li><a href="">Dashboard</a></li>*/
/* */
/*                         </ul>*/
/* */
/*                         <ul class="alt-buttons">*/
/*                             <li><a href="" title=""><i class="icon-signal"></i><span>Statistiques</span></a></li>*/
/*                             <li><a href="#" title=""><i class="icon-comments"></i><span>Messages</span></a></li>*/
/* */
/*                         </ul>*/
/*                     </div>*/
/*                     <!-- /breadcrumbs line -->*/
/* */
/*                     <!-- Page header -->*/
/*                     <div class="page-header">*/
/*                         <div class="page-title">*/
/*                             <h5>Dashboard</h5>*/
/*                             <span>Bienvenue!</span>*/
/*                         </div>*/
/* */
/* */
/* */
/* */
/*                     </div>*/
/*                     <!-- /page header -->*/
/* */
/*                     {% block content %}*/
/* */
/* */
/*                     {% endblock content %}*/
/* */
/* */
/*                     <!-- /content -->*/
/*                 </div>*/
/*                 <!-- /content container -->*/
/*             </div>*/
/*         </div>*/
/* */
/*                 <!-- Footer -->*/
/*                 <div id="footer">*/
/*                     <div class="copyrights">&copy;  Brought to you by AllForDeal.</div>*/
/*                     <ul class="footer-links">*/
/*                         <li><a href="" title=""><i class="icon-cogs"></i>Contacter l'admin</a></li>*/
/*                         <li><a href="" title=""><i class="icon-screenshot"></i>Reporter un bug</a></li>*/
/*                     </ul>*/
/*                 </div>*/
/*                 <!-- /footer -->*/
/* */
/*                 </body>*/
/*                 </html>*/
/* */
