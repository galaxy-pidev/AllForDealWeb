<?php

/* PiDevClientBundle:Commentaire:new.html.twig */
class __TwigTemplate_52bcf42534134ab5408cc4b4a9be604d312e157920d4dff362e9b38d21daccb6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Commentaire:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <head>
        <link href=";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/style.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jnotify/jNotify.jquery.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/style.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/etalage.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
        <script src=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jnotify/jNotify.jquery.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/notifications.js"), "html", null, true);
        echo "></script>
        <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/jquery.etalage.min.js"), "html", null, true);
        echo "\"></script>
        <!-- start details -->
        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/slides.min.jquery.js"), "html", null, true);
        echo "\"></script>
        <script language=\"javascript\">
            function maF(monMot) {
                alert(monMot);
            }
        </script>
        <script>
            \$(function() {
                \$('#products').slides({
                    preload: true,
                    preloadImage: 'img/loading.gif',
                    effect: 'slide, fade',
                    crossfade: true,
                    slideSpeed: 350,
                    fadeSpeed: 500,
                    generateNextPrev: true,
                    generatePagination: false
                });
            });
        </script>

        <script>
            jQuery(document).ready(function(\$) {

                \$('#etalage').etalage({
                    thumb_image_width: 360,
                    thumb_image_height: 360,
                    source_image_width: 900,
                    source_image_height: 900,
                    show_hint: true,
                    click_callback: function(image_anchor, instance_id) {
                        alert('Callback example:\\nYou clicked on an image with the anchor: \"' + image_anchor + '\"\\n(in Etalage instance: \"' + instance_id + '\")');
                    }
                });

            });
        </script>\t
    </head>

    <div class=\"main\">


        <div class=\"wrap\">

            <div class=\"cont span_2_of_3\">
                <div class=\"grid images_3_of_2\">
                    <ul id=\"etalage\">  
                        ";
        // line 64
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : $this->getContext($context, "images")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 65
            echo "                            <li>                                       
                                <a href=\"optionallink.html\">                                         
                                    <img class=\"etalage_thumb_image\"  src=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\" class=\"img-responsive\"  />                                    
                                    <img class=\"etalage_source_image\"  src=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\" class=\"img-responsive\"  />
                                </a>                                    
                            </li>

                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo " 
                        <div class=\"clearfix\"></div>
                    </ul>
                </div>
                <section >
                    ";
        // line 77
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            echo " 
                        <form name=\"rating\" action=\"";
            // line 78
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_produit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
            echo "\" method=\"post\"> 

                            <div class=\"stars\">
                                <input type=\"radio\" name=\"adresse\" class=\"star-1\" id=\"star-1\" value=\"1\" onclick=\"this.form.submit()\"/>
                                <label class=\"star-1\" for=\"star-1\">1</label>
                                <input type=\"radio\" name=\"adresse\" class=\"star-2\" id=\"star-2\" value=\"2\" onclick=\"this.form.submit()\"/>
                                <label class=\"star-2\" for=\"star-2\">2</label>
                                <input type=\"radio\" name=\"adresse\" class=\"star-3\" id=\"star-3\" value=\"3\" onclick=\"this.form.submit()\"/>
                                <label class=\"star-3\" for=\"star-3\">3</label>
                                <input type=\"radio\" name=\"adresse\" class=\"star-4\" id=\"star-4\" value=\"4\" onclick=\"this.form.submit()\"/>
                                <label class=\"star-4\" for=\"star-4\">4</label>
                                <input type=\"radio\" name=\"adresse\" class=\"star-5\" id=\"star-5\" value=\"5\" onclick=\"this.form.submit()\"/>
                                <label class=\"star-5\" for=\"star-5\">5</label>               
                                <span></span>              
                            </div>   

                        </form>
                    ";
        }
        // line 96
        echo "                </section>
                <section>
                    <div class=\"desc1 span_3_of_2\">
                        <h3 class=\"m_3\">";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nom", array()), "html", null, true);
        echo "</h3>
                        <p class=\"m_5\">Prix. ";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prix", array()), "html", null, true);
        echo " DT<span class=\"reducedfrom\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prix", array()), "html", null, true);
        echo " DT</span> </p>

                        <div class=\"availablelhg\">
                            <table class=\"record_properties\">
                                <tbody>


                                    <tr>
                                        <th>Marque</th>
                                        <td>";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "marque", array()), "html", null, true);
        echo "</td>
                                    </tr>
                                    <tr>
                                        <th>Quantite en stock:</th>
                                        <td>";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "qantite", array()), "html", null, true);
        echo "</td>
                                    </tr>
                                    <tr>
                                        <th>Promotion</th>
                                        <td>";
        // line 117
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "promotion", array()), "html", null, true);
        echo "%</td>
                                    </tr>                                
                                    <tr>
                                        <th>---------------------------</th>
                                        <td></td>
                                    </tr>                     
                                </tbody>
                            </table>

                        </div>

                        <p class=\"m_text2\">";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "description", array()), "html", null, true);
        echo "</p> 
                        <p>";
        // line 129
        echo $this->env->getExtension('nomaya_social_bar')->getSocialButtons();
        echo "</p>
                        <div class=\"btn_form\">
                            <a href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_create", array("id" => $this->getAttribute($this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "favoris", array()), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">Favoris</a>
                            <a href=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_create", array("id" => $this->getAttribute($this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "panier", array()), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">Ajouter au panier</a>\t\t\t


                        </div>
                    </div>
                </section>
                <div class=\"clear\"></div>\t



                <div class=\"clients\">

                    </script>
                    <script type=\"text/javascript\" src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.flexisel.js"), "html", null, true);
        echo "\"></script>
                </div>
                <section>
                    <div class=\"toogle\">
                        <h3 class=\"m_3\">Commentaires</h3> 
                        <table> 
                            ";
        // line 151
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 152
            echo "

                                <tr>

                                    <td>";
            // line 156
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "html", null, true);
            echo " : </td></tr>

                                <tr> <td>";
            // line 158
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "corps", array()), "html", null, true);
            echo "</td> </tr>
                                ";
            // line 159
            if (($this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()) == $this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "id", array()))) {
                // line 160
                echo "                                    <tr>   <td><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\">Modifier </a></td>
                                        <td><a href=\"";
                // line 161
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"> Supprimer</a></td>\t\t\t

                                    </tr>

                                ";
            }
            // line 165
            echo "  
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 167
        echo "                        </table>

                        <p class=\"m_text\">
                            <label class=\"control-label\">Corps<span class=\"text-error\">*</span></label>
                        <div class=\"controls\">           
                            <form  name=\"commentaire\" action=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_produit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" method=\"post\">  

                                ";
        // line 174
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Corps", array()), 'widget');
        echo "
                                <button types=\"submit\" action=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_produit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-info\" >Commenter</button>

                            </form>
                        </div>
                        </p>
                        <div class=\"form-actions align-right\">
                            ";
        // line 181
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'widget');
        echo "
                            <button types=\"submit\" action=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_produit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-info\" >Commenter</button>

                            <ul class=\"record_actions\" style=\"display: inline-block\">

                            </ul>
                        </div>
                    </div>
                </section>
            </div>

            <div class=\"clear\"></div>
        </div>
    </div>


</div>
<script src=";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 204
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 205
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 208
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
<!-- END MANDATORY SCRIPTS -->
<script src=";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commentaire:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  419 => 213,  414 => 211,  410 => 210,  406 => 209,  402 => 208,  398 => 207,  394 => 206,  390 => 205,  386 => 204,  382 => 203,  378 => 202,  374 => 201,  370 => 200,  366 => 199,  362 => 198,  343 => 182,  339 => 181,  330 => 175,  326 => 174,  321 => 172,  314 => 167,  307 => 165,  299 => 161,  294 => 160,  292 => 159,  288 => 158,  283 => 156,  277 => 152,  273 => 151,  264 => 145,  248 => 132,  244 => 131,  239 => 129,  235 => 128,  221 => 117,  214 => 113,  207 => 109,  193 => 100,  189 => 99,  184 => 96,  163 => 78,  159 => 77,  152 => 72,  141 => 68,  137 => 67,  133 => 65,  129 => 64,  79 => 17,  74 => 15,  70 => 14,  66 => 13,  62 => 12,  58 => 11,  54 => 10,  50 => 9,  46 => 8,  42 => 7,  38 => 6,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <head>*/
/*         <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/style.min.css')}} rel="stylesheet">*/
/*         <link rel="stylesheet" href="{{asset('plugins/jnotify/jNotify.jquery.css')}}">*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/style.css') }}" />*/
/*         <link rel="stylesheet" href="{{asset('css/etalage.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/style.css')}}">*/
/*         <script src={{asset('plugins/jnotify/jNotify.jquery.min.js')}}></script>*/
/*         <script src={{asset('js/notifications.js')}}></script>*/
/*         <script src="{{asset('bundles/backOffice/js/jquery.etalage.min.js')}}"></script>*/
/*         <!-- start details -->*/
/*         <script src="{{asset('js/slides.min.jquery.js')}}"></script>*/
/*         <script language="javascript">*/
/*             function maF(monMot) {*/
/*                 alert(monMot);*/
/*             }*/
/*         </script>*/
/*         <script>*/
/*             $(function() {*/
/*                 $('#products').slides({*/
/*                     preload: true,*/
/*                     preloadImage: 'img/loading.gif',*/
/*                     effect: 'slide, fade',*/
/*                     crossfade: true,*/
/*                     slideSpeed: 350,*/
/*                     fadeSpeed: 500,*/
/*                     generateNextPrev: true,*/
/*                     generatePagination: false*/
/*                 });*/
/*             });*/
/*         </script>*/
/* */
/*         <script>*/
/*             jQuery(document).ready(function($) {*/
/* */
/*                 $('#etalage').etalage({*/
/*                     thumb_image_width: 360,*/
/*                     thumb_image_height: 360,*/
/*                     source_image_width: 900,*/
/*                     source_image_height: 900,*/
/*                     show_hint: true,*/
/*                     click_callback: function(image_anchor, instance_id) {*/
/*                         alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');*/
/*                     }*/
/*                 });*/
/* */
/*             });*/
/*         </script>	*/
/*     </head>*/
/* */
/*     <div class="main">*/
/* */
/* */
/*         <div class="wrap">*/
/* */
/*             <div class="cont span_2_of_3">*/
/*                 <div class="grid images_3_of_2">*/
/*                     <ul id="etalage">  */
/*                         {% for i in images %}*/
/*                             <li>                                       */
/*                                 <a href="optionallink.html">                                         */
/*                                     <img class="etalage_thumb_image"  src="{{ asset(path('my_image_route', {'id': i.id})) }}" class="img-responsive"  />                                    */
/*                                     <img class="etalage_source_image"  src="{{ asset(path('my_image_route', {'id': i.id})) }}" class="img-responsive"  />*/
/*                                 </a>                                    */
/*                             </li>*/
/* */
/*                         {% endfor %} */
/*                         <div class="clearfix"></div>*/
/*                     </ul>*/
/*                 </div>*/
/*                 <section >*/
/*                     {% if is_granted("ROLE_USER") %} */
/*                         <form name="rating" action="{{path('commentaire_produit',{'id':entity.id})}}" method="post"> */
/* */
/*                             <div class="stars">*/
/*                                 <input type="radio" name="adresse" class="star-1" id="star-1" value="1" onclick="this.form.submit()"/>*/
/*                                 <label class="star-1" for="star-1">1</label>*/
/*                                 <input type="radio" name="adresse" class="star-2" id="star-2" value="2" onclick="this.form.submit()"/>*/
/*                                 <label class="star-2" for="star-2">2</label>*/
/*                                 <input type="radio" name="adresse" class="star-3" id="star-3" value="3" onclick="this.form.submit()"/>*/
/*                                 <label class="star-3" for="star-3">3</label>*/
/*                                 <input type="radio" name="adresse" class="star-4" id="star-4" value="4" onclick="this.form.submit()"/>*/
/*                                 <label class="star-4" for="star-4">4</label>*/
/*                                 <input type="radio" name="adresse" class="star-5" id="star-5" value="5" onclick="this.form.submit()"/>*/
/*                                 <label class="star-5" for="star-5">5</label>               */
/*                                 <span></span>              */
/*                             </div>   */
/* */
/*                         </form>*/
/*                     {% endif %}*/
/*                 </section>*/
/*                 <section>*/
/*                     <div class="desc1 span_3_of_2">*/
/*                         <h3 class="m_3">{{ entity.nom }}</h3>*/
/*                         <p class="m_5">Prix. {{ entity.prix }} DT<span class="reducedfrom">{{ entity.prix }} DT</span> </p>*/
/* */
/*                         <div class="availablelhg">*/
/*                             <table class="record_properties">*/
/*                                 <tbody>*/
/* */
/* */
/*                                     <tr>*/
/*                                         <th>Marque</th>*/
/*                                         <td>{{ entity.marque }}</td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <th>Quantite en stock:</th>*/
/*                                         <td>{{ entity.qantite }}</td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <th>Promotion</th>*/
/*                                         <td>{{ entity.promotion }}%</td>*/
/*                                     </tr>                                */
/*                                     <tr>*/
/*                                         <th>---------------------------</th>*/
/*                                         <td></td>*/
/*                                     </tr>                     */
/*                                 </tbody>*/
/*                             </table>*/
/* */
/*                         </div>*/
/* */
/*                         <p class="m_text2">{{ entity.description }}</p> */
/*                         <p>{{ socialButtons() }}</p>*/
/*                         <div class="btn_form">*/
/*                             <a href="{{path('favoris_create',{'id':utilisateur.favoris.id,'produit':entity.id})}}">Favoris</a>*/
/*                             <a href="{{path('panier_create',{'id':utilisateur.panier.id,'produit':entity.id})}}">Ajouter au panier</a>			*/
/* */
/* */
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/*                 <div class="clear"></div>	*/
/* */
/* */
/* */
/*                 <div class="clients">*/
/* */
/*                     </script>*/
/*                     <script type="text/javascript" src="{{asset('js/jquery.flexisel.js')}}"></script>*/
/*                 </div>*/
/*                 <section>*/
/*                     <div class="toogle">*/
/*                         <h3 class="m_3">Commentaires</h3> */
/*                         <table> */
/*                             {% for entity in entities %}*/
/* */
/* */
/*                                 <tr>*/
/* */
/*                                     <td>{{ entity.user.username }} : </td></tr>*/
/* */
/*                                 <tr> <td>{{ entity.corps }}</td> </tr>*/
/*                                 {% if entity.user.id == utilisateur.id %}*/
/*                                     <tr>   <td><a href="{{path('commentaire_edit',{'id':entity.id})}}">Modifier </a></td>*/
/*                                         <td><a href="{{path('commentaire_delete',{'id':entity.id})}}"> Supprimer</a></td>			*/
/* */
/*                                     </tr>*/
/* */
/*                                 {% endif %}  */
/*                             {% endfor %}*/
/*                         </table>*/
/* */
/*                         <p class="m_text">*/
/*                             <label class="control-label">Corps<span class="text-error">*</span></label>*/
/*                         <div class="controls">           */
/*                             <form  name="commentaire" action="{{path('commentaire_produit',{'id':entity.id})}}" method="post">  */
/* */
/*                                 {{ form_widget(form.Corps)}}*/
/*                                 <button types="submit" action="{{path('commentaire_produit',{'id':entity.id})}}" class="btn btn-info" >Commenter</button>*/
/* */
/*                             </form>*/
/*                         </div>*/
/*                         </p>*/
/*                         <div class="form-actions align-right">*/
/*                             {{ form_widget(form._token) }}*/
/*                             <button types="submit" action="{{path('commentaire_produit',{'id':entity.id})}}" class="btn btn-info" >Commenter</button>*/
/* */
/*                             <ul class="record_actions" style="display: inline-block">*/
/* */
/*                             </ul>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/*             </div>*/
/* */
/*             <div class="clear"></div>*/
/*         </div>*/
/*     </div>*/
/* */
/* */
/* </div>*/
/* <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/* <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/* <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/* <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/* <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/* <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/* <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/* <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/* <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/* <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/* <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/* <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/* <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/* <script src={{asset('js/mailbox.js')}}></script>*/
/* <!-- END MANDATORY SCRIPTS -->*/
/* <script src={{asset('js/application.js')}}></script>*/
/* {% endblock %}*/
