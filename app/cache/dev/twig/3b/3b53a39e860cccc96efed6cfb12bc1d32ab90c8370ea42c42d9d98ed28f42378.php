<?php

/* PiDevClientBundle:adresse:choix.html.twig */
class __TwigTemplate_64229a50a395d14cad37d5d1ba985a62bc147cd79dd72432480c0649aee1a093 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:adresse:choix.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\" lang=\"en-US\"> 
        <head profile=\"http://gmpg.org/xfn/11\"> 
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> 
            <title>CSS Form Tutorial: How to Improve Web Order Form Design</title> 
            <link rel=\"stylesheet\" href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/reset.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 


            <link rel=\"stylesheet\" href=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/style.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
        </head>

        <body>

            <div id=\"wrap\">
                <div class=\"container\">




                    <div class=\"box-170 bg-step1-blue\" ><p class=\"typo-right\"><span class=\"typo-step\">STEP</span><br />Your info</p></div>
                    <div class=\"box-170 ml-3 bg-step2-o-blue\" ><p class=\" typo-right\"><span class=\"typo-step-o\">STEP</span><br /><span class=\"typo-w\">Payment</span></p></div>
                    <div class=\"box-170 ml-3 bg-step3-o-blue\" ><p class=\"typo-right\"><span class=\"typo-step-o\">STEP</span><br /><span class=\"typo-w\">Submit order</span></p></div>
                    <div class=\"box-170 ml-3 bg-step4-o-blue\" ><p class=\"typo-right\"><span class=\"typo-step-o\">STEP</span><br /><span class=\"typo-w\">Confirmation</span></p></div>
                    <div class=\"clear\"></div>

                    <div class=\"box-719\" >
                        <div class=\"rcfg-blue pl-6 pr-6 pt-6 pb-5\">

                            <div class=\"box-660\">
                                <b class=\"rc-lightblue\"><b class=\"rc1-lightblue\"><b></b></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc3-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc5-lightblue\"></b></b>
                                <div class=\"rcfg-lightblue pl-4 pr-4 pb-4\">



                                    <p class=\"ml-3\">Veuillez completer vos informations ici ! </p> <br> <br>




                                            <!-- Form Starting -->
                                            <form  method=\"post\" action=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("choisir_adresse");
        echo "\">
                                                <fieldset>
                                                    <ul class=\"order\">

                                                        <li>
                                                            <p> <b>Adresse de Livraison <span class=\"typo-1\">*</span></b> </p>

                                                            ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["adresse"]) ? $context["adresse"] : $this->getContext($context, "adresse")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 51
            echo "
                                                                <input type=\"radio\" name=\"adresse\" value=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
            echo "\"/> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "rue", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "rue", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "ville", array()), "html", null, true);
            echo " <br> 
                                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "
                                                                <a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("adresse_create");
        echo "\">ajouter une nouvelle adresse</a><br>
                                                                    </li>
                                                                    <li>
                                                                        <label for=\"name\"> mode de livraison<span class=\"typo-1\">*</span></label>
                                                                        <select  class=\"drop1\" name=\"modeLivraison\" >
                                                                            <option value=\"0\">Par poste</option>
                                                                            <option value=\"1\">Livraison à domicile</option>
                                                                        </select>
                                                                    </li>

                                                                    </ul>
                                                                    <div class=\"clear\"></div>






                                                                    <div class=\"clear\"></div>




                                                                    <input class=\"bt-order-blue\" type=\"submit\" value=\"Next step\" />       

                                                                    <div class=\"clear\"></div>
                                                                    </fieldset>
                                                                    </form>
                                                                    <!-- Form Ending -->





                                                                    </div>
                                                                    <b class=\"rc-lightblue\"><b class=\"rc5-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc3-lightblue\"></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc1-lightblue\"><b></b></b></b>
                                                                    </div>


                                                                    <div class=\"clear\"></div>


                                                                    </div>
                                                                    <b class=\"rc-blue\"><b class=\"rc5-blue\"></b><b class=\"rc4-blue\"></b><b class=\"rc3-blue\"></b><b class=\"rc2-blue\"><b></b></b><b class=\"rc1-blue\"><b></b></b></b>
                                                                    </div>
                                                                    <div class=\"clear\"></div>




                                                                    </div>
                                                                    </div>
                                                                    </div>

                                                                    </body>
                                                                    </html>
                                                                ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:adresse:choix.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 55,  109 => 54,  95 => 52,  92 => 51,  88 => 50,  78 => 43,  43 => 11,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US"> */
/*         <head profile="http://gmpg.org/xfn/11"> */
/*             <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> */
/*             <title>CSS Form Tutorial: How to Improve Web Order Form Design</title> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/reset.css')}} type="text/css" media="all" /> */
/* */
/* */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/style.css')}} type="text/css" media="all" /> */
/*         </head>*/
/* */
/*         <body>*/
/* */
/*             <div id="wrap">*/
/*                 <div class="container">*/
/* */
/* */
/* */
/* */
/*                     <div class="box-170 bg-step1-blue" ><p class="typo-right"><span class="typo-step">STEP</span><br />Your info</p></div>*/
/*                     <div class="box-170 ml-3 bg-step2-o-blue" ><p class=" typo-right"><span class="typo-step-o">STEP</span><br /><span class="typo-w">Payment</span></p></div>*/
/*                     <div class="box-170 ml-3 bg-step3-o-blue" ><p class="typo-right"><span class="typo-step-o">STEP</span><br /><span class="typo-w">Submit order</span></p></div>*/
/*                     <div class="box-170 ml-3 bg-step4-o-blue" ><p class="typo-right"><span class="typo-step-o">STEP</span><br /><span class="typo-w">Confirmation</span></p></div>*/
/*                     <div class="clear"></div>*/
/* */
/*                     <div class="box-719" >*/
/*                         <div class="rcfg-blue pl-6 pr-6 pt-6 pb-5">*/
/* */
/*                             <div class="box-660">*/
/*                                 <b class="rc-lightblue"><b class="rc1-lightblue"><b></b></b><b class="rc2-lightblue"><b></b></b><b class="rc3-lightblue"></b><b class="rc4-lightblue"></b><b class="rc5-lightblue"></b></b>*/
/*                                 <div class="rcfg-lightblue pl-4 pr-4 pb-4">*/
/* */
/* */
/* */
/*                                     <p class="ml-3">Veuillez completer vos informations ici ! </p> <br> <br>*/
/* */
/* */
/* */
/* */
/*                                             <!-- Form Starting -->*/
/*                                             <form  method="post" action="{{path ('choisir_adresse')}}">*/
/*                                                 <fieldset>*/
/*                                                     <ul class="order">*/
/* */
/*                                                         <li>*/
/*                                                             <p> <b>Adresse de Livraison <span class="typo-1">*</span></b> </p>*/
/* */
/*                                                             {% for i in adresse %}*/
/* */
/*                                                                 <input type="radio" name="adresse" value="{{i.id}}"/> {{ i.rue}} {{i.rue}} {{i.ville}} <br> */
/*                                                                 {% endfor %}*/
/* */
/*                                                                 <a href="{{path('adresse_create')}}">ajouter une nouvelle adresse</a><br>*/
/*                                                                     </li>*/
/*                                                                     <li>*/
/*                                                                         <label for="name"> mode de livraison<span class="typo-1">*</span></label>*/
/*                                                                         <select  class="drop1" name="modeLivraison" >*/
/*                                                                             <option value="0">Par poste</option>*/
/*                                                                             <option value="1">Livraison à domicile</option>*/
/*                                                                         </select>*/
/*                                                                     </li>*/
/* */
/*                                                                     </ul>*/
/*                                                                     <div class="clear"></div>*/
/* */
/* */
/* */
/* */
/* */
/* */
/*                                                                     <div class="clear"></div>*/
/* */
/* */
/* */
/* */
/*                                                                     <input class="bt-order-blue" type="submit" value="Next step" />       */
/* */
/*                                                                     <div class="clear"></div>*/
/*                                                                     </fieldset>*/
/*                                                                     </form>*/
/*                                                                     <!-- Form Ending -->*/
/* */
/* */
/* */
/* */
/* */
/*                                                                     </div>*/
/*                                                                     <b class="rc-lightblue"><b class="rc5-lightblue"></b><b class="rc4-lightblue"></b><b class="rc3-lightblue"></b><b class="rc2-lightblue"><b></b></b><b class="rc1-lightblue"><b></b></b></b>*/
/*                                                                     </div>*/
/* */
/* */
/*                                                                     <div class="clear"></div>*/
/* */
/* */
/*                                                                     </div>*/
/*                                                                     <b class="rc-blue"><b class="rc5-blue"></b><b class="rc4-blue"></b><b class="rc3-blue"></b><b class="rc2-blue"><b></b></b><b class="rc1-blue"><b></b></b></b>*/
/*                                                                     </div>*/
/*                                                                     <div class="clear"></div>*/
/* */
/* */
/* */
/* */
/*                                                                     </div>*/
/*                                                                     </div>*/
/*                                                                     </div>*/
/* */
/*                                                                     </body>*/
/*                                                                     </html>*/
/*                                                                 {% endblock %}*/
