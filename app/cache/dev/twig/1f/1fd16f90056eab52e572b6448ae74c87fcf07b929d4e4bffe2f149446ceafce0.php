<?php

/* PiDevClientBundle:adresse:choisir.html.twig */
class __TwigTemplate_8f77bfb4f47302447ee0460b52ac27f89aa95f8be146fda272ee4e24e1ada3c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:adresse:choisir.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        echo "   
    <html>
        <head>  
<!-- styles -->
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- theme stylesheet -->
     <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/style.blue.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-stylesheet\">

    <!-- your stylesheet with modifications -->
    <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/custom.css\" rel=\"stylesheet"), "html", null, true);
        echo "\">

    <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/respond.min.js"), "html", null, true);
        echo "\"></script>

    <link rel=\"shortcut icon\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/img/favicon.png"), "html", null, true);
        echo "\">



</head>

<body>";
        // line 27
        echo "    <br>
    
 <div class=\"panel panel-info\">   
   <div id=\"content\">
            <div class=\"container\">

    

                <div class=\"col-md-9\" id=\"checkout\">

                    <div class=\"\">
                          <form  method=\"post\" action=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("choisir_adresse");
        echo "\">
                            <h1>Passer Commande</h1>
                            <ul class=\"nav nav-pills nav-justified\">
                                <li class=\"active\"><a href=\"checkout1.html\"><i class=\"fa fa fa-truck\"></i><br>Livraison</a>
                                </li>
                                <li><a href=\"checkout2.html\"><i class=\"fa fa-money\"></i><br>Payment</a>
                                </li>
                                <li><a href=\"checkout3.html\"><i class=\"fa fa-eye\"></i><br>Confirmation</a>
                                </li>
                                <li ><a href=\"#\"><i class=\"fa fa-archive\"></i><br>Facture</a></li>
                            </ul>
<div class=\"content\">
                                <div class=\"row\">
                                           ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["adresse"]) ? $context["adresse"] : $this->getContext($context, "adresse")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 52
            echo "                                    <div class=\"col-sm-6\">
                                        <div class=\"shipping-method\">

                                            <h4>Adresse : </h4>

                                            <p>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "rue", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "ville", array()), "html", null, true);
            echo ".</p>

                                            <div class=\"text-center\">

                                                <input type=\"radio\" name=\"adresse\" value=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
            echo "\">
                                            </div>
                                        </div>
                                    </div>
                                            
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                   
                      

                                </div>
                                      <a href=\"";
        // line 71
        echo $this->env->getExtension('routing')->getPath("adresse_create");
        echo "\">ajouter une nouvelle adresse</a><br>
                                      <br>
                                                  <hr>
                                                                    <label for=\"name\"> mode de livraison<span class=\"typo-1\">*</span></label>
                                                                        <select  class=\"drop1\" name=\"modeLivraison\" >
                                                                            <option value=\"0\">Par poste</option>
                                                                            <option value=\"1\">Livraison à domicile</option>
                                                                        </select>
                                                                    <br>
                                                                    <br>
                                                                    <hr>
                                <!-- /.row -->

                            </div>

                            <div class=\"\">
                                <div class=\"pull-left\">
                                    <a href=\"basket.html\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Retour au panier</a>
                                </div>
                                <div class=\"pull-right\">
                                    <button type=\"submit\" class=\"btn btn-primary\">payer votre commande<i class=\"fa fa-chevron-right\"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                                      <br>
                                      <br>
                                      <br>
                                      <br>
                                      <br>
                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class=\"col-md-3\">

             <div class=\"\" id=\"order-summary\">
                        <div class=\"box-header\">
                            <h3>Totale </h3>
                        </div>
                        <p class=\"text-muted\">la somme des prix des produits ajoutés dans votre panier .</p>

                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>Totale</td>
                                        <th> DT</th>
                                    </tr>
                                    <tr>
                                        <td>Frais supplimentaires</td>
                                        <th>10.00 DT</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>5.00 DT</th>
                                    </tr>
                                    <tr class=\"total\">
                                        <td>Total</td>
                                        <th>DT</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
    
    
 </div>
 <script src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/waypoints.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/modernizr.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap-hover-dropdown.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/front.js"), "html", null, true);
        echo "\"></script>   
</body>
    </html>
    ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:adresse:choisir.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 157,  256 => 156,  252 => 155,  248 => 154,  244 => 153,  240 => 152,  236 => 151,  232 => 150,  150 => 71,  144 => 67,  132 => 61,  123 => 57,  116 => 52,  112 => 51,  96 => 38,  83 => 27,  74 => 20,  69 => 18,  64 => 16,  58 => 13,  52 => 10,  48 => 9,  44 => 8,  40 => 7,  36 => 6,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}   */
/*     <html>*/
/*         <head>  */
/* <!-- styles -->*/
/*     <link href="{{asset('bundles/panier/css/font-awesome.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/animate.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.carousel.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.theme.css')}}" rel="stylesheet">*/
/* */
/*     <!-- theme stylesheet -->*/
/*      <link href="{{asset('bundles/panier/css/style.blue.css')}}" rel="stylesheet" id="theme-stylesheet">*/
/* */
/*     <!-- your stylesheet with modifications -->*/
/*     <link href="{{asset('bundles/panier/css/custom.css" rel="stylesheet')}}">*/
/* */
/*     <script src="{{asset('bundles/panier/js/respond.min.js')}}"></script>*/
/* */
/*     <link rel="shortcut icon" href="{{asset('bundles/panier/img/favicon.png')}}">*/
/* */
/* */
/* */
/* </head>*/
/* */
/* <body>{# empty Twig template #}*/
/*     <br>*/
/*     */
/*  <div class="panel panel-info">   */
/*    <div id="content">*/
/*             <div class="container">*/
/* */
/*     */
/* */
/*                 <div class="col-md-9" id="checkout">*/
/* */
/*                     <div class="">*/
/*                           <form  method="post" action="{{path ('choisir_adresse')}}">*/
/*                             <h1>Passer Commande</h1>*/
/*                             <ul class="nav nav-pills nav-justified">*/
/*                                 <li class="active"><a href="checkout1.html"><i class="fa fa fa-truck"></i><br>Livraison</a>*/
/*                                 </li>*/
/*                                 <li><a href="checkout2.html"><i class="fa fa-money"></i><br>Payment</a>*/
/*                                 </li>*/
/*                                 <li><a href="checkout3.html"><i class="fa fa-eye"></i><br>Confirmation</a>*/
/*                                 </li>*/
/*                                 <li ><a href="#"><i class="fa fa-archive"></i><br>Facture</a></li>*/
/*                             </ul>*/
/* <div class="content">*/
/*                                 <div class="row">*/
/*                                            {% for i in adresse %}*/
/*                                     <div class="col-sm-6">*/
/*                                         <div class="shipping-method">*/
/* */
/*                                             <h4>Adresse : </h4>*/
/* */
/*                                             <p>{{ i.rue}} {{i.ville}}.</p>*/
/* */
/*                                             <div class="text-center">*/
/* */
/*                                                 <input type="radio" name="adresse" value="{{i.id}}">*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                             */
/*                                     {% endfor %}*/
/*                    */
/*                       */
/* */
/*                                 </div>*/
/*                                       <a href="{{path('adresse_create')}}">ajouter une nouvelle adresse</a><br>*/
/*                                       <br>*/
/*                                                   <hr>*/
/*                                                                     <label for="name"> mode de livraison<span class="typo-1">*</span></label>*/
/*                                                                         <select  class="drop1" name="modeLivraison" >*/
/*                                                                             <option value="0">Par poste</option>*/
/*                                                                             <option value="1">Livraison à domicile</option>*/
/*                                                                         </select>*/
/*                                                                     <br>*/
/*                                                                     <br>*/
/*                                                                     <hr>*/
/*                                 <!-- /.row -->*/
/* */
/*                             </div>*/
/* */
/*                             <div class="">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="basket.html" class="btn btn-default"><i class="fa fa-chevron-left"></i>Retour au panier</a>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <button type="submit" class="btn btn-primary">payer votre commande<i class="fa fa-chevron-right"></i>*/
/*                                     </button>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </form>*/
/*                                       <br>*/
/*                                       <br>*/
/*                                       <br>*/
/*                                       <br>*/
/*                                       <br>*/
/*                     </div>*/
/*                     <!-- /.box -->*/
/* */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-9 -->*/
/* */
/*                 <div class="col-md-3">*/
/* */
/*              <div class="" id="order-summary">*/
/*                         <div class="box-header">*/
/*                             <h3>Totale </h3>*/
/*                         </div>*/
/*                         <p class="text-muted">la somme des prix des produits ajoutés dans votre panier .</p>*/
/* */
/*                         <div class="table-responsive">*/
/*                             <table class="table">*/
/*                                 <tbody>*/
/*                                     <tr>*/
/*                                         <td>Totale</td>*/
/*                                         <th> DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Frais supplimentaires</td>*/
/*                                         <th>10.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Tax</td>*/
/*                                         <th>5.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr class="total">*/
/*                                         <td>Total</td>*/
/*                                         <th>DT</th>*/
/*                                     </tr>*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/* */
/*                     </div>*/
/* */
/*                 </div>*/
/*                 <!-- /.col-md-3 -->*/
/* */
/*             </div>*/
/*             <!-- /.container -->*/
/*         </div>*/
/*     */
/*     */
/*  </div>*/
/*  <script src="{{asset('bundles/panier/js/jquery-1.11.0.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/jquery.cookie.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/waypoints.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/modernizr.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap-hover-dropdown.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/owl.carousel.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/front.js')}}"></script>   */
/* </body>*/
/*     </html>*/
/*     {% endblock %}*/
