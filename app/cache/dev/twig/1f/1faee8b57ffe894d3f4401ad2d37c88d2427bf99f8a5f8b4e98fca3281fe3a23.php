<?php

/* PiDevAdminBundle:Publicite:ajout.html.twig */
class __TwigTemplate_64143d51d1ea5affbf07085caedb7da1efc48d45ca086c14f131f3eb5b9a2536 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:Publicite:ajout.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    
    <!DOCTYPE html>
<html >
  <head>
    <meta charset=\"UTF-8\">
    <title>Simple Login Widget</title>
    
    
    
    <link rel='stylesheet prefetch' href='https://www.google.com/fonts#UsePlace:use/Collection:Roboto:400,300,100,500'>
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>
<link rel='stylesheet prefetch' href='https://www.google.com/fonts#UsePlace:use/Collection:Roboto+Slab:400,700,300,100'>

        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/formulaireAdmin/css/style.css"), "html", null, true);
        echo "\">
          
                <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />
            <title>All for Deal |Espace Prestataire</title>
            <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
            <!--[if IE 8]><link href=\"css/ie8.css\" rel=\"stylesheet\" type=\"text/css\" /><![endif]-->
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

            <script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"></script>
            <script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js\"></script>
            <script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&amp;sensor=false\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/excanvas.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/jquery.flot.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/charts/jquery.sparkline.min.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.easytabs.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.collapsible.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.mousewheel.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/prettify.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.bootbox.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.colorpicker.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.timepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.jgrowl.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.fancybox.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/ui/jquery.elfinder.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/plupload.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/plupload.html4.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/plupload.html5.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/uploader/jquery.plupload.queue.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.autosize.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.inputlimiter.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.inputmask.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.select2.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.listbox.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.validation.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.validationEngine-en.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.form.wizard.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/forms/jquery.form.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/plugins/tables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/files/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/files/functions.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/graph.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/chart1.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/chart2.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/charts/chart3.js"), "html", null, true);
        echo "\"></script>
    
    
    
  </head>

  <body>

  
      <div  class=\"form\">
          <form method=\"POST\" action=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("publicite_create");
        echo "\"  ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " id=\"contactform\"> 
    \t\t\t<p class=\"contact\"><label for=\"name\">Type</label></p> 
                          ";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type", array()), 'widget');
        echo "
    \t\t\t 
    \t\t\t<p class=\"contact\"><label for=\"email\">Sujet</label></p> 
    \t\t\t";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "sujet", array()), 'widget');
        echo "
                <p class=\"contact\"><label for=\"username\">Lieu </label></p> 
    \t\t\t\t";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lieu", array()), 'widget');
        echo " 
                
       
                 <label>Date</label>
                 ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'widget');
        echo "
        
            <p class=\"contact\"><label for=\"phone\">Image </label></p> 
            \t";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'widget');
        echo "
                
                ";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'widget');
        echo "
            <input class=\"buttom\" value=\"Enregistrer!\" type=\"submit\" action=\"";
        // line 99
        echo $this->env->getExtension('routing')->getPath("publicite_create");
        echo " \"> \t 
   </form> 
</div>      


</body>
</html>


    
";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Publicite:ajout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  269 => 99,  265 => 98,  260 => 96,  254 => 93,  247 => 89,  242 => 87,  236 => 84,  229 => 82,  216 => 72,  212 => 71,  208 => 70,  204 => 69,  199 => 67,  194 => 65,  189 => 63,  184 => 61,  180 => 60,  176 => 59,  172 => 58,  168 => 57,  164 => 56,  160 => 55,  156 => 54,  152 => 53,  148 => 52,  144 => 51,  139 => 49,  135 => 48,  131 => 47,  127 => 46,  122 => 44,  118 => 43,  114 => 42,  110 => 41,  106 => 40,  102 => 39,  98 => 38,  94 => 37,  90 => 36,  86 => 35,  82 => 34,  77 => 32,  73 => 31,  69 => 30,  65 => 29,  54 => 21,  46 => 16,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {%extends "::baseAdmin.html.twig"%}*/
/* {% block content %}*/
/*     */
/*     <!DOCTYPE html>*/
/* <html >*/
/*   <head>*/
/*     <meta charset="UTF-8">*/
/*     <title>Simple Login Widget</title>*/
/*     */
/*     */
/*     */
/*     <link rel='stylesheet prefetch' href='https://www.google.com/fonts#UsePlace:use/Collection:Roboto:400,300,100,500'>*/
/* <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>*/
/* <link rel='stylesheet prefetch' href='https://www.google.com/fonts#UsePlace:use/Collection:Roboto+Slab:400,700,300,100'>*/
/* */
/*         <link rel="stylesheet" href="{{asset('bundles/formulaireAdmin/css/style.css')}}">*/
/*           */
/*                 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />*/
/*             <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />*/
/*             <title>All for Deal |Espace Prestataire</title>*/
/*             <link href="{{asset('bundles/backOffice/css/main.css')}}" rel="stylesheet" type="text/css" />*/
/*             <!--[if IE 8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->*/
/*             <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>*/
/* */
/*             <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>*/
/*             <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>*/
/*             <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&amp;sensor=false"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/excanvas.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/jquery.flot.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/jquery.flot.resize.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/charts/jquery.sparkline.min.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.easytabs.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.collapsible.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.mousewheel.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/prettify.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.bootbox.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.colorpicker.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.timepicker.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.jgrowl.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.fancybox.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.fullcalendar.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/ui/jquery.elfinder.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/plupload.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/plupload.html4.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/plupload.html5.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/uploader/jquery.plupload.queue.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.uniform.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.autosize.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.inputlimiter.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.tagsinput.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.inputmask.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.select2.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.listbox.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.validation.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.validationEngine-en.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.form.wizard.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/forms/jquery.form.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/plugins/tables/jquery.dataTables.min.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/files/bootstrap.min.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/files/functions.js')}}"></script>*/
/* */
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/graph.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/chart1.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/chart2.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/backOffice/js/charts/chart3.js')}}"></script>*/
/*     */
/*     */
/*     */
/*   </head>*/
/* */
/*   <body>*/
/* */
/*   */
/*       <div  class="form">*/
/*           <form method="POST" action="{{path('publicite_create')}}"  {{ form_enctype(form) }} id="contactform"> */
/*     			<p class="contact"><label for="name">Type</label></p> */
/*                           {{ form_widget(form.type) }}*/
/*     			 */
/*     			<p class="contact"><label for="email">Sujet</label></p> */
/*     			{{form_widget(form.sujet) }}*/
/*                 <p class="contact"><label for="username">Lieu </label></p> */
/*     				{{form_widget(form.lieu) }} */
/*                 */
/*        */
/*                  <label>Date</label>*/
/*                  {{form_widget(form.date) }}*/
/*         */
/*             <p class="contact"><label for="phone">Image </label></p> */
/*             	{{form_widget(form.file) }}*/
/*                 */
/*                 {{ form_widget(form._token) }}*/
/*             <input class="buttom" value="Enregistrer!" type="submit" action="{{path('publicite_create')}} "> 	 */
/*    </form> */
/* </div>      */
/* */
/* */
/* </body>*/
/* </html>*/
/* */
/* */
/*     */
/* {% endblock %}*/
/* */
