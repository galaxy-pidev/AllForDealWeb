<?php

/* PiDevAdminBundle:Statistique:statistique.html.twig */
class __TwigTemplate_d0b7031264235ab658728ed3a4103da5e05c9e8ec2c2de1ba543dc9d227c7bf3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:Statistique:statistique.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        echo " 
             
    
<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\"
type=\"text/javascript\"></script>

<script src=\"//code.highcharts.com/4.0.1/highcharts.js\"></script>
<script src=\"//code.highcharts.com/4.0.1/modules/exporting.js\"></script>


    <div class=\"col-md-6 general-grids grids-right widget-shadow\">
        <h4 class=\"title2\"></h4>
        <ul id=\"myTabs\" class=\"nav nav-tabs\" role=\"tablist\">
            
            <li role=\"presentation\" class=\"\">
            <li role=\"presentation\" class=\"\">
                <a href=\"#produit\" id=\"produit-tab\" role=\"tab\" data-toggle=\"tab\" aria-controls=\"produit\" aria-expanded=\"false\">Produits</a></li>
            <li role=\"presentation\" class=\"\">
                <a href=\"#profile\" role=\"tab\" id=\"profile-tab\" data-toggle=\"tab\" aria-controls=\"profile\" aria-expanded=\"false\">bilan financiere</a></li>
             </ul>
        <div id=\"myTabContent\" class=\"tab-content scrollbar1\"> 
            <div role=\"tabpanel\" class=\"tab-pane fade active in\" id=\"home\" aria-labelledby=\"home-tab\"> 
            </div>
            <div role=\"tabpanel\" class=\"tab-pane fade \" id=\"produit\" aria-labelledby=\"produit-tab\"> 
                   <script type=\"text/javascript\">
        ";
        // line 27
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart"]) ? $context["chart"] : $this->getContext($context, "chart")));
        echo "
    </script> 
    <div id=\"linechart\" style=\"min-width: 400px; height: 400px; max-width: 1000px; width: 100%; margin: 0 auto\"></div>
         
            <script type=\"text/javascript\">
        ";
        // line 32
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart2"]) ? $context["chart2"] : $this->getContext($context, "chart2")));
        echo "
    </script> 
    <div id=\"piechart\" style=\"min-width: 400px; height: 400px; max-width: 1000px; width: 100%; margin: 0 auto\"></div>
            </div> 
            <div role=\"tabpanel\" class=\"tab-pane fade \" id=\"profile\" aria-labelledby=\"profile-tab\">
                 <script type=\"text/javascript\"> 
     ";
        // line 38
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart1"]) ? $context["chart1"] : $this->getContext($context, "chart1")));
        echo "
 </script> 
 <div id=\"container1\" style=\"min-width: 400px; height: 400px; max-width: 1000px; width: 100%; margin: 0 auto\"\"></div> 
       <script type=\"text/javascript\"> 
     ";
        // line 42
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart3"]) ? $context["chart3"] : $this->getContext($context, "chart3")));
        echo "
 </script> 
 <div id=\"container2\" style=\"min-width: 400px; height: 400px; max-width: 1000px; width: 100%; margin: 0 auto\"\"></div> 
 
            </div>
            <div role=\"tabpanel\" class=\"tab-pane fade\" id=\"dropdown1\" aria-labelledby=\"dropdown1-tab\"> </div>
            <div role=\"tabpanel\" class=\"tab-pane fade\" id=\"dropdown2\" aria-labelledby=\"dropdown2-tab\"></div> </div>
    </div>
    #} 

";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Statistique:statistique.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 42,  74 => 38,  65 => 32,  57 => 27,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::baseAdmin.html.twig' %}*/
/* {% block content%} */
/*              */
/*     */
/* <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"*/
/* type="text/javascript"></script>*/
/* */
/* <script src="//code.highcharts.com/4.0.1/highcharts.js"></script>*/
/* <script src="//code.highcharts.com/4.0.1/modules/exporting.js"></script>*/
/* */
/* */
/*     <div class="col-md-6 general-grids grids-right widget-shadow">*/
/*         <h4 class="title2"></h4>*/
/*         <ul id="myTabs" class="nav nav-tabs" role="tablist">*/
/*             */
/*             <li role="presentation" class="">*/
/*             <li role="presentation" class="">*/
/*                 <a href="#produit" id="produit-tab" role="tab" data-toggle="tab" aria-controls="produit" aria-expanded="false">Produits</a></li>*/
/*             <li role="presentation" class="">*/
/*                 <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">bilan financiere</a></li>*/
/*              </ul>*/
/*         <div id="myTabContent" class="tab-content scrollbar1"> */
/*             <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab"> */
/*             </div>*/
/*             <div role="tabpanel" class="tab-pane fade " id="produit" aria-labelledby="produit-tab"> */
/*                    <script type="text/javascript">*/
/*         {{ chart(chart) }}*/
/*     </script> */
/*     <div id="linechart" style="min-width: 400px; height: 400px; max-width: 1000px; width: 100%; margin: 0 auto"></div>*/
/*          */
/*             <script type="text/javascript">*/
/*         {{ chart(chart2) }}*/
/*     </script> */
/*     <div id="piechart" style="min-width: 400px; height: 400px; max-width: 1000px; width: 100%; margin: 0 auto"></div>*/
/*             </div> */
/*             <div role="tabpanel" class="tab-pane fade " id="profile" aria-labelledby="profile-tab">*/
/*                  <script type="text/javascript"> */
/*      {{ chart(chart1) }}*/
/*  </script> */
/*  <div id="container1" style="min-width: 400px; height: 400px; max-width: 1000px; width: 100%; margin: 0 auto""></div> */
/*        <script type="text/javascript"> */
/*      {{ chart(chart3) }}*/
/*  </script> */
/*  <div id="container2" style="min-width: 400px; height: 400px; max-width: 1000px; width: 100%; margin: 0 auto""></div> */
/*  */
/*             </div>*/
/*             <div role="tabpanel" class="tab-pane fade" id="dropdown1" aria-labelledby="dropdown1-tab"> </div>*/
/*             <div role="tabpanel" class="tab-pane fade" id="dropdown2" aria-labelledby="dropdown2-tab"></div> </div>*/
/*     </div>*/
/*     #} */
/* */
/* {% endblock %}*/
/* */
/* */
