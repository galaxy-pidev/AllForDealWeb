<?php

/* PiDevClientBundle:Pages:erreur.html.twig */
class __TwigTemplate_38464d4f1fd3c045fb1ae8f6e55af0e3ddc474a53af142912ffc99bc1f0bf170 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js sidebar-large lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js sidebar-large lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js sidebar-large lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=\"no-js sidebar-large\">
<!--<![endif]-->

<!-- Added by HTTrack --><meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" /><!-- /Added by HTTrack -->
<head>
    <!-- BEGIN META SECTION -->
    <meta charset=\"utf-8\">

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta content=\"\" name=\"description\" />
    <meta content=\"themes-lab\" name=\"author\" />
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/style.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <!-- END  MANDATORY STYLE -->
    <script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
        echo "\"></script>
</head>

<body class=\"error-page\">
    <div class=\"row\">
        <div class=\"col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-offset-1 col-xs-10\">
            <div class=\"error-container\">
                <div class=\"error-main\">
                    <h1> Oops.</h1>
                    <h3> The page you are trying to reach doesn't exist. </h3>
                    <h4> Go back to our site or <a href=\"email_compose.html\">contact us</a> about the problem. </h4>
                    <br>
                    <div class=\"row\">
                        <div class=\"input-icon col-md-12\">
                            <i class=\"fa fa-search\"></i>
                            <input type=\"text\" class=\"form-control\" placeholder=\"Search for page\">
                        </div>
                    </div>
                    <br>
                    <button class=\"btn btn-dark\" type=\"button\">Search</button>
                </div>
            </div>
        </div>
    </div>
    <div class=\"footer\">

    </div>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/plugins/jquery-1.11.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/plugins/nprogress/nprogress.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/js/application.js"), "html", null, true);
        echo "\"></script>
</body>

</html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Pages:erreur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 57,  100 => 56,  96 => 55,  92 => 54,  88 => 53,  56 => 24,  51 => 22,  47 => 21,  43 => 20,  39 => 19,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->*/
/* <!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->*/
/* <!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->*/
/* <!--[if gt IE 8]><!-->*/
/* <html class="no-js sidebar-large">*/
/* <!--<![endif]-->*/
/* */
/* <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->*/
/* <head>*/
/*     <!-- BEGIN META SECTION -->*/
/*     <meta charset="utf-8">*/
/* */
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <meta content="" name="description" />*/
/*     <meta content="themes-lab" name="author" />*/
/*     <!-- END META SECTION -->*/
/*     <!-- BEGIN MANDATORY STYLE -->*/
/*     <link href="{{asset('bundles/backOffice/css/icons/icons.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/backOffice/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/backOffice/css/plugins.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/backOffice/css/style.min.css')}}" rel="stylesheet">*/
/*     <!-- END  MANDATORY STYLE -->*/
/*     <script src="{{asset('bundles/backOffice/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>*/
/* </head>*/
/* */
/* <body class="error-page">*/
/*     <div class="row">*/
/*         <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-offset-1 col-xs-10">*/
/*             <div class="error-container">*/
/*                 <div class="error-main">*/
/*                     <h1> Oops.</h1>*/
/*                     <h3> The page you are trying to reach doesn't exist. </h3>*/
/*                     <h4> Go back to our site or <a href="email_compose.html">contact us</a> about the problem. </h4>*/
/*                     <br>*/
/*                     <div class="row">*/
/*                         <div class="input-icon col-md-12">*/
/*                             <i class="fa fa-search"></i>*/
/*                             <input type="text" class="form-control" placeholder="Search for page">*/
/*                         </div>*/
/*                     </div>*/
/*                     <br>*/
/*                     <button class="btn btn-dark" type="button">Search</button>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="footer">*/
/* */
/*     </div>*/
/* */
/*     <!-- Placed at the end of the document so the pages load faster -->*/
/*     <script src="{{asset('bundles/backOffice/plugins/jquery-1.11.js')}}"></script>*/
/*     <script src="{{asset('bundles/backOffice/plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/backOffice/plugins/bootstrap/bootstrap.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/backOffice/plugins/nprogress/nprogress.js')}}" type="text/javascript"></script>*/
/*     <script src="{{asset('bundles/backOffice/js/application.js')}}"></script>*/
/* </body>*/
/* */
/* </html>*/
/* */
