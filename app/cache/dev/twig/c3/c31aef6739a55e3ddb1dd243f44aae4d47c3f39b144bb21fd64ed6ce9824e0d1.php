<?php

/* PiDevClientBundle:Service:ajout.html.twig */
class __TwigTemplate_1e59b43e2671c9871797e10fbebb0a2a13a03b767ca42e72f63f1af40da26947 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Service:ajout.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html > 
        <head 
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> 
            <title>CSS Form Tutorial: How to Improve Web Order Form Design</title> 
            <link rel=\"stylesheet\" href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/reset.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <link rel=\"stylesheet\" href=";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/style.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
        </head>

        <body>

            <div id=\"wrap\">

                <div class=\"container\">





                    <div class=\"box-719\" >
                        <div class=\"rcfg-blue pl-6 pr-6 pt-6 pb-5\">

                            <div class=\"box-660\">
                                <b class=\"rc-lightblue\"><b class=\"rc1-lightblue\"><b></b></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc3-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc5-lightblue\"></b></b>
                                <div class=\"rcfg-lightblue pl-4 pr-4 pb-4\">










                                    <p class=\"ml-3\">Ajouter un nouveau Service!</p>
                                     


                                    <!-- Form Starting -->
                                     <form id=\"validate\" class=\"order\" method=\"post\" action=\"\">
                                        <fieldset>
                                            <ul class=\"order\">
                                                <li><label >Nom :<span class=\"typo-1\">*</span></label>";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "nom", array()), 'widget');
        echo "</li>
                                                <li><label >Estimation :<span class=\"typo-1\">*</span></label>";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "estimation", array()), 'widget');
        echo "</li>
                                                <li><label>Mail :<span class=\"typo-1\">*</span></label>";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Mail", array()), 'widget');
        echo "</li>
                                                <li><label>Telephone :<span class=\"typo-1\">*</span></label>";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Telephone", array()), 'widget');
        echo "</li>
                                                <li><label>Adresse :<span class=\"typo-1\">*</span></label>";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Adresse", array()), 'widget');
        echo "</li>
                                                <li><label>Description :<span class=\"typo-1\">*</span></label>";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Description", array()), 'widget');
        echo "</li>
                                                <li><label>Categorie :<span class=\"typo-1\">*</span></label>";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Categorie", array()), 'widget');
        echo "</li>
                                         </ul>
                                            <div class=\"clear\"></div>
                                            <p>NB : ce services ne sera afficher qu'aprés la validation de l'administrateur</p>
                                            <div class=\"clear\"></div>
   <div class=\"form-actions align-right\">
       ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "_token", array()), 'widget');
        echo "

                                            <input class=\"bt-order-blue\" type=\"submit\" value=\"valider\"  />
   </div>
                                            <div class=\"clear\"></div>
                                        </fieldset>
                                    </form>
                                    <!-- Form Ending -->




                                </div>
                                <b class=\"rc-lightblue\"><b class=\"rc5-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc3-lightblue\"></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc1-lightblue\"><b></b></b></b>
                            </div>


                            <div class=\"clear\"></div>


                        </div>
                        <b class=\"rc-blue\"><b class=\"rc5-blue\"></b><b class=\"rc4-blue\"></b><b class=\"rc3-blue\"></b><b class=\"rc2-blue\"><b></b></b><b class=\"rc1-blue\"><b></b></b></b>
                    </div>
                    <div class=\"clear\"></div>




                </div>
            </div>
            </div>
        </body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Service:ajout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 58,  105 => 52,  101 => 51,  97 => 50,  93 => 49,  89 => 48,  85 => 47,  81 => 46,  41 => 9,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html > */
/*         <head */
/*             <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> */
/*             <title>CSS Form Tutorial: How to Improve Web Order Form Design</title> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/reset.css')}} type="text/css" media="all" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/style.css')}} type="text/css" media="all" /> */
/*         </head>*/
/* */
/*         <body>*/
/* */
/*             <div id="wrap">*/
/* */
/*                 <div class="container">*/
/* */
/* */
/* */
/* */
/* */
/*                     <div class="box-719" >*/
/*                         <div class="rcfg-blue pl-6 pr-6 pt-6 pb-5">*/
/* */
/*                             <div class="box-660">*/
/*                                 <b class="rc-lightblue"><b class="rc1-lightblue"><b></b></b><b class="rc2-lightblue"><b></b></b><b class="rc3-lightblue"></b><b class="rc4-lightblue"></b><b class="rc5-lightblue"></b></b>*/
/*                                 <div class="rcfg-lightblue pl-4 pr-4 pb-4">*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                                     <p class="ml-3">Ajouter un nouveau Service!</p>*/
/*                                      */
/* */
/* */
/*                                     <!-- Form Starting -->*/
/*                                      <form id="validate" class="order" method="post" action="">*/
/*                                         <fieldset>*/
/*                                             <ul class="order">*/
/*                                                 <li><label >Nom :<span class="typo-1">*</span></label>{{form_widget(f.nom)}}</li>*/
/*                                                 <li><label >Estimation :<span class="typo-1">*</span></label>{{form_widget(f.estimation)}}</li>*/
/*                                                 <li><label>Mail :<span class="typo-1">*</span></label>{{form_widget(f.Mail)}}</li>*/
/*                                                 <li><label>Telephone :<span class="typo-1">*</span></label>{{form_widget(f.Telephone)}}</li>*/
/*                                                 <li><label>Adresse :<span class="typo-1">*</span></label>{{form_widget(f.Adresse)}}</li>*/
/*                                                 <li><label>Description :<span class="typo-1">*</span></label>{{form_widget(f.Description)}}</li>*/
/*                                                 <li><label>Categorie :<span class="typo-1">*</span></label>{{form_widget(f.Categorie)}}</li>*/
/*                                          </ul>*/
/*                                             <div class="clear"></div>*/
/*                                             <p>NB : ce services ne sera afficher qu'aprés la validation de l'administrateur</p>*/
/*                                             <div class="clear"></div>*/
/*    <div class="form-actions align-right">*/
/*        {{ form_widget(f._token) }}*/
/* */
/*                                             <input class="bt-order-blue" type="submit" value="valider"  />*/
/*    </div>*/
/*                                             <div class="clear"></div>*/
/*                                         </fieldset>*/
/*                                     </form>*/
/*                                     <!-- Form Ending -->*/
/* */
/* */
/* */
/* */
/*                                 </div>*/
/*                                 <b class="rc-lightblue"><b class="rc5-lightblue"></b><b class="rc4-lightblue"></b><b class="rc3-lightblue"></b><b class="rc2-lightblue"><b></b></b><b class="rc1-lightblue"><b></b></b></b>*/
/*                             </div>*/
/* */
/* */
/*                             <div class="clear"></div>*/
/* */
/* */
/*                         </div>*/
/*                         <b class="rc-blue"><b class="rc5-blue"></b><b class="rc4-blue"></b><b class="rc3-blue"></b><b class="rc2-blue"><b></b></b><b class="rc1-blue"><b></b></b></b>*/
/*                     </div>*/
/*                     <div class="clear"></div>*/
/* */
/* */
/* */
/* */
/*                 </div>*/
/*             </div>*/
/*             </div>*/
/*         </body>*/
/*     </html>*/
/* {% endblock %}*/
