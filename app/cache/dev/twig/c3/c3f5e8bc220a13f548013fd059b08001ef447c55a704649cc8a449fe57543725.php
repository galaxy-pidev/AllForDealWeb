<?php

/* ::base.html.twig */
class __TwigTemplate_2eef3b9083f32b27e57059b670df4f30ec06a3b2fbde0ab8a7ccc8331791d94c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'slider' => array($this, 'block_slider'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        <title>All For Deal | Accueil </title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <link href=";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
         <link href=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href=";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
        <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
        <script type=\"text/javascript\" src=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
        <!-- start menu -->
        <link href=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <script type=\"text/javascript\" src=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
        <script  src=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
        <!--start slider -->
        <link rel=\"stylesheet\" href=";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
        <script src=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
        <script src=";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
        <!--end slider -->
        <script src=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>
    </head>
    <body>
    <div class=\"header-top\">
        <div class=\"wrap\"> 
            <div class=\"header-top-left\">
                <div class=\"box\">
 
                </div>
                <div class=\"clear\"></div>
            </div>
            <div class=\"cssmenu\">
                <ul>
                   ";
        // line 35
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            echo "  
                        <li class=\"active\"><a href=\"";
            // line 36
            echo $this->env->getExtension('routing')->getPath("Profile");
            echo "\">Compte</a></li> |  
                        <li><a href=\"";
            // line 37
            echo $this->env->getExtension('routing')->getPath("favoris_show", array("id" => 4));
            echo "\">Favoris</a></li> |
                        <li><a href=\"";
            // line 38
            echo $this->env->getExtension('routing')->getPath("panier_show", array("id" => 1));
            echo "\">Panier</a></li> |
                        <li><a href=\"";
            // line 39
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">Se déconnecter</a></li> |
                       
                        ";
        } else {
            // line 42
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\">Se connecter</a></li> |
                        <li><a href=\"";
            // line 43
            echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
            echo "\">S'inscrire</a></li>
                        ";
        }
        // line 45
        echo "                </ul>
            </div>
            <div class=\"clear\"></div>
        </div>
    </div>
    <div class=\"header-bottom\">
        <div class=\"wrap\">
            <div class=\"header-bottom-left\">
                <div class=\"logo\">
                    <a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("produit");
        echo "\"><img src=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo.png"), "html", null, true);
        echo " alt=\"\"/></a>
                </div>
                <div class=\"menu\">
                      ";
        // line 57
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            echo "  
                    <ul class=\"megamenu skyblue\">
                        <li class=\"active grid\"><a href=\"";
            // line 59
            echo $this->env->getExtension('routing')->getPath("AccueilClient");
            echo "\">Accueil</a></li>
                        
                        <li><a class=\"color4\" href=\"";
            // line 61
            echo $this->env->getExtension('routing')->getPath("produit");
            echo "\">Produits</a>
                            <div class=\"megapanel\">
                                <div class=\"row\">
                                    <div class=\"col1\">
                                        <div class=\"h_nav\">
                                            <h4>Catégorie 1</h4>
                                            <ul>
                                                <li><a href=\"womens.html\">Sou-Catégorie 1</a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 2</a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 3 </a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 4</a></li>
                                            </ul>\t
                                        </div>\t\t\t\t\t\t\t
                                    </div>
                                    <div class=\"col1\">
                                        <div class=\"h_nav\">
                                            <h4>Categorie 2</h4>
                                            <ul>
                                                <li><a href=\"womens.html\">Sou-Catégorie 1</a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 2</a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 3</a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 4</a></li>
                                            </ul>\t
                                        </div>\t\t\t\t\t\t\t
                                    </div>
                                    <div class=\"col1\">
                                        <div class=\"h_nav\">
                                            <h4>Categorie 3</h4>
                                            <ul>
                                                <li><a href=\"womens.html\">Sou-Catégorie 1</a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 2</a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 3</a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 4</a></li>
                                                <li><a href=\"womens.html\">Sou-Catégorie 5</a></li>
                                            </ul>\t
                                        </div>\t\t\t\t\t\t\t\t\t\t\t\t
                                    </div>
                                </div>
                            </div>
                        </li>\t\t\t\t
                        <li><a class=\"color5\" href=\"";
            // line 101
            echo $this->env->getExtension('routing')->getPath("service_show_all");
            echo "\">Services</a>
                            <div class=\"megapanel\">
                                <div class=\"col1\">
                                    <div class=\"h_nav\">
                                        <h4>Categorie 1</h4>
                                        <ul>
                                            <li><a href=\"mens.html\">sou categorie 1</a></li>
                                            <li><a href=\"mens.html\">sou categorie 2</a></li>
                                            <li><a href=\"mens.html\">sou categorie 3</a></li>
                                            <li><a href=\"mens.html\">sou categorie 4</a></li>
                                        </ul>\t
                                    </div>\t\t\t\t\t\t\t
                                </div>
                                <div class=\"col1\">
                                    <div class=\"h_nav\">
                                        <h4>Categorie 2</h4>
                                        <ul>
                                            <li><a href=\"mens.html\">sou categorie 1</a></li>
                                            <li><a href=\"mens.html\">sou categorie 2 </a></li>
                                            <li><a href=\"mens.html\">sou categorie 3 </a></li>
                                            <li><a href=\"mens.html\">sou categorie 4 </a></li>
                                        </ul>\t
                                    </div>\t\t\t\t\t\t\t
                                </div>
                                <div class=\"col1\">
                                    <div class=\"h_nav\">
                                        <h4>categorie 3 </h4>
                                        <ul>
                                            <li><a href=\"mens.html\">sou categorie 1</a></li>
                                            <li><a href=\"mens.html\">sou categorie 2 </a></li>
                                            <li><a href=\"mens.html\">sou categorie 3 </a></li>
                                            <li><a href=\"mens.html\">sou categorie 4 </a></li>
                                        </ul>\t
                                    </div>\t\t\t\t\t\t\t\t\t\t\t\t
                                </div>
                            </div>
                        </li>
                                        <li><a class=\"color7\" href=\"";
            // line 138
            echo $this->env->getExtension('routing')->getPath("recherche_appel_offre");
            echo "\">Appels d'offre</a></li>
                                <li><a class=\"color7\" href=\"";
            // line 139
            echo $this->env->getExtension('routing')->getPath("sujet_show-All");
            echo "\">Forum</a></li>
                    </ul>
                    ";
        }
        // line 142
        echo "                </div>
            </div>
            <div class=\"header-bottom-right\">
                <div class=\"search\">\t  
                    <input type=\"text\" name=\"s\" class=\"textbox\" value=\"rechercher\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {
                                    this.value = 'rechercher';
                                }\">
                    <input type=\"submit\" value=\"Subscribe\" id=\"submit\" name=\"submit\">
                    <div id=\"response\"> </div>
                </div>
                <div class=\"tag-list\">
                    <ul class=\"icon1 sub-icon1 profile_img\">
                        <li><a class=\"active-icon c1\" href=\"#\"> </a>
                            <ul class=\"sub-icon1 list\">
                                <li><h3>sed diam nonummy</h3><a href=\"\"></a></li>
                                <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href=\"\">adipiscing elit, sed diam</a></p></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class=\"icon1 sub-icon1 profile_img\">
                        <li><a class=\"active-icon c2\" href=\"#\"> </a>
                            <ul class=\"sub-icon1 list\">
                                <li><h3>No Products</h3><a href=\"\"></a></li>
                                <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href=\"\">adipiscing elit, sed diam</a></p></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class=\"last\"><li><a href=\"";
        // line 169
        echo $this->env->getExtension('routing')->getPath("listPanier");
        echo "\">Panier(0)</a></li></ul>
                </div>
            </div>
            <div class=\"clear\"></div>
        </div>
    </div>
    <!-- start slider -->
    ";
        // line 176
        $this->displayBlock('slider', $context, $blocks);
        // line 179
        echo "    ";
        $this->displayBlock('content', $context, $blocks);
        // line 182
        echo "



    <div class=\"footer\">
        <div class=\"footer-top\">
            <div class=\"wrap\">
                <div class=\"section group example\">

                    <div class=\"clear\"></div>
                </div>
            </div>
       
        <div class=\"footer-middle\">
            <div class=\"wrap\">

                <div class=\"section group example\">
                    <div class=\"col_1_of_f_1 span_1_of_f_1\">
                        <div class=\"section group example\">
                            <div class=\"col_1_of_f_2 span_1_of_f_2\">
                                <h3>Facebook</h3>
                                <script src=";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Facebook.js"), "html", null, true);
        echo "></script>
                                <div class=\"like_box\">\t
                                    <div class=\"fb-like-box\" data-href=\"http://www.facebook.com/w3layouts\" data-colorscheme=\"light\" data-show-faces=\"true\" data-header=\"true\" data-stream=\"false\" data-show-border=\"true\"></div>
                                </div>
                            </div>
                            <div class=\"col_1_of_f_2 span_1_of_f_2\">
                                <h3>Twitter</h3>
                                <div class=\"recent-tweet\">
                                    <div class=\"recent-tweet-icon\">
                                        <span> </span>
                                    </div>
                                    <div class=\"recent-tweet-info\">
                                        <p>Ds which don't look even slightly believable. If you are <a href=\"#\">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
                                    </div>
                                    <div class=\"clear\"> </div>
                                </div>
                                <div class=\"recent-tweet\">
                                    <div class=\"recent-tweet-icon\">
                                        <span> </span>
                                    </div>
                                    <div class=\"recent-tweet-info\">
                                        <p>Ds which don't look even slightly believable. If you are <a href=\"#\">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
                                    </div>
                                    <div class=\"clear\"> </div>
                                </div>
                            </div>
                            <div class=\"clear\"></div>
                        </div>
                    </div>
                    <div class=\"col_1_of_f_1 span_1_of_f_1\">
                        <div class=\"section group example\">
                            <div class=\"col_1_of_f_2 span_1_of_f_2\">
                                <h3>Information</h3>
                                <ul class=\"f-list1\">
                                    <li><a href=\"#\">Duis autem vel eum iriure </a></li>
                                    <li><a href=\"#\">anteposuerit litterarum formas </a></li>
                                    <li><a href=\"#\">Tduis dolore te feugait nulla</a></li>
                                    <li><a href=\"#\">Duis autem vel eum iriure </a></li>
                                    <li><a href=\"#\">anteposuerit litterarum formas </a></li>
                                    <li><a href=\"#\">Tduis dolore te feugait nulla</a></li>
                                </ul>
                            </div>
                            <div class=\"col_1_of_f_2 span_1_of_f_2\">
                                <h3>Contacter nous</h3>
                                <div class=\"company_address\">
                                    <p>Ariana , Tunis</p>
                                    <p>Chotrana , Jaafar 2,</p>
                                    <p>Tunisia</p>
                                    <p>Phone:(+216) 22 666 444</p>
                                    <p>Fax: (+216) 71 444 555</p>
                                    <p>Email: <span>AllForDeal@gmail.com</span></p>

                                </div>
                                <div class=\"social-media\">
                                    <ul>
                                        <li> <span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Google\"><a href=\"#\" target=\"_blank\"> </a></span></li>
                                        <li><span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Linked in\"><a href=\"#\" target=\"_blank\"> </a> </span></li>
                                        <li><span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Rss\"><a href=\"#\" target=\"_blank\"> </a></span></li>
                                        <li><span class=\"simptip-position-bottom simptip-movable\" data-tooltip=\"Facebook\"><a href=\"#\" target=\"_blank\"> </a></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class=\"clear\"></div>
                        </div>
                    </div>
                    <div class=\"clear\"></div>
                </div>
            </div>
        </div>
    </div>
                                
        <div class=\"footer-bottom\">
            <div class=\"wrap\">
                <div class=\"copy\">
                    <p>© 2016 Copyrights <a href='#'> AllForDeal </a> </p>
                </div>
                <div class=\"f-list2\">
                      ";
        // line 280
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            echo "  
                    <ul>
                        <li class=\"active\"><a href=\"\">Aide</a></li> |
                        <li><a href=\"delivery.html\">Livraison</a></li> |
                        <li><a href=\"delivery.html\">Informations légales</a></li> |
                        <li><a href=\"";
            // line 285
            echo $this->env->getExtension('routing')->getPath("mail_new");
            echo "\">Contacter nous</a></li> 
                        <li><a href=\"";
            // line 286
            echo $this->env->getExtension('routing')->getPath("my_app_reclamation_form");
            echo "\">Reclamation</a></li> 
                    </ul>
                    ";
        }
        // line 289
        echo "                </div>
                <div class=\"clear\"></div>
            </div>
        </div>
    </div>
     </div>
    </body>
    
</html>";
    }

    // line 176
    public function block_slider($context, array $blocks = array())
    {
        // line 177
        echo "        <!--/slider -->
    ";
    }

    // line 179
    public function block_content($context, array $blocks = array())
    {
        // line 180
        echo "        
    ";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  438 => 180,  435 => 179,  430 => 177,  427 => 176,  415 => 289,  409 => 286,  405 => 285,  397 => 280,  317 => 203,  294 => 182,  291 => 179,  289 => 176,  279 => 169,  250 => 142,  244 => 139,  240 => 138,  200 => 101,  157 => 61,  152 => 59,  147 => 57,  139 => 54,  128 => 45,  123 => 43,  118 => 42,  112 => 39,  108 => 38,  104 => 37,  100 => 36,  96 => 35,  80 => 22,  75 => 20,  71 => 19,  67 => 18,  63 => 17,  58 => 15,  54 => 14,  50 => 13,  45 => 11,  40 => 9,  36 => 8,  32 => 7,  28 => 6,  21 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/*         <title>All For Deal | Accueil </title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*          <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href={{asset('css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*         <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*         <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*         <!-- start menu -->*/
/*         <link href={{asset('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*         <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*         <!--start slider -->*/
/*         <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*         <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*         <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*         <script src={{asset('js/fwslider.js')}}></script>*/
/*         <!--end slider -->*/
/*         <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/*     </head>*/
/*     <body>*/
/*     <div class="header-top">*/
/*         <div class="wrap"> */
/*             <div class="header-top-left">*/
/*                 <div class="box">*/
/*  */
/*                 </div>*/
/*                 <div class="clear"></div>*/
/*             </div>*/
/*             <div class="cssmenu">*/
/*                 <ul>*/
/*                    {% if is_granted("ROLE_USER") %}  */
/*                         <li class="active"><a href="{{ path('Profile')}}">Compte</a></li> |  */
/*                         <li><a href="{{ path('favoris_show', { 'id': 4 }) }}">Favoris</a></li> |*/
/*                         <li><a href="{{ path('panier_show', { 'id': 1 }) }}">Panier</a></li> |*/
/*                         <li><a href="{{path('fos_user_security_logout')}}">Se déconnecter</a></li> |*/
/*                        */
/*                         {% else %}*/
/*                         <li><a href="{{path('fos_user_security_login')}}">Se connecter</a></li> |*/
/*                         <li><a href="{{path('fos_user_registration_register')}}">S'inscrire</a></li>*/
/*                         {% endif %}*/
/*                 </ul>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="header-bottom">*/
/*         <div class="wrap">*/
/*             <div class="header-bottom-left">*/
/*                 <div class="logo">*/
/*                     <a href="{{ path('produit')}}"><img src={{asset('images/logo.png')}} alt=""/></a>*/
/*                 </div>*/
/*                 <div class="menu">*/
/*                       {% if is_granted("ROLE_USER") %}  */
/*                     <ul class="megamenu skyblue">*/
/*                         <li class="active grid"><a href="{{ path('AccueilClient')}}">Accueil</a></li>*/
/*                         */
/*                         <li><a class="color4" href="{{ path('produit')}}">Produits</a>*/
/*                             <div class="megapanel">*/
/*                                 <div class="row">*/
/*                                     <div class="col1">*/
/*                                         <div class="h_nav">*/
/*                                             <h4>Catégorie 1</h4>*/
/*                                             <ul>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 1</a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 2</a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 3 </a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 4</a></li>*/
/*                                             </ul>	*/
/*                                         </div>							*/
/*                                     </div>*/
/*                                     <div class="col1">*/
/*                                         <div class="h_nav">*/
/*                                             <h4>Categorie 2</h4>*/
/*                                             <ul>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 1</a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 2</a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 3</a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 4</a></li>*/
/*                                             </ul>	*/
/*                                         </div>							*/
/*                                     </div>*/
/*                                     <div class="col1">*/
/*                                         <div class="h_nav">*/
/*                                             <h4>Categorie 3</h4>*/
/*                                             <ul>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 1</a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 2</a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 3</a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 4</a></li>*/
/*                                                 <li><a href="womens.html">Sou-Catégorie 5</a></li>*/
/*                                             </ul>	*/
/*                                         </div>												*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </li>				*/
/*                         <li><a class="color5" href="{{ path('service_show_all')}}">Services</a>*/
/*                             <div class="megapanel">*/
/*                                 <div class="col1">*/
/*                                     <div class="h_nav">*/
/*                                         <h4>Categorie 1</h4>*/
/*                                         <ul>*/
/*                                             <li><a href="mens.html">sou categorie 1</a></li>*/
/*                                             <li><a href="mens.html">sou categorie 2</a></li>*/
/*                                             <li><a href="mens.html">sou categorie 3</a></li>*/
/*                                             <li><a href="mens.html">sou categorie 4</a></li>*/
/*                                         </ul>	*/
/*                                     </div>							*/
/*                                 </div>*/
/*                                 <div class="col1">*/
/*                                     <div class="h_nav">*/
/*                                         <h4>Categorie 2</h4>*/
/*                                         <ul>*/
/*                                             <li><a href="mens.html">sou categorie 1</a></li>*/
/*                                             <li><a href="mens.html">sou categorie 2 </a></li>*/
/*                                             <li><a href="mens.html">sou categorie 3 </a></li>*/
/*                                             <li><a href="mens.html">sou categorie 4 </a></li>*/
/*                                         </ul>	*/
/*                                     </div>							*/
/*                                 </div>*/
/*                                 <div class="col1">*/
/*                                     <div class="h_nav">*/
/*                                         <h4>categorie 3 </h4>*/
/*                                         <ul>*/
/*                                             <li><a href="mens.html">sou categorie 1</a></li>*/
/*                                             <li><a href="mens.html">sou categorie 2 </a></li>*/
/*                                             <li><a href="mens.html">sou categorie 3 </a></li>*/
/*                                             <li><a href="mens.html">sou categorie 4 </a></li>*/
/*                                         </ul>	*/
/*                                     </div>												*/
/*                                 </div>*/
/*                             </div>*/
/*                         </li>*/
/*                                         <li><a class="color7" href="{{ path('recherche_appel_offre')}}">Appels d'offre</a></li>*/
/*                                 <li><a class="color7" href="{{ path('sujet_show-All')}}">Forum</a></li>*/
/*                     </ul>*/
/*                     {% endif %}*/
/*                 </div>*/
/*             </div>*/
/*             <div class="header-bottom-right">*/
/*                 <div class="search">	  */
/*                     <input type="text" name="s" class="textbox" value="rechercher" onfocus="this.value = '';" onblur="if (this.value == '') {*/
/*                                     this.value = 'rechercher';*/
/*                                 }">*/
/*                     <input type="submit" value="Subscribe" id="submit" name="submit">*/
/*                     <div id="response"> </div>*/
/*                 </div>*/
/*                 <div class="tag-list">*/
/*                     <ul class="icon1 sub-icon1 profile_img">*/
/*                         <li><a class="active-icon c1" href="#"> </a>*/
/*                             <ul class="sub-icon1 list">*/
/*                                 <li><h3>sed diam nonummy</h3><a href=""></a></li>*/
/*                                 <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>*/
/*                             </ul>*/
/*                         </li>*/
/*                     </ul>*/
/*                     <ul class="icon1 sub-icon1 profile_img">*/
/*                         <li><a class="active-icon c2" href="#"> </a>*/
/*                             <ul class="sub-icon1 list">*/
/*                                 <li><h3>No Products</h3><a href=""></a></li>*/
/*                                 <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>*/
/*                             </ul>*/
/*                         </li>*/
/*                     </ul>*/
/*                     <ul class="last"><li><a href="{{ path('listPanier') }}">Panier(0)</a></li></ul>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*         </div>*/
/*     </div>*/
/*     <!-- start slider -->*/
/*     {% block slider %}*/
/*         <!--/slider -->*/
/*     {% endblock %}*/
/*     {% block content %}*/
/*         */
/*     {% endblock %}*/
/* */
/* */
/* */
/* */
/*     <div class="footer">*/
/*         <div class="footer-top">*/
/*             <div class="wrap">*/
/*                 <div class="section group example">*/
/* */
/*                     <div class="clear"></div>*/
/*                 </div>*/
/*             </div>*/
/*        */
/*         <div class="footer-middle">*/
/*             <div class="wrap">*/
/* */
/*                 <div class="section group example">*/
/*                     <div class="col_1_of_f_1 span_1_of_f_1">*/
/*                         <div class="section group example">*/
/*                             <div class="col_1_of_f_2 span_1_of_f_2">*/
/*                                 <h3>Facebook</h3>*/
/*                                 <script src={{asset('js/Facebook.js')}}></script>*/
/*                                 <div class="like_box">	*/
/*                                     <div class="fb-like-box" data-href="http://www.facebook.com/w3layouts" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="col_1_of_f_2 span_1_of_f_2">*/
/*                                 <h3>Twitter</h3>*/
/*                                 <div class="recent-tweet">*/
/*                                     <div class="recent-tweet-icon">*/
/*                                         <span> </span>*/
/*                                     </div>*/
/*                                     <div class="recent-tweet-info">*/
/*                                         <p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>*/
/*                                     </div>*/
/*                                     <div class="clear"> </div>*/
/*                                 </div>*/
/*                                 <div class="recent-tweet">*/
/*                                     <div class="recent-tweet-icon">*/
/*                                         <span> </span>*/
/*                                     </div>*/
/*                                     <div class="recent-tweet-info">*/
/*                                         <p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>*/
/*                                     </div>*/
/*                                     <div class="clear"> </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="clear"></div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col_1_of_f_1 span_1_of_f_1">*/
/*                         <div class="section group example">*/
/*                             <div class="col_1_of_f_2 span_1_of_f_2">*/
/*                                 <h3>Information</h3>*/
/*                                 <ul class="f-list1">*/
/*                                     <li><a href="#">Duis autem vel eum iriure </a></li>*/
/*                                     <li><a href="#">anteposuerit litterarum formas </a></li>*/
/*                                     <li><a href="#">Tduis dolore te feugait nulla</a></li>*/
/*                                     <li><a href="#">Duis autem vel eum iriure </a></li>*/
/*                                     <li><a href="#">anteposuerit litterarum formas </a></li>*/
/*                                     <li><a href="#">Tduis dolore te feugait nulla</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                             <div class="col_1_of_f_2 span_1_of_f_2">*/
/*                                 <h3>Contacter nous</h3>*/
/*                                 <div class="company_address">*/
/*                                     <p>Ariana , Tunis</p>*/
/*                                     <p>Chotrana , Jaafar 2,</p>*/
/*                                     <p>Tunisia</p>*/
/*                                     <p>Phone:(+216) 22 666 444</p>*/
/*                                     <p>Fax: (+216) 71 444 555</p>*/
/*                                     <p>Email: <span>AllForDeal@gmail.com</span></p>*/
/* */
/*                                 </div>*/
/*                                 <div class="social-media">*/
/*                                     <ul>*/
/*                                         <li> <span class="simptip-position-bottom simptip-movable" data-tooltip="Google"><a href="#" target="_blank"> </a></span></li>*/
/*                                         <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Linked in"><a href="#" target="_blank"> </a> </span></li>*/
/*                                         <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Rss"><a href="#" target="_blank"> </a></span></li>*/
/*                                         <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Facebook"><a href="#" target="_blank"> </a></span></li>*/
/*                                     </ul>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="clear"></div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="clear"></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*                                 */
/*         <div class="footer-bottom">*/
/*             <div class="wrap">*/
/*                 <div class="copy">*/
/*                     <p>© 2016 Copyrights <a href='#'> AllForDeal </a> </p>*/
/*                 </div>*/
/*                 <div class="f-list2">*/
/*                       {% if is_granted("ROLE_USER") %}  */
/*                     <ul>*/
/*                         <li class="active"><a href="">Aide</a></li> |*/
/*                         <li><a href="delivery.html">Livraison</a></li> |*/
/*                         <li><a href="delivery.html">Informations légales</a></li> |*/
/*                         <li><a href="{{ path('mail_new')}}">Contacter nous</a></li> */
/*                         <li><a href="{{ path('my_app_reclamation_form')}}">Reclamation</a></li> */
/*                     </ul>*/
/*                     {% endif %}*/
/*                 </div>*/
/*                 <div class="clear"></div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*      </div>*/
/*     </body>*/
/*     */
/* </html>*/
