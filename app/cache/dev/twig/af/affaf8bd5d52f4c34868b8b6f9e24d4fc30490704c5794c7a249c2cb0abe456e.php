<?php

/* PiDevClientBundle:Appeldoffre:showAll.html.twig */
class __TwigTemplate_1b2ad1667596c781ac9479b5208ddaeb25ee12d08b70deba81cca06fc48b961e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Appeldoffre:showAll.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <!DOCTYPE html>
    <html lang=\"en\">

        <!-- Mirrored from forum.azyrusthemes.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Mar 2016 20:23:41 GMT -->
        <head>
            <meta charset=\"utf-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <title>Forum :: Home Page</title>

            <!-- Bootstrap -->
            <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

            <!-- Custom -->
            <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
              <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
              <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
              <![endif]-->

            <!-- fonts -->
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
            <link rel=\"stylesheet\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/font-awesome-4.0.3/css/font-awesome.min.css"), "html", null, true);
        echo "\">

            <!-- CSS STYLE-->
            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/css/style.css"), "html", null, true);
        echo "\" media=\"screen\" />

            <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/rs-plugin/css/settings.css"), "html", null, true);
        echo "\" media=\"screen\" />

        </head>
        <body>

            <div class=\"container-fluid\">

                <!-- Slider -->
                <div class=\"tp-banner-container\">
                    <div class=\"tp-banner\" >
                        <ul>\t
                            <!-- SLIDE  -->
                            <li data-transition=\"fade\" data-slotamount=\"7\" data-masterspeed=\"1500\" >
                                <!-- MAIN IMAGE -->
                                <img src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/offre.jpg"), "html", null, true);
        echo "\"  alt=\"slidebg1\"  data-bgfit=\"cover\" data-bgposition=\"left top\" data-bgrepeat=\"no-repeat\">
                                <p> coucou</p>   <!-- LAYERS -->
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- //Slider -->

               <section class=\"content\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-lg-8 col-xs-12 col-md-8\">
                                <div class=\"pull-left\"><a href=\"#\" class=\"prevnext\"><i class=\"fa fa-angle-left\"></i></a></div>
                                <div class=\"pull-left\">
                                    <ul class=\"paginationforum\">

                                    </ul>
                                </div>
                                <div class=\"pull-left\"><a href=\"#\" class=\"prevnext last\"><i class=\"fa fa-angle-right\"></i></a></div>
                                <div class=\"clearfix\"></div>
                            </div>
                        </div>
                    </div>


                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-lg-8 col-md-8\">
                                <!-- POST -->
                                ";
        // line 78
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 79
            echo "                                    <div class=\"post\">
                                        <div class=\"wrap-ut pull-left\">
                                            <div class=\"userinfo pull-left\">
                                                <div class=\"avatar\">
                                                    <img src=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_image", array("id" => $this->getAttribute($this->getAttribute($context["i"], "proprietaire", array()), "id", array()))), "html", null, true);
            echo "\" alt=\"\" />
                                                    <div class=\"status green\">&nbsp;</div>
                                                </div>

                                                <div class=\"icons\">
                                                    <img src=\"";
            // line 88
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/images/icon1.jpg"), "html", null, true);
            echo "\" alt=\"\" /><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/images/icon4.jpg"), "html", null, true);
            echo "\" alt=\"\" />
                                                </div>
                                            </div>
                                            <div class=\"posttext pull-left\">
                                                <h2><a href=\"02_topic.html\">";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</a></h2>
                                                <p>";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "Description", array()), "html", null, true);
            echo "</p>
                                            </div>
                                            <div class=\"clearfix\"></div>
                                        </div>
                                        <div class=\"postinfo pull-left\">
                                            <div class=\"comments\">
                                                <div class=\"commentbg\">
                                                    560
                                                    <div class=\"mark\"></div>
                                                </div>

                                            </div>
                                            <div class=\"views\"><i class=\"fa fa-eye\"></i> 1,568</div>
                                            <div class=\"time\"><i class=\"fa fa-clock-o\"></i> 24 min</div>                                    
                                        </div>
                                        <div class=\"clearfix\"></div>
                                    </div><!-- POST -->

                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 112
        echo "


                                <!-- POST -->





                            </div>
                            <div class=\"col-lg-4 col-md-4\">

                                <!-- -->
                                <div class=\"sidebarblock\">
                                    <br>
                                         <center> <b><i>Rechercher</i></b></center><br>
                                 <center>    <div class=\"input-group\">
                                       <form method=\"POST\"  action=\"";
        // line 129
        echo $this->env->getExtension('routing')->getPath("recherche_appel_offre");
        echo "\">
                                        <input type=\"text\" name=\"search\" >

                                        <button class=\"btn btn-info\" type=\"submit\">
                                            <i class=\"fa fa-search\"></i>
                                        </button>
                                         </form>
                                     </div>  </center> <hr><hr>
                                    <center> <b><i>Ajouter un appel d'offre</i></b></center><br>
                                    
                                   <div class=\"divline\"></div>
                                    <div class=\"blocktxt\">

                                        Si vous desirez ajouter un sujet clique au-dessous<br><br>
                                            <center>  <a href=\"";
        // line 143
        echo $this->env->getExtension('routing')->getPath("new_appel_offre");
        echo "\" >  <button type=\"submit\" action=\"";
        echo $this->env->getExtension('routing')->getPath("new_appel_offre");
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-plus-square\"></i>  Appel d'offre</button></a>
                                            </center>   

                                        </div>
                                            <hr><hr>
                                            <center> <b><i>Categories</i></b></center><br>
                                    <div class=\"divline\"></div>
                                    <div class=\"blocktxt\">
                                        <ul class=\"cats\">
                                            <li><a href=\"#\">Trading for Money <span class=\"badge pull-right\">20</span></a></li>
                                            <li><a href=\"#\">Vault Keys Giveway <span class=\"badge pull-right\">10</span></a></li>
                                            <li><a href=\"#\">Misc Guns Locations <span class=\"badge pull-right\">50</span></a></li>
                                            <li><a href=\"#\">Looking for Players <span class=\"badge pull-right\">36</span></a></li>
                                            <li><a href=\"#\">Stupid Bugs &amp; Solves <span class=\"badge pull-right\">41</span></a></li>
                                            <li><a href=\"#\">Video &amp; Audio Drivers <span class=\"badge pull-right\">11</span></a></li>
                                            <li><a href=\"#\">2K Official Forums <span class=\"badge pull-right\">5</span></a></li>
                                        </ul>
                                    </div>
                                </div>
    </div>
                                <!-- -->


                                <!-- -->
    
    <div class=\"navigation\" align=\"center\">
        ";
        // line 169
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        echo " </div>


                            </div>
                        </div>
                    </div>





                </section>

        

            <!-- get jQuery from the google apis -->
            <script type=\"text/javascript\" src=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.js"), "html", null, true);
        echo "\"></script>


            <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
            <script type=\"text/javascript\" src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/rs-plugin/js/jquery.themepunch.plugins.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 190
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/rs-plugin/js/jquery.themepunch.revolution.min.js"), "html", null, true);
        echo "\"></script>

            <script src=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/appelOffre/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>


            <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
            <script type=\"text/javascript\">

                var revapi;

                jQuery(document).ready(function() {
                    \"use strict\";
                    revapi = jQuery('.tp-banner').revolution(
                            {
                                delay: 15000,
                                startwidth: 1200,
                                startheight: 278,
                                hideThumbs: 10,
                                fullWidth: \"on\"
                            });

                });\t//ready

            </script>

            <!-- END REVOLUTION SLIDER -->
        </body>

        <!-- Mirrored from forum.azyrusthemes.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Mar 2016 20:24:16 GMT -->
    </html>    





";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Appeldoffre:showAll.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 192,  278 => 190,  274 => 189,  267 => 185,  248 => 169,  217 => 143,  200 => 129,  181 => 112,  156 => 93,  152 => 92,  143 => 88,  135 => 83,  129 => 79,  125 => 78,  93 => 49,  76 => 35,  70 => 32,  64 => 29,  50 => 18,  44 => 15,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* */
/*     <!DOCTYPE html>*/
/*     <html lang="en">*/
/* */
/*         <!-- Mirrored from forum.azyrusthemes.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Mar 2016 20:23:41 GMT -->*/
/*         <head>*/
/*             <meta charset="utf-8">*/
/*             <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*             <title>Forum :: Home Page</title>*/
/* */
/*             <!-- Bootstrap -->*/
/*             <link href="{{asset('bundles/appelOffre/css/bootstrap.min.css')}}" rel="stylesheet">*/
/* */
/*             <!-- Custom -->*/
/*             <link href="{{asset('bundles/appelOffre/css/custom.css')}}" rel="stylesheet">*/
/* */
/*             <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->*/
/*             <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->*/
/*             <!--[if lt IE 9]>*/
/*               <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>*/
/*               <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>*/
/*               <![endif]-->*/
/* */
/*             <!-- fonts -->*/
/*             <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>*/
/*             <link rel="stylesheet" href="{{asset('bundles/appelOffre/font-awesome-4.0.3/css/font-awesome.min.css')}}">*/
/* */
/*             <!-- CSS STYLE-->*/
/*             <link rel="stylesheet" type="text/css" href="{{asset('bundles/appelOffre/css/style.css')}}" media="screen" />*/
/* */
/*             <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->*/
/*             <link rel="stylesheet" type="text/css" href="{{asset('bundles/appelOffre/rs-plugin/css/settings.css')}}" media="screen" />*/
/* */
/*         </head>*/
/*         <body>*/
/* */
/*             <div class="container-fluid">*/
/* */
/*                 <!-- Slider -->*/
/*                 <div class="tp-banner-container">*/
/*                     <div class="tp-banner" >*/
/*                         <ul>	*/
/*                             <!-- SLIDE  -->*/
/*                             <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >*/
/*                                 <!-- MAIN IMAGE -->*/
/*                                 <img src="{{asset('img/offre.jpg')}}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">*/
/*                                 <p> coucou</p>   <!-- LAYERS -->*/
/*                             </li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*                 <!-- //Slider -->*/
/* */
/*                <section class="content">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-lg-8 col-xs-12 col-md-8">*/
/*                                 <div class="pull-left"><a href="#" class="prevnext"><i class="fa fa-angle-left"></i></a></div>*/
/*                                 <div class="pull-left">*/
/*                                     <ul class="paginationforum">*/
/* */
/*                                     </ul>*/
/*                                 </div>*/
/*                                 <div class="pull-left"><a href="#" class="prevnext last"><i class="fa fa-angle-right"></i></a></div>*/
/*                                 <div class="clearfix"></div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/* */
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-lg-8 col-md-8">*/
/*                                 <!-- POST -->*/
/*                                 {% for i in pagination %}*/
/*                                     <div class="post">*/
/*                                         <div class="wrap-ut pull-left">*/
/*                                             <div class="userinfo pull-left">*/
/*                                                 <div class="avatar">*/
/*                                                     <img src="{{path('user_image',{'id':i.proprietaire.id})}}" alt="" />*/
/*                                                     <div class="status green">&nbsp;</div>*/
/*                                                 </div>*/
/* */
/*                                                 <div class="icons">*/
/*                                                     <img src="{{asset('bundles/appelOffre/images/icon1.jpg')}}" alt="" /><img src="{{asset('bundles/appelOffre/images/icon4.jpg')}}" alt="" />*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <div class="posttext pull-left">*/
/*                                                 <h2><a href="02_topic.html">{{i.nom}}</a></h2>*/
/*                                                 <p>{{i.Description}}</p>*/
/*                                             </div>*/
/*                                             <div class="clearfix"></div>*/
/*                                         </div>*/
/*                                         <div class="postinfo pull-left">*/
/*                                             <div class="comments">*/
/*                                                 <div class="commentbg">*/
/*                                                     560*/
/*                                                     <div class="mark"></div>*/
/*                                                 </div>*/
/* */
/*                                             </div>*/
/*                                             <div class="views"><i class="fa fa-eye"></i> 1,568</div>*/
/*                                             <div class="time"><i class="fa fa-clock-o"></i> 24 min</div>                                    */
/*                                         </div>*/
/*                                         <div class="clearfix"></div>*/
/*                                     </div><!-- POST -->*/
/* */
/*                                 {% endfor %}*/
/* */
/* */
/* */
/*                                 <!-- POST -->*/
/* */
/* */
/* */
/* */
/* */
/*                             </div>*/
/*                             <div class="col-lg-4 col-md-4">*/
/* */
/*                                 <!-- -->*/
/*                                 <div class="sidebarblock">*/
/*                                     <br>*/
/*                                          <center> <b><i>Rechercher</i></b></center><br>*/
/*                                  <center>    <div class="input-group">*/
/*                                        <form method="POST"  action="{{path('recherche_appel_offre')}}">*/
/*                                         <input type="text" name="search" >*/
/* */
/*                                         <button class="btn btn-info" type="submit">*/
/*                                             <i class="fa fa-search"></i>*/
/*                                         </button>*/
/*                                          </form>*/
/*                                      </div>  </center> <hr><hr>*/
/*                                     <center> <b><i>Ajouter un appel d'offre</i></b></center><br>*/
/*                                     */
/*                                    <div class="divline"></div>*/
/*                                     <div class="blocktxt">*/
/* */
/*                                         Si vous desirez ajouter un sujet clique au-dessous<br><br>*/
/*                                             <center>  <a href="{{path('new_appel_offre')}}" >  <button type="submit" action="{{path('new_appel_offre')}}" class="btn btn-info"><i class="fa fa-plus-square"></i>  Appel d'offre</button></a>*/
/*                                             </center>   */
/* */
/*                                         </div>*/
/*                                             <hr><hr>*/
/*                                             <center> <b><i>Categories</i></b></center><br>*/
/*                                     <div class="divline"></div>*/
/*                                     <div class="blocktxt">*/
/*                                         <ul class="cats">*/
/*                                             <li><a href="#">Trading for Money <span class="badge pull-right">20</span></a></li>*/
/*                                             <li><a href="#">Vault Keys Giveway <span class="badge pull-right">10</span></a></li>*/
/*                                             <li><a href="#">Misc Guns Locations <span class="badge pull-right">50</span></a></li>*/
/*                                             <li><a href="#">Looking for Players <span class="badge pull-right">36</span></a></li>*/
/*                                             <li><a href="#">Stupid Bugs &amp; Solves <span class="badge pull-right">41</span></a></li>*/
/*                                             <li><a href="#">Video &amp; Audio Drivers <span class="badge pull-right">11</span></a></li>*/
/*                                             <li><a href="#">2K Official Forums <span class="badge pull-right">5</span></a></li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*     </div>*/
/*                                 <!-- -->*/
/* */
/* */
/*                                 <!-- -->*/
/*     */
/*     <div class="navigation" align="center">*/
/*         {{ knp_pagination_render(pagination) }} </div>*/
/* */
/* */
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/* */
/* */
/* */
/* */
/*                 </section>*/
/* */
/*         */
/* */
/*             <!-- get jQuery from the google apis -->*/
/*             <script type="text/javascript" src="{{asset('bundles/appelOffre/ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.js')}}"></script>*/
/* */
/* */
/*             <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->*/
/*             <script type="text/javascript" src="{{asset('bundles/appelOffre/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>*/
/*             <script type="text/javascript" src="{{asset('bundles/appelOffre/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>*/
/* */
/*             <script src="{{asset('bundles/appelOffre/js/bootstrap.min.js')}}"></script>*/
/* */
/* */
/*             <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->*/
/*             <script type="text/javascript">*/
/* */
/*                 var revapi;*/
/* */
/*                 jQuery(document).ready(function() {*/
/*                     "use strict";*/
/*                     revapi = jQuery('.tp-banner').revolution(*/
/*                             {*/
/*                                 delay: 15000,*/
/*                                 startwidth: 1200,*/
/*                                 startheight: 278,*/
/*                                 hideThumbs: 10,*/
/*                                 fullWidth: "on"*/
/*                             });*/
/* */
/*                 });	//ready*/
/* */
/*             </script>*/
/* */
/*             <!-- END REVOLUTION SLIDER -->*/
/*         </body>*/
/* */
/*         <!-- Mirrored from forum.azyrusthemes.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Mar 2016 20:24:16 GMT -->*/
/*     </html>    */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* */
