<?php

/* PiDevClientBundle:Appeldoffre:proprietaire.html.twig */
class __TwigTemplate_7747b28ebbd1adb28a2eb3a68a73bd3423be13f4fba4298c1f044e4be4ac2d79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Appeldoffre:proprietaire.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "<h1>Mes Appels d'offre</h1>
        <div>
             ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["mod"]);
        foreach ($context['_seq'] as $context["_key"] => $context["mod"]) {
            // line 8
            echo "            <br> <th>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["mod"], "nom", array()), "html", null, true);
            echo "</th></br>
            <br> <th>";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["mod"], "description", array()), "html", null, true);
            echo "</th></br>
            <br> <a href=\"";
            // line 10
            echo $this->env->getExtension('routing')->getPath("bae");
            echo "\">Cree une nouvelle Appel d'offre</a></br>
            <br>   <a href=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modif", array("id" => $this->getAttribute($context["mod"], "id", array()))), "html", null, true);
            echo "\">Modifier mon Appel d'offre</a></br>
             <br>   <a href=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supp", array("id" => $this->getAttribute($context["mod"], "id", array()))), "html", null, true);
            echo "\">Supprimer mon Appel d'offre</a></br>
           ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mod'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "        </div>
         ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Appeldoffre:proprietaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 14,  56 => 12,  52 => 11,  48 => 10,  44 => 9,  39 => 8,  35 => 7,  31 => 5,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* {# empty Twig template #}*/
/* <h1>Mes Appels d'offre</h1>*/
/*         <div>*/
/*              {% for mod in mod %}*/
/*             <br> <th>{{mod.nom}}</th></br>*/
/*             <br> <th>{{mod.description}}</th></br>*/
/*             <br> <a href="{{path("bae")}}">Cree une nouvelle Appel d'offre</a></br>*/
/*             <br>   <a href="{{path("modif",{'id':mod.id})}}">Modifier mon Appel d'offre</a></br>*/
/*              <br>   <a href="{{path("supp",{'id':mod.id})}}">Supprimer mon Appel d'offre</a></br>*/
/*            {% endfor %}*/
/*         </div>*/
/*          {% endblock %}*/
/*        */
