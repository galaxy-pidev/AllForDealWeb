<?php

/* PiDevClientBundle:Produit:new.html.twig */
class __TwigTemplate_5a66b0ad51fb06b8103bc45161da2ed0b4f95848b74aff32ed55149f5074c20c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html > 
        <head > 
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> 
            <link rel=\"stylesheet\" href=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/reset.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <link rel=\"stylesheet\" href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/style.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
        </head>

        <body>

            <div id=\"wrap\">

                <div class=\"container\">





                    <div class=\"box-719\" >
                        <div class=\"rcfg-blue pl-6 pr-6 pt-6 pb-5\">

                            <div class=\"box-660\">
                                <b class=\"rc-lightblue\"><b class=\"rc1-lightblue\"><b></b></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc3-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc5-lightblue\"></b></b>
                                <div class=\"rcfg-lightblue pl-4 pr-4 pb-4\">










                                    <p class=\"ml-3\">Ajouter un nouveau produit!</p>
                                     


                                    <!-- Form Starting -->
                                     <form id=\"validate\" class=\"order\" method=\"post\" action=\"\" ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formImg"]) ? $context["formImg"] : $this->getContext($context, "formImg")), 'enctype');
        echo ">
                                        <fieldset>
                                            <ul class=\"order\">
                                                <li><label >Nom <span class=\"typo-1\">*</span></label>";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget');
        echo "</li>
                                                <li><label >Description <span class=\"typo-1\">*</span></label>";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description", array()), 'widget');
        echo "</li>
                                                <li><label>Prix<span class=\"typo-1\">*</span></label> ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prix", array()), 'widget');
        echo " </li>
                                                <li><label>Quantite <span class=\"typo-1\">*</span></label>";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "qantite", array()), 'widget');
        echo "</li>
                                                <li><label>Promotion <span class=\"typo-1\">*</span></label>";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "promotion", array()), 'widget');
        echo "</li>
                                                <li><label>Marque <span class=\"typo-1\">*</span></label> ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "marque", array()), 'widget');
        echo "</li>
                                                <li><label>Categorie <span class=\"typo-1\">*</span></label> ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "categorie", array()), 'widget');
        echo "</li>
                                                <li><label>Image <span class=\"typo-1\">*</span></label>  ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formImg"]) ? $context["formImg"] : $this->getContext($context, "formImg")), "file", array()), 'widget');
        echo "</li>
                                            </ul>
                                            <div class=\"clear\"></div>
                                            <p>NB : ce produit ne sera afficher qu'aprés la validation de l'administrateur</p>
                                            <div class=\"clear\"></div>
   <div class=\"form-actions align-right\">
       ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'widget');
        echo "
       ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formImg"]) ? $context["formImg"] : $this->getContext($context, "formImg")), "_token", array()), 'widget');
        echo "
                                            <input class=\"bt-order-blue\" type=\"submit\" value=\"valider\"  />
   </div>
                                            <div class=\"clear\"></div>
                                        </fieldset>
                                    </form>
                                    <!-- Form Ending -->




                                </div>
                                <b class=\"rc-lightblue\"><b class=\"rc5-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc3-lightblue\"></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc1-lightblue\"><b></b></b></b>
                            </div>


                            <div class=\"clear\"></div>


                        </div>
                        <b class=\"rc-blue\"><b class=\"rc5-blue\"></b><b class=\"rc4-blue\"></b><b class=\"rc3-blue\"></b><b class=\"rc2-blue\"><b></b></b><b class=\"rc1-blue\"><b></b></b></b>
                    </div>
                    <div class=\"clear\"></div>




                </div>
            </div>
            </div>
        </body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 59,  120 => 58,  111 => 52,  107 => 51,  103 => 50,  99 => 49,  95 => 48,  91 => 47,  87 => 46,  83 => 45,  77 => 42,  40 => 8,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html > */
/*         <head > */
/*             <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/reset.css')}} type="text/css" media="all" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/style.css')}} type="text/css" media="all" /> */
/*         </head>*/
/* */
/*         <body>*/
/* */
/*             <div id="wrap">*/
/* */
/*                 <div class="container">*/
/* */
/* */
/* */
/* */
/* */
/*                     <div class="box-719" >*/
/*                         <div class="rcfg-blue pl-6 pr-6 pt-6 pb-5">*/
/* */
/*                             <div class="box-660">*/
/*                                 <b class="rc-lightblue"><b class="rc1-lightblue"><b></b></b><b class="rc2-lightblue"><b></b></b><b class="rc3-lightblue"></b><b class="rc4-lightblue"></b><b class="rc5-lightblue"></b></b>*/
/*                                 <div class="rcfg-lightblue pl-4 pr-4 pb-4">*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                                     <p class="ml-3">Ajouter un nouveau produit!</p>*/
/*                                      */
/* */
/* */
/*                                     <!-- Form Starting -->*/
/*                                      <form id="validate" class="order" method="post" action="" {{ form_enctype(formImg)}}>*/
/*                                         <fieldset>*/
/*                                             <ul class="order">*/
/*                                                 <li><label >Nom <span class="typo-1">*</span></label>{{ form_widget(form.nom)}}</li>*/
/*                                                 <li><label >Description <span class="typo-1">*</span></label>{{ form_widget(form.description) }}</li>*/
/*                                                 <li><label>Prix<span class="typo-1">*</span></label> {{ form_widget(form.prix) }} </li>*/
/*                                                 <li><label>Quantite <span class="typo-1">*</span></label>{{ form_widget(form.qantite) }}</li>*/
/*                                                 <li><label>Promotion <span class="typo-1">*</span></label>{{ form_widget(form.promotion) }}</li>*/
/*                                                 <li><label>Marque <span class="typo-1">*</span></label> {{ form_widget(form.marque) }}</li>*/
/*                                                 <li><label>Categorie <span class="typo-1">*</span></label> {{ form_widget(form.categorie) }}</li>*/
/*                                                 <li><label>Image <span class="typo-1">*</span></label>  {{ form_widget(formImg.file) }}</li>*/
/*                                             </ul>*/
/*                                             <div class="clear"></div>*/
/*                                             <p>NB : ce produit ne sera afficher qu'aprés la validation de l'administrateur</p>*/
/*                                             <div class="clear"></div>*/
/*    <div class="form-actions align-right">*/
/*        {{ form_widget(form._token) }}*/
/*        {{ form_widget(formImg._token) }}*/
/*                                             <input class="bt-order-blue" type="submit" value="valider"  />*/
/*    </div>*/
/*                                             <div class="clear"></div>*/
/*                                         </fieldset>*/
/*                                     </form>*/
/*                                     <!-- Form Ending -->*/
/* */
/* */
/* */
/* */
/*                                 </div>*/
/*                                 <b class="rc-lightblue"><b class="rc5-lightblue"></b><b class="rc4-lightblue"></b><b class="rc3-lightblue"></b><b class="rc2-lightblue"><b></b></b><b class="rc1-lightblue"><b></b></b></b>*/
/*                             </div>*/
/* */
/* */
/*                             <div class="clear"></div>*/
/* */
/* */
/*                         </div>*/
/*                         <b class="rc-blue"><b class="rc5-blue"></b><b class="rc4-blue"></b><b class="rc3-blue"></b><b class="rc2-blue"><b></b></b><b class="rc1-blue"><b></b></b></b>*/
/*                     </div>*/
/*                     <div class="clear"></div>*/
/* */
/* */
/* */
/* */
/*                 </div>*/
/*             </div>*/
/*             </div>*/
/*         </body>*/
/*     </html>*/
/* {% endblock %}*/
