<?php

/* PiDevClientBundle:Service:liste.html.twig */
class __TwigTemplate_4769d9ba128400e9dba8a67f5823c223573415c156886df63e9fe0afb8b0ea33 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevClientBundle:Service:liste.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h1>Listes  des Services a valider </h1>

    
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modeles"]) ? $context["modeles"] : $this->getContext($context, "modeles")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 7
            echo "        <div>
            <br>   Services:<th>";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</th></br>
            <br>    Description:<th>";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</th></br>
      <a href=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("details", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">En savoir Plus!</a>
       <a href=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("valider", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">Valider se service</a>
        </div>
         
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "  ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Service:liste.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 15,  55 => 11,  51 => 10,  47 => 9,  43 => 8,  40 => 7,  36 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::baseAdmin.html.twig' %}*/
/* {% block content %}*/
/* <h1>Listes  des Services a valider </h1>*/
/* */
/*     */
/*     {% for i in modeles %}*/
/*         <div>*/
/*             <br>   Services:<th>{{i.nom}}</th></br>*/
/*             <br>    Description:<th>{{i.description}}</th></br>*/
/*       <a href="{{path("details",{'id':i.id})}}">En savoir Plus!</a>*/
/*        <a href="{{path("valider",{'id':i.id})}}">Valider se service</a>*/
/*         </div>*/
/*          */
/*     {% endfor %}*/
/*   {% endblock %}*/
