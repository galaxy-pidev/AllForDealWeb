<?php

/* PiDevClientBundle:Commande:facturation.html.twig */
class __TwigTemplate_02ffa39ccf539ff6e430febbe9fa223d86e9ed08d11cb3d89629800dd9a90543 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html lang=\"fr\">
    <head>
        <meta charset=\"utf-8\">
        <title>Facture</title>
 
    </head>
    <body>
        <header class=\"clearfix\">
            <div id=\"logo\">
                <img src=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo.png"), "html", null, true);
        echo ">
            </div>
            <h1>Facture</h1>
            <fieldset>
                <legend>Vendeur</legend>
            <div id=\"company\" class=\"clearfix\">
                <div>All For Deal </div>
                <div><a href=\"mailto:company@example.com\">Dealforall@gmail.com</a></div>
            </div>
            </fieldset>  <br> <hr /> <br>
            <fieldset>
                <legend>benificiaire </legend>
            <div id=\"project\">
                <div><span>Nom : </span>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</div>
                <div><span>EMAIL : </span> <a href=\"mailto:john@example.com\">";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</a></div>
                <div><span>Adresse Livraison :  </span>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "adresse", array()), "rue", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "adresse", array()), "ville", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "adresse", array()), "codePostal", array()), "html", null, true);
        echo "  </div>
                <div><span>Mode Livraison : </span>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "modeLivraison", array()), "html", null, true);
        echo "</div>     
                <div><span>Mode Paiement : </span>";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "modepaiement", array()), "html", null, true);
        echo "</div>             
                <div><span>Date d'achat : </span>";
        // line 28
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "dateCommande", array()), "Y-m-d"), "html", null, true);
        echo "</div>
             
            </div>
            </fieldset>
        </header> <br > <br> <hr />
    <main>
        <table border=\"1\" width=\"880px\">
            <thead>
                <tr> <b> Vos Achat : </b> </tr> <hr /><br><br>
                <tr>
                    <th class=\"service\">Produit</th>
                    <th class=\"desc\">Marque</th>
                    <th>Prix</th>
                    <th>Qte</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ligne"]) ? $context["ligne"] : $this->getContext($context, "ligne")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 47
            echo "                    <tr>
                    <td class=\"service\">";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "produit", array()), "nom", array()), "html", null, true);
            echo "</td>
                    <td class=\"desc\">";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "produit", array()), "marque", array()), "html", null, true);
            echo "</td>
                    <td class=\"unit\">";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "produit", array()), "prix", array()), "html", null, true);
            echo "DT</td>
                    <td class=\"qty\">";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "qte", array()), "html", null, true);
            echo "</td>
                    ";
            // line 52
            $context["prixSP"] = ((isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")) + ($this->getAttribute($context["i"], "qte", array()) * $this->getAttribute($this->getAttribute($context["i"], "produit", array()), "prix", array())));
            // line 53
            echo "                    <td class=\"total\"> ";
            echo twig_escape_filter($this->env, ($this->getAttribute($context["i"], "qte", array()) * $this->getAttribute($this->getAttribute($context["i"], "produit", array()), "prix", array())), "html", null, true);
            echo "</td>   
                                    
                
                    <tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                
               
                <tr>
                    <td colspan=\"4\">TOTAL VENTES</td>
                    <td class=\"total\">";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")), "html", null, true);
        echo " DT</td>
                </tr>
                <tr>
                    <td colspan=\"4\">TAX 25%</td>
                    <td class=\"total\"></td>
                </tr>
                <tr>
                    <td colspan=\"4\">FRAIX LIVRAISON</td>
                    <td class=\"total\">12 DT</td>
                </tr>
                <tr>
                    <td colspan=\"4\" class=\"grand total\">MONTANT TOTAL</td>
                    <td class=\"grand total\">";
        // line 74
        echo twig_escape_filter($this->env, ((isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")) + 12), "html", null, true);
        echo "</td>
                </tr>
            </tbody>
        </table>
        <div id=\"notices\">
            <div>NOTICE:</div>
            <div class=\"notice\">Vous ne pouvez avoir cette copie qu'une seule fois.</div>
        </div>
    </main>

</body>
</html>";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commande:facturation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 74,  134 => 62,  128 => 58,  116 => 53,  114 => 52,  110 => 51,  106 => 50,  102 => 49,  98 => 48,  95 => 47,  91 => 46,  70 => 28,  66 => 27,  62 => 26,  54 => 25,  50 => 24,  46 => 23,  30 => 10,  19 => 1,);
    }
}
/* <html lang="fr">*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <title>Facture</title>*/
/*  */
/*     </head>*/
/*     <body>*/
/*         <header class="clearfix">*/
/*             <div id="logo">*/
/*                 <img src={{asset('images/logo.png')}}>*/
/*             </div>*/
/*             <h1>Facture</h1>*/
/*             <fieldset>*/
/*                 <legend>Vendeur</legend>*/
/*             <div id="company" class="clearfix">*/
/*                 <div>All For Deal </div>*/
/*                 <div><a href="mailto:company@example.com">Dealforall@gmail.com</a></div>*/
/*             </div>*/
/*             </fieldset>  <br> <hr /> <br>*/
/*             <fieldset>*/
/*                 <legend>benificiaire </legend>*/
/*             <div id="project">*/
/*                 <div><span>Nom : </span>{{user.username }}</div>*/
/*                 <div><span>EMAIL : </span> <a href="mailto:john@example.com">{{user.email}}</a></div>*/
/*                 <div><span>Adresse Livraison :  </span>{{commande.livraison.adresse.rue}} {{commande.livraison.adresse.ville}} {{commande.livraison.adresse.codePostal}}  </div>*/
/*                 <div><span>Mode Livraison : </span>{{commande.livraison.modeLivraison}}</div>     */
/*                 <div><span>Mode Paiement : </span>{{commande.modepaiement}}</div>             */
/*                 <div><span>Date d'achat : </span>{{commande.dateCommande|date('Y-m-d')}}</div>*/
/*              */
/*             </div>*/
/*             </fieldset>*/
/*         </header> <br > <br> <hr />*/
/*     <main>*/
/*         <table border="1" width="880px">*/
/*             <thead>*/
/*                 <tr> <b> Vos Achat : </b> </tr> <hr /><br><br>*/
/*                 <tr>*/
/*                     <th class="service">Produit</th>*/
/*                     <th class="desc">Marque</th>*/
/*                     <th>Prix</th>*/
/*                     <th>Qte</th>*/
/*                     <th>Total</th>*/
/*                 </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*                 {% for i in ligne %}*/
/*                     <tr>*/
/*                     <td class="service">{{i.produit.nom }}</td>*/
/*                     <td class="desc">{{i.produit.marque}}</td>*/
/*                     <td class="unit">{{i.produit.prix}}DT</td>*/
/*                     <td class="qty">{{i.qte}}</td>*/
/*                     {% set prixSP = prixSP + i.qte*i.produit.prix %}*/
/*                     <td class="total"> {{i.qte*i.produit.prix}}</td>   */
/*                                     */
/*                 */
/*                     <tr>*/
/*                 {% endfor %}*/
/*                 */
/*                */
/*                 <tr>*/
/*                     <td colspan="4">TOTAL VENTES</td>*/
/*                     <td class="total">{{prixSP}} DT</td>*/
/*                 </tr>*/
/*                 <tr>*/
/*                     <td colspan="4">TAX 25%</td>*/
/*                     <td class="total"></td>*/
/*                 </tr>*/
/*                 <tr>*/
/*                     <td colspan="4">FRAIX LIVRAISON</td>*/
/*                     <td class="total">12 DT</td>*/
/*                 </tr>*/
/*                 <tr>*/
/*                     <td colspan="4" class="grand total">MONTANT TOTAL</td>*/
/*                     <td class="grand total">{{prixSP + 12 }}</td>*/
/*                 </tr>*/
/*             </tbody>*/
/*         </table>*/
/*         <div id="notices">*/
/*             <div>NOTICE:</div>*/
/*             <div class="notice">Vous ne pouvez avoir cette copie qu'une seule fois.</div>*/
/*         </div>*/
/*     </main>*/
/* */
/* </body>*/
/* </html>*/
