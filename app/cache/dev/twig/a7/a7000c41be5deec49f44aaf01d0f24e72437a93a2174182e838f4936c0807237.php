<?php

/* PiDevClientBundle:Produit:index.html.twig */
class __TwigTemplate_5e9ce17fb1b14ecd6153f1cdb9831d7c7742bc29a133545eb901be90818ce45d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:index.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "            
<div class=\"main\">
    <div class=\"wrap\">
        <div class=\"section group\">
            <div class=\"cont span_2_of_3\">
                <h2 class=\"head\">Featured Products</h2>
                <div class=\"top-box\">
                    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 11
            echo "                    <div class=\"col_1_of_3 span_1_of_3\"> 
                        <a href=";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo ">
                            <div class=\"inner_content clearfix\">
                                <div class=\"product_image\">
                                    <img src=";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/pic.jpg"), "html", null, true);
            echo " alt=\"\"/>
                                </div>
                                <div class=\"sale-box\"><span class=\"on_sale title_shop\">New</span></div>\t
                                <div class=\"price\">
                                    <div class=\"cart-left\">
                                        <p class=\"title\">";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nom", array()), "html", null, true);
            echo "</p>
                                        <div class=\"price1\">
                                            <span class=\"actual\">";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "prix", array()), "html", null, true);
            echo "</span>
                                        </div>
                                    </div>
                                    <div class=\"cart-right\"> </div>
                                    <div class=\"clear\"></div>
                                </div>\t\t\t\t
                            </div>
                        </a>
                    </div>
                         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "               
                               
                          </div>
                                </div>
                                </div>
                                </div>




";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 31,  66 => 22,  61 => 20,  53 => 15,  47 => 12,  44 => 11,  40 => 10,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/*             */
/* <div class="main">*/
/*     <div class="wrap">*/
/*         <div class="section group">*/
/*             <div class="cont span_2_of_3">*/
/*                 <h2 class="head">Featured Products</h2>*/
/*                 <div class="top-box">*/
/*                     {% for entity in entities %}*/
/*                     <div class="col_1_of_3 span_1_of_3"> */
/*                         <a href={{ path('produit_show', { 'id': entity.id }) }}>*/
/*                             <div class="inner_content clearfix">*/
/*                                 <div class="product_image">*/
/*                                     <img src={{asset('images/pic.jpg')}} alt=""/>*/
/*                                 </div>*/
/*                                 <div class="sale-box"><span class="on_sale title_shop">New</span></div>	*/
/*                                 <div class="price">*/
/*                                     <div class="cart-left">*/
/*                                         <p class="title">{{entity.nom}}</p>*/
/*                                         <div class="price1">*/
/*                                             <span class="actual">{{ entity.prix }}</span>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div class="cart-right"> </div>*/
/*                                     <div class="clear"></div>*/
/*                                 </div>				*/
/*                             </div>*/
/*                         </a>*/
/*                     </div>*/
/*                          {% endfor %}               */
/*                                */
/*                           </div>*/
/*                                 </div>*/
/*                                 </div>*/
/*                                 </div>*/
/* */
/* */
/* */
/* */
/* {% endblock %}*/
