<?php

/* PiDevAdminBundle:Categorie:recherche.html.twig */
class __TwigTemplate_d69227b7bb557767703082aa834813bec6c16f6ad44bb85684b88b6ee7728b79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:Categorie:recherche.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
<h1> Recherche Liste des catégories </h1>
<form method=\"POST\" action=\"\">
  rechercher: <input type=\"text\" name=\"search\" />
  <input type=\"submit\" value=\"chercher\" />
    
    
</form>
</br>
<table border=\"1\">
    <tr>     
               <td>id</td>
               <td> Nom  </td>
               <td> Description </td>
                 
       </tr>
    ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mod"]) ? $context["mod"] : $this->getContext($context, "mod")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            // line 20
            echo "       

       <tr>
            
            <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nom", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "description", array()), "html", null, true);
            echo "</td>
            
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "</table>
    ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Categorie:recherche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 30,  67 => 26,  63 => 25,  59 => 24,  53 => 20,  49 => 19,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {%extends "::baseAdmin.html.twig"%}*/
/* {% block content %}*/
/* */
/* <h1> Recherche Liste des catégories </h1>*/
/* <form method="POST" action="">*/
/*   rechercher: <input type="text" name="search" />*/
/*   <input type="submit" value="chercher" />*/
/*     */
/*     */
/* </form>*/
/* </br>*/
/* <table border="1">*/
/*     <tr>     */
/*                <td>id</td>*/
/*                <td> Nom  </td>*/
/*                <td> Description </td>*/
/*                  */
/*        </tr>*/
/*     {% for j in mod %}*/
/*        */
/* */
/*        <tr>*/
/*             */
/*             <td>{{j.id}}</td>*/
/*             <td>{{j.nom}}</td>*/
/*             <td>{{j.description}}</td>*/
/*             */
/*         </tr>*/
/*     {% endfor %}*/
/* </table>*/
/*     {% endblock %}*/
