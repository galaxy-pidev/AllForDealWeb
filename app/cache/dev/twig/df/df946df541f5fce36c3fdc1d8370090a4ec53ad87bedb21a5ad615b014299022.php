<?php

/* PiDevClientBundle:Sujet:affiche.html.twig */
class __TwigTemplate_28c6cb4e4651645470c2bd467089e446ae8686b892b09dcf88c0f467e83c6e7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Sujet:affiche.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html>
        <head>
            <title>All For Deal | Accueil </title>

            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
            <link href=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.min.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/style.css"), "html", null, true);
        echo "\" />
            <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
            <script type=\"text/javascript\" src=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
            <!-- start menu -->
            <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <script type=\"text/javascript\" src=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
            <script  src=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
            <!--start slider -->
            <link rel=\"stylesheet\" href=";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
            <script src=";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
            <script src=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/bootstrap.min.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/customjs.js"), "html", null, true);
        echo "></script>
            <link rel=\"stylesheet\" href=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/bootstrap.min.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <link rel=\"stylesheet\" href=";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/custom.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <!--end slider -->
            <link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">

            <script src=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.load.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.ready.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.slides.js"), "html", null, true);
        echo "></script>
            <!-- start details -->
            <script src=";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/slides.min.jquery.js"), "html", null, true);
        echo "></script>

            <link rel=\"stylesheet\" href=";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/etalage.css"), "html", null, true);
        echo ">
            <script src=";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.etalage.min.js"), "html", null, true);
        echo "></script>

        </head>
    <body>
    <div id=\"wrapper\">
        <!-- BEGIN MAIN SIDEBAR -->

        <!-- END MAIN SIDEBAR -->
        <!-- BEGIN MAIN CONTENT -->
        <div id=\"main-content\" class=\"blog-result\">
            <div class=\"row\">
                <div class=\"col-lg-8\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-body\" >
                            <h1>A Simple Blog Template for Bootstrap 3</h1>
                            <p class=\"lead\">by <a>";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "user", array()), "username", array()), "html", null, true);
        echo "</a></p>
                            <hr>
                            <p><i class=\"fa fa-calendar\"></i> Posted on December 24, 2013 at 9:00 PM</p>
                            <hr>
                            <img src=\"";
        // line 62
        echo $this->env->getExtension('routing')->getPath("image");
        echo "\" width=\"200px\" height=\"100px\" class=\"img-responsive\">
                            <hr>
                            ";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sujet", array()), "html", null, true);
        echo "
                         <p><strong>Placeholder text by:</strong></p>
                            <ul>
                                <li><a href=\"#\">Space Ipsum</a>
                                </li>
                                <li><a href=\"#\">Cupcake Ipsum</a>
                                </li>
                                <li><a href=\"#\">Tuna Ipsum</a>
                                </li>
                            </ul>
                            <hr>
                            <!-- the comment box -->
                            <div class=\"well\">
                                <h4>Leave a Comment:</h4>
                                <form role=\"form\">
                                    <div class=\"form-group\">
                                        <textarea class=\"form-control\" rows=\"3\"></textarea>
                                    </div>
                                    <button type=\"submit\" class=\"btn btn-danger\">Submit</button>
                                </form>
                            </div>
                            <hr>
                            <!-- the comments -->
                            <h3>Steve Bones<small>9:41 PM on January 24, 2014</small></h3>
                            <p>This has to be the worst blog post I have ever read. It simply makes no sense. You start off by talking about space or something, then you randomly start babbling about cupcakes, and you end off with random fish names.</p>
                            <h3>John Marvin <small>9:47 PM on January 24, 2014</small></h3>
                            <p>Don't listen to this guy, any blog with the categories 'dinosaurs, spaceships, fried foods, wild animals, alien abductions, business casual, robots, and fireworks' has true potential.</p>
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-4 blog-sidebar\">
                    <div class=\"panel panel-default\">
                    <div class=\"panel-body\" >
                        <h4>Blog Search</h4>
                        <div class=\"input-group\">
                            <input type=\"text\" class=\"form-control\">
                            <span class=\"input-group-btn\">
                                <button class=\"btn btn-danger\" type=\"button\">
                                    <span class=\"glyphicon glyphicon-search\"></span>
                                </button>
                            </span>
                        </div>
                      </div>
                    </div>
                    <div class=\"panel panel-default\">
                       <div class=\"panel-body\" >
                            <h4>Blog Categories</h4>
                            <div class=\"row\">
                                <div class=\"col-lg-6\">
                                    <ul class=\"list-unstyled categories-list\">
                                        <li><i class=\"fa fa-angle-right\"></i> <a href=\"#dinosaurs\">Dinosaurs</a>
                                        </li>
                                        <li><i class=\"fa fa-angle-right\"></i> <a href=\"#spaceships\">Spaceships</a>
                                        </li>
                                        <li><i class=\"fa fa-angle-right\"></i> <a href=\"#fried-foods\">Fried Foods</a>
                                        </li>
                                        <li><i class=\"fa fa-angle-right\"></i> <a href=\"#wild-animals\">Wild Animals</a>
                                        </li>
                                        <li><i class=\"fa fa-angle-right\"></i> <a href=\"#fried-foods\">Fried Foods</a>
                                        </li>
                                        <li><i class=\"fa fa-angle-right\"></i> <a href=\"#wild-animals\">Wild Animals</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div> 
    
    
        <script src=";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
        <!-- END MANDATORY SCRIPTS -->
        <script src=";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>
    </body>

</html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Sujet:affiche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  315 => 154,  310 => 152,  306 => 151,  302 => 150,  298 => 149,  294 => 148,  290 => 147,  286 => 146,  282 => 145,  278 => 144,  274 => 143,  270 => 142,  266 => 141,  262 => 140,  258 => 139,  180 => 64,  175 => 62,  168 => 58,  150 => 43,  146 => 42,  141 => 40,  136 => 38,  132 => 37,  128 => 36,  123 => 34,  118 => 32,  114 => 31,  110 => 30,  106 => 29,  102 => 28,  98 => 27,  94 => 26,  90 => 25,  86 => 24,  81 => 22,  77 => 21,  73 => 20,  68 => 18,  63 => 16,  59 => 15,  55 => 14,  51 => 13,  47 => 12,  43 => 11,  39 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html>*/
/*         <head>*/
/*             <title>All For Deal | Accueil </title>*/
/* */
/*             <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.min.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/style.css') }}" />*/
/*             <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*             <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*             <!-- start menu -->*/
/*             <link href={{asset('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*             <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*             <!--start slider -->*/
/*             <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*             <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*             <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*             <script src={{asset('js/fwslider.js')}}></script>*/
/*             <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/bootstrap.min.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/customjs.js')}}></script>*/
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/bootstrap.min.css')}} type="text/css" media="all" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/custom.css')}} type="text/css" media="all" /> */
/*             <!--end slider -->*/
/*             <link rel="stylesheet" href="{{asset('css/style.css')}}">*/
/* */
/*             <script src={{asset('js/jquery.load.js')}}></script>*/
/*             <script src={{asset('js/jquery.ready.js')}}></script>*/
/*             <script src={{asset('js/jquery.slides.js')}}></script>*/
/*             <!-- start details -->*/
/*             <script src={{asset('js/slides.min.jquery.js')}}></script>*/
/* */
/*             <link rel="stylesheet" href={{asset('css/etalage.css')}}>*/
/*             <script src={{asset('js/jquery.etalage.min.js')}}></script>*/
/* */
/*         </head>*/
/*     <body>*/
/*     <div id="wrapper">*/
/*         <!-- BEGIN MAIN SIDEBAR -->*/
/* */
/*         <!-- END MAIN SIDEBAR -->*/
/*         <!-- BEGIN MAIN CONTENT -->*/
/*         <div id="main-content" class="blog-result">*/
/*             <div class="row">*/
/*                 <div class="col-lg-8">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-body" >*/
/*                             <h1>A Simple Blog Template for Bootstrap 3</h1>*/
/*                             <p class="lead">by <a>{{entity.user.username}}</a></p>*/
/*                             <hr>*/
/*                             <p><i class="fa fa-calendar"></i> Posted on December 24, 2013 at 9:00 PM</p>*/
/*                             <hr>*/
/*                             <img src="{{path('image')}}" width="200px" height="100px" class="img-responsive">*/
/*                             <hr>*/
/*                             {{entity.sujet}}*/
/*                          <p><strong>Placeholder text by:</strong></p>*/
/*                             <ul>*/
/*                                 <li><a href="#">Space Ipsum</a>*/
/*                                 </li>*/
/*                                 <li><a href="#">Cupcake Ipsum</a>*/
/*                                 </li>*/
/*                                 <li><a href="#">Tuna Ipsum</a>*/
/*                                 </li>*/
/*                             </ul>*/
/*                             <hr>*/
/*                             <!-- the comment box -->*/
/*                             <div class="well">*/
/*                                 <h4>Leave a Comment:</h4>*/
/*                                 <form role="form">*/
/*                                     <div class="form-group">*/
/*                                         <textarea class="form-control" rows="3"></textarea>*/
/*                                     </div>*/
/*                                     <button type="submit" class="btn btn-danger">Submit</button>*/
/*                                 </form>*/
/*                             </div>*/
/*                             <hr>*/
/*                             <!-- the comments -->*/
/*                             <h3>Steve Bones<small>9:41 PM on January 24, 2014</small></h3>*/
/*                             <p>This has to be the worst blog post I have ever read. It simply makes no sense. You start off by talking about space or something, then you randomly start babbling about cupcakes, and you end off with random fish names.</p>*/
/*                             <h3>John Marvin <small>9:47 PM on January 24, 2014</small></h3>*/
/*                             <p>Don't listen to this guy, any blog with the categories 'dinosaurs, spaceships, fried foods, wild animals, alien abductions, business casual, robots, and fireworks' has true potential.</p>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="col-lg-4 blog-sidebar">*/
/*                     <div class="panel panel-default">*/
/*                     <div class="panel-body" >*/
/*                         <h4>Blog Search</h4>*/
/*                         <div class="input-group">*/
/*                             <input type="text" class="form-control">*/
/*                             <span class="input-group-btn">*/
/*                                 <button class="btn btn-danger" type="button">*/
/*                                     <span class="glyphicon glyphicon-search"></span>*/
/*                                 </button>*/
/*                             </span>*/
/*                         </div>*/
/*                       </div>*/
/*                     </div>*/
/*                     <div class="panel panel-default">*/
/*                        <div class="panel-body" >*/
/*                             <h4>Blog Categories</h4>*/
/*                             <div class="row">*/
/*                                 <div class="col-lg-6">*/
/*                                     <ul class="list-unstyled categories-list">*/
/*                                         <li><i class="fa fa-angle-right"></i> <a href="#dinosaurs">Dinosaurs</a>*/
/*                                         </li>*/
/*                                         <li><i class="fa fa-angle-right"></i> <a href="#spaceships">Spaceships</a>*/
/*                                         </li>*/
/*                                         <li><i class="fa fa-angle-right"></i> <a href="#fried-foods">Fried Foods</a>*/
/*                                         </li>*/
/*                                         <li><i class="fa fa-angle-right"></i> <a href="#wild-animals">Wild Animals</a>*/
/*                                         </li>*/
/*                                         <li><i class="fa fa-angle-right"></i> <a href="#fried-foods">Fried Foods</a>*/
/*                                         </li>*/
/*                                         <li><i class="fa fa-angle-right"></i> <a href="#wild-animals">Wild Animals</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <!-- END MAIN CONTENT -->*/
/*     </div> */
/*     */
/*     */
/*         <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*         <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*         <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*         <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*         <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*         <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*         <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*         <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*         <script src={{asset('js/mailbox.js')}}></script>*/
/*         <!-- END MANDATORY SCRIPTS -->*/
/*         <script src={{asset('js/application.js')}}></script>*/
/*     </body>*/
/* */
/* </html>*/
/* {% endblock %}*/
/* */
