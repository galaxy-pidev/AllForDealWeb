<?php

/* PiDevClientBundle:Adresse:ajout.html.twig */
class __TwigTemplate_64ac4f16d198505006d4df08663693ce7d6d371b9fd9749723dd0eabe37b397b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Adresse:ajout.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\" lang=\"en-US\"> 
        <head profile=\"http://gmpg.org/xfn/11\"> 
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> 
            <title>CSS Form Tutorial: How to Improve Web Order Form Design</title> 
            <link rel=\"stylesheet\" href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/reset.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 


            <link rel=\"stylesheet\" href=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/style.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
        </head>

        <body>

            <div id=\"wrap\">

                <div class=\"container\">





                    <div class=\"box-719\" >
                        <div class=\"rcfg-blue pl-6 pr-6 pt-6 pb-5\">

                            <div class=\"box-660\">
                                <b class=\"rc-lightblue\"><b class=\"rc1-lightblue\"><b></b></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc3-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc5-lightblue\"></b></b>
                                <div class=\"rcfg-lightblue pl-4 pr-4 pb-4\">










                                    <p class=\"ml-3\">Veuillez ajouter votre nouvelle avresse ici !</p>
                                     


                                    <!-- Form Starting -->
                                    <form id=\"validate\" class=\"order\" method=\"post\" action=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("adresse_create");
        echo "\">
                                        <fieldset>
                                            <ul class=\"order\">
                                                <li><label >Rue <span class=\"typo-1\">*</span></label>";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rue", array()), 'widget');
        echo "
                                                   ";
        // line 50
        echo "                                                </li>
                                                <li><label >Ville <span class=\"typo-1\">*</span></label>";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'widget');
        echo "
                                                 ";
        // line 53
        echo "                                                </li>
                                                <li>
                                                    <label>Code postale<span class=\"typo-1\">*</span></label>";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "codepostal", array()), 'widget');
        echo "
                                                  ";
        // line 57
        echo "                                                </li>

                                                <li><label for=\"ccExpDate\">Pays <span class=\"typo-1\">*</span></label>";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays", array()), 'widget');
        echo "
                                                  ";
        // line 61
        echo "                                            </ul>

                                            <div class=\"clear\"></div>

                                          







                                            <div class=\"clear\"></div>
   <div class=\"form-actions align-right\">
       ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'widget');
        echo "
                                            <input class=\"bt-order-blue\" type=\"submit\" value=\"valider\" onclick=\"\" />
   </div>
                                            <div class=\"clear\"></div>
                                        </fieldset>
                                    </form>
                                    <!-- Form Ending -->




                                </div>
                                <b class=\"rc-lightblue\"><b class=\"rc5-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc3-lightblue\"></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc1-lightblue\"><b></b></b></b>
                            </div>


                            <div class=\"clear\"></div>


                        </div>
                        <b class=\"rc-blue\"><b class=\"rc5-blue\"></b><b class=\"rc4-blue\"></b><b class=\"rc3-blue\"></b><b class=\"rc2-blue\"><b></b></b><b class=\"rc1-blue\"><b></b></b></b>
                    </div>
                    <div class=\"clear\"></div>




                </div>
            </div>
            </div>
        </body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Adresse:ajout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 75,  113 => 61,  109 => 59,  105 => 57,  101 => 55,  97 => 53,  93 => 51,  90 => 50,  86 => 48,  80 => 45,  43 => 11,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US"> */
/*         <head profile="http://gmpg.org/xfn/11"> */
/*             <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> */
/*             <title>CSS Form Tutorial: How to Improve Web Order Form Design</title> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/reset.css')}} type="text/css" media="all" /> */
/* */
/* */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/style.css')}} type="text/css" media="all" /> */
/*         </head>*/
/* */
/*         <body>*/
/* */
/*             <div id="wrap">*/
/* */
/*                 <div class="container">*/
/* */
/* */
/* */
/* */
/* */
/*                     <div class="box-719" >*/
/*                         <div class="rcfg-blue pl-6 pr-6 pt-6 pb-5">*/
/* */
/*                             <div class="box-660">*/
/*                                 <b class="rc-lightblue"><b class="rc1-lightblue"><b></b></b><b class="rc2-lightblue"><b></b></b><b class="rc3-lightblue"></b><b class="rc4-lightblue"></b><b class="rc5-lightblue"></b></b>*/
/*                                 <div class="rcfg-lightblue pl-4 pr-4 pb-4">*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                                     <p class="ml-3">Veuillez ajouter votre nouvelle avresse ici !</p>*/
/*                                      */
/* */
/* */
/*                                     <!-- Form Starting -->*/
/*                                     <form id="validate" class="order" method="post" action="{{path('adresse_create')}}">*/
/*                                         <fieldset>*/
/*                                             <ul class="order">*/
/*                                                 <li><label >Rue <span class="typo-1">*</span></label>{{ form_widget(form.rue)}}*/
/*                                                    {# <input class="text" type="text" id="" name="name="form[age]" size="30" maxlength="100" value=""> #}*/
/*                                                 </li>*/
/*                                                 <li><label >Ville <span class="typo-1">*</span></label>{{ form_widget(form.ville) }}*/
/*                                                  {#   <input class="text" type="text" id="" name="" size="30" maxlength="16" value=""> 	#}*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <label>Code postale<span class="typo-1">*</span></label>{{ form_widget(form.codepostal) }}*/
/*                                                   {#  <input class="text" type="text" id="" name="" value="" size="4" maxlength="4" /> #}*/
/*                                                 </li>*/
/* */
/*                                                 <li><label for="ccExpDate">Pays <span class="typo-1">*</span></label>{{ form_widget(form.pays) }}*/
/*                                                   {#  <input class="text" type="text" id="" name="" value="" size="4" maxlength="4" /></li> #}*/
/*                                             </ul>*/
/* */
/*                                             <div class="clear"></div>*/
/* */
/*                                           */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                                             <div class="clear"></div>*/
/*    <div class="form-actions align-right">*/
/*        {{ form_widget(form._token) }}*/
/*                                             <input class="bt-order-blue" type="submit" value="valider" onclick="" />*/
/*    </div>*/
/*                                             <div class="clear"></div>*/
/*                                         </fieldset>*/
/*                                     </form>*/
/*                                     <!-- Form Ending -->*/
/* */
/* */
/* */
/* */
/*                                 </div>*/
/*                                 <b class="rc-lightblue"><b class="rc5-lightblue"></b><b class="rc4-lightblue"></b><b class="rc3-lightblue"></b><b class="rc2-lightblue"><b></b></b><b class="rc1-lightblue"><b></b></b></b>*/
/*                             </div>*/
/* */
/* */
/*                             <div class="clear"></div>*/
/* */
/* */
/*                         </div>*/
/*                         <b class="rc-blue"><b class="rc5-blue"></b><b class="rc4-blue"></b><b class="rc3-blue"></b><b class="rc2-blue"><b></b></b><b class="rc1-blue"><b></b></b></b>*/
/*                     </div>*/
/*                     <div class="clear"></div>*/
/* */
/* */
/* */
/* */
/*                 </div>*/
/*             </div>*/
/*             </div>*/
/*         </body>*/
/*     </html>*/
/* {% endblock %}*/
