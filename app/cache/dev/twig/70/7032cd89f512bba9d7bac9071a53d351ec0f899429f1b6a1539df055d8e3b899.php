<?php

/* PiDevClientBundle:Sujet:Detail.html.twig */
class __TwigTemplate_af30a89561c89be5cb2115666d98e457c23899b0df284d0dc3ddef3e2a380528 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Sujet:Detail.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html>
        <head>
            <title>All For Deal | Accueil </title>

            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
            <link href=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.min.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/style.css"), "html", null, true);
        echo "\" />
            <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
            <script type=\"text/javascript\" src=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
            <!-- start menu -->
            <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <script type=\"text/javascript\" src=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
            <script  src=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
            <!--start slider -->
            <link rel=\"stylesheet\" href=";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
            <script src=";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
            <script src=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/bootstrap.min.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/customjs.js"), "html", null, true);
        echo "></script>
            <link rel=\"stylesheet\" href=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/bootstrap.min.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <link rel=\"stylesheet\" href=";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/custom.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <!--end slider -->
            <link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">

            <script src=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.load.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.ready.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.slides.js"), "html", null, true);
        echo "></script>
            <!-- start details -->
            <script src=";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/slides.min.jquery.js"), "html", null, true);
        echo "></script>

            <link rel=\"stylesheet\" href=";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/etalage.css"), "html", null, true);
        echo ">
            <script src=";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.etalage.min.js"), "html", null, true);
        echo "></script>

        </head>
        <body>

            <div class=\"mens\">    
                <div class=\"main\">
                    <div class=\"wrap\">
                        <div class=\"cont span_2_of_3\">



                            <div class=\"clear\"></div>\t

                            <div class=\"toogle\">

                                <h1>";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sujet"]) ? $context["sujet"] : $this->getContext($context, "sujet")), "titre", array()), "html", null, true);
        echo "</h1>
                                <p class=\"lead\">by <a>";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sujet"]) ? $context["sujet"] : $this->getContext($context, "sujet")), "user", array()), "username", array()), "html", null, true);
        echo "</a></p>
                                <hr>
                                <p><i class=\"fa fa-calendar\"></i> Publié le ";
        // line 62
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["sujet"]) ? $context["sujet"] : $this->getContext($context, "sujet")), "dateAjout", array()), "j M \\à g:ia \\e\\n Y "), "html", null, true);
        echo "</p>
                                <hr>
                                <center>     <img src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("sujet_image", array("id" => $this->getAttribute((isset($context["sujet"]) ? $context["sujet"] : $this->getContext($context, "sujet")), "id", array()))), "html", null, true);
        echo "\" width=\"200px\" height=\"100px\" class=\"img-responsive\">
                                </center>   <hr>
                                ";
        // line 66
        echo $this->getAttribute((isset($context["sujet"]) ? $context["sujet"] : $this->getContext($context, "sujet")), "sujet", array());
        echo "

                                <hr>
                                <h3 class=\"m_3\">Commentaires</h3> 
                                <div class=\"well\">
                                    <h4>Ajouter un commentaire:</h4>
                                    <form name=\"commentaire\" action=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail_sujet", array("sujet" => $this->getAttribute((isset($context["sujet"]) ? $context["sujet"] : $this->getContext($context, "sujet")), "id", array()))), "html", null, true);
        echo "\" method=\"post\">
                                        <div class=\"form-group\">
                                            ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentaire", array()), 'widget');
        echo "
                                        </div>
                                        <button type=\"submit\" action=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail_sujet", array("sujet" => $this->getAttribute((isset($context["sujet"]) ? $context["sujet"] : $this->getContext($context, "sujet")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-comment\"></i> Commenter</button>
                                    </form>
                                </div>
                                <hr>
                                <div>

                                    ";
        // line 82
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 83
            echo "
                                        <h4>  <span >  <img src=";
            // line 84
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
            echo " alt=\"\" width=\"50px\" height=\"50px\"/> </span> 
                                            ";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "html", null, true);
            echo ": <small>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "DateCom", array()), "Y-m-d H:i:s"), "html", null, true);
            echo "</small>
                                            ";
            // line 86
            if (($this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()) == $this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "id", array()))) {
                // line 87
                echo "                                                <span class=\"popbtn\"><a class=\"arrow\"></a></span></h4>
                                                ";
            }
            // line 89
            echo "                                        <p>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "commentaire", array()), "html", null, true);
            echo " </p> 


                                        ";
            // line 92
            if (($this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()) == $this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "id", array()))) {
                // line 93
                echo "                                            <div id=\"popover\" style=\"display: none\">
                                                <a href=\"";
                // line 94
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentairesujet_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-pencil\"></span></a>
                                                <a href=\"";
                // line 95
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentairesujet_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-remove\"></span></a>\t\t\t

                                            </div>



                                        ";
            }
            // line 101
            echo "  
                                        <hr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=\"rsingle span_1_of_single\">

                        <h5 class=\"m_1\">Blog Search</h5>
                        <div class=\"input-group\">

                            <input type=\"text\" name=\"search\" >

                            <button class=\"btn btn-info\" type=\"button\">
                                <i class=\"fa fa-search\"></i>
                            </button>

                        </div>
                        <hr>
                        <h5 class=\"m_1\">Autres sujet</h5>
                        <select class=\"dropdown\" tabindex=\"8\" data-settings='{\"wrapperClass\":\"metro\"}'>
                            <option value=\"1\">Mens</option>
                            <option value=\"2\">Sub Category1</option>
                            <option value=\"3\">Sub Category2</option>
                            <option value=\"4\">Sub Category3</option>
                        </select>
                        <select class=\"dropdown\" tabindex=\"8\" data-settings='{\"wrapperClass\":\"metro\"}'>
                            <option value=\"1\">Womens</option>
                            <option value=\"2\">Sub Category1</option>
                            <option value=\"3\">Sub Category2</option>
                            <option value=\"4\">Sub Category3</option>
                        </select>
                        <ul class=\"kids\">
                            <li><a href=\"#\">Kids</a></li>
                            <li class=\"last\"><a href=\"#\">Glasses Shop</a></li>
                        </ul>
                    </div>
                </div>

                <div class=\"clear\"></div>
            </div>
        </div>


    </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Sujet:Detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  273 => 104,  265 => 101,  255 => 95,  251 => 94,  248 => 93,  246 => 92,  239 => 89,  235 => 87,  233 => 86,  227 => 85,  223 => 84,  220 => 83,  216 => 82,  207 => 76,  202 => 74,  197 => 72,  188 => 66,  183 => 64,  178 => 62,  173 => 60,  169 => 59,  150 => 43,  146 => 42,  141 => 40,  136 => 38,  132 => 37,  128 => 36,  123 => 34,  118 => 32,  114 => 31,  110 => 30,  106 => 29,  102 => 28,  98 => 27,  94 => 26,  90 => 25,  86 => 24,  81 => 22,  77 => 21,  73 => 20,  68 => 18,  63 => 16,  59 => 15,  55 => 14,  51 => 13,  47 => 12,  43 => 11,  39 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html>*/
/*         <head>*/
/*             <title>All For Deal | Accueil </title>*/
/* */
/*             <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.min.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/style.css') }}" />*/
/*             <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*             <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*             <!-- start menu -->*/
/*             <link href={{asset('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*             <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*             <!--start slider -->*/
/*             <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*             <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*             <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*             <script src={{asset('js/fwslider.js')}}></script>*/
/*             <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/bootstrap.min.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/customjs.js')}}></script>*/
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/bootstrap.min.css')}} type="text/css" media="all" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/custom.css')}} type="text/css" media="all" /> */
/*             <!--end slider -->*/
/*             <link rel="stylesheet" href="{{asset('css/style.css')}}">*/
/* */
/*             <script src={{asset('js/jquery.load.js')}}></script>*/
/*             <script src={{asset('js/jquery.ready.js')}}></script>*/
/*             <script src={{asset('js/jquery.slides.js')}}></script>*/
/*             <!-- start details -->*/
/*             <script src={{asset('js/slides.min.jquery.js')}}></script>*/
/* */
/*             <link rel="stylesheet" href={{asset('css/etalage.css')}}>*/
/*             <script src={{asset('js/jquery.etalage.min.js')}}></script>*/
/* */
/*         </head>*/
/*         <body>*/
/* */
/*             <div class="mens">    */
/*                 <div class="main">*/
/*                     <div class="wrap">*/
/*                         <div class="cont span_2_of_3">*/
/* */
/* */
/* */
/*                             <div class="clear"></div>	*/
/* */
/*                             <div class="toogle">*/
/* */
/*                                 <h1>{{sujet.titre}}</h1>*/
/*                                 <p class="lead">by <a>{{sujet.user.username}}</a></p>*/
/*                                 <hr>*/
/*                                 <p><i class="fa fa-calendar"></i> Publié le {{sujet.dateAjout|date('j M \\à g:ia \\e\\n Y ')}}</p>*/
/*                                 <hr>*/
/*                                 <center>     <img src="{{path('sujet_image',{'id':sujet.id})}}" width="200px" height="100px" class="img-responsive">*/
/*                                 </center>   <hr>*/
/*                                 {{sujet.sujet|raw}}*/
/* */
/*                                 <hr>*/
/*                                 <h3 class="m_3">Commentaires</h3> */
/*                                 <div class="well">*/
/*                                     <h4>Ajouter un commentaire:</h4>*/
/*                                     <form name="commentaire" action="{{path('detail_sujet',{'sujet':sujet.id})}}" method="post">*/
/*                                         <div class="form-group">*/
/*                                             {{ form_widget(form.commentaire)}}*/
/*                                         </div>*/
/*                                         <button type="submit" action="{{path('detail_sujet',{'sujet':sujet.id})}}" class="btn btn-info"><i class="fa fa-comment"></i> Commenter</button>*/
/*                                     </form>*/
/*                                 </div>*/
/*                                 <hr>*/
/*                                 <div>*/
/* */
/*                                     {% for entity in entities %}*/
/* */
/*                                         <h4>  <span >  <img src={{asset(path('image'))}} alt="" width="50px" height="50px"/> </span> */
/*                                             {{ entity.user.username }}: <small>{{entity.DateCom|date('Y-m-d H:i:s')}}</small>*/
/*                                             {% if entity.user.id == utilisateur.id %}*/
/*                                                 <span class="popbtn"><a class="arrow"></a></span></h4>*/
/*                                                 {% endif %}*/
/*                                         <p>{{ entity.commentaire }} </p> */
/* */
/* */
/*                                         {% if entity.user.id == utilisateur.id %}*/
/*                                             <div id="popover" style="display: none">*/
/*                                                 <a href="{{path('commentairesujet_edit',{'id':entity.id})}}"><span class="glyphicon glyphicon-pencil"></span></a>*/
/*                                                 <a href="{{path('commentairesujet_delete',{'id':entity.id})}}"><span class="glyphicon glyphicon-remove"></span></a>			*/
/* */
/*                                             </div>*/
/* */
/* */
/* */
/*                                         {% endif %}  */
/*                                         <hr>*/
/*                                     {% endfor %}*/
/* */
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="rsingle span_1_of_single">*/
/* */
/*                         <h5 class="m_1">Blog Search</h5>*/
/*                         <div class="input-group">*/
/* */
/*                             <input type="text" name="search" >*/
/* */
/*                             <button class="btn btn-info" type="button">*/
/*                                 <i class="fa fa-search"></i>*/
/*                             </button>*/
/* */
/*                         </div>*/
/*                         <hr>*/
/*                         <h5 class="m_1">Autres sujet</h5>*/
/*                         <select class="dropdown" tabindex="8" data-settings='{"wrapperClass":"metro"}'>*/
/*                             <option value="1">Mens</option>*/
/*                             <option value="2">Sub Category1</option>*/
/*                             <option value="3">Sub Category2</option>*/
/*                             <option value="4">Sub Category3</option>*/
/*                         </select>*/
/*                         <select class="dropdown" tabindex="8" data-settings='{"wrapperClass":"metro"}'>*/
/*                             <option value="1">Womens</option>*/
/*                             <option value="2">Sub Category1</option>*/
/*                             <option value="3">Sub Category2</option>*/
/*                             <option value="4">Sub Category3</option>*/
/*                         </select>*/
/*                         <ul class="kids">*/
/*                             <li><a href="#">Kids</a></li>*/
/*                             <li class="last"><a href="#">Glasses Shop</a></li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*                 <div class="clear"></div>*/
/*             </div>*/
/*         </div>*/
/* */
/* */
/*     </div>*/
/* </form>*/
/* {% endblock %}*/
