<?php

/* PiDevClientBundle:Panier:new.html.twig */
class __TwigTemplate_dd714925dee1fd682a80f32ce73413281c63cbf7fc431ef65f3fd4abb2f64173 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Panier:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"actions-wrapper\">

\t\t\t


<h5 class=\"widget-name\"><i class=\"icon-th\"></i>Ajouter un nouveau produit </h5>

<div class=\"media row-fluid\">
    
    <form id=\"validate\" class=\"form-horizontal\" method=\"post\" action=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("lignecommande_create", array("produit" => $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "id", array()))), "html", null, true);
        echo "\">
        <fieldset>
        <!-- Form validation -->
             <div class=\"widget\">
\t       
\t         <div class=\"well row-fluid\">
           
          <div class=\"control-group\">
            <label class=\"control-label\">Quantite<span class=\"text-error\">*</span></label>
            <div class=\"controls\">             
                       ";
        // line 23
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
                
            </div>
        </div>
                          
        <div class=\"form-actions align-right\">
        
            <button type=\"submit\" class=\"btn btn-info\">Valider</button>

            <button type=\"reset\" class=\"btn\">Reset</button>
            <ul class=\"record_actions\" style=\"display: inline-block\">
            <li>
                <a href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("panier");
        echo "\">
                    Retour à l'accueil
                </a>
            </li>
        </ul>
        </div>
        
    </div>
  </div>
\t              
        </fieldset>
</form>

</div>
                
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Panier:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 35,  55 => 23,  42 => 13,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* <div class="actions-wrapper">*/
/* */
/* 			*/
/* */
/* */
/* <h5 class="widget-name"><i class="icon-th"></i>Ajouter un nouveau produit </h5>*/
/* */
/* <div class="media row-fluid">*/
/*     */
/*     <form id="validate" class="form-horizontal" method="post" action="{{path('lignecommande_create',{'produit':produit.id})}}">*/
/*         <fieldset>*/
/*         <!-- Form validation -->*/
/*              <div class="widget">*/
/* 	       */
/* 	         <div class="well row-fluid">*/
/*            */
/*           <div class="control-group">*/
/*             <label class="control-label">Quantite<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{form(form)}}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*                           */
/*         <div class="form-actions align-right">*/
/*         */
/*             <button type="submit" class="btn btn-info">Valider</button>*/
/* */
/*             <button type="reset" class="btn">Reset</button>*/
/*             <ul class="record_actions" style="display: inline-block">*/
/*             <li>*/
/*                 <a href="{{ path('panier') }}">*/
/*                     Retour à l'accueil*/
/*                 </a>*/
/*             </li>*/
/*         </ul>*/
/*         </div>*/
/*         */
/*     </div>*/
/*   </div>*/
/* 	              */
/*         </fieldset>*/
/* </form>*/
/* */
/* </div>*/
/*                 */
/* {% endblock %}*/
/* */
