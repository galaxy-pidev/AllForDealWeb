<?php

/* PiDevClientBundle:Produit:list.html.twig */
class __TwigTemplate_de70f66d78c2914fc08d52306769d220777ad90d26945afc28a58bfbab44c105 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:list.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <center>  <title> Liste des Produit All for deal: </title><br> </center>
    <forum>
       <table style=\"width:100%\">
           <tr > <td>id</td>
               <td> Nom  </td>
               <td> Description </td>
               <td> Prix  </td>
               <td> Quantité  </td>
               <td> Promotion  </td> 
               <td> Marque </td> 
               <td> NBvente  </td>
              
               <td> dateAjout  </td>

               <td> Nbpointbase </td>  
               <td> Categorie </td>  
       </tr>
             ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            // line 21
            echo "         
";
            // line 22
            if (($this->getAttribute($context["j"], "valider", array()) == 1)) {
                // line 23
                echo "
      
        <tr>
            
            <td>";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nom", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "description", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "prix", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "qantite", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "promotion", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "marque", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nbvente", array()), "html", null, true);
                echo "</td>
            
            <td>";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["j"], "dateajout", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
                echo "</td>
            <td>";
                // line 37
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nbpointbase", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "categorie", array()), "html", null, true);
                echo "</td>
        </tr>
        
 ";
            }
            // line 41
            echo "        
       
 
         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "   

       </table>
    </forum>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 45,  113 => 41,  106 => 38,  102 => 37,  98 => 36,  93 => 34,  89 => 33,  85 => 32,  81 => 31,  77 => 30,  73 => 29,  69 => 28,  65 => 27,  59 => 23,  57 => 22,  54 => 21,  50 => 20,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/*     <center>  <title> Liste des Produit All for deal: </title><br> </center>*/
/*     <forum>*/
/*        <table style="width:100%">*/
/*            <tr > <td>id</td>*/
/*                <td> Nom  </td>*/
/*                <td> Description </td>*/
/*                <td> Prix  </td>*/
/*                <td> Quantité  </td>*/
/*                <td> Promotion  </td> */
/*                <td> Marque </td> */
/*                <td> NBvente  </td>*/
/*               */
/*                <td> dateAjout  </td>*/
/* */
/*                <td> Nbpointbase </td>  */
/*                <td> Categorie </td>  */
/*        </tr>*/
/*              {% for j in m %}*/
/*          */
/* {% if( j.valider == 1) %}*/
/* */
/*       */
/*         <tr>*/
/*             */
/*             <td>{{j.id}}</td>*/
/*             <td>{{j.nom}}</td>*/
/*             <td>{{j.description}}</td>*/
/*             <td>{{j.prix}}</td>*/
/*             <td>{{j.qantite}}</td>*/
/*             <td>{{j.promotion}}</td>*/
/*             <td>{{j.marque}}</td>*/
/*             <td>{{j.nbvente}}</td>*/
/*             */
/*             <td>{{j.dateajout.format('Y-m-d')}}</td>*/
/*             <td>{{j.nbpointbase}}</td>*/
/*             <td>{{j.categorie}}</td>*/
/*         </tr>*/
/*         */
/*  {% endif %}        */
/*        */
/*  */
/*          {% endfor %}*/
/*    */
/* */
/*        </table>*/
/*     </forum>*/
/* {% endblock %}*/
