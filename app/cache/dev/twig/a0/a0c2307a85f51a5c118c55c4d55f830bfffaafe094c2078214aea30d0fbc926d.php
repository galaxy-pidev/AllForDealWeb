<?php

/* PiDevClientBundle:Commentaire:show.html.twig */
class __TwigTemplate_0d246b73349ec67d48e868d76168fdd5187e52b5af709779e11371c4f74bce1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Commentaire:show.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <!-- start details -->
    <script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/slides.min.jquery.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function() {
            \$('#products').slides({
                preload: true,
                preloadImage: 'img/loading.gif',
                effect: 'slide, fade',
                crossfade: true,
                slideSpeed: 350,
                fadeSpeed: 500,
                generateNextPrev: true,
                generatePagination: false
            });
        });
    </script>
    <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/etalage.css"), "html", null, true);
        echo "\">
    <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.etalage.min.js"), "html", null, true);
        echo "\"></script>
    <script>
           jQuery(document).ready(function(\$) {

               \$('#etalage').etalage({
                   thumb_image_width: 360,
                   thumb_image_height: 360,
                   source_image_width: 900,
                   source_image_height: 900,
                   show_hint: true,
                   click_callback: function(image_anchor, instance_id) {
                       alert('Callback example:\\nYou clicked on an image with the anchor: \"' + image_anchor + '\"\\n(in Etalage instance: \"' + instance_id + '\")');
                   }
               });

           });
    </script>\t
  asset('images/s-img.jpg')
    <div class=\"main\">
        <div class=\"wrap\">
            <ul class=\"breadcrumb breadcrumb__t\"><a class=\"home\" href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("produit");
        echo "\">Accueil</a>
                / <a href=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath("produit");
        echo "\">Produit</a></ul>
            <div class=\"cont span_2_of_3\">
                <div class=\"grid images_3_of_2\">
          <img class=\"etalage5_thumb_image\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "urlImage", array()), "html", null, true);
        echo "\" class=\"img-responsive\" />

                    <div class=\"clearfix\"></div>
                </div>
                         <div class=\"desc1 span_3_of_2\">
                   \t<h3 class=\"m_3\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nom", array()), "html", null, true);
        echo "</h3>
\t\t             <p class=\"m_5\">Prix. ";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prix", array()), "html", null, true);
        echo " DT<span class=\"reducedfrom\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prix", array()), "html", null, true);
        echo " DT</span> </p>
\t\t         \t 
\t\t         \t \t<div class=\"availablelhg\">
\t\t\t\t\t\t  <table class=\"record_properties\">
                            <tbody>

                               
                                <tr>
                                    <th>Marque</th>
                                    <td>";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "marque", array()), "html", null, true);
        echo "</td>
                                </tr>
                                <tr>
                                    <th>Quantite en stock:</th>
                                    <td>";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "qantite", array()), "html", null, true);
        echo "</td>
                                </tr>
                                <tr>
                                    <th>Promotion</th>
                                    <td>";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "promotion", array()), "html", null, true);
        echo "%</td>
                                </tr>                                
                                     <tr>
                                    <th>---------------------------</th>
                                    <td></td>
                                </tr>                     
                            </tbody>
                        </table>
\t\t\t\t\t</div>

\t\t\t\t     <p class=\"m_text2\">";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "description", array()), "html", null, true);
        echo "</p>
\t\t\t\t      <div class=\"btn_form\">
                                          
                                           <form action=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_create", array("id" => $this->getAttribute((isset($context["panier"]) ? $context["panier"] : $this->getContext($context, "panier")), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" method=\"get\">
                            <select name=\"qte\" class=\"span1\">
                                ";
        // line 82
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 10));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 83
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "                            </select> 
                            <div>
                                 <button class=\"btn btn-primary\">Ajouter au panier</button>
                            </div>
                            </form>
                                 <a href=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_create", array("id" => $this->getAttribute((isset($context["favoris"]) ? $context["favoris"] : $this->getContext($context, "favoris")), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">Favoris</a>
\t\t\t\t<a href=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_create", array("id" => $this->getAttribute((isset($context["panier"]) ? $context["panier"] : $this->getContext($context, "panier")), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">Ajouter au panier</a>\t\t\t
\t\t\t\t\t </div>
\t\t\t     </div>

\t\t\t   <div class=\"clear\"></div>\t
\t    <div class=\"clients\">
\t  
\t</script>
\t<script type=\"text/javascript\" src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.flexisel.js"), "html", null, true);
        echo "\"></script>
     </div>
     <div class=\"toogle\">
     \t<h3 class=\"m_3\">Commentaires</h3>
     \t<p class=\"m_text\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
     </div>
     
      </div>
\t\t\t
\t\t\t <div class=\"clear\"></div>
\t\t   </div>
\t\t</div>
                

            </div>
        ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commentaire:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 99,  179 => 91,  175 => 90,  168 => 85,  157 => 83,  153 => 82,  148 => 80,  142 => 77,  129 => 67,  122 => 63,  115 => 59,  101 => 50,  97 => 49,  89 => 44,  83 => 41,  79 => 40,  56 => 20,  52 => 19,  34 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/*     <!-- start details -->*/
/*     <script src="{{asset('js/slides.min.jquery.js')}}"></script>*/
/*     <script>*/
/*         $(function() {*/
/*             $('#products').slides({*/
/*                 preload: true,*/
/*                 preloadImage: 'img/loading.gif',*/
/*                 effect: 'slide, fade',*/
/*                 crossfade: true,*/
/*                 slideSpeed: 350,*/
/*                 fadeSpeed: 500,*/
/*                 generateNextPrev: true,*/
/*                 generatePagination: false*/
/*             });*/
/*         });*/
/*     </script>*/
/*     <link rel="stylesheet" href="{{asset('css/etalage.css')}}">*/
/*     <script src="{{asset('js/jquery.etalage.min.js')}}"></script>*/
/*     <script>*/
/*            jQuery(document).ready(function($) {*/
/* */
/*                $('#etalage').etalage({*/
/*                    thumb_image_width: 360,*/
/*                    thumb_image_height: 360,*/
/*                    source_image_width: 900,*/
/*                    source_image_height: 900,*/
/*                    show_hint: true,*/
/*                    click_callback: function(image_anchor, instance_id) {*/
/*                        alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');*/
/*                    }*/
/*                });*/
/* */
/*            });*/
/*     </script>	*/
/*   asset('images/s-img.jpg')*/
/*     <div class="main">*/
/*         <div class="wrap">*/
/*             <ul class="breadcrumb breadcrumb__t"><a class="home" href="{{ path('produit') }}">Accueil</a>*/
/*                 / <a href="{{ path('produit') }}">Produit</a></ul>*/
/*             <div class="cont span_2_of_3">*/
/*                 <div class="grid images_3_of_2">*/
/*           <img class="etalage5_thumb_image" src="{{entity.urlImage}}" class="img-responsive" />*/
/* */
/*                     <div class="clearfix"></div>*/
/*                 </div>*/
/*                          <div class="desc1 span_3_of_2">*/
/*                    	<h3 class="m_3">{{ entity.nom }}</h3>*/
/* 		             <p class="m_5">Prix. {{ entity.prix }} DT<span class="reducedfrom">{{ entity.prix }} DT</span> </p>*/
/* 		         	 */
/* 		         	 	<div class="availablelhg">*/
/* 						  <table class="record_properties">*/
/*                             <tbody>*/
/* */
/*                                */
/*                                 <tr>*/
/*                                     <th>Marque</th>*/
/*                                     <td>{{ entity.marque }}</td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <th>Quantite en stock:</th>*/
/*                                     <td>{{ entity.qantite }}</td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <th>Promotion</th>*/
/*                                     <td>{{ entity.promotion }}%</td>*/
/*                                 </tr>                                */
/*                                      <tr>*/
/*                                     <th>---------------------------</th>*/
/*                                     <td></td>*/
/*                                 </tr>                     */
/*                             </tbody>*/
/*                         </table>*/
/* 					</div>*/
/* */
/* 				     <p class="m_text2">{{ entity.description }}</p>*/
/* 				      <div class="btn_form">*/
/*                                           */
/*                                            <form action="{{path('panier_create',{'id':panier.id,'produit':entity.id})}}" method="get">*/
/*                             <select name="qte" class="span1">*/
/*                                 {% for i in 1..10 %}*/
/*                                     <option value="{{ i }}">{{ i }}</option>*/
/*                                 {% endfor %}*/
/*                             </select> */
/*                             <div>*/
/*                                  <button class="btn btn-primary">Ajouter au panier</button>*/
/*                             </div>*/
/*                             </form>*/
/*                                  <a href="{{path('favoris_create',{'id':favoris.id,'produit':entity.id})}}">Favoris</a>*/
/* 				<a href="{{path('panier_create',{'id':panier.id,'produit':entity.id})}}">Ajouter au panier</a>			*/
/* 					 </div>*/
/* 			     </div>*/
/* */
/* 			   <div class="clear"></div>	*/
/* 	    <div class="clients">*/
/* 	  */
/* 	</script>*/
/* 	<script type="text/javascript" src="{{asset('js/jquery.flexisel.js')}}"></script>*/
/*      </div>*/
/*      <div class="toogle">*/
/*      	<h3 class="m_3">Commentaires</h3>*/
/*      	<p class="m_text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>*/
/*      </div>*/
/*      */
/*       </div>*/
/* 			*/
/* 			 <div class="clear"></div>*/
/* 		   </div>*/
/* 		</div>*/
/*                 */
/* */
/*             </div>*/
/*         {% endblock %}*/
