<?php

/* PiDevClientBundle:Client:listAppelOffres.html.twig */
class __TwigTemplate_40588d02b829ded5a2777026666a1ba5dc8bdd2fd5cc9a724d0e62d71628ae64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Client:listAppelOffres.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <head>

    </head>
    <body>
        <!-- BEGAIN PRELOADER -->   
        <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Produits</strong></h3>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading bg-red\">
                            <h3 class=\"panel-title\"><strong>Liste des </strong> Produits</h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 m-b-20\">

                                    <div class=\"btn-group pull-right\">
                                        <button class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">Tools <i class=\"fa fa-angle-down\"></i>
                                        </button>
                                        <ul class=\"dropdown-menu pull-right\">
                                            <li><a href=\"#\">Print</a>
                                            </li>
                                            <li><a href=\"#\">Save as PDF</a>
                                            </li>
                                            <li><a href=\"#\">Export to Excel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive table-red\">
                                    <table class=\"table table-striped table-hover dataTable\" id=\"table-editable\">

                                        <tbody>
                                        <thead>
                                            <tr role=\"row\" >
                                                <th class=\"text-center\">Nom</th>
                                                <th class=\"text-center\">Mail</th>
                                                <th class=\"text-center\">Tel</th>
                                                <th class=\"text-center\">Adresse</th>
                                                <th class=\"text-center\">Description</th>
                                                <th class=\"text-center\">Action</th>
                                            </tr>
                                        </thead>
                                        ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["appeloffres"]) ? $context["appeloffres"] : $this->getContext($context, "appeloffres")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 50
            echo "                                            <tr>
                                                <td class=\"text-center\">";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</td>
                                                <td class=\"text-center\">";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "mail", array()), "html", null, true);
            echo "</td>  
                                                <td class=\"text-center\">";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "telephone", array()), "html", null, true);
            echo "</td> 
                                                <td class=\"text-center\">";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "adresse", array()), "html", null, true);
            echo "</td>  
                                                <td class=\"text-center\" >";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</td> 
                                                <td class=\"text-center\">
                                                    <a class=\"edit btn btn-dark\" href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modifier_appel_offre", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-pencil-square-o\"></i>Modifier</a> 
                                                    <a class=\"delete btn btn-danger\" href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supprimer_appel_offre", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-times-circle\"></i> Supprimer</a>
                                                </td>

                                            </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "                                                                                                                               
                                        </tbody>
                                    </table>

                                    <!-- nom  -->                                  

                                    <div id=\"modal\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>

                                        <section class=\"popupBodny\">
                                            <!-- Username & Password Login form -->
                                            <div class=\"user_login\">
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>




    </body>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:listAppelOffres.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 62,  111 => 58,  107 => 57,  102 => 55,  98 => 54,  94 => 53,  90 => 52,  86 => 51,  83 => 50,  79 => 49,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content  %}*/
/*     <head>*/
/* */
/*     </head>*/
/*     <body>*/
/*         <!-- BEGAIN PRELOADER -->   */
/*         <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Produits</strong></h3>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading bg-red">*/
/*                             <h3 class="panel-title"><strong>Liste des </strong> Produits</h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 m-b-20">*/
/* */
/*                                     <div class="btn-group pull-right">*/
/*                                         <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>*/
/*                                         </button>*/
/*                                         <ul class="dropdown-menu pull-right">*/
/*                                             <li><a href="#">Print</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Save as PDF</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Export to Excel</a>*/
/*                                             </li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">*/
/*                                     <table class="table table-striped table-hover dataTable" id="table-editable">*/
/* */
/*                                         <tbody>*/
/*                                         <thead>*/
/*                                             <tr role="row" >*/
/*                                                 <th class="text-center">Nom</th>*/
/*                                                 <th class="text-center">Mail</th>*/
/*                                                 <th class="text-center">Tel</th>*/
/*                                                 <th class="text-center">Adresse</th>*/
/*                                                 <th class="text-center">Description</th>*/
/*                                                 <th class="text-center">Action</th>*/
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         {% for i in appeloffres %}*/
/*                                             <tr>*/
/*                                                 <td class="text-center">{{i.nom}}</td>*/
/*                                                 <td class="text-center">{{i.mail}}</td>  */
/*                                                 <td class="text-center">{{i.telephone}}</td> */
/*                                                 <td class="text-center">{{i.adresse}}</td>  */
/*                                                 <td class="text-center" >{{i.description}}</td> */
/*                                                 <td class="text-center">*/
/*                                                     <a class="edit btn btn-dark" href="{{path('modifier_appel_offre',{'id':i.id})}}"><i class="fa fa-pencil-square-o"></i>Modifier</a> */
/*                                                     <a class="delete btn btn-danger" href="{{path('supprimer_appel_offre',{'id':i.id})}}"><i class="fa fa-times-circle"></i> Supprimer</a>*/
/*                                                 </td>*/
/* */
/*                                             </tr>*/
/*                                         {% endfor %}                                                                                                                               */
/*                                         </tbody>*/
/*                                     </table>*/
/* */
/*                                     <!-- nom  -->                                  */
/* */
/*                                     <div id="modal" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/* */
/*                                         <section class="popupBodny">*/
/*                                             <!-- Username & Password Login form -->*/
/*                                             <div class="user_login">*/
/*                                             </div>*/
/*                                         </section>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- END MAIN CONTENT -->*/
/*         </div>*/
/* */
/* */
/* */
/* */
/*     </body>*/
/* {% endblock %}*/
/* */
