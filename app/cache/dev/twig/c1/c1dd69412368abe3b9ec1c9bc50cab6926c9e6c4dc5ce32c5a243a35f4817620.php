<?php

/* PiDevClientBundle:Reclamation:reclamation.html.twig */
class __TwigTemplate_e595cee0abb44b98d542800a4824f6d11901de965af1915e698fc125db336666 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Reclamation:reclamation.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<body>
Votre demande a été envoyée avec succès
</body>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Reclamation:reclamation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* <body>*/
/* Votre demande a été envoyée avec succès*/
/* </body>*/
/*  {% endblock %}*/
