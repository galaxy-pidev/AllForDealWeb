<?php

/* PiDevClientBundle:Service:modifier.html.twig */
class __TwigTemplate_5457903b40a15ec2a48100055f9de0e613fb3753e220a10c5c992a9103d05dd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Service:modifier.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h1>Modifier Service </h1>
<form action=\"\" method=\"post\" >
    <table> 
    <tr>
        <td>nom: </td>
        <td>";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget');
        echo "</td>  
    </tr>
    <tr>  
    <td>   Description:</td>
    <td>";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Description", array()), 'widget');
        echo "</td>
    </tr>
</table>
    <br>
  
";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
</form>

  ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Service:modifier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 17,  45 => 12,  38 => 8,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* <h1>Modifier Service </h1>*/
/* <form action="" method="post" >*/
/*     <table> */
/*     <tr>*/
/*         <td>nom: </td>*/
/*         <td>{{ form_widget(form.nom)}}</td>  */
/*     </tr>*/
/*     <tr>  */
/*     <td>   Description:</td>*/
/*     <td>{{ form_widget(form.Description)}}</td>*/
/*     </tr>*/
/* </table>*/
/*     <br>*/
/*   */
/* {{form_rest(form)}}*/
/* </form>*/
/* */
/*   {% endblock %}*/
