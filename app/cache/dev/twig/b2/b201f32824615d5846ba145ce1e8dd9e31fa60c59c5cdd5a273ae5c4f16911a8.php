<?php

/* PiDevAdminBundle:CategorieService:ajout.html.twig */
class __TwigTemplate_7333e3bff31722ea6437954d97e9f5c74c71c97fa41dc1f13938bf91ff11ae85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:CategorieService:ajout.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    
";
        // line 6
        echo "<form method='post'>
<table>
    
     <tr><td>Type:</td><td>";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "id", array()), 'widget');
        echo "</td></tr>
    <tr><td>Description:</td><td>";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Description", array()), 'widget');
        echo "</td></tr>
</table>
      ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo "
</form>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:CategorieService:ajout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 12,  43 => 10,  39 => 9,  34 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {%extends "::baseAdmin.html.twig"%}*/
/* {% block content %}*/
/*     */
/* {# empty Twig template #}*/
/* {# empty Twig template #}*/
/* <form method='post'>*/
/* <table>*/
/*     */
/*      <tr><td>Type:</td><td>{{form_widget(f.id)}}</td></tr>*/
/*     <tr><td>Description:</td><td>{{form_widget(f.Description)}}</td></tr>*/
/* </table>*/
/*       {{form_rest(f)}}*/
/* </form>*/
/*  {% endblock %}*/
