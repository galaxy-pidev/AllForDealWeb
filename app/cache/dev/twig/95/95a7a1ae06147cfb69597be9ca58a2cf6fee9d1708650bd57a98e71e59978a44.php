<?php

/* PiDevClientBundle:Produit:ajout.html.twig */
class __TwigTemplate_3734f3338ee0f7b19b04eccecc489b4c224a3b87c91ceb24a15f0eb4d5e7e96c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:ajout.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        // line 125
        echo "  
    

    <h1>Produit creation</h1>
  

    ";
        // line 131
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "


        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 136
        echo $this->env->getExtension('routing')->getPath("produit");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>
    
    
            ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:ajout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 136,  41 => 131,  33 => 125,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/*     {# */
/* <h1>Produit creation</h1>*/
/*   */
/* */
/*     {{ form(form) }}*/
/* */
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('produit') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/* </ul>*/
/*     */
/* <div class="actions-wrapper">*/
/* 				    <div class="actions">*/
/* */
/* 			*/
/* <h5 class="widget-name"><i class="icon-th"></i>Ajouter un nouveau produit </h5>*/
/* */
/* <div class="media row-fluid">*/
/*     {{ form_errors(form) }}*/
/*     <form id="validate" class="form-horizontal" method="post" action="{{path('produit_create')}}">*/
/*         <fieldset>*/
/*         <!-- Form validation -->*/
/*              <div class="widget">*/
/* 	       */
/* 	         <div class="well well row-fluid">*/
/*            */
/*           <div class="control-group">*/
/*             <label class="control-label">Nom<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{ form_widget(form.nom)}}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*    */
/*         <div class="control-group">*/
/*             <label class="control-label">Description :<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{ form_widget(form.description) }}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <div class="control-group">*/
/*             <label class="control-label">prix:<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{ form_widget(form.prix) }}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <div class="control-group">*/
/*             <label class="control-label">qantite :<span class="text-error">*</span></label>*/
/*             <div class="controls"> */
/*                        {{ form_widget(form.qantite) }}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <div class="control-group">*/
/*             <label class="control-label">promotion:<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{ form_widget(form.promotion) }}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*                               */
/*         <div class="control-group">*/
/*             <label class="control-label">marque :<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{ form_widget(form.marque) }}*/
/*                 */
/*             </div>*/
/*                               */
/*         </div>*/
/* */
/*                                  <div class="control-group">*/
/*             <label class="control-label">nbpointbase:<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{ form_widget(form.nbpointbase) }}*/
/*                 */
/*             </div>     */
/*    */
/*         </div>*/
/*         <div class="control-group">*/
/*             <label class="control-label">Categorie:<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{ form_widget(form.categorie) }}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*                        <div class="control-group">*/
/*             <label class="control-label">Image:<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{ form_widget(form.file) }}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*                        */
/*           */
/*                   */
/* <div class="form-actions">*/
/*     {{ form_widget(form._token)}}*/
/*     <button type="submit" class="btn">valider</button>*/
/*             <button type="reset" class="btn">Reset</button>*/
/*             <ul class="record_actions" style="display: inline-block">*/
/*             <li>*/
/*                 <a href="{{ path('produit') }}">*/
/*                     Retour à l'accueil*/
/*                 </a>*/
/*             </li>*/
/*         </ul>*/
/*         </div>*/
/*         */
/*     </div>*/
/*   </div>*/
/* 	              */
/*         </fieldset>*/
/* </form>*/
/* */
/* </div>*/
/*     */
/*  #}  */
/*     */
/* */
/*     <h1>Produit creation</h1>*/
/*   */
/* */
/*     {{ form(form) }}*/
/* */
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('produit') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/* </ul>*/
/*     */
/*     */
/*             {% endblock %}*/
