<?php

/* PiDevClientBundle:Panier:show.html.twig */
class __TwigTemplate_97b234e8459b19227de79ed0cfcb557973b5fcd5354d8a9937557bc477d216c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Panier:show.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html>
        <head>
            <title>Shopping Cart</title>

            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
            <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
            <link rel=\"stylesheet\" href=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/bootstrap.min.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <link rel=\"stylesheet\" href=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/custom.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 


        </head>

        <body>

            <nav class=\"navbar\">
                <div class=\"container\">
                    <a class=\"navbar-brand\" href=\"#\">Votre panier</a>
                    <div class=\"navbar-right\">
                        <div class=\"container minicart\"></div>
                    </div>
                </div>
            </nav>


    <form method=\"POST\" > 
            <div class=\"container text-center\">

                <div class=\"col-md-5 col-sm-12\">



                    <div class=\"bigcart\"></div>
                    <h1>Your shopping cart</h1>
                    <p>
                        This is a free and <b><a href=\"http://tutorialzine.com/2014/04/responsive-shopping-cart-layout-twitter-bootstrap-3/\" title=\"Read the article!\">responsive shopping cart layout, made by Tutorialzine</a></b>. It looks nice on both desktop and mobile browsers. Try it by resizing your window (or opening it on your smartphone and pc).
                    </p>
                </div>
                                     
                <div class=\"col-md-7 col-sm-12 text-left\">
                    <ul>
                    
                        ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            echo " 
                            ";
            // line 46
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["j"]);
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo "                               
                                <li class=\"row\">
                                    <span >      <img src=\"";
                // line 48
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
                echo "\"  width=\"80px\"  />
                                           </span>

                               <select name=\"";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
                echo "\" id=\"qte\">
                                   ";
                // line 52
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["cm"]) ? $context["cm"] : $this->getContext($context, "cm")));
                foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
                    // line 53
                    echo "                                       ";
                    if (($this->getAttribute($this->getAttribute($context["m"], "produit", array()), "id", array()) == $this->getAttribute($context["i"], "id", array()))) {
                        // line 54
                        echo "                                           <option selected disabled value=\"m.id\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "qte", array()), "html", null, true);
                        echo "</option>
                                           ";
                    }
                    // line 56
                    echo "                                              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo "      
                                            <option value=\"1\">1</option>
                                            <option value=\"2\">2</option>
                                            <option value=\"3\">3</option>
                                            <option value=\"4\">4</option>
                                            <option value=\"5\">5</option>
                                            <option value=\"6\">6</option>
                                            <option value=\"7\">7</option>
                                            <option value=\"8\">8</option>
                                            <option value=\"9\">9</option>
                                            <option value=\"10\">10</option>
                                        </select>         </span>
                                    <span class=\"itemName\">";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
                echo "</span>
                                    <span class=\"popbtn\"><a class=\"arrow\"></a></span>
                                    <span class=\"price\">";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
                echo " DT</span>
                                </li>
                                ";
                // line 72
                $context["prixSP"] = ((isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")) + $this->getAttribute($context["i"], "prix", array()));
                // line 73
                echo "                                    ";
                $context["prixP"] = ((isset($context["prixP"]) ? $context["prixP"] : $this->getContext($context, "prixP")) + ($this->getAttribute($context["i"], "prix", array()) - (($this->getAttribute($context["i"], "prix", array()) * $this->getAttribute($context["i"], "promotion", array())) / 100)));
                // line 74
                echo "
                                        <div id=\"popover\" style=\"display: none\">
                                            <a href=\"";
                // line 76
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-pencil\"></span></a>
                                            <a href=\"";
                // line 77
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_remove", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "panier", array()), "id", array()), "produit" => $this->getAttribute(                // line 78
$context["i"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-remove\"></span></a>
                                        </div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "                                          
                                                <li class=\"row totals\">
                                                    <span class=\"itemName\">Total:</span>
                                                    <span class=\"price\"> ";
        // line 85
        echo twig_escape_filter($this->env, (isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")), "html", null, true);
        echo " DT</span>
                                                    <span class=\"order\"> <a class=\"text-center\" href=\"";
        // line 86
        echo $this->env->getExtension('routing')->getPath("choisir_adresse");
        echo "\">Payer </a></span>
                                                </li>
                                          
                                            </ul>
                                        </div>

                                    </div>
                                            </form>
                                    <!-- The popover content -->


                                    <!-- JavaScript includes -->

                                    <script src=\"http://code.jquery.com/jquery-1.11.0.min.js\"></script> 
                                    <script src=";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/bootstrap.min.js"), "html", null, true);
        echo "></script>
                                    <script src=";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/customjs.js"), "html", null, true);
        echo "></script>

                                </body>
                            </html>
                            ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Panier:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 101,  200 => 100,  183 => 86,  179 => 85,  174 => 82,  168 => 81,  159 => 78,  158 => 77,  154 => 76,  150 => 74,  147 => 73,  145 => 72,  140 => 70,  135 => 68,  116 => 56,  110 => 54,  107 => 53,  103 => 52,  99 => 51,  93 => 48,  86 => 46,  80 => 45,  43 => 11,  39 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html>*/
/*         <head>*/
/*             <title>Shopping Cart</title>*/
/* */
/*             <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*             <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>*/
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/bootstrap.min.css')}} type="text/css" media="all" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/custom.css')}} type="text/css" media="all" /> */
/* */
/* */
/*         </head>*/
/* */
/*         <body>*/
/* */
/*             <nav class="navbar">*/
/*                 <div class="container">*/
/*                     <a class="navbar-brand" href="#">Votre panier</a>*/
/*                     <div class="navbar-right">*/
/*                         <div class="container minicart"></div>*/
/*                     </div>*/
/*                 </div>*/
/*             </nav>*/
/* */
/* */
/*     <form method="POST" > */
/*             <div class="container text-center">*/
/* */
/*                 <div class="col-md-5 col-sm-12">*/
/* */
/* */
/* */
/*                     <div class="bigcart"></div>*/
/*                     <h1>Your shopping cart</h1>*/
/*                     <p>*/
/*                         This is a free and <b><a href="http://tutorialzine.com/2014/04/responsive-shopping-cart-layout-twitter-bootstrap-3/" title="Read the article!">responsive shopping cart layout, made by Tutorialzine</a></b>. It looks nice on both desktop and mobile browsers. Try it by resizing your window (or opening it on your smartphone and pc).*/
/*                     </p>*/
/*                 </div>*/
/*                                      */
/*                 <div class="col-md-7 col-sm-12 text-left">*/
/*                     <ul>*/
/*                     */
/*                         {% for j in produits %} */
/*                             {% for i in j %}                               */
/*                                 <li class="row">*/
/*                                     <span >      <img src="{{ asset(path('my_image_route', {'id': i.id})) }}"  width="80px"  />*/
/*                                            </span>*/
/* */
/*                                <select name="{{i.id}}" id="qte">*/
/*                                    {% for m in cm %}*/
/*                                        {% if m.produit.id == i.id %}*/
/*                                            <option selected disabled value="m.id">{{m.qte}}</option>*/
/*                                            {% endif %}*/
/*                                               {% endfor %}      */
/*                                             <option value="1">1</option>*/
/*                                             <option value="2">2</option>*/
/*                                             <option value="3">3</option>*/
/*                                             <option value="4">4</option>*/
/*                                             <option value="5">5</option>*/
/*                                             <option value="6">6</option>*/
/*                                             <option value="7">7</option>*/
/*                                             <option value="8">8</option>*/
/*                                             <option value="9">9</option>*/
/*                                             <option value="10">10</option>*/
/*                                         </select>         </span>*/
/*                                     <span class="itemName">{{i.nom}}</span>*/
/*                                     <span class="popbtn"><a class="arrow"></a></span>*/
/*                                     <span class="price">{{ i.prix}} DT</span>*/
/*                                 </li>*/
/*                                 {% set prixSP = prixSP + i.prix  %}*/
/*                                     {% set prixP = prixP + (i.prix - (i.prix *i.promotion)/100) %}*/
/* */
/*                                         <div id="popover" style="display: none">*/
/*                                             <a href="{{ path('produit_detail', { 'id': i.id }) }}"><span class="glyphicon glyphicon-pencil"></span></a>*/
/*                                             <a href="{{ path('panier_remove', { 'id': entity.panier.id ,*/
/*                                    'produit': i.id }  ) }}"><span class="glyphicon glyphicon-remove"></span></a>*/
/*                                         </div>*/
/*                                         {% endfor %}*/
/*                                             {% endfor %}*/
/*                                           */
/*                                                 <li class="row totals">*/
/*                                                     <span class="itemName">Total:</span>*/
/*                                                     <span class="price"> {{prixSP}} DT</span>*/
/*                                                     <span class="order"> <a class="text-center" href="{{ path('choisir_adresse') }}">Payer </a></span>*/
/*                                                 </li>*/
/*                                           */
/*                                             </ul>*/
/*                                         </div>*/
/* */
/*                                     </div>*/
/*                                             </form>*/
/*                                     <!-- The popover content -->*/
/* */
/* */
/*                                     <!-- JavaScript includes -->*/
/* */
/*                                     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> */
/*                                     <script src={{asset('bundles/commande/js/bootstrap.min.js')}}></script>*/
/*                                     <script src={{asset('bundles/commande/js/customjs.js')}}></script>*/
/* */
/*                                 </body>*/
/*                             </html>*/
/*                             {% endblock %}*/
