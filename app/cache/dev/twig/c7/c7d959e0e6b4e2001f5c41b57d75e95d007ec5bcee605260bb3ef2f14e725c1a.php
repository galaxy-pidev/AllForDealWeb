<?php

/* PiDevClientBundle:Pages:Accueil.html.twig */
class __TwigTemplate_1a19ff0417cdfcb18de735b282a261e70dcf6768d1e3fa728c3235bccb47e93b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Pages:Accueil.html.twig", 1);
        $this->blocks = array(
            'slider' => array($this, 'block_slider'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_slider($context, array $blocks = array())
    {
        echo "   
    <script src=";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/login.js"), "html", null, true);
        echo " ></script>
    <div id=\"fwslider\">
        <div class=\"slider_container\">
            ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["publicites"]) ? $context["publicites"] : $this->getContext($context, "publicites")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 7
            echo "                <div class=\"slide\"> 
                    <!-- Slide image -->
                    <img src=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("publicite_image", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\" width=\"200px\" height=\"400px\"alt=\"special offers\"/>                           
                    <!-- /Slide image -->
                    <!-- Texts container -->
                    <div class=\"slide_content\">
                        <div class=\"slide_content_wrap\">
                            <!-- Text title -->
                            <h4 class=\"title\">";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "type", array()), "html", null, true);
            echo "</h4>
                            <!-- /Text title -->

                            <!-- Text description -->
                            <p class=\"description\">";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "sujet", array()), "html", null, true);
            echo "</p>
                            <!-- /Text description -->
                        </div>
                    </div>
                    <!-- /Texts container -->
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "
            <!-- /Duplicate to create more slides -->

            <!--/slide -->
        </div>
        <div class=\"timers\"></div>
        <div class=\"slidePrev\"><span></span></div>
        <div class=\"slideNext\"><span></span></div>
    </div>


";
    }

    // line 38
    public function block_content($context, array $blocks = array())
    {
        // line 39
        echo "    <link href=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link href=";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.min.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link href=";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link href=";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link href=";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    <link href=";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
    <!-- Bootstrap style --> 
    <link href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/Accueil/themes/css/base.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\"/>
    <!-- Bootstrap style responsive -->\t
    <link href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/Accueil/themes/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
    <link href=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/Accueil/themes/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <!-- Google-code-prettify -->\t
    <link href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/Accueil/themes/js/google-code-prettify/prettify.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
    <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/Accueil/themes/js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

    <!-- Bootstrap style responsive -->\t
    <style type=\"text/css\" id=\"enject\"></style>
    <script src=";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.load.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.ready.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.slides.js"), "html", null, true);
        echo "></script>
    <!-- start details -->
    <script src=";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/slides.min.jquery.js"), "html", null, true);
        echo "></script>

    <link rel=\"stylesheet\" href=";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/etalage.css"), "html", null, true);
        echo ">
    <script src=";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.etalage.min.js"), "html", null, true);
        echo "></script>     
    <div id=\"mainBody\">
        <div class=\"container\">
            <div class=\"row\">
                <!-- Sidebar ================================================== -->

                <!-- Sidebar end=============================================== -->
                <div class=\"span9\">\t\t
                    <div class=\"well well-small\">
                        <h4>Nouveau Produit <small class=\"pull-right\">200+ nouveaux produits</small></h4>
                        <div class=\"row-fluid\">
                            <div id=\"featured\" class=\"carousel slide\">
                                <div class=\"carousel-inner\">
                                    <div class=\"item active\">
                                        <ul class=\"thumbnails\">
                                            ";
        // line 78
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), 0, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 79
            echo "                                                <li class=\"span3\">
                                                    <div class=\"thumbnail\">
                                                        <i class=\"tag\"></i>
                                                        <div class=\"product_image\">
                                                            <img class=\"etalage_thumb_image\" src=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\" alt=\"\" width=\"150px\" height=\"150px\">
                                                       
                                                        </div>  <div class=\"caption\">
                                                            <h5>";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</h5>
                                                            <h4><a class=\"btn\" href=\"";
            // line 87
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">VIEW</a> <span class=\"pull-right\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
            echo " DT</span></h4>
                                                       
                                                        </div>
                                                        \t
                                                    </div>
                                                </li>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "
                                        </ul>
                                    </div>
                                       <div class=\"item\">
                                        <ul class=\"thumbnails\">
                                            ";
        // line 99
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")), 5, 9));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 100
            echo "                                                <li class=\"span3\">
                                                    <div class=\"thumbnail\">
                                                        <i class=\"tag\"></i>
                                                        <div class=\"product_image\">
                                                            <img class=\"etalage_thumb_image\" src=\"";
            // line 104
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\" alt=\"\" width=\"150px\" height=\"150px\">
                                                        </div>  <div class=\"caption\">
                                                            <h5>";
            // line 106
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</h5>
                                                            <h4><a class=\"btn\" href=\"";
            // line 107
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">VIEW</a> <span class=\"pull-right\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
            echo " DT</span></h4>
                                                        </div>
                                                    </div>
                                                </li>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 112
        echo "
                                        </ul>
                                    </div>
                                </div>
                                <a class=\"left carousel-control\" href=\"#featured\" data-slide=\"prev\">‹</a>
                                <a class=\"right carousel-control\" href=\"#featured\" data-slide=\"next\">›</a>
                            </div>
                        </div>
                    </div>
                    <h4>Nos produits </h4>
                    <ul class=\"thumbnails\">
                        ";
        // line 123
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 124
            echo "                            <li class=\"span3\">
                                <div class=\"thumbnail\">
                                    <div class=\"product_image\">
                                        <img class=\"etalage_thumb_image\" src=\"";
            // line 127
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\" alt=\"\" width=\"150px\" height=\"150px\">
                                    </div>
                                    <div class=\"caption\">
                                        <h5>";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</h5>
                                        <p> 
                                            ";
            // line 132
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["i"], "description", array()), 0, 20), "html", null, true);
            echo "
                                        </p>

                                        <h4 style=\"text-align:center\"><a class=\"btn\" href=\"";
            // line 135
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"> <i class=\"icon-zoom-in\"></i></a> <a class=\"btn\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("lignecommande_create", array("produit" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">Add to <i class=\"icon-shopping-cart\"></i></a><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_create", array("id" => $this->getAttribute((isset($context["favoris"]) ? $context["favoris"] : $this->getContext($context, "favoris")), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></a > <a class=\"btn btn-primary\" href=\"#\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
            echo " DT</a></h4>
                                    </div>
                                </div>
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 140
        echo "
                    </ul>\t
                    <br>
                    <div class=\"navigation\" align=\"center\">
                        ";
        // line 144
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        echo " </div>

                </div>


                <div class=\"rsingle span_1_of_single\">


                    <h5 class=\"m_1\">Rechercher un produit</h5>
                    <div class=\"input-group\">

                        <input type=\"text\" name=\"search\" >

                        <button class=\"btn btn-info\" type=\"button\">
                            <i class=\"fa fa-search\"></i>
                        </button>

                    </div>
                    <hr>
                    <h5 class=\"m_1\">Categories</h5>
                    ";
        // line 164
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 165
            echo "                        <ul class=\"kids\">
                            <li><a href=\"#\">";
            // line 166
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</a></li>

                        </ul>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 170
        echo "                </div>

            </div>



        </div>

    ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Pages:Accueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  362 => 170,  352 => 166,  349 => 165,  345 => 164,  322 => 144,  316 => 140,  299 => 135,  293 => 132,  288 => 130,  282 => 127,  277 => 124,  273 => 123,  260 => 112,  247 => 107,  243 => 106,  238 => 104,  232 => 100,  228 => 99,  221 => 94,  206 => 87,  202 => 86,  196 => 83,  190 => 79,  186 => 78,  168 => 63,  164 => 62,  159 => 60,  154 => 58,  150 => 57,  146 => 56,  139 => 52,  135 => 51,  130 => 49,  126 => 48,  121 => 46,  116 => 44,  112 => 43,  108 => 42,  104 => 41,  100 => 40,  95 => 39,  92 => 38,  77 => 26,  64 => 19,  57 => 15,  48 => 9,  44 => 7,  40 => 6,  34 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block slider %}   */
/*     <script src={{asset('js/login.js')}} ></script>*/
/*     <div id="fwslider">*/
/*         <div class="slider_container">*/
/*             {% for i in publicites %}*/
/*                 <div class="slide"> */
/*                     <!-- Slide image -->*/
/*                     <img src="{{path('publicite_image',{'id':i.id})}}" width="200px" height="400px"alt="special offers"/>                           */
/*                     <!-- /Slide image -->*/
/*                     <!-- Texts container -->*/
/*                     <div class="slide_content">*/
/*                         <div class="slide_content_wrap">*/
/*                             <!-- Text title -->*/
/*                             <h4 class="title">{{i.type}}</h4>*/
/*                             <!-- /Text title -->*/
/* */
/*                             <!-- Text description -->*/
/*                             <p class="description">{{i.sujet}}</p>*/
/*                             <!-- /Text description -->*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- /Texts container -->*/
/*                 </div>*/
/*             {% endfor %}*/
/* */
/*             <!-- /Duplicate to create more slides -->*/
/* */
/*             <!--/slide -->*/
/*         </div>*/
/*         <div class="timers"></div>*/
/*         <div class="slidePrev"><span></span></div>*/
/*         <div class="slideNext"><span></span></div>*/
/*     </div>*/
/* */
/* */
/* {% endblock %}*/
/* {% block content %}*/
/*     <link href={{asset('font-awesome-4.5.0/css/font-awesome.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*     <link href={{asset('font-awesome-4.5.0/css/font-awesome.min.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*     <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*     <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*     <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*     <link href={{asset('css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*     <!-- Bootstrap style --> */
/*     <link href="{{asset('bundles/Accueil/themes/css/base.css')}}" rel="stylesheet" media="screen"/>*/
/*     <!-- Bootstrap style responsive -->	*/
/*     <link href="{{asset('bundles/Accueil/themes/css/bootstrap-responsive.min.css')}}" rel="stylesheet"/>*/
/*     <link href="{{asset('bundles/Accueil/themes/css/font-awesome.css')}}" rel="stylesheet" type="text/css">*/
/*     <!-- Google-code-prettify -->	*/
/*     <link href="{{asset('bundles/Accueil/themes/js/google-code-prettify/prettify.css')}}" rel="stylesheet"/>*/
/*     <script src="{{asset('bundles/Accueil/themes/js/bootstrap.min.js')}}" type="text/javascript"></script>*/
/* */
/*     <!-- Bootstrap style responsive -->	*/
/*     <style type="text/css" id="enject"></style>*/
/*     <script src={{asset('js/jquery.load.js')}}></script>*/
/*     <script src={{asset('js/jquery.ready.js')}}></script>*/
/*     <script src={{asset('js/jquery.slides.js')}}></script>*/
/*     <!-- start details -->*/
/*     <script src={{asset('js/slides.min.jquery.js')}}></script>*/
/* */
/*     <link rel="stylesheet" href={{asset('css/etalage.css')}}>*/
/*     <script src={{asset('js/jquery.etalage.min.js')}}></script>     */
/*     <div id="mainBody">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <!-- Sidebar ================================================== -->*/
/* */
/*                 <!-- Sidebar end=============================================== -->*/
/*                 <div class="span9">		*/
/*                     <div class="well well-small">*/
/*                         <h4>Nouveau Produit <small class="pull-right">200+ nouveaux produits</small></h4>*/
/*                         <div class="row-fluid">*/
/*                             <div id="featured" class="carousel slide">*/
/*                                 <div class="carousel-inner">*/
/*                                     <div class="item active">*/
/*                                         <ul class="thumbnails">*/
/*                                             {% for i in entities|slice(0, 4) %}*/
/*                                                 <li class="span3">*/
/*                                                     <div class="thumbnail">*/
/*                                                         <i class="tag"></i>*/
/*                                                         <div class="product_image">*/
/*                                                             <img class="etalage_thumb_image" src="{{path('my_image_route',{'id':i.id})}}" alt="" width="150px" height="150px">*/
/*                                                        */
/*                                                         </div>  <div class="caption">*/
/*                                                             <h5>{{i.nom}}</h5>*/
/*                                                             <h4><a class="btn" href="{{ path('produit_detail', { 'id': i.id }) }}">VIEW</a> <span class="pull-right">{{i.prix}} DT</span></h4>*/
/*                                                        */
/*                                                         </div>*/
/*                                                         	*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                             {% endfor %}*/
/* */
/*                                         </ul>*/
/*                                     </div>*/
/*                                        <div class="item">*/
/*                                         <ul class="thumbnails">*/
/*                                             {% for i in entities|slice(5, 9) %}*/
/*                                                 <li class="span3">*/
/*                                                     <div class="thumbnail">*/
/*                                                         <i class="tag"></i>*/
/*                                                         <div class="product_image">*/
/*                                                             <img class="etalage_thumb_image" src="{{path('my_image_route',{'id':i.id})}}" alt="" width="150px" height="150px">*/
/*                                                         </div>  <div class="caption">*/
/*                                                             <h5>{{i.nom}}</h5>*/
/*                                                             <h4><a class="btn" href="{{ path('produit_detail', { 'id': i.id }) }}">VIEW</a> <span class="pull-right">{{i.prix}} DT</span></h4>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                             {% endfor %}*/
/* */
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <a class="left carousel-control" href="#featured" data-slide="prev">‹</a>*/
/*                                 <a class="right carousel-control" href="#featured" data-slide="next">›</a>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <h4>Nos produits </h4>*/
/*                     <ul class="thumbnails">*/
/*                         {% for i in pagination %}*/
/*                             <li class="span3">*/
/*                                 <div class="thumbnail">*/
/*                                     <div class="product_image">*/
/*                                         <img class="etalage_thumb_image" src="{{path('my_image_route',{'id':i.id})}}" alt="" width="150px" height="150px">*/
/*                                     </div>*/
/*                                     <div class="caption">*/
/*                                         <h5>{{i.nom}}</h5>*/
/*                                         <p> */
/*                                             {{i.description|slice(0, 20)}}*/
/*                                         </p>*/
/* */
/*                                         <h4 style="text-align:center"><a class="btn" href="{{ path('produit_detail', { 'id': i.id }) }}"> <i class="icon-zoom-in"></i></a> <a class="btn" href="{{path('lignecommande_create',{'produit':i.id})}}">Add to <i class="icon-shopping-cart"></i></a><a href="{{path('favoris_create',{'id':favoris.id,'produit':entity.id})}}"><i class="fa fa-star" aria-hidden="true"></i></a > <a class="btn btn-primary" href="#">{{i.prix}} DT</a></h4>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </li>*/
/*                         {% endfor %}*/
/* */
/*                     </ul>	*/
/*                     <br>*/
/*                     <div class="navigation" align="center">*/
/*                         {{ knp_pagination_render(pagination) }} </div>*/
/* */
/*                 </div>*/
/* */
/* */
/*                 <div class="rsingle span_1_of_single">*/
/* */
/* */
/*                     <h5 class="m_1">Rechercher un produit</h5>*/
/*                     <div class="input-group">*/
/* */
/*                         <input type="text" name="search" >*/
/* */
/*                         <button class="btn btn-info" type="button">*/
/*                             <i class="fa fa-search"></i>*/
/*                         </button>*/
/* */
/*                     </div>*/
/*                     <hr>*/
/*                     <h5 class="m_1">Categories</h5>*/
/*                     {% for i in categories %}*/
/*                         <ul class="kids">*/
/*                             <li><a href="#">{{i.nom}}</a></li>*/
/* */
/*                         </ul>*/
/*                     {% endfor %}*/
/*                 </div>*/
/* */
/*             </div>*/
/* */
/* */
/* */
/*         </div>*/
/* */
/*     {% endblock %}*/
/* */
