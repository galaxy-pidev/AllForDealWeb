<?php

/* PiDevClientBundle:Produit:recherche.html.twig */
class __TwigTemplate_3a9fd849ece86a42a1c35db92deb002a1e38eaaf91f3a5b9bc11593d70771c85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:recherche.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Listes  des Produits </h1>
<form method=\"POST\"  action=\"\">
    Rechercher un Produit! : <select name=\"search\">
";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
            // line 8
            echo "<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cat"], "nom", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cat"], "nom", array()), "html", null, true);
            echo "</option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "</select>
    <input type=\"submit\" value=\"chercher\"/>
</form>

    
    ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modeles"]) ? $context["modeles"] : $this->getContext($context, "modeles")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 16
            echo "        <div>
            <br>  Produits:<th>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</th></br>
            <br>  Description  :<th>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</th></br>
            <br>  Prix :<th>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
            echo "</th></br>
            <br>  Promotion :<th>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "promotion", array()), "html", null, true);
            echo "</th></br>
            <br>  Marque  :<th>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "marque", array()), "html", null, true);
            echo "</th></br>
            <br>  NbVente  :<th>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nbvente", array()), "html", null, true);
            echo "</th></br>
            <br>  Quantité  :<th>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "qantite", array()), "html", null, true);
            echo "</th></br>
            <br>  Date d'ajout  :<th>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "dateajout", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "</th></br>

                                          
      <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">En savoir Plus!</a>
        </div>
         
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:recherche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 27,  93 => 24,  89 => 23,  85 => 22,  81 => 21,  77 => 20,  73 => 19,  69 => 18,  65 => 17,  62 => 16,  58 => 15,  51 => 10,  40 => 8,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* <h1>Listes  des Produits </h1>*/
/* <form method="POST"  action="">*/
/*     Rechercher un Produit! : <select name="search">*/
/* {% for cat in categories %}*/
/* <option value="{{ cat.nom }}">{{ cat.nom }}</option>*/
/* {% endfor %}*/
/* </select>*/
/*     <input type="submit" value="chercher"/>*/
/* </form>*/
/* */
/*     */
/*     {% for i in modeles %}*/
/*         <div>*/
/*             <br>  Produits:<th>{{i.nom}}</th></br>*/
/*             <br>  Description  :<th>{{i.description}}</th></br>*/
/*             <br>  Prix :<th>{{i.prix}}</th></br>*/
/*             <br>  Promotion :<th>{{i.promotion}}</th></br>*/
/*             <br>  Marque  :<th>{{i.marque}}</th></br>*/
/*             <br>  NbVente  :<th>{{i.nbvente}}</th></br>*/
/*             <br>  Quantité  :<th>{{i.qantite}}</th></br>*/
/*             <br>  Date d'ajout  :<th>{{i.dateajout.format('Y-m-d')}}</th></br>*/
/* */
/*                                           */
/*       <a href="{{path("produit_detail",{'id':i.id})}}">En savoir Plus!</a>*/
/*         </div>*/
/*          */
/*     {% endfor %}*/
/* {% endblock %}*/
/* */
