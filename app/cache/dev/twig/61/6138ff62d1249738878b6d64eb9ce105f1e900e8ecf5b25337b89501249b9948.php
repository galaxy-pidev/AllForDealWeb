<?php

/* PiDevClientBundle:Service:valider.html.twig */
class __TwigTemplate_bec4dfd72ae71992d0c0fc4e5853d58d87f3f963b11cfd04b5c4a5ed42d87d21 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Service:valider.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h1>Valider se Service </h1>
<form action=\"\" method=\"post\" >
    <table> 
   
    <tr>  
    <td>   valider:</td>
    <td>";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "valider", array()), 'widget');
        echo "</td>
    </tr>
</table>
    <br>
  
";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
</form>
  ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Service:valider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 14,  39 => 9,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* <h1>Valider se Service </h1>*/
/* <form action="" method="post" >*/
/*     <table> */
/*    */
/*     <tr>  */
/*     <td>   valider:</td>*/
/*     <td>{{ form_widget(form.valider)}}</td>*/
/*     </tr>*/
/* </table>*/
/*     <br>*/
/*   */
/* {{form_rest(form)}}*/
/* </form>*/
/*   {% endblock %}*/
