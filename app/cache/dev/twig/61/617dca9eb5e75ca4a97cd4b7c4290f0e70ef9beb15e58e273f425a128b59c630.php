<?php

/* PiDevClientBundle:Commentaire:test.html.twig */
class __TwigTemplate_ac817b4c9ba734340eb4751ab507e6ec7dde3f8d685eead2478c832c78765c44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Commentaire:test.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html>
        <head>
            <title>All For Deal | Accueil </title>

            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
            <link href=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.min.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/style.css"), "html", null, true);
        echo "\" />
            <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
            <script type=\"text/javascript\" src=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
            <!-- start menu -->
            <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <script type=\"text/javascript\" src=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
            <script  src=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
            <!--start slider -->
            <link rel=\"stylesheet\" href=";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
            <script src=";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
            <script src=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/bootstrap.min.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/customjs.js"), "html", null, true);
        echo "></script>
            <link rel=\"stylesheet\" href=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/bootstrap.min.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <link rel=\"stylesheet\" href=";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/custom.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <!--end slider -->
            <link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">

            <script src=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.load.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.ready.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.slides.js"), "html", null, true);
        echo "></script>
            <!-- start details -->
            <script src=";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/slides.min.jquery.js"), "html", null, true);
        echo "></script>

            <link rel=\"stylesheet\" href=";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/etalage.css"), "html", null, true);
        echo ">
            <script src=";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.etalage.min.js"), "html", null, true);
        echo "></script>

        </head>
        <body>

            <div class=\"mens\">    
                <div class=\"main\">
                    <div class=\"wrap\">
                        <div class=\"cont span_2_of_3\">

                            <div class=\"grid images_3_of_2\">                           
                                <ul id=\"etalage\">  
                                    ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : $this->getContext($context, "images")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 56
            echo "                                        <li>                                       
                                            <a href=\"optionallink.html\">                                         
                                                <img class=\"etalage_thumb_image\"  src=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\" class=\"img-responsive\"  />                                    
                                                <img class=\"etalage_source_image\"  src=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\" class=\"img-responsive\"  />
                                            </a>                                    
                                        </li>

                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo " 

                                </ul>

                                <div class=\"clearfix\"></div>
                            </div>

                            <div class=\"desc1 span_3_of_2\">
                                <h3 class=\"m_3\">";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nom", array()), "html", null, true);
        echo "</h3>
                                <p class=\"m_5\">Prix. ";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prix", array()), "html", null, true);
        echo " DT<span class=\"reducedfrom\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prix", array()), "html", null, true);
        echo " DT</span> </p>

                                <div class=\"availablelhg\">
                                    <table class=\"record_properties\">
                                        <tbody>


                                            <tr>
                                                <th>Marque</th>
                                                <td>";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "marque", array()), "html", null, true);
        echo "</td>
                                            </tr>
                                            <tr>
                                                <th>Quantite en stock:</th>
                                                <td>";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "qantite", array()), "html", null, true);
        echo "</td>
                                            </tr>
                                            <tr>
                                                <th>Promotion</th>
                                                <td>";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "promotion", array()), "html", null, true);
        echo "%</td>
                                            </tr>                                
                                            <tr>
                                                <th>---------------------------</th>

                                            </tr>      
                                            <tr><th>Description </th>
                                                <td>  <p class=\"m_text2\"> ";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "description", array()), "html", null, true);
        echo "</p> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>


                                <div>";
        // line 104
        echo $this->env->getExtension('nomaya_social_bar')->getSocialButtons();
        echo "</div> 
                                <div class=\"btn_form\">
                                    <br>  <hr>

                                    <center> 
                                        <a href=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_create", array("id" => $this->getAttribute($this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "favoris", array()), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\"> <button type=\"submit\" class=\"btn btn-info\"><i class=\"fa fa-heart\"></i> Like</button></a>
                                        <a href=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_create", array("id" => $this->getAttribute($this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "panier", array()), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\"> <button type=\"submit\" class=\"btn btn-info\"><i class=\"fa fa-shopping-cart\"></i> Panier</button></a>
                                    </center>
                                    <hr>
                                    <form name=\"rating\" action=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" method=\"post\"> 
                                        <font color=\"#110D58\" size=\"4px\"> <center>  <p> <b>Notez ce produit</b></p> </center></font>
                                        <div class=\"stars\">
                                            <input type=\"radio\" name=\"adresse\" class=\"star-1\" id=\"star-1\" value=\"1\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-1\" for=\"star-1\">1</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-2\" id=\"star-2\" value=\"2\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-2\" for=\"star-2\">2</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-3\" id=\"star-3\" value=\"3\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-3\" for=\"star-3\">3</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-4\" id=\"star-4\" value=\"4\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-4\" for=\"star-4\">4</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-5\" id=\"star-5\" value=\"5\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-5\" for=\"star-5\">5</label>               
                                            <span></span>              
                                        </div>   

                                        ";
        // line 129
        if (((isset($context["vote"]) ? $context["vote"] : $this->getContext($context, "vote")) == true)) {
            // line 130
            echo "                                            <center>     <p>    vous avez donnez un note ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["evaluation"]) ? $context["evaluation"] : $this->getContext($context, "evaluation")), "rating", array()), "html", null, true);
            echo "/5 <p><br>
                                                <p> si vous desirez changer votre avis vous pouvez voter de nouveau </p>
                                            </center>
                                        ";
        }
        // line 134
        echo "
                                    </form>



                                    <hr>
                                </div> 
                            </div>

                            <div class=\"clear\"></div>\t
                            <div class=\"clients\">

                                </script>
                                <script type=\"text/javascript\" src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.flexisel.js"), "html", null, true);
        echo "\"></script>
                            </div>
                            <div class=\"toogle\">
                                <h3 class=\"m_3\">Commentaires</h3> 
                                <div class=\"well\">
                                    <h4>Leave a Comment:</h4>
                                    <form name=\"commentaire\" action=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" method=\"post\">
                                        <div class=\"form-group\">
                                            ";
        // line 155
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Corps", array()), 'widget');
        echo "
                                        </div>
                                        <button type=\"submit\" action=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-comment\"></i> Commenter</button>
                                    </form>
                                </div>
                                <hr>
                                <div>

                                    ";
        // line 163
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 164
            echo "
                                        <h4>  <span >  <img src=";
            // line 165
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
            echo " alt=\"\" width=\"50px\" height=\"50px\"/> </span> 
                                            ";
            // line 166
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "html", null, true);
            echo ": <small>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "DateCom", array()), "Y-m-d H:i:s"), "html", null, true);
            echo "</small>
                                            ";
            // line 167
            if (($this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()) == $this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "id", array()))) {
                // line 168
                echo "                                                <span class=\"popbtn\"><a class=\"arrow\"></a></span></h4>
                                                ";
            }
            // line 170
            echo "                                        <p>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "corps", array()), "html", null, true);
            echo " </p> 


                                        ";
            // line 173
            if (($this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()) == $this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "id", array()))) {
                // line 174
                echo "                                            <div id=\"popover\" style=\"display: none\">
                                                <a href=\"";
                // line 175
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-pencil\"></span></a>
                                                <a href=\"";
                // line 176
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-remove\"></span></a>\t\t\t

                                            </div>



                                        ";
            }
            // line 182
            echo "  
                                        <hr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 185
        echo "
                                </div>
                            </div>

                        </div>

                        <div class=\"clear\"></div>
                    </div>
                </div>


            </div>
        </form>
    ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commentaire:test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  398 => 185,  390 => 182,  380 => 176,  376 => 175,  373 => 174,  371 => 173,  364 => 170,  360 => 168,  358 => 167,  352 => 166,  348 => 165,  345 => 164,  341 => 163,  332 => 157,  327 => 155,  322 => 153,  313 => 147,  298 => 134,  290 => 130,  288 => 129,  269 => 113,  263 => 110,  259 => 109,  251 => 104,  240 => 96,  230 => 89,  223 => 85,  216 => 81,  202 => 72,  198 => 71,  188 => 63,  177 => 59,  173 => 58,  169 => 56,  165 => 55,  150 => 43,  146 => 42,  141 => 40,  136 => 38,  132 => 37,  128 => 36,  123 => 34,  118 => 32,  114 => 31,  110 => 30,  106 => 29,  102 => 28,  98 => 27,  94 => 26,  90 => 25,  86 => 24,  81 => 22,  77 => 21,  73 => 20,  68 => 18,  63 => 16,  59 => 15,  55 => 14,  51 => 13,  47 => 12,  43 => 11,  39 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html>*/
/*         <head>*/
/*             <title>All For Deal | Accueil </title>*/
/* */
/*             <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.min.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/style.css') }}" />*/
/*             <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*             <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*             <!-- start menu -->*/
/*             <link href={{asset('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*             <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*             <!--start slider -->*/
/*             <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*             <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*             <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*             <script src={{asset('js/fwslider.js')}}></script>*/
/*             <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/bootstrap.min.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/customjs.js')}}></script>*/
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/bootstrap.min.css')}} type="text/css" media="all" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/custom.css')}} type="text/css" media="all" /> */
/*             <!--end slider -->*/
/*             <link rel="stylesheet" href="{{asset('css/style.css')}}">*/
/* */
/*             <script src={{asset('js/jquery.load.js')}}></script>*/
/*             <script src={{asset('js/jquery.ready.js')}}></script>*/
/*             <script src={{asset('js/jquery.slides.js')}}></script>*/
/*             <!-- start details -->*/
/*             <script src={{asset('js/slides.min.jquery.js')}}></script>*/
/* */
/*             <link rel="stylesheet" href={{asset('css/etalage.css')}}>*/
/*             <script src={{asset('js/jquery.etalage.min.js')}}></script>*/
/* */
/*         </head>*/
/*         <body>*/
/* */
/*             <div class="mens">    */
/*                 <div class="main">*/
/*                     <div class="wrap">*/
/*                         <div class="cont span_2_of_3">*/
/* */
/*                             <div class="grid images_3_of_2">                           */
/*                                 <ul id="etalage">  */
/*                                     {% for i in images %}*/
/*                                         <li>                                       */
/*                                             <a href="optionallink.html">                                         */
/*                                                 <img class="etalage_thumb_image"  src="{{ asset(path('my_image_route', {'id': i.id})) }}" class="img-responsive"  />                                    */
/*                                                 <img class="etalage_source_image"  src="{{ asset(path('my_image_route', {'id': i.id})) }}" class="img-responsive"  />*/
/*                                             </a>                                    */
/*                                         </li>*/
/* */
/*                                     {% endfor %} */
/* */
/*                                 </ul>*/
/* */
/*                                 <div class="clearfix"></div>*/
/*                             </div>*/
/* */
/*                             <div class="desc1 span_3_of_2">*/
/*                                 <h3 class="m_3">{{ entity.nom }}</h3>*/
/*                                 <p class="m_5">Prix. {{ entity.prix }} DT<span class="reducedfrom">{{ entity.prix }} DT</span> </p>*/
/* */
/*                                 <div class="availablelhg">*/
/*                                     <table class="record_properties">*/
/*                                         <tbody>*/
/* */
/* */
/*                                             <tr>*/
/*                                                 <th>Marque</th>*/
/*                                                 <td>{{ entity.marque }}</td>*/
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <th>Quantite en stock:</th>*/
/*                                                 <td>{{ entity.qantite }}</td>*/
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <th>Promotion</th>*/
/*                                                 <td>{{ entity.promotion }}%</td>*/
/*                                             </tr>                                */
/*                                             <tr>*/
/*                                                 <th>---------------------------</th>*/
/* */
/*                                             </tr>      */
/*                                             <tr><th>Description </th>*/
/*                                                 <td>  <p class="m_text2"> {{entity.description }}</p> </td>*/
/*                                             </tr>*/
/*                                         </tbody>*/
/*                                     </table>*/
/*                                     <hr>*/
/*                                 </div>*/
/* */
/* */
/*                                 <div>{{socialButtons()}}</div> */
/*                                 <div class="btn_form">*/
/*                                     <br>  <hr>*/
/* */
/*                                     <center> */
/*                                         <a href="{{path('favoris_create',{'id':utilisateur.favoris.id,'produit':entity.id})}}"> <button type="submit" class="btn btn-info"><i class="fa fa-heart"></i> Like</button></a>*/
/*                                         <a href="{{path('panier_create',{'id':utilisateur.panier.id,'produit':entity.id})}}"> <button type="submit" class="btn btn-info"><i class="fa fa-shopping-cart"></i> Panier</button></a>*/
/*                                     </center>*/
/*                                     <hr>*/
/*                                     <form name="rating" action="{{path('produit_detail',{'id':entity.id})}}" method="post"> */
/*                                         <font color="#110D58" size="4px"> <center>  <p> <b>Notez ce produit</b></p> </center></font>*/
/*                                         <div class="stars">*/
/*                                             <input type="radio" name="adresse" class="star-1" id="star-1" value="1" onclick="this.form.submit()"/>*/
/*                                             <label class="star-1" for="star-1">1</label>*/
/*                                             <input type="radio" name="adresse" class="star-2" id="star-2" value="2" onclick="this.form.submit()"/>*/
/*                                             <label class="star-2" for="star-2">2</label>*/
/*                                             <input type="radio" name="adresse" class="star-3" id="star-3" value="3" onclick="this.form.submit()"/>*/
/*                                             <label class="star-3" for="star-3">3</label>*/
/*                                             <input type="radio" name="adresse" class="star-4" id="star-4" value="4" onclick="this.form.submit()"/>*/
/*                                             <label class="star-4" for="star-4">4</label>*/
/*                                             <input type="radio" name="adresse" class="star-5" id="star-5" value="5" onclick="this.form.submit()"/>*/
/*                                             <label class="star-5" for="star-5">5</label>               */
/*                                             <span></span>              */
/*                                         </div>   */
/* */
/*                                         {% if vote==true %}*/
/*                                             <center>     <p>    vous avez donnez un note {{evaluation.rating}}/5 <p><br>*/
/*                                                 <p> si vous desirez changer votre avis vous pouvez voter de nouveau </p>*/
/*                                             </center>*/
/*                                         {% endif %}*/
/* */
/*                                     </form>*/
/* */
/* */
/* */
/*                                     <hr>*/
/*                                 </div> */
/*                             </div>*/
/* */
/*                             <div class="clear"></div>	*/
/*                             <div class="clients">*/
/* */
/*                                 </script>*/
/*                                 <script type="text/javascript" src="{{asset('js/jquery.flexisel.js')}}"></script>*/
/*                             </div>*/
/*                             <div class="toogle">*/
/*                                 <h3 class="m_3">Commentaires</h3> */
/*                                 <div class="well">*/
/*                                     <h4>Leave a Comment:</h4>*/
/*                                     <form name="commentaire" action="{{path('produit_detail',{'id':entity.id})}}" method="post">*/
/*                                         <div class="form-group">*/
/*                                             {{ form_widget(form.Corps)}}*/
/*                                         </div>*/
/*                                         <button type="submit" action="{{path('produit_detail',{'id':entity.id})}}" class="btn btn-info"><i class="fa fa-comment"></i> Commenter</button>*/
/*                                     </form>*/
/*                                 </div>*/
/*                                 <hr>*/
/*                                 <div>*/
/* */
/*                                     {% for entity in entities %}*/
/* */
/*                                         <h4>  <span >  <img src={{asset(path('image'))}} alt="" width="50px" height="50px"/> </span> */
/*                                             {{ entity.user.username }}: <small>{{entity.DateCom|date('Y-m-d H:i:s')}}</small>*/
/*                                             {% if entity.user.id == utilisateur.id %}*/
/*                                                 <span class="popbtn"><a class="arrow"></a></span></h4>*/
/*                                                 {% endif %}*/
/*                                         <p>{{ entity.corps }} </p> */
/* */
/* */
/*                                         {% if entity.user.id == utilisateur.id %}*/
/*                                             <div id="popover" style="display: none">*/
/*                                                 <a href="{{path('commentaire_edit',{'id':entity.id})}}"><span class="glyphicon glyphicon-pencil"></span></a>*/
/*                                                 <a href="{{path('commentaire_delete',{'id':entity.id})}}"><span class="glyphicon glyphicon-remove"></span></a>			*/
/* */
/*                                             </div>*/
/* */
/* */
/* */
/*                                         {% endif %}  */
/*                                         <hr>*/
/*                                     {% endfor %}*/
/* */
/*                                 </div>*/
/*                             </div>*/
/* */
/*                         </div>*/
/* */
/*                         <div class="clear"></div>*/
/*                     </div>*/
/*                 </div>*/
/* */
/* */
/*             </div>*/
/*         </form>*/
/*     {% endblock %}*/
