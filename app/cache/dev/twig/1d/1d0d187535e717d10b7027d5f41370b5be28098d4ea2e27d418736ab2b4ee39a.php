<?php

/* PiDevClientBundle:Sujet:showAll.html.twig */
class __TwigTemplate_6e261671cff61ccbf38235df0e831c955154c76ff0be6a7a881fdc6668691a20 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Sujet:showAll.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <html class=\"no-js sidebar-large\"> <!--<![endif]-->

        <!-- Added by HTTrack --><meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" /><!-- /Added by HTTrack -->
        <head>
            <!-- BEGIN META SECTION -->
            <meta charset=\"utf-8\">
            <title>Pixit - Responsive Boostrap3 Admin</title>
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <meta content=\"\" name=\"description\" />
            <meta content=\"themes-lab\" name=\"author\" />
                   <link href=\"cssBlog.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
      
            <!-- END META SECTION -->
            <!-- BEGIN MANDATORY STYLE -->
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
            <link href=";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
            <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
            <script type=\"text/javascript\" src=";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
            <!-- start menu -->
            <link href=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <script type=\"text/javascript\" src=";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
            <script  src=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
            <!--start slider -->
            <link rel=\"stylesheet\" href=";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
            <script src=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
            <script src=";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
            <!--end slider -->
            <script src=";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>
            <link href=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
            <link href=";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
            <link href=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
            <script src=";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/ckeditor/ckeditor.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>

        </head>
        <body>

            <div id=\"main-content\">
                <div class=\"row\">
                    <div class=\"col-lg-8 blog-results\">
                        <!-- blog entry -->
                        <div class=\"panel panel-default\">
                            ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 50
            echo "                                ";
            $context["nb"] = 0;
            echo "  
                                    <tr ";
            // line 51
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                echo "class=\"color\"";
            }
            echo "> 
                                        ";
            // line 52
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["com"]);
            foreach ($context['_seq'] as $context["_key"] => $context["com"]) {
                // line 53
                echo "                                            ";
                if (($this->getAttribute($this->getAttribute($context["com"], "sujet", array()), "id", array()) == $this->getAttribute($context["entity"], "id", array()))) {
                    // line 54
                    echo "                                                ";
                    $context["nb"] = ((isset($context["nb"]) ? $context["nb"] : $this->getContext($context, "nb")) + 1);
                    echo "  
                                                    ";
                }
                // line 56
                echo "                                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['com'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
                                                        <div class=\"panel-body\" >
                                                            <h1><a href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail_sujet", array("sujet" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\" class=\"c-dark\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "titre", array()), "html", null, true);
            echo "</a></h1>
                                                            <p class=\"lead\">by <a href=\"#\">";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "html", null, true);
            echo "</a>
                                                            </p>
                                                            <hr>
                                                            <p>
                                                                <i class=\"fa fa-calendar\"></i>  Publié le ";
            // line 63
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "dateAjout", array()), "j M \\à g:ia \\e\\n Y "), "html", null, true);
            echo " </p>
                                                            <i class=\"fa fa-comments\"></i>  ";
            // line 64
            echo twig_escape_filter($this->env, (isset($context["nb"]) ? $context["nb"] : $this->getContext($context, "nb")), "html", null, true);
            echo "
                                                            <hr>
                                                            <center><a ";
            // line 66
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail_sujet", array("sujet" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo ">  <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("sujet_image", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\" width=\"200px\" class=\"img-responsive\">
                                                                </a> </center>  <hr>
                                                                <p> ";
            // line 68
            echo twig_slice($this->env, $this->getAttribute($context["entity"], "sujet", array()), 0, 20);
            echo " </p>
                                                            <a class=\"btn btn-info\" href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("detail_sujet", array("sujet" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">Read More <i class=\"fa fa-angle-right\"></i> </a>
                                                           
                                                        </div>
                                                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "                                                        </div>
                                                        <!-- pager -->
                                                        <ul class=\"pager\">
                                                            <li class=\"previous\"><a href=\"#\">← Older</a>
                                                            </li>
                                                            <li class=\"next\"><a href=\"#\">Newer →</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                        <!-- Recherche -->
                                                    <div class=\"rsingle span_1_of_single\">

                                                        <h5 class=\"m_1\">Rechercher un sujet</h5>
                                                        <div class=\"input-group\">

                                                            <input type=\"text\" name=\"search\" >

                                                            <button class=\"btn btn-info\" type=\"button\">
                                                                <i class=\"fa fa-search\"></i>
                                                            </button>

                                                        </div> <hr>
                                                        <h5 class=\"m_1\">Ajouter un sujet</h5>
                                                        <div class=\"panel panel-default\">

                                                            <div class=\"panel-body\" >

                                                                Si vous desirez ajouter un sujet clique au-dessous<br>
                                                                <center>  <a href=\"";
        // line 101
        echo $this->env->getExtension('routing')->getPath("sujet_create");
        echo "\" >  <button type=\"submit\" action=\"";
        echo $this->env->getExtension('routing')->getPath("sujet_create");
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-plus-square\"></i>  Sujet</button></a>
                                                                </center>   

                                                            </div>
                                                        </div>
                                                    </div>


                                                    <!-- End -->

                                                </div>
                                            </div>
                                        </div>
                                        <!-- END MAIN CONTENT -->
                                    </div>
                                    <!-- END WRAPPER -->
                                </div>
                                <!-- END WRAPPER -->


                                <script src=";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
                                <script src=";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
                                <!-- END MANDATORY SCRIPTS -->
                                <script src=";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>
                            </body>
                            <div class=\"navigation\" align=\"center\">
                                ";
        // line 139
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        echo " </div>
                            <div class=\"clear\"></div>
                            ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Sujet:showAll.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  349 => 139,  343 => 136,  338 => 134,  334 => 133,  330 => 132,  326 => 131,  322 => 130,  318 => 129,  314 => 128,  310 => 127,  306 => 126,  302 => 125,  298 => 124,  294 => 123,  290 => 122,  286 => 121,  261 => 101,  231 => 73,  213 => 69,  209 => 68,  202 => 66,  197 => 64,  193 => 63,  186 => 59,  180 => 58,  171 => 56,  165 => 54,  162 => 53,  158 => 52,  152 => 51,  147 => 50,  130 => 49,  117 => 39,  113 => 38,  109 => 37,  105 => 36,  101 => 35,  96 => 33,  92 => 32,  88 => 31,  84 => 30,  79 => 28,  75 => 27,  71 => 26,  66 => 24,  61 => 22,  57 => 21,  53 => 20,  49 => 19,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/*     <html class="no-js sidebar-large"> <!--<![endif]-->*/
/* */
/*         <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->*/
/*         <head>*/
/*             <!-- BEGIN META SECTION -->*/
/*             <meta charset="utf-8">*/
/*             <title>Pixit - Responsive Boostrap3 Admin</title>*/
/*             <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*             <meta content="" name="description" />*/
/*             <meta content="themes-lab" name="author" />*/
/*                    <link href="cssBlog.css" rel="stylesheet" type="text/css" media="all" />*/
/*       */
/*             <!-- END META SECTION -->*/
/*             <!-- BEGIN MANDATORY STYLE -->*/
/*             <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*             <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*             <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*             <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*             <!-- start menu -->*/
/*             <link href={{asset('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*             <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*             <!--start slider -->*/
/*             <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*             <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*             <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*             <script src={{asset('js/fwslider.js')}}></script>*/
/*             <!--end slider -->*/
/*             <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/*             <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*             <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*             <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*             <script src={{asset('bundles/ckeditor/ckeditor.js')}} type="text/javascript"></script>*/
/* */
/*         </head>*/
/*         <body>*/
/* */
/*             <div id="main-content">*/
/*                 <div class="row">*/
/*                     <div class="col-lg-8 blog-results">*/
/*                         <!-- blog entry -->*/
/*                         <div class="panel panel-default">*/
/*                             {% for entity in pagination %}*/
/*                                 {% set nb=0%}  */
/*                                     <tr {% if loop.index is odd %}class="color"{% endif %}> */
/*                                         {% for com in com %}*/
/*                                             {% if com.sujet.id==entity.id %}*/
/*                                                 {% set nb=nb+1%}  */
/*                                                     {% endif %}*/
/*                                                         {% endfor %} */
/*                                                         <div class="panel-body" >*/
/*                                                             <h1><a href="{{path('detail_sujet',{'sujet':entity.id})}}" class="c-dark">{{entity.titre}}</a></h1>*/
/*                                                             <p class="lead">by <a href="#">{{entity.user.username}}</a>*/
/*                                                             </p>*/
/*                                                             <hr>*/
/*                                                             <p>*/
/*                                                                 <i class="fa fa-calendar"></i>  Publié le {{entity.dateAjout|date('j M \\à g:ia \\e\\n Y ')}} </p>*/
/*                                                             <i class="fa fa-comments"></i>  {{ nb }}*/
/*                                                             <hr>*/
/*                                                             <center><a {{path('detail_sujet',{'sujet':entity.id})}}>  <img src="{{path('sujet_image',{'id':entity.id})}}" width="200px" class="img-responsive">*/
/*                                                                 </a> </center>  <hr>*/
/*                                                                 <p> {{entity.sujet|slice(0,20)|raw}} </p>*/
/*                                                             <a class="btn btn-info" href="{{path('detail_sujet',{'sujet':entity.id})}}">Read More <i class="fa fa-angle-right"></i> </a>*/
/*                                                            */
/*                                                         </div>*/
/*                                                         {% endfor %}*/
/*                                                         </div>*/
/*                                                         <!-- pager -->*/
/*                                                         <ul class="pager">*/
/*                                                             <li class="previous"><a href="#">← Older</a>*/
/*                                                             </li>*/
/*                                                             <li class="next"><a href="#">Newer →</a>*/
/*                                                             </li>*/
/*                                                         </ul>*/
/*                                                     </div>*/
/*                                                         <!-- Recherche -->*/
/*                                                     <div class="rsingle span_1_of_single">*/
/* */
/*                                                         <h5 class="m_1">Rechercher un sujet</h5>*/
/*                                                         <div class="input-group">*/
/* */
/*                                                             <input type="text" name="search" >*/
/* */
/*                                                             <button class="btn btn-info" type="button">*/
/*                                                                 <i class="fa fa-search"></i>*/
/*                                                             </button>*/
/* */
/*                                                         </div> <hr>*/
/*                                                         <h5 class="m_1">Ajouter un sujet</h5>*/
/*                                                         <div class="panel panel-default">*/
/* */
/*                                                             <div class="panel-body" >*/
/* */
/*                                                                 Si vous desirez ajouter un sujet clique au-dessous<br>*/
/*                                                                 <center>  <a href="{{path('sujet_create')}}" >  <button type="submit" action="{{path('sujet_create')}}" class="btn btn-info"><i class="fa fa-plus-square"></i>  Sujet</button></a>*/
/*                                                                 </center>   */
/* */
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/* */
/* */
/*                                                     <!-- End -->*/
/* */
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <!-- END MAIN CONTENT -->*/
/*                                     </div>*/
/*                                     <!-- END WRAPPER -->*/
/*                                 </div>*/
/*                                 <!-- END WRAPPER -->*/
/* */
/* */
/*                                 <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*                                 <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*                                 <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*                                 <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*                                 <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*                                 <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*                                 <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*                                 <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*                                 <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*                                 <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*                                 <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*                                 <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*                                 <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*                                 <script src={{asset('js/mailbox.js')}}></script>*/
/*                                 <!-- END MANDATORY SCRIPTS -->*/
/*                                 <script src={{asset('js/application.js')}}></script>*/
/*                             </body>*/
/*                             <div class="navigation" align="center">*/
/*                                 {{ knp_pagination_render(pagination) }} </div>*/
/*                             <div class="clear"></div>*/
/*                             {% endblock %}*/
/* */
