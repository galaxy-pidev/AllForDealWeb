<?php

/* PiDevClientBundle:Commande:Confirmer.html.twig */
class __TwigTemplate_db51842b60bc86451c222d6dad70bed0842a183211a696587be10672fc70ee28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Commande:Confirmer.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        echo "   
    <html>
        <head>  
<!-- styles -->
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- theme stylesheet -->
    <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/style.blue.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-stylesheet\">

    <!-- your stylesheet with modifications -->
    <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/custom.css\" rel=\"stylesheet"), "html", null, true);
        echo "\">

    <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/respond.min.js"), "html", null, true);
        echo "\"></script>

    <link rel=\"shortcut icon\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/img/favicon.png"), "html", null, true);
        echo "\">


</head>";
        // line 24
        echo "<body>
<br>
<div class=\"panel panel-info\">

        <div id=\"content\">
            <div class=\"container\">



                <div class=\"col-md-9\" id=\"checkout\">

                    <div class=\"\">
                        <form method=\"post\" action=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("confirmer_commande", array("id" => $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "id", array()))), "html", null, true);
        echo "\">
                            <h1>Confirmation de votre commande</h1>
                            <ul class=\"nav nav-pills nav-justified\">
                                <li><a href=\"checkout1.html\"><i class=\"fa fa fa-truck\"></i><br>Livraison</a>
                                </li>
                                <li><a href=\"checkout2.html\"><i class=\"fa fa-money\"></i><br>Payment</a>
                                </li>
                                <li class=\"active\"><a href=\"checkout3.html\"><i class=\"fa fa-eye\"></i><br>Confirmation</a>
                                </li>
                                <li ><a href=\"#\"><i class=\"fa fa-archive\"></i><br>Facture</a></li>
                            </ul>
                            <div class=\"content\">
                                <div class=\"table-responsive\">
                                    <table class=\"table\">
                                        <thead>
                                            <tr>
                                                <th colspan=\"5\">Veuillez verifier les information que vous venez de les saisir !! .</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Id commande : </a>
                                                </td>
                                                <td>";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "id", array()), "html", null, true);
        echo "</td>

                                            </tr>      
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Nom : </a>
                                                </td>
                                                <td> Mr/Mme ";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</td>

                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Tel : </a>
                                                </td>
                                                <td>9586235 </td>

                                            </tr>

                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Email : </a>
                                                </td>
                                                <td>";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo " </td>

                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Adresse livraison : </a>
                                                </td>
                                                <td>";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "adresse", array()), "rue", array()), "html", null, true);
        echo " 
                                                    ";
        // line 91
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "adresse", array()), "ville", array()), "html", null, true);
        echo "
                                                    ";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "adresse", array()), "codePostal", array()), "html", null, true);
        echo " </td>
                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Mode de livraison : </a>
                                                </td>
                                                       ";
        // line 98
        if (($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "modeLivraison", array()) == 0)) {
            // line 99
            echo "                                                    <td>Par poste</td>
                                                ";
        } elseif (($this->getAttribute(        // line 100
(isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "modepaiement", array()) == 1)) {
            // line 101
            echo "                                                    <td>Livraison à domicile</td>
                                        
                                                ";
        }
        // line 104
        echo "                                                <td>   </td>

                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Mode de paiement : </a>
                                                </td>
                                                ";
        // line 111
        if (($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "modepaiement", array()) == 1)) {
            // line 112
            echo "                                                    <td>point bonus</td>
                                                ";
        } elseif (($this->getAttribute(        // line 113
(isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "modepaiement", array()) == 2)) {
            // line 114
            echo "                                                    <td>carte bancaire</td>
                                                ";
        } elseif (($this->getAttribute(        // line 115
(isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "modepaiement", array()) == 3)) {
            // line 116
            echo "                                                     <td>à la livraison</td>
                                                ";
        }
        // line 118
        echo "
                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Prix sans Tva : </a>
                                                </td>
                                                <td>";
        // line 124
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "totalCommande", array()), "html", null, true);
        echo " </td>

                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Prix avec Tva : </a>
                                                </td>
                                                <td>";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "totalCommande", array()), "html", null, true);
        echo " </td>

                                            </tr>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan=\"5\"></th>

                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                                <!-- /.table-responsive -->
                            </div>


                            <div class=\"\">
                                <div class=\"pull-left\">
                                    <a href=\"basket.html\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>retour</a>
                                </div>
                                <div class=\"pull-right\">
                                    <button type=\"submit\" class=\"btn btn-primary\">confirmer <i class=\"fa fa-chevron-right\"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class=\"col-md-3\">


                             <div class=\"\" id=\"order-summary\">
                        <div class=\"box-header\">
                            <h3>Totale </h3>
                        </div>
                        <p class=\"text-muted\">la somme des prix des produits ajoutés dans votre panier .</p>

                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>Totale</td>
                                        <th>DT</th>
                                    </tr>
                                    <tr>
                                        <td>Frais supplimentaires</td>
                                        <th>10.00 DT</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>5.00 DT</th>
                                    </tr>
                                    <tr class=\"total\">
                                        <td>Total</td>
                                        <th>DT</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>


                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
                                                <br>
                                                <br> <br>
                                                <hr>
</div>


        <script src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/waypoints.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/modernizr.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap-hover-dropdown.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/front.js"), "html", null, true);
        echo "\"></script>   
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commande:Confirmer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 220,  341 => 219,  337 => 218,  333 => 217,  329 => 216,  325 => 215,  321 => 214,  317 => 213,  232 => 131,  222 => 124,  214 => 118,  210 => 116,  208 => 115,  205 => 114,  203 => 113,  200 => 112,  198 => 111,  189 => 104,  184 => 101,  182 => 100,  179 => 99,  177 => 98,  168 => 92,  164 => 91,  160 => 90,  150 => 83,  132 => 68,  122 => 61,  94 => 36,  80 => 24,  74 => 20,  69 => 18,  64 => 16,  58 => 13,  52 => 10,  48 => 9,  44 => 8,  40 => 7,  36 => 6,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}   */
/*     <html>*/
/*         <head>  */
/* <!-- styles -->*/
/*     <link href="{{asset('bundles/panier/css/font-awesome.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/animate.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.carousel.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.theme.css')}}" rel="stylesheet">*/
/* */
/*     <!-- theme stylesheet -->*/
/*     <link href="{{asset('bundles/panier/css/style.blue.css')}}" rel="stylesheet" id="theme-stylesheet">*/
/* */
/*     <!-- your stylesheet with modifications -->*/
/*     <link href="{{asset('bundles/panier/css/custom.css" rel="stylesheet')}}">*/
/* */
/*     <script src="{{asset('bundles/panier/js/respond.min.js')}}"></script>*/
/* */
/*     <link rel="shortcut icon" href="{{asset('bundles/panier/img/favicon.png')}}">*/
/* */
/* */
/* </head>{# empty Twig template #}{# empty Twig template #}*/
/* <body>*/
/* <br>*/
/* <div class="panel panel-info">*/
/* */
/*         <div id="content">*/
/*             <div class="container">*/
/* */
/* */
/* */
/*                 <div class="col-md-9" id="checkout">*/
/* */
/*                     <div class="">*/
/*                         <form method="post" action="{{path ('confirmer_commande',{'id':commande.id})}}">*/
/*                             <h1>Confirmation de votre commande</h1>*/
/*                             <ul class="nav nav-pills nav-justified">*/
/*                                 <li><a href="checkout1.html"><i class="fa fa fa-truck"></i><br>Livraison</a>*/
/*                                 </li>*/
/*                                 <li><a href="checkout2.html"><i class="fa fa-money"></i><br>Payment</a>*/
/*                                 </li>*/
/*                                 <li class="active"><a href="checkout3.html"><i class="fa fa-eye"></i><br>Confirmation</a>*/
/*                                 </li>*/
/*                                 <li ><a href="#"><i class="fa fa-archive"></i><br>Facture</a></li>*/
/*                             </ul>*/
/*                             <div class="content">*/
/*                                 <div class="table-responsive">*/
/*                                     <table class="table">*/
/*                                         <thead>*/
/*                                             <tr>*/
/*                                                 <th colspan="5">Veuillez verifier les information que vous venez de les saisir !! .</th>*/
/* */
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         <tbody>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Id commande : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{commande.id}}</td>*/
/* */
/*                                             </tr>      */
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Nom : </a>*/
/*                                                 </td>*/
/*                                                 <td> Mr/Mme {{user.username }}</td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Tel : </a>*/
/*                                                 </td>*/
/*                                                 <td>9586235 </td>*/
/* */
/*                                             </tr>*/
/* */
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Email : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{user.email}} </td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Adresse livraison : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{commande.livraison.adresse.rue}} */
/*                                                     {{commande.livraison.adresse.ville}}*/
/*                                                     {{commande.livraison.adresse.codePostal}} </td>*/
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Mode de livraison : </a>*/
/*                                                 </td>*/
/*                                                        {% if commande.livraison.modeLivraison == 0 %}*/
/*                                                     <td>Par poste</td>*/
/*                                                 {% elseif commande.modepaiement == 1 %}*/
/*                                                     <td>Livraison à domicile</td>*/
/*                                         */
/*                                                 {% endif %}*/
/*                                                 <td>   </td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Mode de paiement : </a>*/
/*                                                 </td>*/
/*                                                 {% if commande.modepaiement == 1 %}*/
/*                                                     <td>point bonus</td>*/
/*                                                 {% elseif commande.modepaiement == 2 %}*/
/*                                                     <td>carte bancaire</td>*/
/*                                                 {% elseif commande.modepaiement == 3 %}*/
/*                                                      <td>à la livraison</td>*/
/*                                                 {% endif %}*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Prix sans Tva : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{commande.totalCommande}} </td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Prix avec Tva : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{commande.totalCommande}} </td>*/
/* */
/*                                             </tr>*/
/* */
/*                                         </tbody>*/
/*                                         <tfoot>*/
/*                                             <tr>*/
/*                                                 <th colspan="5"></th>*/
/* */
/*                                             </tr>*/
/*                                         </tfoot>*/
/*                                     </table>*/
/* */
/*                                 </div>*/
/*                                 <!-- /.table-responsive -->*/
/*                             </div>*/
/* */
/* */
/*                             <div class="">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="basket.html" class="btn btn-default"><i class="fa fa-chevron-left"></i>retour</a>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <button type="submit" class="btn btn-primary">confirmer <i class="fa fa-chevron-right"></i>*/
/*                                     </button>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </form>*/
/*                     </div>*/
/*                     <!-- /.box -->*/
/* */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-9 -->*/
/* */
/*                 <div class="col-md-3">*/
/* */
/* */
/*                              <div class="" id="order-summary">*/
/*                         <div class="box-header">*/
/*                             <h3>Totale </h3>*/
/*                         </div>*/
/*                         <p class="text-muted">la somme des prix des produits ajoutés dans votre panier .</p>*/
/* */
/*                         <div class="table-responsive">*/
/*                             <table class="table">*/
/*                                 <tbody>*/
/*                                     <tr>*/
/*                                         <td>Totale</td>*/
/*                                         <th>DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Frais supplimentaires</td>*/
/*                                         <th>10.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Tax</td>*/
/*                                         <th>5.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr class="total">*/
/*                                         <td>Total</td>*/
/*                                         <th>DT</th>*/
/*                                     </tr>*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/* */
/*                     </div>*/
/* */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-3 -->*/
/* */
/*             </div>*/
/*             <!-- /.container -->*/
/*         </div>*/
/*                                                 <br>*/
/*                                                 <br> <br>*/
/*                                                 <hr>*/
/* </div>*/
/* */
/* */
/*         <script src="{{asset('bundles/panier/js/jquery-1.11.0.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/bootstrap.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/jquery.cookie.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/waypoints.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/modernizr.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/bootstrap-hover-dropdown.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/owl.carousel.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/front.js')}}"></script>   */
/*     </body>*/
/* </html>*/
/* {% endblock %}*/
/* */
/* */
/* */
