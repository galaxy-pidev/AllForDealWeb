<?php

/* PiDevClientBundle:Client:parametre.html.twig */
class __TwigTemplate_1805db13e94afcc02247fe80660a4f90fda8051a3095e54329fa0a5a170538c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Client:parametre.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <head>

        <script type=\"text/javascript\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/popup/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/popup/js/jquery.leanModal.min.js"), "html", null, true);
        echo "\"></script>
        <link rel=\"stylesheet\" href=\"http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" />
        <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/popup/css/style.css"), "html", null, true);
        echo "\" />
    </head>
    <body>
        <!-- BEGAIN PRELOADER -->   
        <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Paramétres</strong></h3>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading bg-red\">
                            <h3 class=\"panel-title\"><strong>Paramétres du </strong> Compte</h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 m-b-20\">

                                    <div class=\"btn-group pull-right\">
                                        <button class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">Tools <i class=\"fa fa-angle-down\"></i>
                                        </button>
                                        <ul class=\"dropdown-menu pull-right\">
                                            <li><a href=\"#\">Print</a>
                                            </li>
                                            <li><a href=\"#\">Save as PDF</a>
                                            </li>
                                            <li><a href=\"#\">Export to Excel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive table-red\">
                                    <table class=\"table table-striped table-hover dataTable\" id=\"table-editable\">

                                        <tbody>
                                            <tr>
                                                <td>Nom</td>
                                                <td>";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</td>                                          
                                                <td class=\"text-center\"><a class=\"edit btn-dark\" href=\"#ok\" id=\"modal_trigger\"><i class=\"fa fa-pencil-square-o\"></i>Edit</a> </td>

                                            </tr>
                                            <tr>
                                                <td>Mot de Passe</td>
                                                <td>********</td>                                          
                                                <td class=\"text-center\"><a class=\"edit btn-dark\" href=\"#ok1\" id=\"test\"><i class=\"fa fa-pencil-square-o\"></i>Edit</a> </td>
                                            </tr>
                                            <tr>
                                                <td><a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("Client_Desactiver_compte");
        echo "\">Desactiver Compte</a></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>


                                    <!--email -->      
                                    <div id=\"ok\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>

                                        <section class=\"popupBodny\">
                                            <!-- Register Form -->
                                            <div class=\"user_register\">
                                                ";
        // line 74
        echo "
                                                <form action=\"";
        // line 75
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form1"]) ? $context["form1"] : $this->getContext($context, "form1")), 'enctype');
        echo " method=\"POST\" class=\"fos_user_profile_edit\">
                                                    ";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form1"]) ? $context["form1"] : $this->getContext($context, "form1")), 'widget');
        echo "
                                                    <div>
                                                        <input type=\"submit\" value=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.edit.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
                                                    </div>
                                                </form>
                                            </div>
                                        </section>
                                    </div>
                                    <div id=\"ok1\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>

                                        <section class=\"popupBodny\">
                                            <!-- Register Form -->
                                            <div class=\"user_register\">
                                              ";
        // line 94
        echo "
                                                <form action=\"";
        // line 95
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " method=\"POST\" class=\"fos_user_change_password\">
                                                    ";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                                                    <div>
                                                        <input type=\"submit\" value=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change_password.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
                                                    </div>
                                                </form> 
                                            </div>
                                        </section>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>

        <script type=\"text/javascript\">
            \$(\"#test\").leanModal({top: 200, overlay: 0.6, closeButton: \".modal_close\"});


        </script>

        <script type=\"text/javascript\">
            \$(\"#modal_trigger\").leanModal({top: 200, overlay: 0.6, closeButton: \".modal_close\"});
        </script>
        <script type=\"text/javascript\">
            \$(\"#test2\").leanModal({top: 200, overlay: 0.6, closeButton: \".modal_close\"});
        </script>

    </body>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:parametre.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 98,  159 => 96,  153 => 95,  150 => 94,  132 => 78,  127 => 76,  121 => 75,  118 => 74,  97 => 55,  84 => 45,  44 => 8,  39 => 6,  35 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content  %}*/
/*     <head>*/
/* */
/*         <script type="text/javascript" src="{{asset('bundles/popup/js/jquery-1.11.0.min.js')}}"></script>*/
/*         <script type="text/javascript" src="{{asset('bundles/popup/js/jquery.leanModal.min.js')}}"></script>*/
/*         <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />*/
/*         <link type="text/css" rel="stylesheet" href="{{asset('bundles/popup/css/style.css')}}" />*/
/*     </head>*/
/*     <body>*/
/*         <!-- BEGAIN PRELOADER -->   */
/*         <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Paramétres</strong></h3>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading bg-red">*/
/*                             <h3 class="panel-title"><strong>Paramétres du </strong> Compte</h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 m-b-20">*/
/* */
/*                                     <div class="btn-group pull-right">*/
/*                                         <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>*/
/*                                         </button>*/
/*                                         <ul class="dropdown-menu pull-right">*/
/*                                             <li><a href="#">Print</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Save as PDF</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Export to Excel</a>*/
/*                                             </li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">*/
/*                                     <table class="table table-striped table-hover dataTable" id="table-editable">*/
/* */
/*                                         <tbody>*/
/*                                             <tr>*/
/*                                                 <td>Nom</td>*/
/*                                                 <td>{{user.username}}</td>                                          */
/*                                                 <td class="text-center"><a class="edit btn-dark" href="#ok" id="modal_trigger"><i class="fa fa-pencil-square-o"></i>Edit</a> </td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td>Mot de Passe</td>*/
/*                                                 <td>********</td>                                          */
/*                                                 <td class="text-center"><a class="edit btn-dark" href="#ok1" id="test"><i class="fa fa-pencil-square-o"></i>Edit</a> </td>*/
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td><a href="{{path('Client_Desactiver_compte')}}">Desactiver Compte</a></td>*/
/*                                                 <td></td>*/
/*                                                 <td></td>*/
/*                                             </tr>*/
/*                                         </tbody>*/
/*                                     </table>*/
/* */
/* */
/*                                     <!--email -->      */
/*                                     <div id="ok" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/* */
/*                                         <section class="popupBodny">*/
/*                                             <!-- Register Form -->*/
/*                                             <div class="user_register">*/
/*                                                 {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/*                                                 <form action="{{ path('fos_user_profile_edit') }}" {{ form_enctype(form1) }} method="POST" class="fos_user_profile_edit">*/
/*                                                     {{ form_widget(form1) }}*/
/*                                                     <div>*/
/*                                                         <input type="submit" value="{{ 'profile.edit.submit'|trans }}" />*/
/*                                                     </div>*/
/*                                                 </form>*/
/*                                             </div>*/
/*                                         </section>*/
/*                                     </div>*/
/*                                     <div id="ok1" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/* */
/*                                         <section class="popupBodny">*/
/*                                             <!-- Register Form -->*/
/*                                             <div class="user_register">*/
/*                                               {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/*                                                 <form action="{{ path('fos_user_change_password') }}" {{ form_enctype(form) }} method="POST" class="fos_user_change_password">*/
/*                                                     {{ form_widget(form) }}*/
/*                                                     <div>*/
/*                                                         <input type="submit" value="{{ 'change_password.submit'|trans }}" />*/
/*                                                     </div>*/
/*                                                 </form> */
/*                                             </div>*/
/*                                         </section>*/
/*                                     </div>*/
/* */
/* */
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- END MAIN CONTENT -->*/
/*         </div>*/
/* */
/*         <script type="text/javascript">*/
/*             $("#test").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});*/
/* */
/* */
/*         </script>*/
/* */
/*         <script type="text/javascript">*/
/*             $("#modal_trigger").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});*/
/*         </script>*/
/*         <script type="text/javascript">*/
/*             $("#test2").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});*/
/*         </script>*/
/* */
/*     </body>*/
/* {% endblock %}*/
