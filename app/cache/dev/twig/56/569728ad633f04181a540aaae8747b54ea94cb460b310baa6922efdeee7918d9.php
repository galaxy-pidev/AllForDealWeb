<?php

/* PiDevClientBundle:Service:showAll.html.twig */
class __TwigTemplate_10ce9f1aefa56d5767f0b4fda3757e631c96fbb79e53ecff3c350ef6fb4603ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Service:showAll.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo " <!DOCTYPE html>
<html lang=\"en\">
<head>
\t<title>Crafty |  Free HTML5/CSS3 template</title>
\t<meta charset=\"utf-8\">
\t<meta name=\"author\" content=\"pixelhint.com\">
\t<meta name=\"description\" content=\"Crafty is a stunning HTML5/CSS3 multi-purpose template, well-coded, commented code and easy to customize\"/>
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=1.0\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/service/css/reset.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/service/css/main_responsive.css"), "html", null, true);
        echo "\">
    <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/service/js/jquery.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/service/js/carouFredSel.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/service/js/main.js"), "html", null, true);
        echo "\"></script>
          <link href=";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
            <script type=\"text/javascript\" src=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
            <!-- start menu -->
            <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <script type=\"text/javascript\" src=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
            <script  src=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
            <!--start slider -->
            <link rel=\"stylesheet\" href=";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
            <script src=";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
            <script src=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
            <!--end slider -->
            <script src=";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>

            <!-- CSS  -->      
            <link href=";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/materialize.css"), "html", null, true);
        echo " type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>
            <!-- Font Awesome -->
            <link href=";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/font-awesome.css"), "html", null, true);
        echo " rel=\"stylesheet\">
            <!-- Skill Progress Bar -->
            <link href=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/pro-bars.css\" rel=\"stylesheet"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" />
            <!-- Owl Carousel -->
            <link rel=\"stylesheet\" href=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/owl.carousel.css"), "html", null, true);
        echo ">
            <!-- Default Theme CSS File-->
            <link id=\"switcher\" href=";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/css/themes/blue-theme.css"), "html", null, true);
        echo " type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>     
            <!-- Main css File -->
            <link rel=\"stylesheet\" href=";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo ">
            <link href=";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/style.css"), "html", null, true);
        echo " type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>
            <!-- Font -->
            <link href=\"http://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\"> 
</head>
<body>

          <main role=\"main\">
                    <section id=\"banner\">
                <div class=\"parallax-container\">
                    <div class=\"parallax\">
                        <img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/s.jpg"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"overlay-header\">       
                    </div>
                    <div class=\"overlay-content blog-head\">
                        <div class=\"container\">
                            <h1 class=\"header-title\">Services</h1>                  
                        </div>
                    </div>
                </div>
            </section>
          </main>
    <section class=\"features\">

\t\t<div class=\"\">
                    ";
        // line 68
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 69
            echo "\t\t\t\t<div class=\"feature\">
\t\t\t\t\t<div class=\"ficon\">
\t\t\t\t\t\t<img src=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/service/img/icon1.png"), "html", null, true);
            echo "\" alt=\"\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"details_exp\">
\t\t\t\t\t\t<h3>";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</h3>
                                                <p>";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("service_details", array("service" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"> <u>more details</u><span>→</span></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "\t\t\t</div>
                                        <center>    <div> ";
        // line 80
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["pagination"]) ? $context["pagination"] : $this->getContext($context, "pagination")));
        echo " </div></center>
\t</section><!--  End Features  --> 

    
                         <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-2.1.1.min.js\"></script>
                        <!-- Materialize js -->
                        <script type=\"text/javascript\" src=";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/materialize.min.js"), "html", null, true);
        echo "></script>
                        <!-- Skill Progress Bar -->
                        <script src=";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/appear.min.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>
                        <script src=";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/pro-bars.min.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>
                        <!-- Owl slider -->      
                        <script src=";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/owl.carousel.min.js"), "html", null, true);
        echo "></script>    
                        <!-- Mixit slider  -->
                        <script src=\"http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js\"></script>
                        <!-- counter -->
                        <script src=";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/waypoints.min.js"), "html", null, true);
        echo "></script>
                        <script src=";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/jquery.counterup.min.js"), "html", null, true);
        echo "></script>     

                        <!-- Custom Js -->
                        <script src=";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/profile/js/custom.js"), "html", null, true);
        echo "></script>      
                        </body>
                        </html>   
    ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Service:showAll.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 99,  234 => 96,  230 => 95,  223 => 91,  218 => 89,  214 => 88,  209 => 86,  200 => 80,  197 => 79,  185 => 75,  181 => 74,  175 => 71,  171 => 69,  167 => 68,  149 => 53,  136 => 43,  132 => 42,  127 => 40,  122 => 38,  117 => 36,  112 => 34,  107 => 32,  101 => 29,  96 => 27,  92 => 26,  88 => 25,  84 => 24,  79 => 22,  75 => 21,  71 => 20,  66 => 18,  61 => 16,  57 => 15,  53 => 14,  49 => 13,  45 => 12,  41 => 11,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/*  <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/* 	<title>Crafty |  Free HTML5/CSS3 template</title>*/
/* 	<meta charset="utf-8">*/
/* 	<meta name="author" content="pixelhint.com">*/
/* 	<meta name="description" content="Crafty is a stunning HTML5/CSS3 multi-purpose template, well-coded, commented code and easy to customize"/>*/
/* 	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />*/
/* 	<link rel="stylesheet" type="text/css" href="{{asset('bundles/service/css/reset.css')}}">*/
/* 	<link rel="stylesheet" type="text/css" href="{{asset('bundles/service/css/main_responsive.css')}}">*/
/*     <script type="text/javascript" src="{{asset('bundles/service/js/jquery.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('bundles/service/js/carouFredSel.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('bundles/service/js/main.js')}}"></script>*/
/*           <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*             <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*             <!-- start menu -->*/
/*             <link href={{asset ('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*             <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*             <!--start slider -->*/
/*             <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*             <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*             <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*             <script src={{asset('js/fwslider.js')}}></script>*/
/*             <!--end slider -->*/
/*             <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/* */
/*             <!-- CSS  -->      */
/*             <link href={{asset('bundles/profile/css/materialize.css')}} type="text/css" rel="stylesheet" media="screen,projection"/>*/
/*             <!-- Font Awesome -->*/
/*             <link href={{asset('bundles/profile/css/font-awesome.css')}} rel="stylesheet">*/
/*             <!-- Skill Progress Bar -->*/
/*             <link href={{asset('bundles/profile/css/pro-bars.css" rel="stylesheet')}} type="text/css" media="all" />*/
/*             <!-- Owl Carousel -->*/
/*             <link rel="stylesheet" href={{asset('bundles/profile/css/owl.carousel.css')}}>*/
/*             <!-- Default Theme CSS File-->*/
/*             <link id="switcher" href={{asset('bundles/profile/css/themes/blue-theme.css')}} type="text/css" rel="stylesheet" media="screen,projection"/>     */
/*             <!-- Main css File -->*/
/*             <link rel="stylesheet" href={{asset('css/style.css')}}>*/
/*             <link href={{asset('bundles/profile/style.css')}} type="text/css" rel="stylesheet" media="screen,projection"/>*/
/*             <!-- Font -->*/
/*             <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> */
/* </head>*/
/* <body>*/
/* */
/*           <main role="main">*/
/*                     <section id="banner">*/
/*                 <div class="parallax-container">*/
/*                     <div class="parallax">*/
/*                         <img src="{{asset('img/s.jpg')}}">*/
/*                     </div>*/
/*                     <div class="overlay-header">       */
/*                     </div>*/
/*                     <div class="overlay-content blog-head">*/
/*                         <div class="container">*/
/*                             <h1 class="header-title">Services</h1>                  */
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </section>*/
/*           </main>*/
/*     <section class="features">*/
/* */
/* 		<div class="">*/
/*                     {% for i in pagination %}*/
/* 				<div class="feature">*/
/* 					<div class="ficon">*/
/* 						<img src="{{asset('bundles/service/img/icon1.png')}}" alt="">*/
/* 					</div>*/
/* 					<div class="details_exp">*/
/* 						<h3>{{i.nom}}</h3>*/
/*                                                 <p>{{i.description}}<a href="{{path('service_details',{'service':i.id})}}"> <u>more details</u><span>→</span></a>*/
/* 					</div>*/
/* 				</div>*/
/*                                         {% endfor %}*/
/* 			</div>*/
/*                                         <center>    <div> {{ knp_pagination_render(pagination) }} </div></center>*/
/* 	</section><!--  End Features  --> */
/* */
/*     */
/*                          <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>*/
/*                         <!-- Materialize js -->*/
/*                         <script type="text/javascript" src={{asset('bundles/profile/js/materialize.min.js')}}></script>*/
/*                         <!-- Skill Progress Bar -->*/
/*                         <script src={{asset('bundles/profile/js/appear.min.js')}} type="text/javascript"></script>*/
/*                         <script src={{asset('bundles/profile/js/pro-bars.min.js')}} type="text/javascript"></script>*/
/*                         <!-- Owl slider -->      */
/*                         <script src={{asset('bundles/profile/js/owl.carousel.min.js')}}></script>    */
/*                         <!-- Mixit slider  -->*/
/*                         <script src="http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>*/
/*                         <!-- counter -->*/
/*                         <script src={{asset('bundles/profile/js/waypoints.min.js')}}></script>*/
/*                         <script src={{asset('bundles/profile/js/jquery.counterup.min.js')}}></script>     */
/* */
/*                         <!-- Custom Js -->*/
/*                         <script src={{asset('bundles/profile/js/custom.js')}}></script>      */
/*                         </body>*/
/*                         </html>   */
/*     {% endblock %}*/
/* */
