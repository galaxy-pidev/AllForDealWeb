<?php

/* PiDevClientBundle:Mail:new.html.twig */
class __TwigTemplate_4be4ccec3666b3c07f9d331ee61e0016b9680a3f650a39c70e52a9f1a923ea04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Mail:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
    <head>
        <!-- BEGIN META SECTION -->
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta content=\"\" name=\"description\" />
        <meta content=\"themes-lab\" name=\"author\" />
        <!-- END META SECTION -->
        <!-- BEGIN MANDATORY STYLE -->


        <link href=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/style.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
       <link href=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/summernote/summernote.css"), "html", null, true);
        echo " rel=\"stylesheet\">
       <script src=";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/ckeditor/ckeditor.js"), "html", null, true);
        echo " type=\"text/javascript\"></script>

        <!-- END  MANDATORY STYLE -->
        <script src=\"assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js\"></script>
    </head>
    <div id=\"main-content\" class=\"send-message\">
        <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
            <h3><strong>New</strong> Message</h3>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <div class=\"border-bottom\">
                                    <div class=\"pull-left\">
                                        <a href=\"#\" class=\" btn btn-default\"><i class=\"fa fa-arrow-left fa-fw\"></i> back</a> 
                                    </div>
                                    <div class=\"pull-left p-l-20\">
                                        <a href=\"#\" class=\" btn btn-success\"><i class=\"fa fa-floppy-o fa-fw\"></i> Save Draft</a>
                                    </div>
                                    <div class=\"clearfix m-b-20\"></div>
                                </div>
                                <form class=\"form-horizontal p-t-20\" method=\"POST\" action='";
        // line 43
        echo $this->env->getExtension('routing')->getPath("my_app_mail_sendpage");
        echo "' ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo ">
                                    <div class=\"form-group\">
                                        <label for=\"to\" class=\"col-sm-2 control-label\">To:</label>
                                        <div class=\"col-sm-10\">
                                            <div class=\"input-group\">
                                                <input type=\"text\" class=\"form-control\" name=\"to\">
                                                <span class=\"input-group-addon bg-white cursor-pointer\" data-toggle=\"collapse\" data-target=\"#cc\">     
                                                    CC <span class=\"caret\"></span>
                                            </div>
                                        </div>
                                    </div>
                                                                        <div class=\"form-group\">
                                        <label for=\"to\" class=\"col-sm-2 control-label\">Subject:</label>
                                        <div class=\"col-sm-10\">
                                            <div class=\"input-group\">
                                                <input type=\"text\" class=\"form-control\" id=\"to\" name=\"subject\">
                                                <span class=\"input-group-addon bg-white cursor-pointer\" data-toggle=\"collapse\" data-target=\"#cc\">     
                                                    Subject <span class=\"caret\"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=\"form-group\">
                                        <label for=\"Bcc\" class=\"col-sm-2 control-label\">From:</label>
                                        <div class=\"col-sm-6\">
                                            <div class=\"btn-group\">
                                                <button type=\"button\" class=\"btn btn-default dropdown-toggle width-230\" data-toggle=\"dropdown\">
                                                    <i class=\"fa fa-ellipsis-horizontal\"></i><span class=\"float-left\"><strong>";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</strong></span>
                                                    <span class=\"float-right\"><i class=\"fa fa-angle-down\"></i></span>
                                                </button>
                                                <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"#\">";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                                      
                                    <div class=\"clearfix\"></div>
                            
                                <div>
                                    <div>
                                      <textarea class=\"ckeditor\" name=\"message\" /></textarea>
                                </div>
                                <div class=\"text-center footer-message\">
                                    ";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'widget');
        echo "
                                    <a class=\"btn btn-default\"><i class=\"fa fa-times-circle\"></i> Cancel</a>
                              
                                     
                                    <input type=\"submit\" class=\"btn btn-primary fa fa-times-circle\" value=\"Send email\">
                                </div>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <script src=";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
         <script src=";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/summernote/summernote.js"), "html", null, true);
        echo "></script>
        
        <script src=";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
        <!-- END MANDATORY SCRIPTS -->
        <script src=";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Mail:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  228 => 121,  223 => 119,  218 => 117,  214 => 116,  210 => 115,  206 => 114,  202 => 113,  198 => 112,  194 => 111,  190 => 110,  186 => 109,  182 => 108,  178 => 107,  174 => 106,  170 => 105,  166 => 104,  148 => 89,  131 => 75,  123 => 70,  91 => 43,  64 => 19,  60 => 18,  56 => 17,  52 => 16,  48 => 15,  44 => 14,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content   %}*/
/* */
/*     <head>*/
/*         <!-- BEGIN META SECTION -->*/
/*         <meta charset="utf-8">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <meta content="" name="description" />*/
/*         <meta content="themes-lab" name="author" />*/
/*         <!-- END META SECTION -->*/
/*         <!-- BEGIN MANDATORY STYLE -->*/
/* */
/* */
/*         <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/style.min.css')}} rel="stylesheet">*/
/*        <link href={{asset('plugins/summernote/summernote.css')}} rel="stylesheet">*/
/*        <script src={{asset('bundles/ckeditor/ckeditor.js')}} type="text/javascript"></script>*/
/* */
/*         <!-- END  MANDATORY STYLE -->*/
/*         <script src="assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>*/
/*     </head>*/
/*     <div id="main-content" class="send-message">*/
/*         <div class="page-title"> <i class="icon-custom-left"></i>*/
/*             <h3><strong>New</strong> Message</h3>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <div class="panel panel-default">*/
/*                     <div class="panel-body">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <div class="border-bottom">*/
/*                                     <div class="pull-left">*/
/*                                         <a href="#" class=" btn btn-default"><i class="fa fa-arrow-left fa-fw"></i> back</a> */
/*                                     </div>*/
/*                                     <div class="pull-left p-l-20">*/
/*                                         <a href="#" class=" btn btn-success"><i class="fa fa-floppy-o fa-fw"></i> Save Draft</a>*/
/*                                     </div>*/
/*                                     <div class="clearfix m-b-20"></div>*/
/*                                 </div>*/
/*                                 <form class="form-horizontal p-t-20" method="POST" action='{{path('my_app_mail_sendpage')}}' {{form_enctype(form)}}>*/
/*                                     <div class="form-group">*/
/*                                         <label for="to" class="col-sm-2 control-label">To:</label>*/
/*                                         <div class="col-sm-10">*/
/*                                             <div class="input-group">*/
/*                                                 <input type="text" class="form-control" name="to">*/
/*                                                 <span class="input-group-addon bg-white cursor-pointer" data-toggle="collapse" data-target="#cc">     */
/*                                                     CC <span class="caret"></span>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                                                         <div class="form-group">*/
/*                                         <label for="to" class="col-sm-2 control-label">Subject:</label>*/
/*                                         <div class="col-sm-10">*/
/*                                             <div class="input-group">*/
/*                                                 <input type="text" class="form-control" id="to" name="subject">*/
/*                                                 <span class="input-group-addon bg-white cursor-pointer" data-toggle="collapse" data-target="#cc">     */
/*                                                     Subject <span class="caret"></span>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/* */
/*                                     <div class="form-group">*/
/*                                         <label for="Bcc" class="col-sm-2 control-label">From:</label>*/
/*                                         <div class="col-sm-6">*/
/*                                             <div class="btn-group">*/
/*                                                 <button type="button" class="btn btn-default dropdown-toggle width-230" data-toggle="dropdown">*/
/*                                                     <i class="fa fa-ellipsis-horizontal"></i><span class="float-left"><strong>{{user.email}}</strong></span>*/
/*                                                     <span class="float-right"><i class="fa fa-angle-down"></i></span>*/
/*                                                 </button>*/
/*                                                 <ul class="dropdown-menu">*/
/*                                                     <li>*/
/*                                                         <a href="#">{{user.email}}</a>*/
/*                                                     </li>*/
/*                                                 </ul>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                                       */
/*                                     <div class="clearfix"></div>*/
/*                             */
/*                                 <div>*/
/*                                     <div>*/
/*                                       <textarea class="ckeditor" name="message" /></textarea>*/
/*                                 </div>*/
/*                                 <div class="text-center footer-message">*/
/*                                     {{ form_widget(form._token) }}*/
/*                                     <a class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>*/
/*                               */
/*                                      */
/*                                     <input type="submit" class="btn btn-primary fa fa-times-circle" value="Send email">*/
/*                                 </div>*/
/*                                         </form>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*         <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*         <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*         <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*         <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*         <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*         <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*         <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*         <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*          <script src={{asset('plugins/summernote/summernote.js')}}></script>*/
/*         */
/*         <script src={{asset('js/mailbox.js')}}></script>*/
/*         <!-- END MANDATORY SCRIPTS -->*/
/*         <script src={{asset('js/application.js')}}></script>*/
/* {% endblock %}*/
