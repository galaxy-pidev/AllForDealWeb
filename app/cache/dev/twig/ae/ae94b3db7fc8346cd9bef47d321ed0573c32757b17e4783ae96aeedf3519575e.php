<?php

/* PiDevClientBundle:Client:listProduits.html.twig */
class __TwigTemplate_477d4272ebf2397fd534caeb4e2bbc7156235fb219b1ead69e50932b41330de6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Client:listProduits.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <head>

    </head>
    <body>
        <!-- BEGAIN PRELOADER -->   
        <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Produits</strong></h3>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading bg-red\">
                            <h3 class=\"panel-title\"><strong>Liste des </strong> Produits</h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 m-b-20\">

                                    <div class=\"btn-group pull-right\">
                                        <button class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">Tools <i class=\"fa fa-angle-down\"></i>
                                        </button>
                                        <ul class=\"dropdown-menu pull-right\">
                                            <li><a href=\"#\">Print</a>
                                            </li>
                                            <li><a href=\"#\">Save as PDF</a>
                                            </li>
                                            <li><a href=\"#\">Export to Excel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive table-red\">
                                    <table class=\"table table-striped table-hover dataTable\" id=\"table-editable\">

                                        <tbody>
                                        <thead>
                                            <tr role=\"row\">
                                                <th>Nom</th>
                                                <th>Prix</th>
                                                <th>Quantité</th>
                                                <th>Marque</th>
                                                <th>Prodmotion</th>
                                                <th>Description</th>
                                                <th class=\"text-center\">Action</th>
                                            </tr>
                                        </thead>
                                        ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 51
            echo "                                            <tr>
                                                <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</td>
                                                <td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
            echo "</td> 
                                                <td>";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "qantite", array()), "html", null, true);
            echo "</td>  
                                                <td>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "marque", array()), "html", null, true);
            echo "</td> 
                                                <td>";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "promotion", array()), "html", null, true);
            echo "</td>  
                                                <td>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</td> 
                                                <td class=\"text-center\">
                                                    <a class=\"edit btn btn-dark\" href=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_edit", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-pencil-square-o\"></i>Modifier</a> 
                                                    <a class=\"delete btn btn-danger\" href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_delete", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-times-circle\"></i> Supprimer</a>
                                                </td>

                                            </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "                                                                                                                               
                                        </tbody>
                                    </table>

                                    <!-- nom  -->                                  

                                    <div id=\"modal\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>

                                        <section class=\"popupBodny\">
                                            <!-- Username & Password Login form -->
                                            <div class=\"user_login\">
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>




    </body>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:listProduits.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 64,  116 => 60,  112 => 59,  107 => 57,  103 => 56,  99 => 55,  95 => 54,  91 => 53,  87 => 52,  84 => 51,  80 => 50,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content  %}*/
/*     <head>*/
/* */
/*     </head>*/
/*     <body>*/
/*         <!-- BEGAIN PRELOADER -->   */
/*         <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Produits</strong></h3>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading bg-red">*/
/*                             <h3 class="panel-title"><strong>Liste des </strong> Produits</h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 m-b-20">*/
/* */
/*                                     <div class="btn-group pull-right">*/
/*                                         <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>*/
/*                                         </button>*/
/*                                         <ul class="dropdown-menu pull-right">*/
/*                                             <li><a href="#">Print</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Save as PDF</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Export to Excel</a>*/
/*                                             </li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">*/
/*                                     <table class="table table-striped table-hover dataTable" id="table-editable">*/
/* */
/*                                         <tbody>*/
/*                                         <thead>*/
/*                                             <tr role="row">*/
/*                                                 <th>Nom</th>*/
/*                                                 <th>Prix</th>*/
/*                                                 <th>Quantité</th>*/
/*                                                 <th>Marque</th>*/
/*                                                 <th>Prodmotion</th>*/
/*                                                 <th>Description</th>*/
/*                                                 <th class="text-center">Action</th>*/
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         {% for i in produits %}*/
/*                                             <tr>*/
/*                                                 <td>{{i.nom}}</td>*/
/*                                                 <td>{{i.prix}}</td> */
/*                                                 <td>{{i.qantite}}</td>  */
/*                                                 <td>{{i.marque}}</td> */
/*                                                 <td>{{i.promotion}}</td>  */
/*                                                 <td>{{i.description}}</td> */
/*                                                 <td class="text-center">*/
/*                                                     <a class="edit btn btn-dark" href="{{path('produit_edit',{'id':i.id})}}"><i class="fa fa-pencil-square-o"></i>Modifier</a> */
/*                                                     <a class="delete btn btn-danger" href="{{path('produit_delete',{'id':i.id})}}"><i class="fa fa-times-circle"></i> Supprimer</a>*/
/*                                                 </td>*/
/* */
/*                                             </tr>*/
/*                                         {% endfor %}                                                                                                                               */
/*                                         </tbody>*/
/*                                     </table>*/
/* */
/*                                     <!-- nom  -->                                  */
/* */
/*                                     <div id="modal" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/* */
/*                                         <section class="popupBodny">*/
/*                                             <!-- Username & Password Login form -->*/
/*                                             <div class="user_login">*/
/*                                             </div>*/
/*                                         </section>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- END MAIN CONTENT -->*/
/*         </div>*/
/* */
/* */
/* */
/* */
/*     </body>*/
/* {% endblock %}*/
/* */
