<?php

/* PiDevClientBundle:Service:recherche.html.twig */
class __TwigTemplate_b830a2aafb1db0a389aae1a86d378025395f206b025a8eb9d50d79c1a62bfc15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Service:recherche.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h1>Listes  des Services </h1>
<form method=\"POST\"  action=\"\">
    Rechercher Un Service! : <select name=\"search\">
";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
            // line 7
            echo "<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cat"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cat"], "id", array()), "html", null, true);
            echo "</option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "</select>
    <input type=\"submit\" value=\"chercher\"/>
</form>

    
    ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modeles"]) ? $context["modeles"] : $this->getContext($context, "modeles")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 15
            echo "        <div>
            <br>   Services:<th>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</th></br>
            <br>    Description:<th>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</th></br>
      <a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("details", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">En savoir Plus!</a>
        </div>
         
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "  ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Service:recherche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 22,  73 => 18,  69 => 17,  65 => 16,  62 => 15,  58 => 14,  51 => 9,  40 => 7,  36 => 6,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* <h1>Listes  des Services </h1>*/
/* <form method="POST"  action="">*/
/*     Rechercher Un Service! : <select name="search">*/
/* {% for cat in categories %}*/
/* <option value="{{ cat.id }}">{{ cat.id }}</option>*/
/* {% endfor %}*/
/* </select>*/
/*     <input type="submit" value="chercher"/>*/
/* </form>*/
/* */
/*     */
/*     {% for i in modeles %}*/
/*         <div>*/
/*             <br>   Services:<th>{{i.nom}}</th></br>*/
/*             <br>    Description:<th>{{i.description}}</th></br>*/
/*       <a href="{{path("details",{'id':i.id})}}">En savoir Plus!</a>*/
/*         </div>*/
/*          */
/*     {% endfor %}*/
/*   {% endblock %}*/
