<?php

/* PiDevClientBundle:Mail:show.html.twig */
class __TwigTemplate_d94ca64521b0dc158b52284f2753ff487aa097bd7091b1c12af9a6ec1300e8ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Mail:show.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "  <!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js sidebar-large lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js sidebar-large lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js sidebar-large lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js sidebar-large\"> <!--<![endif]-->

<!-- Added by HTTrack --><meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" /><!-- /Added by HTTrack -->
<head>
    <!-- BEGIN META SECTION -->
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta content=\"\" name=\"description\" />
    <meta content=\"themes-lab\" name=\"author\" />
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    
    
    <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    <link href=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    <link href=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    <link href=";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/style.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    
    
    <!-- END  MANDATORY STYLE -->
    <script src=\"assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js\"></script>
</head>
        <!-- END MAIN SIDEBAR -->
        <div id=\"main-content\" class=\"page-mailbox container-fluid\">
            <div class=\"row\" data-equal-height=\"true\">
                <div class=\"col-md-4 list-messages\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-body messages\">
                            <div class=\"input-group input-group-lg border-bottom\">
                                <span class=\"input-group-btn\">
                                    <a href=\"#\" class=\"btn\"><i class=\"fa fa-search\"></i></a>
                                  </span>
                                <input type=\"text\" class=\"form-control bd-0 bd-white\" placeholder=\"Search\">
                            </div>
                            <div id=\"messages-list\" class=\"panel panel-default withScroll\" data-height=\"window\" data-padding=\"90\">
                                  ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mail"]) ? $context["mail"] : $this->getContext($context, "mail")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "   
                                        ";
            // line 43
            if (($this->getAttribute($context["i"], "destinaire", array()) == $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()))) {
                // line 44
                echo "                                <a class=\"message-item hire-me-btn media\" href=\"#message\" >
                                    <div class=\"pull-left text-center\">
                                        <div class=\"pos-rel message-checkbox\">
                                            <input type=\"checkbox\" data-style=\"flat-red\">
                                        </div>
                                        <div>
                                            <strong><i class=\"fa fa-paperclip\"></i> 2</strong>
                                        </div>
                                    </div>
   
       
 <div class=\"message-item-right\">
    
                                        <div class=\"media\">
                                            <img src=";
                // line 58
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
                echo " alt=\"avatar 3\" width=\"50\" class=\"pull-left\">
                                            <div class=\"media-body\">
                                                <small class=\"pull-right\">23 Sept</small>
                                                <h5 class=\"c-dark\"><strong>";
                // line 61
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "expediteur", array()), "html", null, true);
                echo "</strong></h5>
                                                <h4 class=\"c-dark\">";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
                echo "</h4>
                                            </div>
</div>
                                        <p class=\"f-14 c-gray\">";
                // line 65
                echo $this->getAttribute($context["i"], "Text", array());
                echo "</p>
                                          
          </div>       
                                        </a>
                                        ";
            }
            // line 70
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "  
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-8 col-md-8 email-hidden-sm detail-message\">
                    <div id=\"message-detail\" class=\"panel panel-default withScroll\" data-height=\"window\" data-padding=\"40\">
                        <div class=\"panel-heading messages message-result\">
                            <div class=\"message-action-btn clearfix p-t-20\">
                                <div class=\"pull-left\">
                                    <div id=\"go-back\" rel=\"tooltip\" title=\"Go back\" class=\"icon-rounded m-r-10 email-go-back\"><i class=\"fa fa-hand-o-left\"></i>
                                    </div>
                                    <div rel=\"tooltip\" title=\"Reply\" class=\"icon-rounded m-r-10\"><i class=\"fa fa-reply\"></i>
                                    </div>
                                    <div rel=\"tooltip\" title=\"Forward\" class=\"icon-rounded m-r-10\"><i class=\"fa fa-long-arrow-right\"></i>
                                    </div>
                                    <div rel=\"tooltip\" title=\"Remove from favs\" class=\"icon-rounded heart-red m-r-10\"><i class=\"fa fa-heart\"></i>
                                    </div>
                                    <div rel=\"tooltip\" title=\"Print\" class=\"icon-rounded m-r-10\"><i class=\"fa fa-print\"></i>
                                    </div>
                                    <div rel=\"tooltip\" title=\"Move to trash\" class=\"icon-rounded m-r-10\"><i class=\"fa fa-trash-o\"></i>
                                    </div>
                                </div>
                                <div class=\"pull-right\">
                                    <div rel=\"tooltip\" title=\"Prev\" class=\"icon-rounded m-r-10\"><i class=\"fa fa-angle-double-left\"></i>
                                    </div>
                                    <div rel=\"tooltip\" title=\"Next\" class=\"icon-rounded m-r-10\"><i class=\"fa fa-angle-double-right\"></i>
                                    </div>
                                    <div rel=\"tooltip\" title=\"Parameters\" class=\"icon-rounded gear m-r-10\"><i class=\"fa fa-gear\"></i>
                                    </div>
                                </div>
                            </div>
                            <h2 class=\"p-t-20 w-500\">Objet : ";
        // line 102
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nom", array()), "html", null, true);
        echo "</h2>
                        </div>
                        <div class=\"panel-body messages message-result\">
                            <div class=\"row\">
                                <div class=\"col-md-12 col-sm-12 col-xs-12\">
                                    <div class=\"p-20\">
                                        <div class=\"message-item media\">
                                            <div class=\"message-item-right\">
                                                <div class=\"media\">
                                                    <img src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
        echo "\" alt=\"avatar 4\" width=\"50\" class=\"pull-left\">
                                                    <div class=\"media-body\">
                                                        <small class=\"pull-right\">23 Sept</small>
                                                        <h5 class=\"c-dark\"><strong>";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "expediteur", array()), "html", null, true);
        echo "</strong></h5>
                                                        <p class=\"c-gray\">";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "expediteur", array()), "html", null, true);
        echo "</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"message-body\">
                                       ";
        // line 122
        echo $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "text", array());
        echo "
                                        <div class=\"message-attache\">
                                            <div class=\"media\">
                                                <i class=\"fa fa-paperclip pull-left fa-2x\"></i>
                                                <div class=\"media-body\">
                                                    <div><a href=\"#\" class=\"strong text-regular\">Project.zip</a>
                                                    </div>
                                                    <span>12 MB</span>
                                                    <div class=\"clearfix\"></div>
                                                </div>
                                            </div>
                                            <div class=\"media\">
                                                <i class=\"fa fa-file pull-left fa-2x\"></i>
                                                <div class=\"media-body\">
                                                    <div><a href=\"#\" class=\"strong text-regular\">Contract.pdf</a>
                                                    </div>
                                                    <span>228 KB</span>
                                                    <div class=\"clearfix\"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"message-between\"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END WRAPPER -->
    <!-- BEGIN CHAT MENU -->
   
    <!-- END CHAT MENU -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src=";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
    <!-- END MANDATORY SCRIPTS -->
    <script src=";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>
</body>

</html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Mail:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  301 => 175,  296 => 173,  292 => 172,  288 => 171,  284 => 170,  280 => 169,  276 => 168,  272 => 167,  268 => 166,  264 => 165,  260 => 164,  256 => 163,  252 => 162,  248 => 161,  244 => 160,  203 => 122,  193 => 115,  189 => 114,  183 => 111,  171 => 102,  132 => 70,  124 => 65,  118 => 62,  114 => 61,  108 => 58,  92 => 44,  90 => 43,  84 => 42,  62 => 23,  58 => 22,  54 => 21,  50 => 20,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content   %}*/
/*   <!DOCTYPE html>*/
/* <!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->*/
/* <!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->*/
/* <!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->*/
/* <!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->*/
/* */
/* <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->*/
/* <head>*/
/*     <!-- BEGIN META SECTION -->*/
/*     <meta charset="utf-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <meta content="" name="description" />*/
/*     <meta content="themes-lab" name="author" />*/
/*     <!-- END META SECTION -->*/
/*     <!-- BEGIN MANDATORY STYLE -->*/
/*     */
/*     */
/*     <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*     <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*     <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*     <link href={{asset('bundles/backOffice/css/style.min.css')}} rel="stylesheet">*/
/*     */
/*     */
/*     <!-- END  MANDATORY STYLE -->*/
/*     <script src="assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>*/
/* </head>*/
/*         <!-- END MAIN SIDEBAR -->*/
/*         <div id="main-content" class="page-mailbox container-fluid">*/
/*             <div class="row" data-equal-height="true">*/
/*                 <div class="col-md-4 list-messages">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-body messages">*/
/*                             <div class="input-group input-group-lg border-bottom">*/
/*                                 <span class="input-group-btn">*/
/*                                     <a href="#" class="btn"><i class="fa fa-search"></i></a>*/
/*                                   </span>*/
/*                                 <input type="text" class="form-control bd-0 bd-white" placeholder="Search">*/
/*                             </div>*/
/*                             <div id="messages-list" class="panel panel-default withScroll" data-height="window" data-padding="90">*/
/*                                   {% for i in mail %}   */
/*                                         {% if i.destinaire == user.email %}*/
/*                                 <a class="message-item hire-me-btn media" href="#message" >*/
/*                                     <div class="pull-left text-center">*/
/*                                         <div class="pos-rel message-checkbox">*/
/*                                             <input type="checkbox" data-style="flat-red">*/
/*                                         </div>*/
/*                                         <div>*/
/*                                             <strong><i class="fa fa-paperclip"></i> 2</strong>*/
/*                                         </div>*/
/*                                     </div>*/
/*    */
/*        */
/*  <div class="message-item-right">*/
/*     */
/*                                         <div class="media">*/
/*                                             <img src={{asset(path('image'))}} alt="avatar 3" width="50" class="pull-left">*/
/*                                             <div class="media-body">*/
/*                                                 <small class="pull-right">23 Sept</small>*/
/*                                                 <h5 class="c-dark"><strong>{{i.expediteur}}</strong></h5>*/
/*                                                 <h4 class="c-dark">{{i.nom}}</h4>*/
/*                                             </div>*/
/* </div>*/
/*                                         <p class="f-14 c-gray">{{i.Text|raw}}</p>*/
/*                                           */
/*           </div>       */
/*                                         </a>*/
/*                                         {% endif %}*/
/*                                 {% endfor %}  */
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="col-lg-8 col-md-8 email-hidden-sm detail-message">*/
/*                     <div id="message-detail" class="panel panel-default withScroll" data-height="window" data-padding="40">*/
/*                         <div class="panel-heading messages message-result">*/
/*                             <div class="message-action-btn clearfix p-t-20">*/
/*                                 <div class="pull-left">*/
/*                                     <div id="go-back" rel="tooltip" title="Go back" class="icon-rounded m-r-10 email-go-back"><i class="fa fa-hand-o-left"></i>*/
/*                                     </div>*/
/*                                     <div rel="tooltip" title="Reply" class="icon-rounded m-r-10"><i class="fa fa-reply"></i>*/
/*                                     </div>*/
/*                                     <div rel="tooltip" title="Forward" class="icon-rounded m-r-10"><i class="fa fa-long-arrow-right"></i>*/
/*                                     </div>*/
/*                                     <div rel="tooltip" title="Remove from favs" class="icon-rounded heart-red m-r-10"><i class="fa fa-heart"></i>*/
/*                                     </div>*/
/*                                     <div rel="tooltip" title="Print" class="icon-rounded m-r-10"><i class="fa fa-print"></i>*/
/*                                     </div>*/
/*                                     <div rel="tooltip" title="Move to trash" class="icon-rounded m-r-10"><i class="fa fa-trash-o"></i>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <div rel="tooltip" title="Prev" class="icon-rounded m-r-10"><i class="fa fa-angle-double-left"></i>*/
/*                                     </div>*/
/*                                     <div rel="tooltip" title="Next" class="icon-rounded m-r-10"><i class="fa fa-angle-double-right"></i>*/
/*                                     </div>*/
/*                                     <div rel="tooltip" title="Parameters" class="icon-rounded gear m-r-10"><i class="fa fa-gear"></i>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <h2 class="p-t-20 w-500">Objet : {{entity.nom}}</h2>*/
/*                         </div>*/
/*                         <div class="panel-body messages message-result">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                                     <div class="p-20">*/
/*                                         <div class="message-item media">*/
/*                                             <div class="message-item-right">*/
/*                                                 <div class="media">*/
/*                                                     <img src="{{asset(path('image'))}}" alt="avatar 4" width="50" class="pull-left">*/
/*                                                     <div class="media-body">*/
/*                                                         <small class="pull-right">23 Sept</small>*/
/*                                                         <h5 class="c-dark"><strong>{{entity.expediteur}}</strong></h5>*/
/*                                                         <p class="c-gray">{{entity.expediteur}}</p>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                     <div class="message-body">*/
/*                                        {{entity.text|raw}}*/
/*                                         <div class="message-attache">*/
/*                                             <div class="media">*/
/*                                                 <i class="fa fa-paperclip pull-left fa-2x"></i>*/
/*                                                 <div class="media-body">*/
/*                                                     <div><a href="#" class="strong text-regular">Project.zip</a>*/
/*                                                     </div>*/
/*                                                     <span>12 MB</span>*/
/*                                                     <div class="clearfix"></div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <div class="media">*/
/*                                                 <i class="fa fa-file pull-left fa-2x"></i>*/
/*                                                 <div class="media-body">*/
/*                                                     <div><a href="#" class="strong text-regular">Contract.pdf</a>*/
/*                                                     </div>*/
/*                                                     <span>228 KB</span>*/
/*                                                     <div class="clearfix"></div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="message-between"></div>*/
/* */
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <!-- END MAIN CONTENT -->*/
/*     </div>*/
/*     <!-- END WRAPPER -->*/
/*     <!-- BEGIN CHAT MENU -->*/
/*    */
/*     <!-- END CHAT MENU -->*/
/*     <!-- BEGIN MANDATORY SCRIPTS -->*/
/*     <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*     <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*     <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*     <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*     <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*     <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*     <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*     <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*     <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*     <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*     <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*     <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*     <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*     <script src={{asset('js/mailbox.js')}}></script>*/
/*     <!-- END MANDATORY SCRIPTS -->*/
/*     <script src={{asset('js/application.js')}}></script>*/
/* </body>*/
/* */
/* </html>*/
/* {% endblock %}*/
