<?php

/* ::compte.html.twig */
class __TwigTemplate_2ea1739a938002263075210bd479d11dbf4cec4f186a1fc604f906db24545a68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js sidebar-large lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js sidebar-large lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js sidebar-large lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js sidebar-large\"> <!--<![endif]-->

    <!-- Added by HTTrack --><meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" /><!-- /Added by HTTrack -->
    <head>
        <!-- BEGIN META SECTION -->
        <meta charset=\"utf-8\">
        <title>All For Deal Profile</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta content=\"\" name=\"description\" />
        <meta content=\"themes-lab\" name=\"author\" />
        <!-- END META SECTION -->
        <!-- BEGIN MANDATORY STYLE -->
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <link href=";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
         <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
        <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
        <script type=\"text/javascript\" src=";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
        <!-- start menu -->
        <link href=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <script type=\"text/javascript\" src=";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
        <script  src=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
        <!--start slider -->
        <link rel=\"stylesheet\" href=";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
        <script src=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
        <script src=";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
        <!--end slider -->
        <script src=";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>
        <link href=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/style.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">


        <!-- END  MANDATORY STYLE -->
        <script src=";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
        echo "></script>
    </head>

    <body data-page=\"mailbox\">
        <!-- BEGIN TOP MENU -->

        <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
                        <div class=\"header-top\">
        <div class=\"wrap\"> 

            <div class=\"cssmenu\">
                <ul>
                   ";
        // line 55
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            echo "  
                        <li><a href=\"";
            // line 56
            echo $this->env->getExtension('routing')->getPath("produit");
            echo "\">Accueil</a></li> |
                        <li class=\"active\"><a href=\"";
            // line 57
            echo $this->env->getExtension('routing')->getPath("Profile");
            echo "\">Compte</a></li> |  
                        <li><a href=\"";
            // line 58
            echo $this->env->getExtension('routing')->getPath("favoris_show", array("id" => 4));
            echo "\">Favoris</a></li> |
                        <li><a href=\"";
            // line 59
            echo $this->env->getExtension('routing')->getPath("panier_show", array("id" => 1));
            echo "\">Panier</a></li> |
                        <li><a href=\"";
            // line 60
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">Se déconnecter</a></li> |
                       
                        ";
        } else {
            // line 63
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\">Se connecter</a></li> |
                        <li><a href=\"";
            // line 64
            echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
            echo "\">S'inscrire</a></li>
                        ";
        }
        // line 66
        echo "                </ul>
            </div>
            <div class=\"clear\"></div>
        </div>
    </div>
           

    
        </nav>
        <!-- END TOP MENU -->
        <!-- BEGIN WRAPPER -->
        <div id=\"wrapper\">
            <!-- BEGIN MAIN SIDEBAR -->
            <nav id=\"sidebar\">
                <div id=\"main-menu\">
                    <ul class=\"sidebar-nav\">
                        <li class=\"current active hasSub\">
                            <a href=\"";
        // line 83
        echo $this->env->getExtension('routing')->getPath("Profile");
        echo "\"><i class=\"fa fa-dashboard\"></i><span class=\"sidebar-text\">Votre profile</span></a>
                        </li>
                        <li>
                            <a href=";
        // line 86
        echo $this->env->getExtension('routing')->getPath("Inbox");
        echo "><i class=\"fa fa-envelope\"></i><span class=\"sidebar-text\">Boite de reception </span><span class=\"fa arrow\"></span></a>
                        </li>
                        <li>
                            <a href=\"";
        // line 89
        echo $this->env->getExtension('routing')->getPath("mail_new");
        echo "\"><i class=\"fa fa-pencil\"></i><span class=\"sidebar-text\">Nouveau Message</span><span class=\"fa arrow\"></span></a>
                        </li>
                   
                        <li>
                            <a href=";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_show", array("id" => $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "panier", array()), "id", array()))), "html", null, true);
        echo "><i class=\"fa fa-shopping-cart\"></i><span class=\"sidebar-text\">Panier</span><span class=\"fa arrow\"></span></a>

                        </li>
                        <li>                
                            <a href=\"";
        // line 97
        echo $this->env->getExtension('routing')->getPath("client_list_produit");
        echo "\"><i class=\"glyph-icon flaticon-forms\"></i><span class=\"sidebar-text\">Produit</span><span class=\"fa arrow\"></span></a>

                        </li>
                        <li>
                            <a href=\"";
        // line 101
        echo $this->env->getExtension('routing')->getPath("client_list_Forums");
        echo "\"><i class=\"fa fa-comments-o\"></i><span class=\"sidebar-text\">Forums</span><span class=\"fa arrow\"></span></a>

                        </li>
                        <li>
                            <a href=\"";
        // line 105
        echo $this->env->getExtension('routing')->getPath("client_list_services");
        echo "\"><i class=\"glyph-icon flaticon-pages\"></i><span class=\"sidebar-text\">Services</span><span class=\"fa arrow\"></span></a>

                        </li>
                        <li>
                            <a href=\"";
        // line 109
        echo $this->env->getExtension('routing')->getPath("client_list_appeloffres");
        echo "\"><i class=\"glyph-icon flaticon-world\"></i><span class=\"sidebar-text\">Apples d'offre</span><span class=\"fa arrow\"></span></a>
                        </li>
                        <li>
                            <a href=\"";
        // line 112
        echo $this->env->getExtension('routing')->getPath("listCommande");
        echo "\"><i class=\"glyph-icon flaticon-panels\"></i><span class=\"sidebar-text\">Commandes</span><span class=\"fa arrow\"></span></a>

                        </li>
                        <li>
                            <a  href=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_show", array("id" => $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "favoris", array()), "id", array()))), "html", null, true);
        echo "\"><i class=\"fa fa-heart\"></i><span class=\"sidebar-text\">Favoris</span><span class=\"fa arrow\"></span></a>

                        </li>
                        <li>
                            <a  href=\"";
        // line 120
        echo $this->env->getExtension('routing')->getPath("client_compte_parametres");
        echo "\" ><i class=\"fa fa-cogs\"></i><span class=\"sidebar-text\">Parametres</span><span class=\"fa arrow\"></span></a>

                        </li>

                    </ul>
                </div>
                <div class=\"footer-widget\">
                    <img src=";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/gradient.png"), "html", null, true);
        echo " alt=\"gradient effet\" class=\"sidebar-gradient-img\" />
                         <div id=\"sidebar-charts\">

                        <div class=\"sidebar-footer clearfix\">
                            <a class=\"pull-left\" href=\"profil.html\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Settings\"><i class=\"glyph-icon flaticon-settings21\"></i></a>
                            <a class=\"pull-left toggle_fullscreen\" href=\"#\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Fullscreen\"><i class=\"glyph-icon flaticon-fullscreen3\"></i></a>
                            <a class=\"pull-left\" href=\"";
        // line 133
        echo $this->env->getExtension('routing')->getPath("Client_Desactiver_compte");
        echo "\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Desactiver\"><i class=\"glyph-icon flaticon-padlock23\"></i></a>
                            <a class=\"pull-left\" href=\"";
        // line 134
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Deconnexion\"><i class=\"fa fa-power-off\"></i></a>
                        </div> 
                    </div>
            </nav>
            <!-- END MAIN SIDEBAR -->
            ";
        // line 139
        $this->displayBlock('content', $context, $blocks);
        // line 140
        echo "  
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END WRAPPER -->
        <!-- BEGIN CHAT MENU -->

        <!-- END CHAT MENU -->
        <!-- BEGIN MANDATORY SCRIPTS -->
        <script src=";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
        <!-- END MANDATORY SCRIPTS -->
        <script src=";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>
    </body>

</html>


";
    }

    // line 139
    public function block_content($context, array $blocks = array())
    {
        // line 140
        echo "            ";
    }

    public function getTemplateName()
    {
        return "::compte.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  366 => 140,  363 => 139,  352 => 163,  347 => 161,  343 => 160,  339 => 159,  335 => 158,  331 => 157,  327 => 156,  323 => 155,  319 => 154,  315 => 153,  311 => 152,  307 => 151,  303 => 150,  299 => 149,  295 => 148,  285 => 140,  283 => 139,  275 => 134,  271 => 133,  262 => 127,  252 => 120,  245 => 116,  238 => 112,  232 => 109,  225 => 105,  218 => 101,  211 => 97,  204 => 93,  197 => 89,  191 => 86,  185 => 83,  166 => 66,  161 => 64,  156 => 63,  150 => 60,  146 => 59,  142 => 58,  138 => 57,  134 => 56,  130 => 55,  115 => 43,  108 => 39,  104 => 38,  100 => 37,  96 => 36,  92 => 35,  87 => 33,  83 => 32,  79 => 31,  75 => 30,  70 => 28,  66 => 27,  62 => 26,  57 => 24,  52 => 22,  48 => 21,  44 => 20,  40 => 19,  20 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->*/
/* <!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->*/
/* <!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->*/
/* <!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->*/
/* */
/*     <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->*/
/*     <head>*/
/*         <!-- BEGIN META SECTION -->*/
/*         <meta charset="utf-8">*/
/*         <title>All For Deal Profile</title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <meta content="" name="description" />*/
/*         <meta content="themes-lab" name="author" />*/
/*         <!-- END META SECTION -->*/
/*         <!-- BEGIN MANDATORY STYLE -->*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*          <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <link href={{asset('css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*         <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*         <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*         <!-- start menu -->*/
/*         <link href={{asset('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*         <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*         <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*         <!--start slider -->*/
/*         <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*         <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*         <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*         <script src={{asset('js/fwslider.js')}}></script>*/
/*         <!--end slider -->*/
/*         <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/*         <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/style.min.css')}} rel="stylesheet">*/
/* */
/* */
/*         <!-- END  MANDATORY STYLE -->*/
/*         <script src={{asset('plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js')}}></script>*/
/*     </head>*/
/* */
/*     <body data-page="mailbox">*/
/*         <!-- BEGIN TOP MENU -->*/
/* */
/*         <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*                         <div class="header-top">*/
/*         <div class="wrap"> */
/* */
/*             <div class="cssmenu">*/
/*                 <ul>*/
/*                    {% if is_granted("ROLE_USER") %}  */
/*                         <li><a href="{{ path('produit')}}">Accueil</a></li> |*/
/*                         <li class="active"><a href="{{ path('Profile')}}">Compte</a></li> |  */
/*                         <li><a href="{{ path('favoris_show', { 'id': 4 }) }}">Favoris</a></li> |*/
/*                         <li><a href="{{ path('panier_show', { 'id': 1 }) }}">Panier</a></li> |*/
/*                         <li><a href="{{path('fos_user_security_logout')}}">Se déconnecter</a></li> |*/
/*                        */
/*                         {% else %}*/
/*                         <li><a href="{{path('fos_user_security_login')}}">Se connecter</a></li> |*/
/*                         <li><a href="{{path('fos_user_registration_register')}}">S'inscrire</a></li>*/
/*                         {% endif %}*/
/*                 </ul>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*         </div>*/
/*     </div>*/
/*            */
/* */
/*     */
/*         </nav>*/
/*         <!-- END TOP MENU -->*/
/*         <!-- BEGIN WRAPPER -->*/
/*         <div id="wrapper">*/
/*             <!-- BEGIN MAIN SIDEBAR -->*/
/*             <nav id="sidebar">*/
/*                 <div id="main-menu">*/
/*                     <ul class="sidebar-nav">*/
/*                         <li class="current active hasSub">*/
/*                             <a href="{{ path('Profile')}}"><i class="fa fa-dashboard"></i><span class="sidebar-text">Votre profile</span></a>*/
/*                         </li>*/
/*                         <li>*/
/*                             <a href={{ path('Inbox')}}><i class="fa fa-envelope"></i><span class="sidebar-text">Boite de reception </span><span class="fa arrow"></span></a>*/
/*                         </li>*/
/*                         <li>*/
/*                             <a href="{{path('mail_new')}}"><i class="fa fa-pencil"></i><span class="sidebar-text">Nouveau Message</span><span class="fa arrow"></span></a>*/
/*                         </li>*/
/*                    */
/*                         <li>*/
/*                             <a href={{ path('panier_show', { 'id': user.panier.id }) }}><i class="fa fa-shopping-cart"></i><span class="sidebar-text">Panier</span><span class="fa arrow"></span></a>*/
/* */
/*                         </li>*/
/*                         <li>                */
/*                             <a href="{{ path('client_list_produit') }}"><i class="glyph-icon flaticon-forms"></i><span class="sidebar-text">Produit</span><span class="fa arrow"></span></a>*/
/* */
/*                         </li>*/
/*                         <li>*/
/*                             <a href="{{path('client_list_Forums')}}"><i class="fa fa-comments-o"></i><span class="sidebar-text">Forums</span><span class="fa arrow"></span></a>*/
/* */
/*                         </li>*/
/*                         <li>*/
/*                             <a href="{{path('client_list_services')}}"><i class="glyph-icon flaticon-pages"></i><span class="sidebar-text">Services</span><span class="fa arrow"></span></a>*/
/* */
/*                         </li>*/
/*                         <li>*/
/*                             <a href="{{ path('client_list_appeloffres')}}"><i class="glyph-icon flaticon-world"></i><span class="sidebar-text">Apples d'offre</span><span class="fa arrow"></span></a>*/
/*                         </li>*/
/*                         <li>*/
/*                             <a href="{{ path('listCommande')}}"><i class="glyph-icon flaticon-panels"></i><span class="sidebar-text">Commandes</span><span class="fa arrow"></span></a>*/
/* */
/*                         </li>*/
/*                         <li>*/
/*                             <a  href="{{ path('favoris_show', { 'id': user.favoris.id }) }}"><i class="fa fa-heart"></i><span class="sidebar-text">Favoris</span><span class="fa arrow"></span></a>*/
/* */
/*                         </li>*/
/*                         <li>*/
/*                             <a  href="{{ path('client_compte_parametres') }}" ><i class="fa fa-cogs"></i><span class="sidebar-text">Parametres</span><span class="fa arrow"></span></a>*/
/* */
/*                         </li>*/
/* */
/*                     </ul>*/
/*                 </div>*/
/*                 <div class="footer-widget">*/
/*                     <img src={{asset('img/gradient.png')}} alt="gradient effet" class="sidebar-gradient-img" />*/
/*                          <div id="sidebar-charts">*/
/* */
/*                         <div class="sidebar-footer clearfix">*/
/*                             <a class="pull-left" href="profil.html" rel="tooltip" data-placement="top" data-original-title="Settings"><i class="glyph-icon flaticon-settings21"></i></a>*/
/*                             <a class="pull-left toggle_fullscreen" href="#" rel="tooltip" data-placement="top" data-original-title="Fullscreen"><i class="glyph-icon flaticon-fullscreen3"></i></a>*/
/*                             <a class="pull-left" href="{{path('Client_Desactiver_compte')}}" rel="tooltip" data-placement="top" data-original-title="Desactiver"><i class="glyph-icon flaticon-padlock23"></i></a>*/
/*                             <a class="pull-left" href="{{path('fos_user_security_logout')}}" rel="tooltip" data-placement="top" data-original-title="Deconnexion"><i class="fa fa-power-off"></i></a>*/
/*                         </div> */
/*                     </div>*/
/*             </nav>*/
/*             <!-- END MAIN SIDEBAR -->*/
/*             {% block content   %}*/
/*             {% endblock %}  */
/*             <!-- END MAIN CONTENT -->*/
/*         </div>*/
/*         <!-- END WRAPPER -->*/
/*         <!-- BEGIN CHAT MENU -->*/
/* */
/*         <!-- END CHAT MENU -->*/
/*         <!-- BEGIN MANDATORY SCRIPTS -->*/
/*         <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*         <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*         <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*         <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*         <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*         <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*         <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*         <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*         <script src={{asset('js/mailbox.js')}}></script>*/
/*         <!-- END MANDATORY SCRIPTS -->*/
/*         <script src={{asset('js/application.js')}}></script>*/
/*     </body>*/
/* */
/* </html>*/
/* */
/* */
/* */
