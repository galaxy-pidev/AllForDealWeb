<?php

/* PiDevClientBundle:Panier:affiche.html.twig */
class __TwigTemplate_b1dd9280a209a40fd98084036bb37ec8bf85b057f3f13c2af4b416b47eb18514 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Panier:affiche.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        echo "   
    <html>
        <head>  
<!-- styles -->
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- theme stylesheet -->
    <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/style.blue.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-stylesheet\">

    <!-- your stylesheet with modifications -->
    <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/custom.css\" rel=\"stylesheet"), "html", null, true);
        echo "\">

    <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/respond.min.js"), "html", null, true);
        echo "\"></script>

    <link rel=\"shortcut icon\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/img/favicon.png"), "html", null, true);
        echo "\">


</head>

<body>";
        // line 26
        echo "    
<div class=\"panel panel-info\">
    
    
  <div id=\"content\">
            <div class=\"container\">


                <div class=\"col-md-9\" id=\"basket\">

                    <div class=\"\">

                        <form method=\"POST\" action=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_show", array("id" => $this->getAttribute((isset($context["panier"]) ? $context["panier"] : $this->getContext($context, "panier")), "id", array()))), "html", null, true);
        echo "\" > 
 ";
        // line 39
        $context["nbcmd"] = 0;
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cm"]) ? $context["cm"] : $this->getContext($context, "cm")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 41
            echo "    ";
            $context["nbcmd"] = ((isset($context["nbcmd"]) ? $context["nbcmd"] : $this->getContext($context, "nbcmd")) + 1);
            // line 42
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "                            <h1>Votre pnaier</h1>
                            <p class=\"text-muted\">vous avez ";
        // line 44
        echo twig_escape_filter($this->env, (isset($context["nbcmd"]) ? $context["nbcmd"] : $this->getContext($context, "nbcmd")), "html", null, true);
        echo " article(s) dans votre panier.</p>
                            <div class=\"table-responsive\">
                                <table class=\"table\">
                                    <thead>
                                        <tr>
                                            <th colspan=\"2\">Produit</th>
                                            <th>Quantité</th>
                                            <th>Prix</th>
                                            <th>Promotion</th>
                                            <th colspan=\"2\">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody
                                        ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            echo " 
                            ";
            // line 58
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["j"]);
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo "  
                                        <tr>
                                            <td>
                                                <a href=\"#\">
                                                    <img src=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
                echo "\" alt=\"White Blouse Armani\">
                                                </a>
                                            </td>
                                            <td><a href=\"#\">";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
                echo "</a>
                                            </td>
                                            <td>
                                                ";
                // line 68
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["cm"]) ? $context["cm"] : $this->getContext($context, "cm")));
                foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
                    // line 69
                    echo "                                       ";
                    if (($this->getAttribute($this->getAttribute($context["m"], "produit", array()), "id", array()) == $this->getAttribute($context["i"], "id", array()))) {
                        // line 70
                        echo "                                        
                                                <input name=";
                        // line 71
                        echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "id", array()), "html", null, true);
                        echo " type=\"number\" value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "qte", array()), "html", null, true);
                        echo "\" class=\"form-control\">
                                              
                                                ";
                    }
                    // line 74
                    echo "                                              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " 
                                            </td>
                                            <td>";
                // line 76
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
                echo " DT</td>
                                            <td>";
                // line 77
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
                echo " DT</td>
                                            <td>";
                // line 78
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
                echo " DT</td>
                                            <td><a href=\"";
                // line 79
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_remove", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "panier", array()), "id", array()), "produit" => $this->getAttribute(                // line 80
$context["i"], "id", array()))), "html", null, true);
                echo "\"><i class=\"fa fa-trash-o\"></i></a>
                                            </td>
                                        </tr>
                                        
                                        ";
                // line 84
                $context["prixSP"] = ((isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")) + $this->getAttribute($context["i"], "prix", array()));
                // line 85
                echo "                                    ";
                $context["prixP"] = ((isset($context["prixP"]) ? $context["prixP"] : $this->getContext($context, "prixP")) + ($this->getAttribute($context["i"], "prix", array()) - (($this->getAttribute($context["i"], "prix", array()) * $this->getAttribute($context["i"], "promotion", array())) / 100)));
                // line 86
                echo "                                              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 87
            echo "                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "                                     
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan=\"5\">Total</th>
                                            <th colspan=\"2\">";
        // line 93
        echo twig_escape_filter($this->env, (isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")), "html", null, true);
        echo " DT</th>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                            <!-- /.table-responsive -->

                            <div class=\"\">
                                <div class=\"pull-left\">
                                    <a href=\"category.html\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i> Continuer shopping</a>
                                </div>
                                <div class=\"pull-right\">
                                    <a class=\"btn btn-default\"><i class=\"fa fa-refresh\"></i> Actualiser</a>
                             
                                    <a href=\"";
        // line 108
        echo $this->env->getExtension('routing')->getPath("choisir_adresse");
        echo "\" class=\"btn btn-primary\">passer commande <i class=\"fa fa-chevron-right\"></i>
                                    </a>
                                </div>
                            </div>
                                    <hr>

                        </form>

                    </div>
                                    <hr><hr>
                    <!-- /.box -->


                    <div class=\"row same-height-row\">
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"same-height\">
                                <h3>Vous pourriez aimer </h3>
                            </div>
                        </div>
                        ";
        // line 127
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, (isset($context["prods"]) ? $context["prods"] : $this->getContext($context, "prods")), 0, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 128
            echo "                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"product same-height\">
                                <div class=\"flip-container\">
                                    <div class=\"flipper\">
                                        <div class=\"front\">
                                            <a href=\"";
            // line 133
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "\">
                                                <img src=\"";
            // line 134
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\"  width=\"182px\" height=\"200px\" alt=\"\">
                                            </a>
                                        </div>
                                        <div class=\"back\">
                                            <a href=\"";
            // line 138
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "\">
                                                <img src=\"";
            // line 139
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\"  width=\"182px\" height=\"200px\" alt=\"\" >
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href=\"";
            // line 144
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "\" class=\"invisible\">
                                    <img src=\"";
            // line 145
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\" width=\"182px\" height=\"200px\" alt=\"\" >
                                </a>
                                <div class=\"text\">
                                    <h3>";
            // line 148
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</h3>
                                    <p class=\"price\">";
            // line 149
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
            echo "</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 155
        echo "
          

                    </div>


                </div>
                <!-- /.col-md-9 -->

                <div class=\"col-md-3\">
                    <div class=\"\" id=\"order-summary\">
                        <div class=\"box-header\">
                            <h3>Totale </h3>
                        </div>
                        <p class=\"text-muted\">la somme des prix des produits ajoutés dans votre panier .</p>

                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>Totale</td>
                                        <th>";
        // line 176
        echo twig_escape_filter($this->env, (isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")), "html", null, true);
        echo " DT</th>
                                    </tr>
                                    <tr>
                                        <td>Frais supplimentaires</td>
                                        <th>10.00 DT</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>5.00 DT</th>
                                    </tr>
                                    <tr class=\"total\">
                                        <td>Total</td>
                                        <th>";
        // line 188
        echo twig_escape_filter($this->env, ((isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")) + 10), "html", null, true);
        echo "DT</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                                    <hr>
                                    <hr>
                 

                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
                                            </div>
  <script src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 208
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/waypoints.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/modernizr.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap-hover-dropdown.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/front.js"), "html", null, true);
        echo "\"></script>   
</body>
    </html>
    ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Panier:affiche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  420 => 214,  416 => 213,  412 => 212,  408 => 211,  404 => 210,  400 => 209,  396 => 208,  392 => 207,  370 => 188,  355 => 176,  332 => 155,  320 => 149,  316 => 148,  310 => 145,  306 => 144,  298 => 139,  294 => 138,  287 => 134,  283 => 133,  276 => 128,  272 => 127,  250 => 108,  232 => 93,  225 => 88,  219 => 87,  213 => 86,  210 => 85,  208 => 84,  201 => 80,  200 => 79,  196 => 78,  192 => 77,  188 => 76,  179 => 74,  171 => 71,  168 => 70,  165 => 69,  161 => 68,  155 => 65,  149 => 62,  140 => 58,  134 => 57,  118 => 44,  115 => 43,  109 => 42,  106 => 41,  102 => 40,  100 => 39,  96 => 38,  82 => 26,  74 => 20,  69 => 18,  64 => 16,  58 => 13,  52 => 10,  48 => 9,  44 => 8,  40 => 7,  36 => 6,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}   */
/*     <html>*/
/*         <head>  */
/* <!-- styles -->*/
/*     <link href="{{asset('bundles/panier/css/font-awesome.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/animate.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.carousel.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/panier/css/owl.theme.css')}}" rel="stylesheet">*/
/* */
/*     <!-- theme stylesheet -->*/
/*     <link href="{{asset('bundles/panier/css/style.blue.css')}}" rel="stylesheet" id="theme-stylesheet">*/
/* */
/*     <!-- your stylesheet with modifications -->*/
/*     <link href="{{asset('bundles/panier/css/custom.css" rel="stylesheet')}}">*/
/* */
/*     <script src="{{asset('bundles/panier/js/respond.min.js')}}"></script>*/
/* */
/*     <link rel="shortcut icon" href="{{asset('bundles/panier/img/favicon.png')}}">*/
/* */
/* */
/* </head>*/
/* */
/* <body>{# empty Twig template #}*/
/*     */
/* <div class="panel panel-info">*/
/*     */
/*     */
/*   <div id="content">*/
/*             <div class="container">*/
/* */
/* */
/*                 <div class="col-md-9" id="basket">*/
/* */
/*                     <div class="">*/
/* */
/*                         <form method="POST" action="{{path('panier_show',{'id':panier.id})}}" > */
/*  {% set nbcmd= 0%}*/
/* {% for i in cm %}*/
/*     {% set nbcmd=nbcmd+1 %}*/
/*     {% endfor %}*/
/*                             <h1>Votre pnaier</h1>*/
/*                             <p class="text-muted">vous avez {{nbcmd}} article(s) dans votre panier.</p>*/
/*                             <div class="table-responsive">*/
/*                                 <table class="table">*/
/*                                     <thead>*/
/*                                         <tr>*/
/*                                             <th colspan="2">Produit</th>*/
/*                                             <th>Quantité</th>*/
/*                                             <th>Prix</th>*/
/*                                             <th>Promotion</th>*/
/*                                             <th colspan="2">Total</th>*/
/*                                         </tr>*/
/*                                     </thead>*/
/*                                     <tbody*/
/*                                         {% for j in produits %} */
/*                             {% for i in j %}  */
/*                                         <tr>*/
/*                                             <td>*/
/*                                                 <a href="#">*/
/*                                                     <img src="{{ asset(path('my_image_route', {'id': i.id})) }}" alt="White Blouse Armani">*/
/*                                                 </a>*/
/*                                             </td>*/
/*                                             <td><a href="#">{{i.nom}}</a>*/
/*                                             </td>*/
/*                                             <td>*/
/*                                                 {% for m in cm %}*/
/*                                        {% if m.produit.id == i.id %}*/
/*                                         */
/*                                                 <input name={{m.id}} type="number" value="{{m.qte}}" class="form-control">*/
/*                                               */
/*                                                 {% endif %}*/
/*                                               {% endfor %} */
/*                                             </td>*/
/*                                             <td>{{ i.prix}} DT</td>*/
/*                                             <td>{{ i.prix}} DT</td>*/
/*                                             <td>{{ i.prix}} DT</td>*/
/*                                             <td><a href="{{ path('panier_remove', { 'id': entity.panier.id ,*/
/*                                    'produit': i.id }  ) }}"><i class="fa fa-trash-o"></i></a>*/
/*                                             </td>*/
/*                                         </tr>*/
/*                                         */
/*                                         {% set prixSP = prixSP + i.prix  %}*/
/*                                     {% set prixP = prixP + (i.prix - (i.prix *i.promotion)/100) %}*/
/*                                               {% endfor %}*/
/*                                             {% endfor %}*/
/*                                      */
/*                                     </tbody>*/
/*                                     <tfoot>*/
/*                                         <tr>*/
/*                                             <th colspan="5">Total</th>*/
/*                                             <th colspan="2">{{prixSP}} DT</th>*/
/*                                         </tr>*/
/*                                     </tfoot>*/
/*                                 </table>*/
/* */
/*                             </div>*/
/*                             <!-- /.table-responsive -->*/
/* */
/*                             <div class="">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="category.html" class="btn btn-default"><i class="fa fa-chevron-left"></i> Continuer shopping</a>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <a class="btn btn-default"><i class="fa fa-refresh"></i> Actualiser</a>*/
/*                              */
/*                                     <a href="{{ path('choisir_adresse') }}" class="btn btn-primary">passer commande <i class="fa fa-chevron-right"></i>*/
/*                                     </a>*/
/*                                 </div>*/
/*                             </div>*/
/*                                     <hr>*/
/* */
/*                         </form>*/
/* */
/*                     </div>*/
/*                                     <hr><hr>*/
/*                     <!-- /.box -->*/
/* */
/* */
/*                     <div class="row same-height-row">*/
/*                         <div class="col-md-3 col-sm-6">*/
/*                             <div class="same-height">*/
/*                                 <h3>Vous pourriez aimer </h3>*/
/*                             </div>*/
/*                         </div>*/
/*                         {% for i in prods|slice(0, 3) %}*/
/*                         <div class="col-md-3 col-sm-6">*/
/*                             <div class="product same-height">*/
/*                                 <div class="flip-container">*/
/*                                     <div class="flipper">*/
/*                                         <div class="front">*/
/*                                             <a href="{{i.nom}}">*/
/*                                                 <img src="{{ asset(path('my_image_route', {'id': i.id})) }}"  width="182px" height="200px" alt="">*/
/*                                             </a>*/
/*                                         </div>*/
/*                                         <div class="back">*/
/*                                             <a href="{{i.nom}}">*/
/*                                                 <img src="{{ asset(path('my_image_route', {'id': i.id})) }}"  width="182px" height="200px" alt="" >*/
/*                                             </a>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <a href="{{i.nom}}" class="invisible">*/
/*                                     <img src="{{ asset(path('my_image_route', {'id': i.id})) }}" width="182px" height="200px" alt="" >*/
/*                                 </a>*/
/*                                 <div class="text">*/
/*                                     <h3>{{i.nom}}</h3>*/
/*                                     <p class="price">{{i.prix}}</p>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <!-- /.product -->*/
/*                         </div>*/
/*                         {% endfor %}*/
/* */
/*           */
/* */
/*                     </div>*/
/* */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-9 -->*/
/* */
/*                 <div class="col-md-3">*/
/*                     <div class="" id="order-summary">*/
/*                         <div class="box-header">*/
/*                             <h3>Totale </h3>*/
/*                         </div>*/
/*                         <p class="text-muted">la somme des prix des produits ajoutés dans votre panier .</p>*/
/* */
/*                         <div class="table-responsive">*/
/*                             <table class="table">*/
/*                                 <tbody>*/
/*                                     <tr>*/
/*                                         <td>Totale</td>*/
/*                                         <th>{{prixSP}} DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Frais supplimentaires</td>*/
/*                                         <th>10.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Tax</td>*/
/*                                         <th>5.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr class="total">*/
/*                                         <td>Total</td>*/
/*                                         <th>{{prixSP +10 }}DT</th>*/
/*                                     </tr>*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/* */
/*                     </div>*/
/* */
/*                                     <hr>*/
/*                                     <hr>*/
/*                  */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-3 -->*/
/* */
/*             </div>*/
/*             <!-- /.container -->*/
/*         </div>*/
/*                                             </div>*/
/*   <script src="{{asset('bundles/panier/js/jquery-1.11.0.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/jquery.cookie.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/waypoints.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/modernizr.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/bootstrap-hover-dropdown.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/owl.carousel.min.js')}}"></script>*/
/*     <script src="{{asset('bundles/panier/js/front.js')}}"></script>   */
/* </body>*/
/*     </html>*/
/*     {% endblock %}*/
/* */
