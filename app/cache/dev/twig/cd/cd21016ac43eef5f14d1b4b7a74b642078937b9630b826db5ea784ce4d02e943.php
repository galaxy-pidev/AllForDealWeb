<?php

/* PiDevClientBundle:LigneCommande:new.html.twig */
class __TwigTemplate_37dedd9198fa21cfe6696d1bd8084e03b5ddda5b80a4aa61b33742468249f61a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:LigneCommande:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "            <link href=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    ";
        // line 5
        if (((isset($context["existe"]) ? $context["existe"] : $this->getContext($context, "existe")) == false)) {
            echo "            
        <div class=\"alert-box error\">
            <strong>ERROR!</strong> 
        </div>                   
    ";
        }
        // line 10
        echo "<div class=\"actions-wrapper\">
          ";
        // line 11
        if (((isset($context["existe"]) ? $context["existe"] : $this->getContext($context, "existe")) == true)) {
            echo "            
                            <div class=\"alert-box error\">
                                <strong>ERROR!</strong> 
                            </div>                   
                        ";
        }
        // line 16
        echo "\t\t\t


<h5 class=\"widget-name\"><i class=\"icon-th\"></i>Ajouter un nouveau produit </h5>

<div class=\"media row-fluid\">
    
    <form id=\"validate\" class=\"form-horizontal\" method=\"post\" action=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("lignecommande_create", array("produit" => $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "id", array()))), "html", null, true);
        echo "\">
        <fieldset>
        <!-- Form validation -->
             <div class=\"widget\">
\t       
\t         <div class=\"well row-fluid\">
           
          <div class=\"control-group\">
            <label class=\"control-label\">Quantite<span class=\"text-error\">*</span></label>
            <div class=\"controls\">             
                       ";
        // line 33
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
                
            </div>
        </div>
                          
        <div class=\"form-actions align-right\">
        
            <button type=\"submit\" class=\"btn btn-info\">Valider</button>

            <button type=\"reset\" class=\"btn\">Reset</button>
            <ul class=\"record_actions\" style=\"display: inline-block\">
            <li>
                <a href=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("panier");
        echo "\">
                    Retour à l'accueil
                </a>
            </li>
        </ul>
        </div>
        
    </div>
  </div>
\t              
        </fieldset>
</form>

</div>
                
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:LigneCommande:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 45,  77 => 33,  64 => 23,  55 => 16,  47 => 11,  44 => 10,  36 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*             <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*     {% if existe==false%}            */
/*         <div class="alert-box error">*/
/*             <strong>ERROR!</strong> */
/*         </div>                   */
/*     {% endif %}*/
/* <div class="actions-wrapper">*/
/*           {% if existe==true %}            */
/*                             <div class="alert-box error">*/
/*                                 <strong>ERROR!</strong> */
/*                             </div>                   */
/*                         {% endif %}*/
/* 			*/
/* */
/* */
/* <h5 class="widget-name"><i class="icon-th"></i>Ajouter un nouveau produit </h5>*/
/* */
/* <div class="media row-fluid">*/
/*     */
/*     <form id="validate" class="form-horizontal" method="post" action="{{path('lignecommande_create',{'produit':produit.id})}}">*/
/*         <fieldset>*/
/*         <!-- Form validation -->*/
/*              <div class="widget">*/
/* 	       */
/* 	         <div class="well row-fluid">*/
/*            */
/*           <div class="control-group">*/
/*             <label class="control-label">Quantite<span class="text-error">*</span></label>*/
/*             <div class="controls">             */
/*                        {{form(form)}}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*                           */
/*         <div class="form-actions align-right">*/
/*         */
/*             <button type="submit" class="btn btn-info">Valider</button>*/
/* */
/*             <button type="reset" class="btn">Reset</button>*/
/*             <ul class="record_actions" style="display: inline-block">*/
/*             <li>*/
/*                 <a href="{{ path('panier') }}">*/
/*                     Retour à l'accueil*/
/*                 </a>*/
/*             </li>*/
/*         </ul>*/
/*         </div>*/
/*         */
/*     </div>*/
/*   </div>*/
/* 	              */
/*         </fieldset>*/
/* </form>*/
/* */
/* </div>*/
/*                 */
/* {% endblock %}*/
/* */
