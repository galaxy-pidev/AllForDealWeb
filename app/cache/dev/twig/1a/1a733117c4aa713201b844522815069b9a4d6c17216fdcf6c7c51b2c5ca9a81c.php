<?php

/* PiDevClientBundle:Commande:Payer.html.twig */
class __TwigTemplate_1e006e43b45350679c8d503cdee55d366b265d3bf32f982156aa25c16ce19d12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Commande:Payer.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        echo "   
    <html>
        <head>  
            <!-- styles -->
            <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

            <!-- theme stylesheet -->
            <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/style.blue.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-stylesheet\">

            <!-- your stylesheet with modifications -->
            <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/custom.css\" rel=\"stylesheet"), "html", null, true);
        echo "\">

            <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/respond.min.js"), "html", null, true);
        echo "\"></script>

            <link rel=\"shortcut icon\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/img/favicon.png"), "html", null, true);
        echo "\">



        </head>

        <body>";
        // line 27
        echo "            <br>
            <div class=\"panel panel-info\">


                <div id=\"content\">
                    <div class=\"container\">



                        <div class=\"col-md-9\" id=\"checkout\">

                            <div class=\"\">
                                <form method=\"post\" action=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("payer_commande", array("id" => $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "id", array()))), "html", null, true);
        echo "\">
                                    <h1>Payment de votre commande</h1>
                                    <ul class=\"nav nav-pills nav-justified\">
                                        <li><a href=\"checkout1.html\"><i class=\"fa fa fa-truck\"></i><br>Livraison</a>
                                        </li>
                                        <li class=\"active\"><a href=\"checkout2.html\"><i class=\"fa fa-money\"></i><br>Payment</a>
                                        </li>
                                        <li><a href=\"checkout3.html\"><i class=\"fa fa-eye\"></i><br>Confirmation</a>
                                        </li>
                                        <li ><a href=\"#\"><i class=\"fa fa-archive\"></i><br>Facture</a>
                                        </li>
                                    </ul>

                                    <div class=\"content\">
                                        <div class=\"row\">
                                            <div class=\"col-sm-6\">
                                                <div class=\" shipping-method\">

                                                    <h4>point bonus</h4>

                                                    <p>Points bonnus cummulé dans votre compte .</p>

                                                    <div class=\"box-footer text-center\">

                                                        <input type=\"radio\" name=\"type\" value=\"1\">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class=\"col-sm-6\">
                                                <div class=\"shipping-method\">

                                                    <h4>carte bancaire</h4>

                                                    <p>nous acceptons que les carte bancaire type paypall.</p>

                                                    <div class=\"box-footer text-center\">

                                                        <input type=\"radio\" name=\"type\" value=\"2\">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <hr>
                                            <div class=\"col-sm-6\">
                                                <div class=\"shipping-method\">

                                                    <h4>à la livraison</h4>

                                                    <p>vous payez lors de la reception de votre commande.</p>

                                                    <div class=\"box-footer text-center\">

                                                        <input type=\"radio\" name=\"type\" value=\"3\">
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <!-- /.row -->

                                    </div>
                                    <!-- /.content -->
                                    <br>
                                    <br>
                                    <br>
                                    <div class=\"box-footer\">
                                        <div class=\"pull-left\">
                                            <a href=\"basket.html\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Retour </a>
                                        </div>
                                        <div class=\"pull-right\">
                                            <button type=\"submit\" class=\"btn btn-primary\">Confirmer votre commande<i class=\"fa fa-chevron-right\"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box -->

                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <!-- /.col-md-9 -->

                        <div class=\"col-md-3\">

                             <div class=\"\" id=\"order-summary\">
                        <div class=\"box-header\">
                            <h3>Totale </h3>
                        </div>
                        <p class=\"text-muted\">la somme des prix des produits ajoutés dans votre panier .</p>

                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>Totale</td>
                                        <th>DT</th>
                                    </tr>
                                    <tr>
                                        <td>Frais supplimentaires</td>
                                        <th>10.00 DT</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>5.00 DT</th>
                                    </tr>
                                    <tr class=\"total\">
                                        <td>Total</td>
                                        <th>DT</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                        </div>
                        <!-- /.col-md-3 -->

                    </div>
                    <!-- /.container -->
                </div>
                <!-- /#content -->
            </div>
            <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/waypoints.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/modernizr.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap-hover-dropdown.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/front.js"), "html", null, true);
        echo "\"></script>   
        </body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commande:Payer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 174,  252 => 173,  248 => 172,  244 => 171,  240 => 170,  236 => 169,  232 => 168,  228 => 167,  97 => 39,  83 => 27,  74 => 20,  69 => 18,  64 => 16,  58 => 13,  52 => 10,  48 => 9,  44 => 8,  40 => 7,  36 => 6,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}   */
/*     <html>*/
/*         <head>  */
/*             <!-- styles -->*/
/*             <link href="{{asset('bundles/panier/css/font-awesome.css')}}" rel="stylesheet">*/
/*             <link href="{{asset('bundles/panier/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*             <link href="{{asset('bundles/panier/css/animate.min.css')}}" rel="stylesheet">*/
/*             <link href="{{asset('bundles/panier/css/owl.carousel.css')}}" rel="stylesheet">*/
/*             <link href="{{asset('bundles/panier/css/owl.theme.css')}}" rel="stylesheet">*/
/* */
/*             <!-- theme stylesheet -->*/
/*             <link href="{{asset('bundles/panier/css/style.blue.css')}}" rel="stylesheet" id="theme-stylesheet">*/
/* */
/*             <!-- your stylesheet with modifications -->*/
/*             <link href="{{asset('bundles/panier/css/custom.css" rel="stylesheet')}}">*/
/* */
/*             <script src="{{asset('bundles/panier/js/respond.min.js')}}"></script>*/
/* */
/*             <link rel="shortcut icon" href="{{asset('bundles/panier/img/favicon.png')}}">*/
/* */
/* */
/* */
/*         </head>*/
/* */
/*         <body>{# empty Twig template #}{# empty Twig template #}*/
/*             <br>*/
/*             <div class="panel panel-info">*/
/* */
/* */
/*                 <div id="content">*/
/*                     <div class="container">*/
/* */
/* */
/* */
/*                         <div class="col-md-9" id="checkout">*/
/* */
/*                             <div class="">*/
/*                                 <form method="post" action="{{path('payer_commande',{'id':commande.id})}}">*/
/*                                     <h1>Payment de votre commande</h1>*/
/*                                     <ul class="nav nav-pills nav-justified">*/
/*                                         <li><a href="checkout1.html"><i class="fa fa fa-truck"></i><br>Livraison</a>*/
/*                                         </li>*/
/*                                         <li class="active"><a href="checkout2.html"><i class="fa fa-money"></i><br>Payment</a>*/
/*                                         </li>*/
/*                                         <li><a href="checkout3.html"><i class="fa fa-eye"></i><br>Confirmation</a>*/
/*                                         </li>*/
/*                                         <li ><a href="#"><i class="fa fa-archive"></i><br>Facture</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/* */
/*                                     <div class="content">*/
/*                                         <div class="row">*/
/*                                             <div class="col-sm-6">*/
/*                                                 <div class=" shipping-method">*/
/* */
/*                                                     <h4>point bonus</h4>*/
/* */
/*                                                     <p>Points bonnus cummulé dans votre compte .</p>*/
/* */
/*                                                     <div class="box-footer text-center">*/
/* */
/*                                                         <input type="radio" name="type" value="1">*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/* */
/*                                             <div class="col-sm-6">*/
/*                                                 <div class="shipping-method">*/
/* */
/*                                                     <h4>carte bancaire</h4>*/
/* */
/*                                                     <p>nous acceptons que les carte bancaire type paypall.</p>*/
/* */
/*                                                     <div class="box-footer text-center">*/
/* */
/*                                                         <input type="radio" name="type" value="2">*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <br>*/
/*                                             <hr>*/
/*                                             <div class="col-sm-6">*/
/*                                                 <div class="shipping-method">*/
/* */
/*                                                     <h4>à la livraison</h4>*/
/* */
/*                                                     <p>vous payez lors de la reception de votre commande.</p>*/
/* */
/*                                                     <div class="box-footer text-center">*/
/* */
/*                                                         <input type="radio" name="type" value="3">*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/* */
/* */
/*                                         </div>*/
/*                                         <!-- /.row -->*/
/* */
/*                                     </div>*/
/*                                     <!-- /.content -->*/
/*                                     <br>*/
/*                                     <br>*/
/*                                     <br>*/
/*                                     <div class="box-footer">*/
/*                                         <div class="pull-left">*/
/*                                             <a href="basket.html" class="btn btn-default"><i class="fa fa-chevron-left"></i>Retour </a>*/
/*                                         </div>*/
/*                                         <div class="pull-right">*/
/*                                             <button type="submit" class="btn btn-primary">Confirmer votre commande<i class="fa fa-chevron-right"></i>*/
/*                                             </button>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </form>*/
/*                             </div>*/
/*                             <!-- /.box -->*/
/* */
/*                             <br>*/
/*                             <br>*/
/*                             <br>*/
/*                             <br>*/
/*                         </div>*/
/*                         <!-- /.col-md-9 -->*/
/* */
/*                         <div class="col-md-3">*/
/* */
/*                              <div class="" id="order-summary">*/
/*                         <div class="box-header">*/
/*                             <h3>Totale </h3>*/
/*                         </div>*/
/*                         <p class="text-muted">la somme des prix des produits ajoutés dans votre panier .</p>*/
/* */
/*                         <div class="table-responsive">*/
/*                             <table class="table">*/
/*                                 <tbody>*/
/*                                     <tr>*/
/*                                         <td>Totale</td>*/
/*                                         <th>DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Frais supplimentaires</td>*/
/*                                         <th>10.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Tax</td>*/
/*                                         <th>5.00 DT</th>*/
/*                                     </tr>*/
/*                                     <tr class="total">*/
/*                                         <td>Total</td>*/
/*                                         <th>DT</th>*/
/*                                     </tr>*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/* */
/*                     </div>*/
/* */
/*                         </div>*/
/*                         <!-- /.col-md-3 -->*/
/* */
/*                     </div>*/
/*                     <!-- /.container -->*/
/*                 </div>*/
/*                 <!-- /#content -->*/
/*             </div>*/
/*             <script src="{{asset('bundles/panier/js/jquery-1.11.0.min.js')}}"></script>*/
/*             <script src="{{asset('bundles/panier/js/bootstrap.min.js')}}"></script>*/
/*             <script src="{{asset('bundles/panier/js/jquery.cookie.js')}}"></script>*/
/*             <script src="{{asset('bundles/panier/js/waypoints.min.js')}}"></script>*/
/*             <script src="{{asset('bundles/panier/js/modernizr.js')}}"></script>*/
/*             <script src="{{asset('bundles/panier/js/bootstrap-hover-dropdown.js')}}"></script>*/
/*             <script src="{{asset('bundles/panier/js/owl.carousel.min.js')}}"></script>*/
/*             <script src="{{asset('bundles/panier/js/front.js')}}"></script>   */
/*         </body>*/
/*     </html>*/
/* {% endblock %}*/
