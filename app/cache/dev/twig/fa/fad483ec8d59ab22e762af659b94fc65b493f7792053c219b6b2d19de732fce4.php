<?php

/* PiDevClientBundle:Appeldoffre:ajout2.html.twig */
class __TwigTemplate_774425c014fe8ed933a5625e69172b3bbcfd4b7824a1107addf6d1a534f29a8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Appeldoffre:ajout2.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Ajouter un Appel D'offre</h1>
<form method='post'>
<table>
    
     <tr><td>nom:</td><td>";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "nom", array()), 'widget');
        echo "</td></tr>
       <tr><td>Description:</td><td>";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Description", array()), 'widget');
        echo "</td></tr>
        
             <tr><td>Mail:</td><td>";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Mail", array()), 'widget');
        echo "</td></tr>
                <tr><td>Telephone:</td><td>";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Telephone", array()), 'widget');
        echo "</td></tr>
                   <tr><td>Adresse:</td><td>";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "Adresse", array()), 'widget');
        echo "</td></tr>
</table>
      ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo "
</form>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Appeldoffre:ajout2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 15,  54 => 13,  50 => 12,  46 => 11,  41 => 9,  37 => 8,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* <h1>Ajouter un Appel D'offre</h1>*/
/* <form method='post'>*/
/* <table>*/
/*     */
/*      <tr><td>nom:</td><td>{{form_widget(f.nom)}}</td></tr>*/
/*        <tr><td>Description:</td><td>{{form_widget(f.Description)}}</td></tr>*/
/*         */
/*              <tr><td>Mail:</td><td>{{form_widget(f.Mail)}}</td></tr>*/
/*                 <tr><td>Telephone:</td><td>{{form_widget(f.Telephone)}}</td></tr>*/
/*                    <tr><td>Adresse:</td><td>{{form_widget(f.Adresse)}}</td></tr>*/
/* </table>*/
/*       {{form_rest(f)}}*/
/* </form>*/
/*  {% endblock %}*/
