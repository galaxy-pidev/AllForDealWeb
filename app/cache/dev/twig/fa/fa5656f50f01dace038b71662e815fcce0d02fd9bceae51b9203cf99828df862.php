<?php

/* PiDevClientBundle:Produit:RechercherSelonCritere.html.twig */
class __TwigTemplate_94c45749b11b9018e4f395db69f0838cd8d1783687c0c99c530c792c3a9a2844 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:RechercherSelonCritere.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h1> Recherche Liste des produits </h1>
<form method=\"POST\" action=\"\">
  rechercher: <input type=\"text\" name=\"search\" />
  <input type=\"submit\" value=\"chercher\" />



<div>
        
    
    
        
     
        </div>
         
   <table border=\"1\" style=\"width:100%\">
    <tr>     
               <td>id</td>
               <td> Nom  </td>
               <td> Description </td>
               <td> Prix  </td>
               <td> Quantité  </td>
               <td> Promotion  </td> 
               <td> Marque </td> 
               <td> NBvente  </td>
               <td> Valider </td>
               <td> dateAjout  </td>
               <td> Nbpointbase </td>  
               <td> Categorie </td>  
       </tr>
    ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modeles"]) ? $context["modeles"] : $this->getContext($context, "modeles")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            // line 34
            echo "       

       <tr>
            
            <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nom", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "description", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "prix", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "qantite", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "promotion", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "marque", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nbvente", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "valider", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["j"], "dateajout", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "</td>
            <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nbpointbase", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "categorie", array()), "html", null, true);
            echo "</td>
        </tr>
        
        
         
       
            
        
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "</table>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:RechercherSelonCritere.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 58,  117 => 49,  113 => 48,  109 => 47,  105 => 46,  101 => 45,  97 => 44,  93 => 43,  89 => 42,  85 => 41,  81 => 40,  77 => 39,  73 => 38,  67 => 34,  63 => 33,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* <h1> Recherche Liste des produits </h1>*/
/* <form method="POST" action="">*/
/*   rechercher: <input type="text" name="search" />*/
/*   <input type="submit" value="chercher" />*/
/* */
/* */
/* */
/* <div>*/
/*         */
/*     */
/*     */
/*         */
/*      */
/*         </div>*/
/*          */
/*    <table border="1" style="width:100%">*/
/*     <tr>     */
/*                <td>id</td>*/
/*                <td> Nom  </td>*/
/*                <td> Description </td>*/
/*                <td> Prix  </td>*/
/*                <td> Quantité  </td>*/
/*                <td> Promotion  </td> */
/*                <td> Marque </td> */
/*                <td> NBvente  </td>*/
/*                <td> Valider </td>*/
/*                <td> dateAjout  </td>*/
/*                <td> Nbpointbase </td>  */
/*                <td> Categorie </td>  */
/*        </tr>*/
/*     {% for j in modeles %}*/
/*        */
/* */
/*        <tr>*/
/*             */
/*             <td>{{j.id}}</td>*/
/*             <td>{{j.nom}}</td>*/
/*             <td>{{j.description}}</td>*/
/*             <td>{{j.prix}}</td>*/
/*             <td>{{j.qantite}}</td>*/
/*             <td>{{j.promotion}}</td>*/
/*             <td>{{j.marque}}</td>*/
/*             <td>{{j.nbvente}}</td>*/
/*             <td>{{j.valider}}</td>*/
/*             <td>{{j.dateajout.format('Y-m-d')}}</td>*/
/*             <td>{{j.nbpointbase}}</td>*/
/*             <td>{{j.categorie}}</td>*/
/*         </tr>*/
/*         */
/*         */
/*          */
/*        */
/*             */
/*         */
/*         {% endfor %}*/
/* </table>*/
/* {% endblock %}*/
