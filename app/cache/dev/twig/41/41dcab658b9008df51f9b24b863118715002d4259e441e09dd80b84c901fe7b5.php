<?php

/* PiDevClientBundle:Commande:Confirmer.html.twig */
class __TwigTemplate_59025817c3b78e7da9e6cd73723a94df886fcd8c83978aa9ef78ddd51cefff71 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<html>
    <head>  
        <!-- styles -->
        <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- theme stylesheet -->
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/style.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-stylesheet\">

        <!-- your stylesheet with modifications -->
        <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/custom.css\" rel=\"stylesheet"), "html", null, true);
        echo "\">

        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/respond.min.js"), "html", null, true);
        echo "\"></script>

        <link rel=\"shortcut icon\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/img/favicon.png"), "html", null, true);
        echo "\">



    </head>

    <body>";
        // line 26
        echo "


        <div id=\"content\">
            <div class=\"container\">

                <div class=\"col-md-12\">
                    <ul class=\"breadcrumb\">
                        <li><a href=\"#\">Home</a>
                        </li>
                        <li>Checkout - Payment method</li>
                    </ul>
                </div>

                <div class=\"col-md-9\" id=\"checkout\">

                    <div class=\"box\">
                        <form method=\"post\" action=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("confirmer_commande", array("id" => $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "id", array()))), "html", null, true);
        echo "\">
                            <h1>Checkout - Payment method</h1>
                            <ul class=\"nav nav-pills nav-justified\">
                                <li><a href=\"checkout1.html\"><i class=\"fa fa fa-truck\"></i><br>Livraison</a>
                                </li>
                                <li><a href=\"checkout2.html\"><i class=\"fa fa-money\"></i><br>Payment</a>
                                </li>
                                <li class=\"active\"><a href=\"checkout3.html\"><i class=\"fa fa-eye\"></i><br>Confirmation</a>
                                </li>
                                <li ><a href=\"#\"><i class=\"fa fa-archive\"></i><br>Facture</a></li>
                            </ul>
                            <div class=\"content\">
                                <div class=\"table-responsive\">
                                    <table class=\"table\">
                                        <thead>
                                            <tr>
                                                <th colspan=\"5\">Veuillez verifier les information que vous venez de les saisir !! .</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Id commande : </a>
                                                </td>
                                                <td>";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "id", array()), "html", null, true);
        echo "</td>

                                            </tr>      
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Nom : </a>
                                                </td>
                                                <td> Mr/Mme ";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</td>

                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Tel : </a>
                                                </td>
                                                <td>9586235 </td>

                                            </tr>

                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Email : </a>
                                                </td>
                                                <td>";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo " </td>

                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Adresse livraison : </a>
                                                </td>
                                                <td>";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "adresse", array()), "rue", array()), "html", null, true);
        echo " 
                                                    ";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "adresse", array()), "ville", array()), "html", null, true);
        echo "
                                                    ";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "adresse", array()), "codePostal", array()), "html", null, true);
        echo " </td>
                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Mode de livraison : </a>
                                                </td>
                                                       ";
        // line 105
        if (($this->getAttribute($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "livraison", array()), "modeLivraison", array()) == 0)) {
            // line 106
            echo "                                                    <td>Par poste</td>
                                                ";
        } elseif (($this->getAttribute(        // line 107
(isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "modepaiement", array()) == 1)) {
            // line 108
            echo "                                                    <td>Livraison à domicile</td>
                                        
                                                ";
        }
        // line 111
        echo "                                                <td>   </td>

                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Mode de paiement : </a>
                                                </td>
                                                ";
        // line 118
        if (($this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "modepaiement", array()) == 1)) {
            // line 119
            echo "                                                    <td>point bonus</td>
                                                ";
        } elseif (($this->getAttribute(        // line 120
(isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "modepaiement", array()) == 2)) {
            // line 121
            echo "                                                    <td>carte bancaire</td>
                                                ";
        } elseif (($this->getAttribute(        // line 122
(isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "modepaiement", array()) == 3)) {
            // line 123
            echo "                                                     <td>à la livraison</td>
                                                ";
        }
        // line 125
        echo "
                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Prix sans Tva : </a>
                                                </td>
                                                <td>";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "totalCommande", array()), "html", null, true);
        echo " </td>

                                            </tr>
                                            <tr>
                                                <td> </td>
                                                <td><a href=\"#\">Prix avec Tva : </a>
                                                </td>
                                                <td>";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "totalCommande", array()), "html", null, true);
        echo " </td>

                                            </tr>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan=\"5\"></th>

                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                                <!-- /.table-responsive -->
                            </div>


                            <div class=\"box-footer\">
                                <div class=\"pull-left\">
                                    <a href=\"basket.html\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Back to Shipping method</a>
                                </div>
                                <div class=\"pull-right\">
                                    <button type=\"submit\" class=\"btn btn-primary\">Continue to Order review<i class=\"fa fa-chevron-right\"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class=\"col-md-3\">

                    <div class=\"box\" id=\"order-summary\">
                        <div class=\"box-header\">
                            <h3>Order summary</h3>
                        </div>
                        <p class=\"text-muted\">Shipping and additional costs are calculated based on the values you have entered.</p>

                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>Order subtotal</td>
                                        <th>\$446.00</th>
                                    </tr>
                                    <tr>
                                        <td>Shipping and handling</td>
                                        <th>\$10.00</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>\$0.00</th>
                                    </tr>
                                    <tr class=\"total\">
                                        <td>Total</td>
                                        <th>\$456.00</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
        <


        <script src=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/waypoints.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/modernizr.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap-hover-dropdown.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/front.js"), "html", null, true);
        echo "\"></script>   
    </body>
</html>


";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commande:Confirmer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  337 => 222,  333 => 221,  329 => 220,  325 => 219,  321 => 218,  317 => 217,  313 => 216,  309 => 215,  229 => 138,  219 => 131,  211 => 125,  207 => 123,  205 => 122,  202 => 121,  200 => 120,  197 => 119,  195 => 118,  186 => 111,  181 => 108,  179 => 107,  176 => 106,  174 => 105,  165 => 99,  161 => 98,  157 => 97,  147 => 90,  129 => 75,  119 => 68,  91 => 43,  72 => 26,  63 => 19,  58 => 17,  53 => 15,  47 => 12,  41 => 9,  37 => 8,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* */
/* <html>*/
/*     <head>  */
/*         <!-- styles -->*/
/*         <link href="{{asset('bundles/panier/css/font-awesome.css')}}" rel="stylesheet">*/
/*         <link href="{{asset('bundles/panier/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*         <link href="{{asset('bundles/panier/css/animate.min.css')}}" rel="stylesheet">*/
/*         <link href="{{asset('bundles/panier/css/owl.carousel.css')}}" rel="stylesheet">*/
/*         <link href="{{asset('bundles/panier/css/owl.theme.css')}}" rel="stylesheet">*/
/* */
/*         <!-- theme stylesheet -->*/
/*         <link href="{{asset('bundles/panier/css/style.default.css')}}" rel="stylesheet" id="theme-stylesheet">*/
/* */
/*         <!-- your stylesheet with modifications -->*/
/*         <link href="{{asset('bundles/panier/css/custom.css" rel="stylesheet')}}">*/
/* */
/*         <script src="{{asset('bundles/panier/js/respond.min.js')}}"></script>*/
/* */
/*         <link rel="shortcut icon" href="{{asset('bundles/panier/img/favicon.png')}}">*/
/* */
/* */
/* */
/*     </head>*/
/* */
/*     <body>{# empty Twig template #}{# empty Twig template #}*/
/* */
/* */
/* */
/*         <div id="content">*/
/*             <div class="container">*/
/* */
/*                 <div class="col-md-12">*/
/*                     <ul class="breadcrumb">*/
/*                         <li><a href="#">Home</a>*/
/*                         </li>*/
/*                         <li>Checkout - Payment method</li>*/
/*                     </ul>*/
/*                 </div>*/
/* */
/*                 <div class="col-md-9" id="checkout">*/
/* */
/*                     <div class="box">*/
/*                         <form method="post" action="{{path ('confirmer_commande',{'id':commande.id})}}">*/
/*                             <h1>Checkout - Payment method</h1>*/
/*                             <ul class="nav nav-pills nav-justified">*/
/*                                 <li><a href="checkout1.html"><i class="fa fa fa-truck"></i><br>Livraison</a>*/
/*                                 </li>*/
/*                                 <li><a href="checkout2.html"><i class="fa fa-money"></i><br>Payment</a>*/
/*                                 </li>*/
/*                                 <li class="active"><a href="checkout3.html"><i class="fa fa-eye"></i><br>Confirmation</a>*/
/*                                 </li>*/
/*                                 <li ><a href="#"><i class="fa fa-archive"></i><br>Facture</a></li>*/
/*                             </ul>*/
/*                             <div class="content">*/
/*                                 <div class="table-responsive">*/
/*                                     <table class="table">*/
/*                                         <thead>*/
/*                                             <tr>*/
/*                                                 <th colspan="5">Veuillez verifier les information que vous venez de les saisir !! .</th>*/
/* */
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         <tbody>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Id commande : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{commande.id}}</td>*/
/* */
/*                                             </tr>      */
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Nom : </a>*/
/*                                                 </td>*/
/*                                                 <td> Mr/Mme {{user.username }}</td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Tel : </a>*/
/*                                                 </td>*/
/*                                                 <td>9586235 </td>*/
/* */
/*                                             </tr>*/
/* */
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Email : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{user.email}} </td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Adresse livraison : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{commande.livraison.adresse.rue}} */
/*                                                     {{commande.livraison.adresse.ville}}*/
/*                                                     {{commande.livraison.adresse.codePostal}} </td>*/
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Mode de livraison : </a>*/
/*                                                 </td>*/
/*                                                        {% if commande.livraison.modeLivraison == 0 %}*/
/*                                                     <td>Par poste</td>*/
/*                                                 {% elseif commande.modepaiement == 1 %}*/
/*                                                     <td>Livraison à domicile</td>*/
/*                                         */
/*                                                 {% endif %}*/
/*                                                 <td>   </td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Mode de paiement : </a>*/
/*                                                 </td>*/
/*                                                 {% if commande.modepaiement == 1 %}*/
/*                                                     <td>point bonus</td>*/
/*                                                 {% elseif commande.modepaiement == 2 %}*/
/*                                                     <td>carte bancaire</td>*/
/*                                                 {% elseif commande.modepaiement == 3 %}*/
/*                                                      <td>à la livraison</td>*/
/*                                                 {% endif %}*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Prix sans Tva : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{commande.totalCommande}} </td>*/
/* */
/*                                             </tr>*/
/*                                             <tr>*/
/*                                                 <td> </td>*/
/*                                                 <td><a href="#">Prix avec Tva : </a>*/
/*                                                 </td>*/
/*                                                 <td>{{commande.totalCommande}} </td>*/
/* */
/*                                             </tr>*/
/* */
/*                                         </tbody>*/
/*                                         <tfoot>*/
/*                                             <tr>*/
/*                                                 <th colspan="5"></th>*/
/* */
/*                                             </tr>*/
/*                                         </tfoot>*/
/*                                     </table>*/
/* */
/*                                 </div>*/
/*                                 <!-- /.table-responsive -->*/
/*                             </div>*/
/* */
/* */
/*                             <div class="box-footer">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="basket.html" class="btn btn-default"><i class="fa fa-chevron-left"></i>Back to Shipping method</a>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <button type="submit" class="btn btn-primary">Continue to Order review<i class="fa fa-chevron-right"></i>*/
/*                                     </button>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </form>*/
/*                     </div>*/
/*                     <!-- /.box -->*/
/* */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-9 -->*/
/* */
/*                 <div class="col-md-3">*/
/* */
/*                     <div class="box" id="order-summary">*/
/*                         <div class="box-header">*/
/*                             <h3>Order summary</h3>*/
/*                         </div>*/
/*                         <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>*/
/* */
/*                         <div class="table-responsive">*/
/*                             <table class="table">*/
/*                                 <tbody>*/
/*                                     <tr>*/
/*                                         <td>Order subtotal</td>*/
/*                                         <th>$446.00</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Shipping and handling</td>*/
/*                                         <th>$10.00</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Tax</td>*/
/*                                         <th>$0.00</th>*/
/*                                     </tr>*/
/*                                     <tr class="total">*/
/*                                         <td>Total</td>*/
/*                                         <th>$456.00</th>*/
/*                                     </tr>*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/* */
/*                     </div>*/
/* */
/*                 </div>*/
/*                 <!-- /.col-md-3 -->*/
/* */
/*             </div>*/
/*             <!-- /.container -->*/
/*         </div>*/
/*         <*/
/* */
/* */
/*         <script src="{{asset('bundles/panier/js/jquery-1.11.0.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/bootstrap.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/jquery.cookie.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/waypoints.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/modernizr.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/bootstrap-hover-dropdown.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/owl.carousel.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/front.js')}}"></script>   */
/*     </body>*/
/* </html>*/
/* */
/* */
/* */
