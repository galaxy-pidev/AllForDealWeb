<?php

/* PiDevClientBundle:Favoris:show.html.twig */
class __TwigTemplate_b71d0e2367ab2bd31ee6baffbc032d2a676e7400f2a3d9431852c5c682fc44ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Favoris:show.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    </br>
    </br>
    ";
        // line 5
        if (twig_test_empty((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")))) {
            // line 6
            echo "          </div>
         <div class=\"register_account\">
           <div class=\"wrap\">
  <h4 class=\"title\">votre liste d'envie est vide</h4>
  <p class=\"cart\">vous n'avez aucun produit dans votre liste.<br>Cliquer<a href=\"";
            // line 10
            echo $this->env->getExtension('routing')->getPath("produit");
            echo "\"> ici</a> 
      pour continuer le shopping</p>
    \t   </div>
    \t</div>
       
          
    ";
        } else {
            // line 17
            echo " <ul class=\"record_actions \">

    </ul>




<div class=\"panel panel-info\">
  <div class=\"panel-heading\">
      
    <h3 class=\"panel-title\">Vos Favoris</h3>
  </div>
    <!-- Table -->
  <table class=\"table table-striped panel panel-info\">
      <thead>
       <th class=\"info\"></th>
      <th class=\"info\">Nom</th>
      <th class=\"info\">Prix</th>
      <th class=\"info\">Marque</th>
      <th class=\"info\">Détail</th>
      <th class=\"info\">Retirer</th>
      </thead>
        
      
      <tbody> 
          ";
            // line 42
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo "            
          <tr>
              <td>
                  <img src=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
                echo "\" alt=\"\" width=\"150px\"/>
              </td>
              
              <td>";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
                echo "</td>
              <td>";
                // line 49
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
                echo " DT</td>
              <td>";
                // line 50
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "marque", array()), "html", null, true);
                echo "</td>
              <td>  <a href=";
                // line 51
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
                echo ">Détail</a></td>
              <td><a href=\"";
                // line 52
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_remove", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "favoris", array()), "id", array()), "produit" => $this->getAttribute(                // line 53
$context["i"], "id", array()))), "html", null, true);
                echo "\">retirer</a></td>
          </tr>
           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "          
      </tbody>
  </table>
</div>
           
    ";
        }
        // line 61
        echo "      
          
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Favoris:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 61,  120 => 56,  111 => 53,  110 => 52,  106 => 51,  102 => 50,  98 => 49,  94 => 48,  88 => 45,  80 => 42,  53 => 17,  43 => 10,  37 => 6,  35 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content  %}*/
/*     </br>*/
/*     </br>*/
/*     {% if produits is empty  %}*/
/*           </div>*/
/*          <div class="register_account">*/
/*            <div class="wrap">*/
/*   <h4 class="title">votre liste d'envie est vide</h4>*/
/*   <p class="cart">vous n'avez aucun produit dans votre liste.<br>Cliquer<a href="{{ path('produit') }}"> ici</a> */
/*       pour continuer le shopping</p>*/
/*     	   </div>*/
/*     	</div>*/
/*        */
/*           */
/*     {% else %}*/
/*  <ul class="record_actions ">*/
/* */
/*     </ul>*/
/* */
/* */
/* */
/* */
/* <div class="panel panel-info">*/
/*   <div class="panel-heading">*/
/*       */
/*     <h3 class="panel-title">Vos Favoris</h3>*/
/*   </div>*/
/*     <!-- Table -->*/
/*   <table class="table table-striped panel panel-info">*/
/*       <thead>*/
/*        <th class="info"></th>*/
/*       <th class="info">Nom</th>*/
/*       <th class="info">Prix</th>*/
/*       <th class="info">Marque</th>*/
/*       <th class="info">Détail</th>*/
/*       <th class="info">Retirer</th>*/
/*       </thead>*/
/*         */
/*       */
/*       <tbody> */
/*           {% for i in produits %}            */
/*           <tr>*/
/*               <td>*/
/*                   <img src="{{ asset(path('my_image_route', {'id': i.id})) }}" alt="" width="150px"/>*/
/*               </td>*/
/*               */
/*               <td>{{ i.nom}}</td>*/
/*               <td>{{ i.prix}} DT</td>*/
/*               <td>{{ i.marque}}</td>*/
/*               <td>  <a href={{ path('produit_detail', { 'id': i.id }) }}>Détail</a></td>*/
/*               <td><a href="{{ path('favoris_remove', { 'id': entity.favoris.id ,*/
/*                                    'produit': i.id }  ) }}">retirer</a></td>*/
/*           </tr>*/
/*            {% endfor %}*/
/*           */
/*       </tbody>*/
/*   </table>*/
/* </div>*/
/*            */
/*     {% endif %}      */
/*           */
/* {% endblock %}*/
