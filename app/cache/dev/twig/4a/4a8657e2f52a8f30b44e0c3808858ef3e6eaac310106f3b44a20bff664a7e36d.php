<?php

/* PiDevClientBundle:Adresse:template.html.twig */
class __TwigTemplate_ecad628ace17c1b57caa8e009811c597f1e3f9581fd736bbc2756c630cc92e53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'gmap_address_widget' => array($this, 'block_gmap_address_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('gmap_address_widget', $context, $blocks);
    }

    public function block_gmap_address_widget($context, array $blocks = array())
    {
        // line 2
        echo "<div id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_gmap_address_widget\"></div>
<div id=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_input\">";
        $this->displayBlock("form_widget", $context, $blocks);
        echo "</div>
<div id=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_map\" class=\"gmap_address_map\"></div>
 
<script type=\"text/javascript\">
\$(function() {
            var addresspickerMap = \$(\"#";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "address", array()), "get", array(0 => "id"), "method"), "html", null, true);
        echo "\").addresspicker({
                regionBias: \"fr\",
                elements: {
                    map: \"#";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_map\",
                    locality: '#";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "locality", array()), "get", array(0 => "id"), "method"), "html", null, true);
        echo "',
                    country:  '#";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "country", array()), "get", array(0 => "id"), "method"), "html", null, true);
        echo "',
                    lat:      \"#";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lat", array()), "get", array(0 => "id"), "method"), "html", null, true);
        echo "\",
                    lng:      \"#";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lng", array()), "get", array(0 => "id"), "method"), "html", null, true);
        echo "\"
                }
            });
            var gmarker = addresspickerMap.addresspicker(\"marker\");
            gmarker.setVisible(true);
            addresspickerMap.addresspicker(\"updatePosition\");
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Adresse:template.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 15,  62 => 14,  58 => 13,  54 => 12,  50 => 11,  44 => 8,  37 => 4,  31 => 3,  26 => 2,  20 => 1,);
    }
}
/* {% block gmap_address_widget %}*/
/* <div id="{{ id }}_gmap_address_widget"></div>*/
/* <div id="{{ id }}_input">{{ block('form_widget') }}</div>*/
/* <div id="{{ id }}_map" class="gmap_address_map"></div>*/
/*  */
/* <script type="text/javascript">*/
/* $(function() {*/
/*             var addresspickerMap = $("#{{ form.address.get("id") }}").addresspicker({*/
/*                 regionBias: "fr",*/
/*                 elements: {*/
/*                     map: "#{{ id }}_map",*/
/*                     locality: '#{{ form.locality.get("id") }}',*/
/*                     country:  '#{{ form.country.get("id") }}',*/
/*                     lat:      "#{{ form.lat.get("id") }}",*/
/*                     lng:      "#{{ form.lng.get("id") }}"*/
/*                 }*/
/*             });*/
/*             var gmarker = addresspickerMap.addresspicker("marker");*/
/*             gmarker.setVisible(true);*/
/*             addresspickerMap.addresspicker("updatePosition");*/
/*     });*/
/* </script>*/
/* {% endblock %}{# empty Twig template #}*/
/* */
