<?php

/* PiDevClientBundle:EvaluationProduit:test.html.twig */
class __TwigTemplate_9dad5b57ba7656f529ccaf52cd0a3794c490213967cdecf23cc10a13d27391e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:EvaluationProduit:test.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/rating.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/rating.css"), "html", null, true);
        echo "\" />
    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js\"></script>
    ";
        // line 6
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "8bb605b_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8bb605b_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/8bb605b_rating_1.js");
            // line 8
            echo "      <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/js/rating.js"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "8bb605b"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8bb605b") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/8bb605b.js");
            echo "      <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/js/rating.js"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        // line 10
        echo "
    <h1>Evaluation d'un produit </h1>
    
     <form method=\"post\">  
      <fieldset>  <legend>Produit ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "nom", array()), "html", null, true);
        echo " </legend>


    <table class=\"record_properties\">
        <tbody>
 
            </tr>
            <tr>
                <th>Marque</th>
                <td>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "marque", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "description", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Prix</th>
                <td>";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "prix", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Quantite</th>
                <td>";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "qantite", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Promotion</th>
                <td>";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "promotion", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nombre de point bonus</th>
                <td>";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produit"]) ? $context["produit"] : $this->getContext($context, "produit")), "nbPointBase", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>

<div class=\"rating\">


    ";
        // line 50
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
    ";
        // line 51
        echo $this->env->getExtension('star_rating_extension')->rating(5, 5, "fa-lg");
        echo "
 
<span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>


</div>
        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("evaluationproduit");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:EvaluationProduit:test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 59,  126 => 51,  122 => 50,  112 => 43,  105 => 39,  98 => 35,  91 => 31,  84 => 27,  77 => 23,  65 => 14,  59 => 10,  45 => 8,  41 => 6,  36 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/rating.css') }}" />*/
/* <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/rating.css') }}" />*/
/*     <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>*/
/*     {% javascripts*/
/*       '@StarRatingBundle/Resources/public/js/rating.js' %}*/
/*       <script src="{{ asset('bundles/starrating/js/rating.js') }}"></script>*/
/*     {% endjavascripts %}*/
/* */
/*     <h1>Evaluation d'un produit </h1>*/
/*     */
/*      <form method="post">  */
/*       <fieldset>  <legend>Produit {{ produit.nom }} </legend>*/
/* */
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*  */
/*             </tr>*/
/*             <tr>*/
/*                 <th>Marque</th>*/
/*                 <td>{{ produit.marque }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Description</th>*/
/*                 <td>{{ produit.description }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Prix</th>*/
/*                 <td>{{ produit.prix }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Quantite</th>*/
/*                 <td>{{ produit.qantite }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Promotion</th>*/
/*                 <td>{{ produit.promotion }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Nombre de point bonus</th>*/
/*                 <td>{{ produit.nbPointBase }}</td>*/
/*             </tr>*/
/*             <tr>*/
/* */
/* <div class="rating">*/
/* */
/* */
/*     {{form(form)}}*/
/*     {{ 5|rating(5, "fa-lg") }}*/
/*  */
/* <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>*/
/* */
/* */
/* </div>*/
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('evaluationproduit') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/* </ul>*/
/* {% endblock %}*/
