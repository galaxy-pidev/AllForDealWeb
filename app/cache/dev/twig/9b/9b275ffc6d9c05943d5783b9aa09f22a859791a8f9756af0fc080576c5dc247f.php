<?php

/* PiDevClientBundle:Panier:showPanierlist.html.twig */
class __TwigTemplate_e57806fe6d9ee0ca91ac8462c6966ab071c49d65531e074f57407139d1d84591 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

    <h1>Produit</h1>

    <table class=\"record_properties\">
        <tbody>
           
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "produit", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 9
            echo "                
            <tr>
                <th>Nom</th>
                <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</td>
            </tr>
            
            <tr>
                <th>Nom</th>
                <td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "marque", array()), "html", null, true);
            echo "</td>
            </tr>
                        

          <tr>
                <th>Description</th>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "description", array()), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <th>Prix</th>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "prix", array()), "html", null, true);
            echo "</td>
            </tr>

            <tr>
                <th>Promotion</th>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "promotion", array()), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <th>Nbrpoint</th>
                <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nbrpoint", array()), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <th>quantite</th>
                <td><select name=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
            echo "\">
                        <option value=\"1\">1</option>
                        <option value=\"2\">2</option>
                        <option value=\"3\">3</option>
                        <option value=\"4\">4</option>
                        <option value=\"5\">5</option>
                    </select>
                </td>
            </tr>
            ";
            // line 49
            $context["prixSP"] = ((isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")) + $this->getAttribute($context["i"], "prix", array()));
            // line 50
            echo "            ";
            $context["prixP"] = ((isset($context["prixP"]) ? $context["prixP"] : $this->getContext($context, "prixP")) + ($this->getAttribute($context["i"], "prix", array()) - (($this->getAttribute($context["i"], "prix", array()) * $this->getAttribute($context["i"], "promotion", array())) / 100)));
            // line 51
            echo "                  
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo " 
      
              <td>   
                  prix sans promotion : ";
        // line 56
        echo twig_escape_filter($this->env, (isset($context["prixSP"]) ? $context["prixSP"] : $this->getContext($context, "prixSP")), "html", null, true);
        echo "</br>
                  prix apres promotion : ";
        // line 57
        echo twig_escape_filter($this->env, (isset($context["prixP"]) ? $context["prixP"] : $this->getContext($context, "prixP")), "html", null, true);
        echo "</br>
           
                   <a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("choisir_adresse");
        echo "\"> Passer Commande </a>
                 
                </td>           
        </tbody>
    </table>


";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Panier:showPanierlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 59,  116 => 57,  112 => 56,  107 => 53,  100 => 51,  97 => 50,  95 => 49,  83 => 40,  76 => 36,  69 => 32,  61 => 27,  54 => 23,  45 => 17,  37 => 12,  32 => 9,  28 => 8,  19 => 1,);
    }
}
/* */
/* */
/*     <h1>Produit</h1>*/
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*            */
/*             {% for i in entity.produit %}*/
/*                 */
/*             <tr>*/
/*                 <th>Nom</th>*/
/*                 <td>{{ i.nom}}</td>*/
/*             </tr>*/
/*             */
/*             <tr>*/
/*                 <th>Nom</th>*/
/*                 <td>{{ i.marque}}</td>*/
/*             </tr>*/
/*                         */
/* */
/*           <tr>*/
/*                 <th>Description</th>*/
/*                 <td>{{ i.description }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Prix</th>*/
/*                 <td>{{ i.prix }}</td>*/
/*             </tr>*/
/* */
/*             <tr>*/
/*                 <th>Promotion</th>*/
/*                 <td>{{ i.promotion }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Nbrpoint</th>*/
/*                 <td>{{ i.nbrpoint }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>quantite</th>*/
/*                 <td><select name="{{ i.id}}">*/
/*                         <option value="1">1</option>*/
/*                         <option value="2">2</option>*/
/*                         <option value="3">3</option>*/
/*                         <option value="4">4</option>*/
/*                         <option value="5">5</option>*/
/*                     </select>*/
/*                 </td>*/
/*             </tr>*/
/*             {% set prixSP = prixSP + i.prix  %}*/
/*             {% set prixP = prixP + (i.prix - (i.prix *i.promotion)/100) %}*/
/*                   */
/*       {% endfor %}*/
/*  */
/*       */
/*               <td>   */
/*                   prix sans promotion : {{prixSP}}</br>*/
/*                   prix apres promotion : {{prixP}}</br>*/
/*            */
/*                    <a href="{{path('choisir_adresse')}}"> Passer Commande </a>*/
/*                  */
/*                 </td>           */
/*         </tbody>*/
/*     </table>*/
/* */
/* */
/* */
