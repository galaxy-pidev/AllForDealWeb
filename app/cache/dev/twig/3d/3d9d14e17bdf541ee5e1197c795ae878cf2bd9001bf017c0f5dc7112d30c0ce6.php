<?php

/* PiDevClientBundle:Client:home.html.twig */
class __TwigTemplate_5d274ee94ef785106006e780470df7183fd9000863a6f8f8ab8023cdb5dd4bd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
                    <!-- Action tabs -->
\t\t\t    <div class=\"actions-wrapper\">
\t\t\t\t    <div class=\"actions\">

\t\t\t\t    \t<div>
\t\t\t\t\t        
\t\t\t\t\t            <li><a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_create", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">Ajouter un produit</a></div></li>
\t\t\t\t\t            <li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">Liste des articles</a></div></li>
\t\t\t\t\t       
\t\t\t\t\t       
\t\t\t\t    \t</div>
\t\t\t\t    \t";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 9,  28 => 8,  19 => 1,);
    }
}
/* */
/*                     <!-- Action tabs -->*/
/* 			    <div class="actions-wrapper">*/
/* 				    <div class="actions">*/
/* */
/* 				    	<div>*/
/* 					        */
/* 					            <li><a href="{{path('produit_create',{'id':entity.id})}}">Ajouter un produit</a></div></li>*/
/* 					            <li><a href="{{path('produit_show',{'id':entity.id})}}">Liste des articles</a></div></li>*/
/* 					       */
/* 					       */
/* 				    	</div>*/
/* 				    	*/
