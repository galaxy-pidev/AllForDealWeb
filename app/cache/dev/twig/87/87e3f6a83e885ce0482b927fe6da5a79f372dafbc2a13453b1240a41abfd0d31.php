<?php

/* PiDevClientBundle:Client:listForums.html.twig */
class __TwigTemplate_b6dd757accb54c0e6b0b0c0628413ebc79a0cc15f2c247fe73f3f6de199ea80a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Client:listForums.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <head>

    </head>
    <body>
        <!-- BEGAIN PRELOADER -->   
        <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Produits</strong></h3>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading bg-red\">
                            <h3 class=\"panel-title\"><strong>Liste des </strong> Produits</h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 m-b-20\">

                                    <div class=\"btn-group pull-right\">
                                        <button class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">Tools <i class=\"fa fa-angle-down\"></i>
                                        </button>
                                        <ul class=\"dropdown-menu pull-right\">
                                          <li><a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("sujet_create");
        echo "\">Ajouter</a>
                                            </li>
                                            <li><a href=\"#\">Save as PDF</a>
                                            </li>
                                            <li><a href=\"#\">Export to Excel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive table-red\">
                                    <table class=\"table table-striped table-hover dataTable\" id=\"table-editable\">

                                        <tbody>
                                        <thead>
                                            <tr role=\"row\" >
                                                <th class=\"text-center\">Titre</th>
                                                <th class=\"text-center\">Sujet</th>
                                                <th class=\"text-center\">Image</th>                                            
                                                <th class=\"text-center\">Action</th>
                                            </tr>
                                        </thead>
                                        ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["forums"]) ? $context["forums"] : $this->getContext($context, "forums")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 48
            echo "                                            <tr>
                                                <td class=\"text-center\">";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "titre", array()), "html", null, true);
            echo "</td>
                                                <td class=\"text-center\">";
            // line 50
            echo twig_slice($this->env, $this->getAttribute($context["i"], "sujet", array()), 0, 10);
            echo "</td> 
                                                <td class=\"text-center\"></td>                                             
                                                <td class=\"text-center\">
                                                    <a class=\"edit btn btn-dark\" href=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("sujet_edit", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-pencil-square-o\"></i>Modifier</a> 
                                                    <a class=\"delete btn btn-danger\" href=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("sujet_delete", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-times-circle\"></i> Supprimer</a>
                                                </td>

                                            </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                                                                                                                               
                                        </tbody>
                                    </table>

                                    <!-- nom  -->                                  

                                    <div id=\"modal\" class=\"popupContainer\" style=\"display:none;\">
                                        <header class=\"popupHeader\">
                                            <span class=\"header_title\">Modifer</span>
                                            <span class=\"modal_close\"><i class=\"fa fa-times\"></i></span>
                                        </header>

                                        <section class=\"popupBodny\">
                                            <!-- Username & Password Login form -->
                                            <div class=\"user_login\">
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>




    </body>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Client:listForums.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 58,  101 => 54,  97 => 53,  91 => 50,  87 => 49,  84 => 48,  80 => 47,  56 => 26,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content  %}*/
/*     <head>*/
/* */
/*     </head>*/
/*     <body>*/
/*         <!-- BEGAIN PRELOADER -->   */
/*         <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Produits</strong></h3>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading bg-red">*/
/*                             <h3 class="panel-title"><strong>Liste des </strong> Produits</h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 m-b-20">*/
/* */
/*                                     <div class="btn-group pull-right">*/
/*                                         <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>*/
/*                                         </button>*/
/*                                         <ul class="dropdown-menu pull-right">*/
/*                                           <li><a href="{{path('sujet_create')}}">Ajouter</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Save as PDF</a>*/
/*                                             </li>*/
/*                                             <li><a href="#">Export to Excel</a>*/
/*                                             </li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">*/
/*                                     <table class="table table-striped table-hover dataTable" id="table-editable">*/
/* */
/*                                         <tbody>*/
/*                                         <thead>*/
/*                                             <tr role="row" >*/
/*                                                 <th class="text-center">Titre</th>*/
/*                                                 <th class="text-center">Sujet</th>*/
/*                                                 <th class="text-center">Image</th>                                            */
/*                                                 <th class="text-center">Action</th>*/
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         {% for i in forums %}*/
/*                                             <tr>*/
/*                                                 <td class="text-center">{{i.titre}}</td>*/
/*                                                 <td class="text-center">{{i.sujet|slice(0,10)|raw}}</td> */
/*                                                 <td class="text-center"></td>                                             */
/*                                                 <td class="text-center">*/
/*                                                     <a class="edit btn btn-dark" href="{{path('sujet_edit',{'id':i.id})}}"><i class="fa fa-pencil-square-o"></i>Modifier</a> */
/*                                                     <a class="delete btn btn-danger" href="{{path('sujet_delete',{'id':i.id})}}"><i class="fa fa-times-circle"></i> Supprimer</a>*/
/*                                                 </td>*/
/* */
/*                                             </tr>*/
/*                                         {% endfor %}                                                                                                                               */
/*                                         </tbody>*/
/*                                     </table>*/
/* */
/*                                     <!-- nom  -->                                  */
/* */
/*                                     <div id="modal" class="popupContainer" style="display:none;">*/
/*                                         <header class="popupHeader">*/
/*                                             <span class="header_title">Modifer</span>*/
/*                                             <span class="modal_close"><i class="fa fa-times"></i></span>*/
/*                                         </header>*/
/* */
/*                                         <section class="popupBodny">*/
/*                                             <!-- Username & Password Login form -->*/
/*                                             <div class="user_login">*/
/*                                             </div>*/
/*                                         </section>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- END MAIN CONTENT -->*/
/*         </div>*/
/* */
/* */
/* */
/* */
/*     </body>*/
/* {% endblock %}*/
/* */
