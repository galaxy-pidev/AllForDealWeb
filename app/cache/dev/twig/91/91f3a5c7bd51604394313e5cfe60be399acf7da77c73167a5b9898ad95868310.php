<?php

/* PiDevAdminBundle:Publicite:showAll.html.twig */
class __TwigTemplate_8f4ecb39934e6d1166c448b647a789ea6f188d0f68678af9c1ee240ab2b711df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:Publicite:showAll.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        echo " 
 
  <html>
    <head>  
        <link href=";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/icons/icons.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/bootstrap.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
        <link href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/css/plugins.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
       <link href=";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/backOffice/zoom.css"), "html", null, true);
        echo " rel=\"stylesheet\">


        <!-- END  MANDATORY STYLE -->
        <script src=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
        echo "></script>
    </head>
<style>
        img {
            cursor: pointer;
        transition: -webkit-transform 0.1s ease
        }
    img:focus {
        -webkit-transform: scale(10);
        -ms-transform: scale(10);
        -background-position: center center;
        -moz-box-align: center;
    }
</style>
<script>
    document.addEventListener('DOMContentLoaded', function(){
        var imgs = document.querySelectorAll('img');
        Array.prototype.forEach.call(imgs, function(el, i) {
            if (el.tabIndex <= 0) el.tabIndex = 10000;
        });
    });
</script>
    <body>
      <div id=\"main-content\">
            <div class=\"page-title\"> <i class=\"icon-custom-left\"></i>
                <h3><strong>Liste</strong> des publicités</h3>
            </div>
          <a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("publicite_create");
        echo "\"> Ajouter une publicité </a>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\"><strong>toutes les informations  </strong>des publicités</h3>
                        </div>
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-12 col-sm-12 col-xs-12 table-responsive\">
                                    <table class=\"table table-striped table-hover\">
                                        <thead class=\"no-bd\">
                                            <tr>
                                                <th style=\"width:30px;\" class=\"div_checkbox\">
                                                    <div class=\"div_checkbox\">
                                                        <input class=\"toggle_checkbox\" type=\"checkbox\">
                                                    </div>
                                                </th>
                                                <th><strong>Image</strong>
                                                </th>
                                                <th><strong>type</strong>
                                                </th>
                                                <th><strong>Sujet</strong>
                                                </th>
                                                <th><strong>Lieu</strong>
                                                </th>
                                                <th><strong>Date</strong>
                                                </th>
                                           
                                            
                                                <th class=\"text-center\"><strong>Actions</strong>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class=\"no-bd-y\">
                                          ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pubs"]) ? $context["pubs"] : $this->getContext($context, "pubs")));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            echo " 
                                            
                                            <tr>
                                                <td>
                                                    <div class=\"div_checkbox\">
                                                        <input type=\"checkbox\">
                                                    </div>
                                                </td>
                                                <td>
                                                  
                                                  <img src=\"";
            // line 85
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("publicite_image", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
            echo "\" width=\"50px\" height=\"50px\">
 
                                                </td>
                                                <td>";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "type", array()), "html", null, true);
            echo "</td>
                 
                                                <td>";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "sujet", array()), "html", null, true);
            echo "</td>
                                                <td class=\"color-success\"> ";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "lieu", array()), "html", null, true);
            echo "</td>
                                                <td > ";
            // line 92
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["j"], "Date", array()), "y:m:d"), "html", null, true);
            echo " </td>
                       
                                                <td >
                                                 <div class=\"onoffswitch\">
            <a href=\"";
            // line 96
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("publicite_delete", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>
            <a href=\"";
            // line 97
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("publicite_edit", array("id" => $this->getAttribute($context["j"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-check-square\" aria-hidden=\"true\"></i></a>
                                                </td>
                                            </tr>
                                       
         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    </div>
         
   
        <script src=";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
        <!-- END MANDATORY SCRIPTS -->
        <script src=";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>  
  </body>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:Publicite:showAll.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  250 => 129,  245 => 127,  241 => 126,  237 => 125,  233 => 124,  229 => 123,  225 => 122,  221 => 121,  217 => 120,  213 => 119,  209 => 118,  205 => 117,  201 => 116,  197 => 115,  193 => 114,  179 => 102,  168 => 97,  164 => 96,  157 => 92,  153 => 91,  149 => 90,  144 => 88,  138 => 85,  123 => 75,  85 => 40,  55 => 13,  48 => 9,  44 => 8,  40 => 7,  36 => 6,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::baseAdmin.html.twig' %}*/
/* {% block content%} */
/*  */
/*   <html>*/
/*     <head>  */
/*         <link href={{asset('bundles/backOffice/css/icons/icons.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/bootstrap.min.css')}} rel="stylesheet">*/
/*         <link href={{asset('bundles/backOffice/css/plugins.min.css')}} rel="stylesheet">*/
/*        <link href={{asset('bundles/backOffice/zoom.css')}} rel="stylesheet">*/
/* */
/* */
/*         <!-- END  MANDATORY STYLE -->*/
/*         <script src={{asset('plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js')}}></script>*/
/*     </head>*/
/* <style>*/
/*         img {*/
/*             cursor: pointer;*/
/*         transition: -webkit-transform 0.1s ease*/
/*         }*/
/*     img:focus {*/
/*         -webkit-transform: scale(10);*/
/*         -ms-transform: scale(10);*/
/*         -background-position: center center;*/
/*         -moz-box-align: center;*/
/*     }*/
/* </style>*/
/* <script>*/
/*     document.addEventListener('DOMContentLoaded', function(){*/
/*         var imgs = document.querySelectorAll('img');*/
/*         Array.prototype.forEach.call(imgs, function(el, i) {*/
/*             if (el.tabIndex <= 0) el.tabIndex = 10000;*/
/*         });*/
/*     });*/
/* </script>*/
/*     <body>*/
/*       <div id="main-content">*/
/*             <div class="page-title"> <i class="icon-custom-left"></i>*/
/*                 <h3><strong>Liste</strong> des publicités</h3>*/
/*             </div>*/
/*           <a href="{{path('publicite_create')}}"> Ajouter une publicité </a>*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading">*/
/*                             <h3 class="panel-title"><strong>toutes les informations  </strong>des publicités</h3>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <div class="row">*/
/*                                 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">*/
/*                                     <table class="table table-striped table-hover">*/
/*                                         <thead class="no-bd">*/
/*                                             <tr>*/
/*                                                 <th style="width:30px;" class="div_checkbox">*/
/*                                                     <div class="div_checkbox">*/
/*                                                         <input class="toggle_checkbox" type="checkbox">*/
/*                                                     </div>*/
/*                                                 </th>*/
/*                                                 <th><strong>Image</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>type</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Sujet</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Lieu</strong>*/
/*                                                 </th>*/
/*                                                 <th><strong>Date</strong>*/
/*                                                 </th>*/
/*                                            */
/*                                             */
/*                                                 <th class="text-center"><strong>Actions</strong>*/
/*                                                 </th>*/
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         <tbody class="no-bd-y">*/
/*                                           {% for j in pubs %} */
/*                                             */
/*                                             <tr>*/
/*                                                 <td>*/
/*                                                     <div class="div_checkbox">*/
/*                                                         <input type="checkbox">*/
/*                                                     </div>*/
/*                                                 </td>*/
/*                                                 <td>*/
/*                                                   */
/*                                                   <img src="{{path('publicite_image',{'id':j.id})}}" width="50px" height="50px">*/
/*  */
/*                                                 </td>*/
/*                                                 <td>{{j.type}}</td>*/
/*                  */
/*                                                 <td>{{j.sujet}}</td>*/
/*                                                 <td class="color-success"> {{j.lieu}}</td>*/
/*                                                 <td > {{j.Date|date('y:m:d')}} </td>*/
/*                        */
/*                                                 <td >*/
/*                                                  <div class="onoffswitch">*/
/*             <a href="{{path('publicite_delete',{'id':j.id})}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>*/
/*             <a href="{{path('publicite_edit',{'id':j.id})}}"><i class="fa fa-check-square" aria-hidden="true"></i></a>*/
/*                                                 </td>*/
/*                                             </tr>*/
/*                                        */
/*          {% endfor %}*/
/*                                             */
/*                                         </tbody>*/
/*                                     </table>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*                     </div>*/
/*          */
/*    */
/*         <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*         <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*         <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*         <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*         <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*         <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*         <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*         <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*         <script src={{asset('js/mailbox.js')}}></script>*/
/*         <!-- END MANDATORY SCRIPTS -->*/
/*         <script src={{asset('js/application.js')}}></script>  */
/*   </body>*/
/*  {% endblock %}*/
