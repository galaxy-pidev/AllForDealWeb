<?php

/* PiDevClientBundle:Mail:Inbox.html.twig */
class __TwigTemplate_7c87dd00b2f45f3b7c44caac76708e201263c31d6290db9ae0152a14cbf3dd7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::compte.html.twig", "PiDevClientBundle:Mail:Inbox.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::compte.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "

    <div id=\"main-content\" class=\"page-mailbox container-fluid\">
        <div class=\"row\" data-equal-height=\"true\">
           
                  <div class=\"col-md-12\" list-messages>
                <div class=\"panel panel-default\">
                    <div class=\"panel-body messages\">
                        <div class=\"input-group input-group-lg border-bottom\">
                            <span class=\"input-group-btn\">
                                <a href=\"#\" class=\"btn\"><i class=\"fa fa-search\"></i></a>
                            </span>
                            <input type=\"text\" class=\"form-control bd-0 bd-white\" placeholder=\"Search\" name=\"search\">
                        </div>
                        <div id=\"messages-list\" class=\"panel panel-default withScroll\" data-height=\"window\" data-padding=\"90\">
                            ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mail"]) ? $context["mail"] : $this->getContext($context, "mail")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo " 
                              
                    <a   class=\"message-item \" href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("mail_show", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"  >
                              <div class=\"pull-left text-center\">
                                        <div class=\"pos-rel message-checkbox\">
                                            <input type=\"checkbox\" data-style=\"flat-red\">
                                        </div>
                                        <div>
                                            <strong><i class=\"fa fa-paperclip\"></i> 2</strong>
                                        </div>
                                    </div>


                                    <div class=\"message-item-right\">

                                        <div class=\"media\">
                                            <img src=";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("image")), "html", null, true);
            echo " alt=\"avatar 3\" width=\"50\" class=\"pull-left\">
                                            <div class=\"media-body\">
                                                <small class=\"pull-right\">";
            // line 36
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["i"], "dateAjout", array()), "j M Y "), "html", null, true);
            echo "</small>
                                                <h5 class=\"c-dark\"><strong>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "expediteur", array()), "html", null, true);
            echo "</strong></h5>
                                                <h4 class=\"c-dark\">";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</h4>
                                            </div>
                                        </div>
                                        <p class=\"f-14 c-gray\">";
            // line 41
            echo $this->getAttribute($context["i"], "Text", array());
            echo "</p>

                                    </div>       
                             </a>
                                   
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "  

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
        <script src=";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-1.11.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jquery-ui/jquery-ui-1.10.4.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-select/bootstrap-select.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/icheck/icheck.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/mmenu/js/jquery.mmenu.min.all.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/nprogress/nprogress.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/charts-sparkline/sparkline.min.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/breakpoints/breakpoints.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/numerator/jquery-numerator.js"), "html", null, true);
        echo "></script>
        <script src=";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/mailbox.js"), "html", null, true);
        echo "></script>
        <!-- END MANDATORY SCRIPTS -->
        <script src=";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/application.js"), "html", null, true);
        echo "></script>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Mail:Inbox.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 70,  166 => 68,  162 => 67,  158 => 66,  154 => 65,  150 => 64,  146 => 63,  142 => 62,  138 => 61,  134 => 60,  130 => 59,  126 => 58,  122 => 57,  118 => 56,  114 => 55,  103 => 46,  91 => 41,  85 => 38,  81 => 37,  77 => 36,  72 => 34,  55 => 20,  48 => 18,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::compte.html.twig' %}                  */
/* {% block content   %}*/
/* */
/* */
/*     <div id="main-content" class="page-mailbox container-fluid">*/
/*         <div class="row" data-equal-height="true">*/
/*            */
/*                   <div class="col-md-12" list-messages>*/
/*                 <div class="panel panel-default">*/
/*                     <div class="panel-body messages">*/
/*                         <div class="input-group input-group-lg border-bottom">*/
/*                             <span class="input-group-btn">*/
/*                                 <a href="#" class="btn"><i class="fa fa-search"></i></a>*/
/*                             </span>*/
/*                             <input type="text" class="form-control bd-0 bd-white" placeholder="Search" name="search">*/
/*                         </div>*/
/*                         <div id="messages-list" class="panel panel-default withScroll" data-height="window" data-padding="90">*/
/*                             {% for i in mail %} */
/*                               */
/*                     <a   class="message-item " href="{{path('mail_show',{'id':i.id})}}"  >*/
/*                               <div class="pull-left text-center">*/
/*                                         <div class="pos-rel message-checkbox">*/
/*                                             <input type="checkbox" data-style="flat-red">*/
/*                                         </div>*/
/*                                         <div>*/
/*                                             <strong><i class="fa fa-paperclip"></i> 2</strong>*/
/*                                         </div>*/
/*                                     </div>*/
/* */
/* */
/*                                     <div class="message-item-right">*/
/* */
/*                                         <div class="media">*/
/*                                             <img src={{asset(path('image'))}} alt="avatar 3" width="50" class="pull-left">*/
/*                                             <div class="media-body">*/
/*                                                 <small class="pull-right">{{i.dateAjout|date('j M Y ')}}</small>*/
/*                                                 <h5 class="c-dark"><strong>{{i.expediteur}}</strong></h5>*/
/*                                                 <h4 class="c-dark">{{i.nom}}</h4>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <p class="f-14 c-gray">{{i.Text|raw}}</p>*/
/* */
/*                                     </div>       */
/*                              </a>*/
/*                                    */
/*                             {% endfor %}  */
/* */
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         </div>*/
/*     </div>*/
/*         <script src={{asset('plugins/jquery-1.11.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-migrate-1.2.1.js')}}></script>*/
/*         <script src={{asset('plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap/bootstrap.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}></script>*/
/*         <script src={{asset('plugins/bootstrap-select/bootstrap-select.js')}}></script>*/
/*         <script src={{asset('plugins/icheck/icheck.js')}}></script>*/
/*         <script src={{asset('plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}></script>*/
/*         <script src={{asset('plugins/mmenu/js/jquery.mmenu.min.all.js')}}></script>*/
/*         <script src={{asset('plugins/nprogress/nprogress.js')}}></script>*/
/*         <script src={{asset('plugins/charts-sparkline/sparkline.min.js')}}></script>*/
/*         <script src={{asset('plugins/breakpoints/breakpoints.js')}}></script>*/
/*         <script src={{asset('plugins/numerator/jquery-numerator.js')}}></script>*/
/*         <script src={{asset('js/mailbox.js')}}></script>*/
/*         <!-- END MANDATORY SCRIPTS -->*/
/*         <script src={{asset('js/application.js')}}></script>*/
/* {% endblock %}  */
