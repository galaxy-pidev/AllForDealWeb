<?php

/* PiDevAdminBundle:User:Affichage.html.twig */
class __TwigTemplate_fe3dd626a3dafe4550e74131dca415ef0b1aa6ef516b19ce08cd2c785384c43c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::baseAdmin.html.twig", "PiDevAdminBundle:User:Affichage.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::baseAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    
    
        <h2> Liste des utilisateurs </h2>
        
        <table border='2'>
            <tr> 
                <td> Nom </td> 
                <td> E-mail </td>  
                
                <td> Supprimer </td> 
                <td> Administration </td>
            </tr>
            
            ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mods"]) ? $context["mods"] : $this->getContext($context, "mods")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 17
            echo "
                <tr> 
                    <td>  ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "username", array()), "html", null, true);
            echo "  </td> 
                    <td>  ";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "email", array()), "html", null, true);
            echo "  </td> 
                 
                <td>  <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("ecommerceadmin_supprimer", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"> 
                        <input type=\"submit\" value=\"Supprimer\" > </a> </td>
                        
                  <td>  <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("ecommerceadmin_Role", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\"> 
                        <input type=\"submit\" value=\"Ajouter comme Admin\" > </a> </td>        
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "            
        </table>
    
 
    ";
    }

    public function getTemplateName()
    {
        return "PiDevAdminBundle:User:Affichage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 29,  69 => 25,  63 => 22,  58 => 20,  54 => 19,  50 => 17,  46 => 16,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {%extends "::baseAdmin.html.twig"%}*/
/* {% block content %}*/
/*     */
/*     */
/*         <h2> Liste des utilisateurs </h2>*/
/*         */
/*         <table border='2'>*/
/*             <tr> */
/*                 <td> Nom </td> */
/*                 <td> E-mail </td>  */
/*                 */
/*                 <td> Supprimer </td> */
/*                 <td> Administration </td>*/
/*             </tr>*/
/*             */
/*             {% for i in mods %}*/
/* */
/*                 <tr> */
/*                     <td>  {{i.username}}  </td> */
/*                     <td>  {{i.email}}  </td> */
/*                  */
/*                 <td>  <a href="{{path('ecommerceadmin_supprimer',{'id':i.id})}}"> */
/*                         <input type="submit" value="Supprimer" > </a> </td>*/
/*                         */
/*                   <td>  <a href="{{path('ecommerceadmin_Role',{'id':i.id})}}"> */
/*                         <input type="submit" value="Ajouter comme Admin" > </a> </td>        */
/*                 </tr>*/
/*             {% endfor %}*/
/*             */
/*         </table>*/
/*     */
/*  */
/*     {% endblock %}*/
/* */
