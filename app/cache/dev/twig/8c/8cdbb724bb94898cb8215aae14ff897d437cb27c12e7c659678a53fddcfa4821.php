<?php

/* PiDevClientBundle:Produit:Detail.html.twig */
class __TwigTemplate_b27e671b9af7446068ba09096ee2a63b8d641dc125441abdf1a32c3d9f11c722 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Produit:Detail.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <html>
        <head>
            <title>All For Deal | Accueil </title>

            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
            <link href=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("font-awesome-4.5.0/css/font-awesome.min.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-theme.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/form.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />  
            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/starrating/css/style.css"), "html", null, true);
        echo "\" />
            <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
            <script type=\"text/javascript\" src=";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery1.min.js"), "html", null, true);
        echo "></script>
            <!-- start menu -->
            <link href=";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/megamenu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <script type=\"text/javascript\" src=";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/megamenu.js"), "html", null, true);
        echo "></script>
            <script  src=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/Megamenufunction.js"), "html", null, true);
        echo " ></script>
            <!--start slider -->
            <link rel=\"stylesheet\" href=";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/fwslider.css"), "html", null, true);
        echo " media=\"all\">
            <script src=";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo " ></script>
            <script src=";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/css3-mediaqueries.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fwslider.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.easydropdown.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/bootstrap.min.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/js/customjs.js"), "html", null, true);
        echo "></script>
            <link rel=\"stylesheet\" href=";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/bootstrap.min.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <link rel=\"stylesheet\" href=";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/custom.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <!--end slider -->
            <link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">

            <script src=";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.load.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.ready.js"), "html", null, true);
        echo "></script>
            <script src=";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.slides.js"), "html", null, true);
        echo "></script>
            <!-- start details -->
            <script src=";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/slides.min.jquery.js"), "html", null, true);
        echo "></script>

            <link rel=\"stylesheet\" href=";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/etalage.css"), "html", null, true);
        echo ">
            <script src=";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.etalage.min.js"), "html", null, true);
        echo "></script>
            <link rel=\"stylesheet\" href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jQRangeSliders/iThing.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-slider/bootstrap-slider.css"), "html", null, true);
        echo "\">
            <script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jQRangeSliders/jQAllRangeSliders-min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/jQRangeSliders/jQAllRangeSliders-withRuler-min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-slider/bootstrap-slider.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/slider.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("plugins/bootstrap-progressbar/bootstrap-progressbar.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 51
        echo "plugins/nprogress/nprogress.js";
        echo "\"></script>
            <link href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bar.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        </head>
        <body>

            <div class=\"mens\">    
                <div class=\"main\">
                    <div class=\"wrap\">
                        <div class=\"cont span_2_of_3\">

                            <div class=\"grid images_3_of_2\">                           
                                <ul id=\"etalage\">  
                                    ";
        // line 64
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : $this->getContext($context, "images")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 65
            echo "                                        <li>                                       
                                            <a href=\"optionallink.html\">                                         
                                                <img class=\"etalage_thumb_image\"  src=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\" class=\"img-responsive\"  />                                    
                                                <img class=\"etalage_source_image\"  src=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute($context["i"], "id", array())))), "html", null, true);
            echo "\" class=\"img-responsive\"  />
                                            </a>                                    
                                        </li>

                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo " 

                                </ul>

                                <div class=\"clearfix\"></div>
                            </div>

                            <div class=\"desc1 span_3_of_2\">
                                <h3 class=\"m_3\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nom", array()), "html", null, true);
        echo " :    ";
        if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "qantite", array()) > 0)) {
            // line 81
            echo "                                                
                                                    <b>En stock</b>
                                                ";
        } else {
            // line 84
            echo "                                                    <b>N'est pas dispo </b>
                                                  
                                                  ";
        }
        // line 86
        echo "</h3>
                                <p class=\"m_5\">Prix. ";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prix", array()), "html", null, true);
        echo " DT<span class=\"reducedfrom\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prix", array()), "html", null, true);
        echo " DT</span> </p>

                                <div class=\"availablelhg\">
                                    <table class=\"record_properties\">
                                        <tbody>


                                            <tr>
                                                <th>Marque</th>
                                                <td>";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "marque", array()), "html", null, true);
        echo "</td>
                                            </tr>

                                            <tr>
                                                <th>Promotion</th>
                                                <td>";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "promotion", array()), "html", null, true);
        echo "%</td>
                                            </tr>                                
                                            <tr>
                                                <th>---------------------------</th>

                                            </tr>      
                                            <tr><th>Description </th>
                                                <td>  <p class=\"m_text2\"> ";
        // line 108
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "description", array()), "html", null, true);
        echo "</p> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>


                                <div>";
        // line 116
        echo $this->env->getExtension('nomaya_social_bar')->getSocialButtons();
        echo "</div> 
                                <div class=\"btn_form\">
                                    <br>  <hr>

                                    <center> 
                                        <a href=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_create", array("id" => $this->getAttribute($this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "favoris", array()), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\"> <button type=\"submit\" class=\"btn btn-info\"><i class=\"fa fa-heart\"></i> Like</button></a>
                                        <a href=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("panier_create", array("id" => $this->getAttribute($this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "panier", array()), "id", array()), "produit" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\"> <button type=\"submit\" class=\"btn btn-info\"><i class=\"fa fa-shopping-cart\"></i> Panier</button></a>
                                    </center>
                                    <hr>
                                    <form name=\"rating\" action=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" method=\"post\"> 
                                        <font color=\"#110D58\" size=\"4px\"> <center>  <p> <b>Notez ce produit</b></p> </center></font>
                                        <div class=\"stars\">
                                            <input type=\"radio\" name=\"adresse\" class=\"star-1\" id=\"star-1\" value=\"1\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-1\" for=\"star-1\">1</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-2\" id=\"star-2\" value=\"2\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-2\" for=\"star-2\">2</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-3\" id=\"star-3\" value=\"3\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-3\" for=\"star-3\">3</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-4\" id=\"star-4\" value=\"4\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-4\" for=\"star-4\">4</label>
                                            <input type=\"radio\" name=\"adresse\" class=\"star-5\" id=\"star-5\" value=\"5\" onclick=\"this.form.submit()\"/>
                                            <label class=\"star-5\" for=\"star-5\">5</label>               
                                            <span></span>              
                                        </div>   

                                        ";
        // line 141
        if (((isset($context["vote"]) ? $context["vote"] : $this->getContext($context, "vote")) == true)) {
            // line 142
            echo "                                            <center>     <p>    vous avez donnez un note ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["evaluation"]) ? $context["evaluation"] : $this->getContext($context, "evaluation")), "rating", array()), "html", null, true);
            echo "/5 <p><br>
                                                <p> si vous desirez changer votre avis vous pouvez voter de nouveau </p>
                                            </center>
                                        ";
        }
        // line 146
        echo "
                                    </form>



                                    <hr>
                                </div> 
                            </div>

                            <div class=\"clear\"></div>\t
                            <div class=\"clients\">

                                </script>
                                <script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.flexisel.js"), "html", null, true);
        echo "\"></script>
                            </div>
                            <div class=\"toogle\">
                                <h3 class=\"m_3\">Commentaires</h3> 
                                <div class=\"well\">
                                    <h4>Leave a Comment:</h4>
                                    <form name=\"commentaire\" action=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" method=\"post\">
                                        <div class=\"form-group\">
                                            ";
        // line 167
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Corps", array()), 'widget');
        echo "
                                        </div>
                                        <button type=\"submit\" action=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("produit_detail", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-comment\"></i> Commenter</button>
                                    </form>
                                </div>
                                <hr>
                                <div>

                                    ";
        // line 175
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 176
            echo "
                                        <h4>  <span >  <img src=";
            // line 177
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("user_image", array("id" => $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array())))), "html", null, true);
            echo " alt=\"\" width=\"50px\" height=\"50px\"/> </span> 
                                            ";
            // line 178
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "html", null, true);
            echo ": <small>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "DateCom", array()), "Y-m-d H:i:s"), "html", null, true);
            echo "</small>
                                            ";
            // line 179
            if (($this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()) == $this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "id", array()))) {
                // line 180
                echo "                                                <span class=\"popbtn\"><a class=\"arrow\"></a></span></h4>
                                                ";
            }
            // line 182
            echo "                                        <p>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "corps", array()), "html", null, true);
            echo " </p> 


                                        ";
            // line 185
            if (($this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()) == $this->getAttribute((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")), "id", array()))) {
                // line 186
                echo "                                            <div id=\"popover\" style=\"display: none\">
                                                <a href=\"";
                // line 187
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-pencil\"></span></a>
                                                <a href=\"";
                // line 188
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("commentaire_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-remove\"></span></a>\t\t\t

                                            </div>



                                        ";
            }
            // line 194
            echo "  
                                        <hr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 197
        echo "
                                </div>
                            </div>
                        </div>
                        <!-- Recherche -->
                        <div class=\"row\">
                            <div class=\"rsingle span_1_of_single\">

                                <h5 class=\"m_1\">Rechercher un produit</h5>
                                <div class=\"input-group\">

                                    <input type=\"text\" name=\"search\" >

                                    <button class=\"btn btn-info\" type=\"button\">
                                        <i class=\"fa fa-search\"></i>
                                    </button>

                                </div> <hr>
                                <h5 class=\"m_1\">Ajouter un produit</h5>
                                <div class=\"panel panel-default\">

                                    <div class=\"panel-body\" >

                                        Si vous desirez ajouter un sujet clique au-dessous<br>
                                        <center>  <a href=\"";
        // line 221
        echo $this->env->getExtension('routing')->getPath("sujet_create");
        echo "\" >  <button type=\"submit\" action=\"";
        echo $this->env->getExtension('routing')->getPath("sujet_create");
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-plus-square\"></i>  Sujet</button></a>
                                        </center>   

                                    </div>

                                </div><hr>
                                <h5 class=\"m_1\">Avis sur ce produit</h5>
                                <div>
                                    ";
        // line 229
        $context["exelent"] = 0;
        // line 230
        echo "                                        ";
        $context["bien"] = 0;
        // line 231
        echo "                                            ";
        $context["assezbien"] = 0;
        // line 232
        echo "                                                ";
        $context["moyen"] = 0;
        // line 233
        echo "                                                    ";
        $context["mauvais"] = 0;
        // line 234
        echo "                                                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["evaluations"]) ? $context["evaluations"] : $this->getContext($context, "evaluations")));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            // line 235
            echo "
                                                            ";
            // line 236
            if (($this->getAttribute($context["e"], "rating", array()) == 5)) {
                // line 237
                echo "                                                                ";
                $context["exelent"] = ((isset($context["exelent"]) ? $context["exelent"] : $this->getContext($context, "exelent")) + 1);
                // line 238
                echo "                                                                    ";
            }
            // line 239
            echo "                                                                        ";
            if (($this->getAttribute($context["e"], "rating", array()) == 4)) {
                // line 240
                echo "                                                                            ";
                $context["bien"] = ((isset($context["bien"]) ? $context["bien"] : $this->getContext($context, "bien")) + 1);
                // line 241
                echo "                                                                                ";
            }
            // line 242
            echo "                                                                                    ";
            if (($this->getAttribute($context["e"], "rating", array()) == 3)) {
                // line 243
                echo "                                                                                        ";
                $context["assezbien"] = ((isset($context["assezbien"]) ? $context["assezbien"] : $this->getContext($context, "assezbien")) + 1);
                // line 244
                echo "                                                                                            ";
            }
            // line 245
            echo "                                                                                                ";
            if (($this->getAttribute($context["e"], "rating", array()) == 2)) {
                // line 246
                echo "                                                                                                    ";
                $context["moyen"] = ((isset($context["moyen"]) ? $context["moyen"] : $this->getContext($context, "moyen")) + 1);
                // line 247
                echo "                                                                                                        ";
            }
            // line 248
            echo "                                                                                                            ";
            if (($this->getAttribute($context["e"], "rating", array()) == 1)) {
                // line 249
                echo "                                                                                                                ";
                $context["mauvais"] = ((isset($context["mauvais"]) ? $context["mauvais"] : $this->getContext($context, "mauvais")) + 1);
                // line 250
                echo "                                                                                                                    ";
            }
            // line 251
            echo "                                                                                                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 252
        echo "
                                                                                                                           
                                                                                                                           
                                                                                                                            
                                                                                                                      
                                                                                                                     

                                                                                                                            <br>
                                                                                                                              <p> Mauvais : ";
        // line 260
        echo twig_escape_filter($this->env, (isset($context["mauvais"]) ? $context["mauvais"] : $this->getContext($context, "mauvais")), "html", null, true);
        echo " Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 20%\">
                                                                                                                                    <span>40% Complete (success)</span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                                   <p>Moyen : ";
        // line 266
        echo twig_escape_filter($this->env, (isset($context["moyen"]) ? $context["moyen"] : $this->getContext($context, "moyen")), "html", null, true);
        echo " Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">                               
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%\">
                                                                                                                                    <span class=\"sr-only\">40% Complete (success)</span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <p>Assez bien :";
        // line 272
        echo twig_escape_filter($this->env, (isset($context["assezbien"]) ? $context["assezbien"] : $this->getContext($context, "assezbien")), "html", null, true);
        echo "Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%\">
                                                                                                                                    <span class=\"sr-only\">40% Complete (success)</span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                             <p>Bien :";
        // line 278
        echo twig_escape_filter($this->env, (isset($context["bien"]) ? $context["bien"] : $this->getContext($context, "bien")), "html", null, true);
        echo " Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"80\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 80%\">
                                                                                                                                    <span class=\"sr-only\">40% Complete (success)</span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                             <p>Excelent : ";
        // line 284
        echo twig_escape_filter($this->env, (isset($context["exelent"]) ? $context["exelent"] : $this->getContext($context, "exelent")), "html", null, true);
        echo " Avis </p>
                                                                                                                            <div class=\"progress progress-bar-thin\">
                                                                                                                                <div class=\"progress-bar progress-bar-primary progress-bar-thin\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%\">
                                                                                                                                    <span class=\"sr-only\">40% Complete (success)</span>
                                                                                                                                </div>
                                                                                                                            </div>

                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <!-- End recherche -->
                                                                                                            </div>

                                                                                                            <div class=\"clear\"></div>
                                                                                                        </div>
                                                                                                    </div>                                                                                           
                                                                                            </form>
                                                                                            ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Produit:Detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  598 => 284,  589 => 278,  580 => 272,  571 => 266,  562 => 260,  552 => 252,  546 => 251,  543 => 250,  540 => 249,  537 => 248,  534 => 247,  531 => 246,  528 => 245,  525 => 244,  522 => 243,  519 => 242,  516 => 241,  513 => 240,  510 => 239,  507 => 238,  504 => 237,  502 => 236,  499 => 235,  494 => 234,  491 => 233,  488 => 232,  485 => 231,  482 => 230,  480 => 229,  467 => 221,  441 => 197,  433 => 194,  423 => 188,  419 => 187,  416 => 186,  414 => 185,  407 => 182,  403 => 180,  401 => 179,  395 => 178,  391 => 177,  388 => 176,  384 => 175,  375 => 169,  370 => 167,  365 => 165,  356 => 159,  341 => 146,  333 => 142,  331 => 141,  312 => 125,  306 => 122,  302 => 121,  294 => 116,  283 => 108,  273 => 101,  265 => 96,  251 => 87,  248 => 86,  243 => 84,  238 => 81,  234 => 80,  224 => 72,  213 => 68,  209 => 67,  205 => 65,  201 => 64,  186 => 52,  182 => 51,  178 => 50,  174 => 49,  170 => 48,  166 => 47,  162 => 46,  158 => 45,  154 => 44,  150 => 43,  146 => 42,  141 => 40,  136 => 38,  132 => 37,  128 => 36,  123 => 34,  118 => 32,  114 => 31,  110 => 30,  106 => 29,  102 => 28,  98 => 27,  94 => 26,  90 => 25,  86 => 24,  81 => 22,  77 => 21,  73 => 20,  68 => 18,  63 => 16,  59 => 15,  55 => 14,  51 => 13,  47 => 12,  43 => 11,  39 => 10,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/*     <html>*/
/*         <head>*/
/*             <title>All For Deal | Accueil </title>*/
/* */
/*             <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('font-awesome-4.5.0/css/font-awesome.min.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap-theme.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/style.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/form.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <link href={{asset('css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all" />  */
/*             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/starrating/css/style.css') }}" />*/
/*             <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>*/
/*             <script type="text/javascript" src={{asset('js/jquery1.min.js')}}></script>*/
/*             <!-- start menu -->*/
/*             <link href={{asset('css/megamenu.css')}} rel="stylesheet" type="text/css" media="all" />*/
/*             <script type="text/javascript" src={{asset('js/megamenu.js')}}></script>*/
/*             <script  src={{asset('js/Megamenufunction.js')}} ></script>*/
/*             <!--start slider -->*/
/*             <link rel="stylesheet" href={{asset('css/fwslider.css')}} media="all">*/
/*             <script src={{asset('js/jquery-ui.min.js')}} ></script>*/
/*             <script src={{asset('js/css3-mediaqueries.js')}}></script>*/
/*             <script src={{asset('js/fwslider.js')}}></script>*/
/*             <script src={{asset('js/jquery.easydropdown.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/bootstrap.min.js')}}></script>*/
/*             <script src={{asset('bundles/commande/js/customjs.js')}}></script>*/
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/bootstrap.min.css')}} type="text/css" media="all" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/custom.css')}} type="text/css" media="all" /> */
/*             <!--end slider -->*/
/*             <link rel="stylesheet" href="{{asset('css/style.css')}}">*/
/* */
/*             <script src={{asset('js/jquery.load.js')}}></script>*/
/*             <script src={{asset('js/jquery.ready.js')}}></script>*/
/*             <script src={{asset('js/jquery.slides.js')}}></script>*/
/*             <!-- start details -->*/
/*             <script src={{asset('js/slides.min.jquery.js')}}></script>*/
/* */
/*             <link rel="stylesheet" href={{asset('css/etalage.css')}}>*/
/*             <script src={{asset('js/jquery.etalage.min.js')}}></script>*/
/*             <link rel="stylesheet" href="{{asset('plugins/jQRangeSliders/iThing.css')}}">*/
/*             <link rel="stylesheet" href="{{asset('plugins/bootstrap-slider/bootstrap-slider.css')}}">*/
/*             <script src="{{asset('plugins/jQRangeSliders/jQAllRangeSliders-min.js')}}"></script>*/
/*             <script src="{{asset('plugins/jQRangeSliders/jQAllRangeSliders-withRuler-min.js')}}"></script>*/
/*             <script src="{{asset('plugins/bootstrap-slider/bootstrap-slider.js')}}"></script>*/
/*             <script src="{{asset('js/slider.js')}}"></script>*/
/*             <script src="{{asset('plugins/bootstrap-progressbar/bootstrap-progressbar.js')}}"></script>*/
/*             <script src="{{('plugins/nprogress/nprogress.js')}}"></script>*/
/*             <link href="{{asset('css/bar.css')}}" rel="stylesheet">*/
/* */
/*         </head>*/
/*         <body>*/
/* */
/*             <div class="mens">    */
/*                 <div class="main">*/
/*                     <div class="wrap">*/
/*                         <div class="cont span_2_of_3">*/
/* */
/*                             <div class="grid images_3_of_2">                           */
/*                                 <ul id="etalage">  */
/*                                     {% for i in images %}*/
/*                                         <li>                                       */
/*                                             <a href="optionallink.html">                                         */
/*                                                 <img class="etalage_thumb_image"  src="{{ asset(path('my_image_route', {'id': i.id})) }}" class="img-responsive"  />                                    */
/*                                                 <img class="etalage_source_image"  src="{{ asset(path('my_image_route', {'id': i.id})) }}" class="img-responsive"  />*/
/*                                             </a>                                    */
/*                                         </li>*/
/* */
/*                                     {% endfor %} */
/* */
/*                                 </ul>*/
/* */
/*                                 <div class="clearfix"></div>*/
/*                             </div>*/
/* */
/*                             <div class="desc1 span_3_of_2">*/
/*                                 <h3 class="m_3">{{ entity.nom }} :    {% if entity.qantite > 0%}*/
/*                                                 */
/*                                                     <b>En stock</b>*/
/*                                                 {% else %}*/
/*                                                     <b>N'est pas dispo </b>*/
/*                                                   */
/*                                                   {% endif %}</h3>*/
/*                                 <p class="m_5">Prix. {{ entity.prix }} DT<span class="reducedfrom">{{ entity.prix }} DT</span> </p>*/
/* */
/*                                 <div class="availablelhg">*/
/*                                     <table class="record_properties">*/
/*                                         <tbody>*/
/* */
/* */
/*                                             <tr>*/
/*                                                 <th>Marque</th>*/
/*                                                 <td>{{ entity.marque }}</td>*/
/*                                             </tr>*/
/* */
/*                                             <tr>*/
/*                                                 <th>Promotion</th>*/
/*                                                 <td>{{ entity.promotion }}%</td>*/
/*                                             </tr>                                */
/*                                             <tr>*/
/*                                                 <th>---------------------------</th>*/
/* */
/*                                             </tr>      */
/*                                             <tr><th>Description </th>*/
/*                                                 <td>  <p class="m_text2"> {{entity.description }}</p> </td>*/
/*                                             </tr>*/
/*                                         </tbody>*/
/*                                     </table>*/
/*                                     <hr>*/
/*                                 </div>*/
/* */
/* */
/*                                 <div>{{socialButtons()}}</div> */
/*                                 <div class="btn_form">*/
/*                                     <br>  <hr>*/
/* */
/*                                     <center> */
/*                                         <a href="{{path('favoris_create',{'id':utilisateur.favoris.id,'produit':entity.id})}}"> <button type="submit" class="btn btn-info"><i class="fa fa-heart"></i> Like</button></a>*/
/*                                         <a href="{{path('panier_create',{'id':utilisateur.panier.id,'produit':entity.id})}}"> <button type="submit" class="btn btn-info"><i class="fa fa-shopping-cart"></i> Panier</button></a>*/
/*                                     </center>*/
/*                                     <hr>*/
/*                                     <form name="rating" action="{{path('produit_detail',{'id':entity.id})}}" method="post"> */
/*                                         <font color="#110D58" size="4px"> <center>  <p> <b>Notez ce produit</b></p> </center></font>*/
/*                                         <div class="stars">*/
/*                                             <input type="radio" name="adresse" class="star-1" id="star-1" value="1" onclick="this.form.submit()"/>*/
/*                                             <label class="star-1" for="star-1">1</label>*/
/*                                             <input type="radio" name="adresse" class="star-2" id="star-2" value="2" onclick="this.form.submit()"/>*/
/*                                             <label class="star-2" for="star-2">2</label>*/
/*                                             <input type="radio" name="adresse" class="star-3" id="star-3" value="3" onclick="this.form.submit()"/>*/
/*                                             <label class="star-3" for="star-3">3</label>*/
/*                                             <input type="radio" name="adresse" class="star-4" id="star-4" value="4" onclick="this.form.submit()"/>*/
/*                                             <label class="star-4" for="star-4">4</label>*/
/*                                             <input type="radio" name="adresse" class="star-5" id="star-5" value="5" onclick="this.form.submit()"/>*/
/*                                             <label class="star-5" for="star-5">5</label>               */
/*                                             <span></span>              */
/*                                         </div>   */
/* */
/*                                         {% if vote==true %}*/
/*                                             <center>     <p>    vous avez donnez un note {{evaluation.rating}}/5 <p><br>*/
/*                                                 <p> si vous desirez changer votre avis vous pouvez voter de nouveau </p>*/
/*                                             </center>*/
/*                                         {% endif %}*/
/* */
/*                                     </form>*/
/* */
/* */
/* */
/*                                     <hr>*/
/*                                 </div> */
/*                             </div>*/
/* */
/*                             <div class="clear"></div>	*/
/*                             <div class="clients">*/
/* */
/*                                 </script>*/
/*                                 <script type="text/javascript" src="{{asset('js/jquery.flexisel.js')}}"></script>*/
/*                             </div>*/
/*                             <div class="toogle">*/
/*                                 <h3 class="m_3">Commentaires</h3> */
/*                                 <div class="well">*/
/*                                     <h4>Leave a Comment:</h4>*/
/*                                     <form name="commentaire" action="{{path('produit_detail',{'id':entity.id})}}" method="post">*/
/*                                         <div class="form-group">*/
/*                                             {{ form_widget(form.Corps)}}*/
/*                                         </div>*/
/*                                         <button type="submit" action="{{path('produit_detail',{'id':entity.id})}}" class="btn btn-info"><i class="fa fa-comment"></i> Commenter</button>*/
/*                                     </form>*/
/*                                 </div>*/
/*                                 <hr>*/
/*                                 <div>*/
/* */
/*                                     {% for entity in entities %}*/
/* */
/*                                         <h4>  <span >  <img src={{asset(path('user_image',{'id':entity.user.id}))}} alt="" width="50px" height="50px"/> </span> */
/*                                             {{ entity.user.username }}: <small>{{entity.DateCom|date('Y-m-d H:i:s')}}</small>*/
/*                                             {% if entity.user.id == utilisateur.id %}*/
/*                                                 <span class="popbtn"><a class="arrow"></a></span></h4>*/
/*                                                 {% endif %}*/
/*                                         <p>{{ entity.corps }} </p> */
/* */
/* */
/*                                         {% if entity.user.id == utilisateur.id %}*/
/*                                             <div id="popover" style="display: none">*/
/*                                                 <a href="{{path('commentaire_edit',{'id':entity.id})}}"><span class="glyphicon glyphicon-pencil"></span></a>*/
/*                                                 <a href="{{path('commentaire_delete',{'id':entity.id})}}"><span class="glyphicon glyphicon-remove"></span></a>			*/
/* */
/*                                             </div>*/
/* */
/* */
/* */
/*                                         {% endif %}  */
/*                                         <hr>*/
/*                                     {% endfor %}*/
/* */
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <!-- Recherche -->*/
/*                         <div class="row">*/
/*                             <div class="rsingle span_1_of_single">*/
/* */
/*                                 <h5 class="m_1">Rechercher un produit</h5>*/
/*                                 <div class="input-group">*/
/* */
/*                                     <input type="text" name="search" >*/
/* */
/*                                     <button class="btn btn-info" type="button">*/
/*                                         <i class="fa fa-search"></i>*/
/*                                     </button>*/
/* */
/*                                 </div> <hr>*/
/*                                 <h5 class="m_1">Ajouter un produit</h5>*/
/*                                 <div class="panel panel-default">*/
/* */
/*                                     <div class="panel-body" >*/
/* */
/*                                         Si vous desirez ajouter un sujet clique au-dessous<br>*/
/*                                         <center>  <a href="{{path('sujet_create')}}" >  <button type="submit" action="{{path('sujet_create')}}" class="btn btn-info"><i class="fa fa-plus-square"></i>  Sujet</button></a>*/
/*                                         </center>   */
/* */
/*                                     </div>*/
/* */
/*                                 </div><hr>*/
/*                                 <h5 class="m_1">Avis sur ce produit</h5>*/
/*                                 <div>*/
/*                                     {% set exelent = 0 %}*/
/*                                         {% set bien = 0 %}*/
/*                                             {% set assezbien = 0 %}*/
/*                                                 {% set  moyen= 0 %}*/
/*                                                     {% set mauvais = 0 %}*/
/*                                                         {% for e in evaluations %}*/
/* */
/*                                                             {% if e.rating==5 %}*/
/*                                                                 {% set exelent=exelent+1 %}*/
/*                                                                     {% endif %}*/
/*                                                                         {% if e.rating==4 %}*/
/*                                                                             {% set bien=bien+1 %}*/
/*                                                                                 {% endif %}*/
/*                                                                                     {% if e.rating==3 %}*/
/*                                                                                         {% set assezbien=assezbien+1 %}*/
/*                                                                                             {% endif %}*/
/*                                                                                                 {% if e.rating==2 %}*/
/*                                                                                                     {% set moyen=moyen+1 %}*/
/*                                                                                                         {% endif %}*/
/*                                                                                                             {% if e.rating==1 %}*/
/*                                                                                                                 {% set mauvais=mauvais+1 %}*/
/*                                                                                                                     {% endif %}*/
/*                                                                                                                         {% endfor %}*/
/* */
/*                                                                                                                            */
/*                                                                                                                            */
/*                                                                                                                             */
/*                                                                                                                       */
/*                                                                                                                      */
/* */
/*                                                                                                                             <br>*/
/*                                                                                                                               <p> Mauvais : {{ mauvais }} Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">*/
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">*/
/*                                                                                                                                     <span>40% Complete (success)</span>*/
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/*                                                                                                                                    <p>Moyen : {{ moyen }} Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">                               */
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">*/
/*                                                                                                                                     <span class="sr-only">40% Complete (success)</span>*/
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/*                                                                                                                             <p>Assez bien :{{ assezbien }}Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">*/
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">*/
/*                                                                                                                                     <span class="sr-only">40% Complete (success)</span>*/
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/*                                                                                                                              <p>Bien :{{ bien }} Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">*/
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">*/
/*                                                                                                                                     <span class="sr-only">40% Complete (success)</span>*/
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/*                                                                                                                              <p>Excelent : {{ exelent }} Avis </p>*/
/*                                                                                                                             <div class="progress progress-bar-thin">*/
/*                                                                                                                                 <div class="progress-bar progress-bar-primary progress-bar-thin" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">*/
/*                                                                                                                                     <span class="sr-only">40% Complete (success)</span>*/
/*                                                                                                                                 </div>*/
/*                                                                                                                             </div>*/
/* */
/*                                                                                                                         </div>*/
/*                                                                                                                     </div>*/
/*                                                                                                                 </div>*/
/* */
/*                                                                                                                 <!-- End recherche -->*/
/*                                                                                                             </div>*/
/* */
/*                                                                                                             <div class="clear"></div>*/
/*                                                                                                         </div>*/
/*                                                                                                     </div>                                                                                           */
/*                                                                                             </form>*/
/*                                                                                             {% endblock %}*/
