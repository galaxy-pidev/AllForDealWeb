<?php

/* PiDevClientBundle:Reclamation:new.html.twig */
class __TwigTemplate_2b5a0792ee85c661980b2a0aaf05c91abecd851b588625f80dfdb82eb33c8e3c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Reclamation:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<body>
<h2> <strong> Envoyer une reclamtion</strong> </h2>
<hr>

<p>";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "</p>
<hr>
<form id=\"fr\" method=\"POST\" action='";
        // line 9
        echo $this->env->getExtension('routing')->getPath("my_app_reclamation_sendpage");
        echo "' ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo ">
    <table>
        <tr><td>Reclamation : </td></tr>
        <tr><td>From : </td><td><input type=\"text\" name=\"from\"></td></tr>
        <tr><td>Message : </td><td><input type=\"text\" name=\"message\"></td></tr>
        ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'widget');
        echo "
            <button type=\"submit\" class=\"btn btn-info\">Valider</button>
    </table>

</form>
</body>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Reclamation:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 14,  42 => 9,  37 => 7,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* <body>*/
/* <h2> <strong> Envoyer une reclamtion</strong> </h2>*/
/* <hr>*/
/* */
/* <p>{{form_errors(form)}}</p>*/
/* <hr>*/
/* <form id="fr" method="POST" action='{{path('my_app_reclamation_sendpage')}}' {{form_enctype(form)}}>*/
/*     <table>*/
/*         <tr><td>Reclamation : </td></tr>*/
/*         <tr><td>From : </td><td><input type="text" name="from"></td></tr>*/
/*         <tr><td>Message : </td><td><input type="text" name="message"></td></tr>*/
/*         {{ form_widget(form._token) }}*/
/*             <button type="submit" class="btn btn-info">Valider</button>*/
/*     </table>*/
/* */
/* </form>*/
/* </body>*/
/*  {% endblock %}*/
/* */
