<?php

/* PiDevClientBundle:Commande:payment.html.twig */
class __TwigTemplate_5b28c2686cb7e1a88f7c4b1a589da3581c1c6263cb3e12b7398f992aad3936ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Commande:payment.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\" lang=\"en-US\"> 
<head profile=\"http://gmpg.org/xfn/11\"> 
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /> 
            <link rel=\"stylesheet\" href=";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/reset.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
            <link rel=\"stylesheet\" href=";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/commande/css/style.css"), "html", null, true);
        echo " type=\"text/css\" media=\"all\" /> 
       </head>

<body>

\t<div id=\"wrap\">
\t\t
<div class=\"container\">




<div class=\"box-170 bg-step1-o-blue\" ><p class=\"typo-right\"><span class=\"typo-step-o\">STEP</span><br /><span class=\"typo-w\">Your info</span></p></div>
<div class=\"box-170 ml-3 bg-step2-blue\" ><p class=\" typo-right\"><span class=\"typo-step\">STEP</span><br /><span class=\"typo\">Payment</span></p></div>
<div class=\"box-170 ml-3 bg-step3-o-blue\" ><p class=\"typo-right\"><span class=\"typo-step-o\">STEP</span><br /><span class=\"typo-w\">Submit order</span></p></div>
<div class=\"box-170 ml-3 bg-step4-o-blue\" ><p class=\"typo-right\"><span class=\"typo-step-o\">STEP</span><br /><span class=\"typo-w\">Confirmation</span></p></div>
<div class=\"clear\"></div>

<div class=\"box-719\" >
<div class=\"rcfg-blue pl-6 pr-6 pt-6 pb-5\">

<div class=\"box-660\">
<b class=\"rc-lightblue\"><b class=\"rc1-lightblue\"><b></b></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc3-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc5-lightblue\"></b></b>
<div class=\"rcfg-lightblue pl-4 pr-4 pb-4\">










<p class=\"ml-3\">Please fill out your order information here</p>


<!-- Form Starting -->
<form  method=\"post\" action=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("payer_commande", array("id" => $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "id", array()))), "html", null, true);
        echo "\">
<fieldset>
<ul class=\"order\">
    <li><label   for=\"ccHolder\">totale de votre commande<span class=\"typo-1\">*</span></label>  <b></b>
        <li><label   for=\"ccHolder\">totale de vos point bonnus<span class=\"typo-1\">*</span></label> <b> ";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "ptBonus", array()), "html", null, true);
        echo "</b>
</li>
<li><label  >Payer par <span class=\"typo-1\">*</span></label>
      <select  class=\"drop\" id=\"\" name=\"type\" >
      \t<option value=\"point-bonus\">point bonus</option>
\t    <option value=\"2\">carte bancaire</option>
      \t<option value=\"3\">à la livraison</option>
      </select>  
</li>
</ul>

<div class=\"clear\"></div>
<div class=\"clear\"></div>

<input class=\"bt-order-blue\" type=\"submit\" value=\"payer\"/>
<div class=\"clear\"></div>
</fieldset>
</form>
<!-- Form Ending -->




</div>
<b class=\"rc-lightblue\"><b class=\"rc5-lightblue\"></b><b class=\"rc4-lightblue\"></b><b class=\"rc3-lightblue\"></b><b class=\"rc2-lightblue\"><b></b></b><b class=\"rc1-lightblue\"><b></b></b></b>
</div>


<div class=\"clear\"></div>


</div>
<b class=\"rc-blue\"><b class=\"rc5-blue\"></b><b class=\"rc4-blue\"></b><b class=\"rc3-blue\"></b><b class=\"rc2-blue\"><b></b></b><b class=\"rc1-blue\"><b></b></b></b>
</div>
<div class=\"clear\"></div>




</div>
</div>
</div>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commande:payment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 50,  81 => 46,  40 => 8,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block content %}*/
/* <html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US"> */
/* <head profile="http://gmpg.org/xfn/11"> */
/* <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/reset.css')}} type="text/css" media="all" /> */
/*             <link rel="stylesheet" href={{asset('bundles/commande/css/style.css')}} type="text/css" media="all" /> */
/*        </head>*/
/* */
/* <body>*/
/* */
/* 	<div id="wrap">*/
/* 		*/
/* <div class="container">*/
/* */
/* */
/* */
/* */
/* <div class="box-170 bg-step1-o-blue" ><p class="typo-right"><span class="typo-step-o">STEP</span><br /><span class="typo-w">Your info</span></p></div>*/
/* <div class="box-170 ml-3 bg-step2-blue" ><p class=" typo-right"><span class="typo-step">STEP</span><br /><span class="typo">Payment</span></p></div>*/
/* <div class="box-170 ml-3 bg-step3-o-blue" ><p class="typo-right"><span class="typo-step-o">STEP</span><br /><span class="typo-w">Submit order</span></p></div>*/
/* <div class="box-170 ml-3 bg-step4-o-blue" ><p class="typo-right"><span class="typo-step-o">STEP</span><br /><span class="typo-w">Confirmation</span></p></div>*/
/* <div class="clear"></div>*/
/* */
/* <div class="box-719" >*/
/* <div class="rcfg-blue pl-6 pr-6 pt-6 pb-5">*/
/* */
/* <div class="box-660">*/
/* <b class="rc-lightblue"><b class="rc1-lightblue"><b></b></b><b class="rc2-lightblue"><b></b></b><b class="rc3-lightblue"></b><b class="rc4-lightblue"></b><b class="rc5-lightblue"></b></b>*/
/* <div class="rcfg-lightblue pl-4 pr-4 pb-4">*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* <p class="ml-3">Please fill out your order information here</p>*/
/* */
/* */
/* <!-- Form Starting -->*/
/* <form  method="post" action="{{path('payer_commande',{'id':commande.id})}}">*/
/* <fieldset>*/
/* <ul class="order">*/
/*     <li><label   for="ccHolder">totale de votre commande<span class="typo-1">*</span></label>  <b></b>*/
/*         <li><label   for="ccHolder">totale de vos point bonnus<span class="typo-1">*</span></label> <b> {{user.ptBonus}}</b>*/
/* </li>*/
/* <li><label  >Payer par <span class="typo-1">*</span></label>*/
/*       <select  class="drop" id="" name="type" >*/
/*       	<option value="point-bonus">point bonus</option>*/
/* 	    <option value="2">carte bancaire</option>*/
/*       	<option value="3">à la livraison</option>*/
/*       </select>  */
/* </li>*/
/* </ul>*/
/* */
/* <div class="clear"></div>*/
/* <div class="clear"></div>*/
/* */
/* <input class="bt-order-blue" type="submit" value="payer"/>*/
/* <div class="clear"></div>*/
/* </fieldset>*/
/* </form>*/
/* <!-- Form Ending -->*/
/* */
/* */
/* */
/* */
/* </div>*/
/* <b class="rc-lightblue"><b class="rc5-lightblue"></b><b class="rc4-lightblue"></b><b class="rc3-lightblue"></b><b class="rc2-lightblue"><b></b></b><b class="rc1-lightblue"><b></b></b></b>*/
/* </div>*/
/* */
/* */
/* <div class="clear"></div>*/
/* */
/* */
/* </div>*/
/* <b class="rc-blue"><b class="rc5-blue"></b><b class="rc4-blue"></b><b class="rc3-blue"></b><b class="rc2-blue"><b></b></b><b class="rc1-blue"><b></b></b></b>*/
/* </div>*/
/* <div class="clear"></div>*/
/* */
/* */
/* */
/* */
/* </div>*/
/* </div>*/
/* </div>*/
/* </body>*/
/* </html>*/
/* {% endblock %}*/
