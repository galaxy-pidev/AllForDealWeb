<?php

/* PiDevClientBundle:Mail:mail.html.twig */
class __TwigTemplate_38009de5b50ded345ca36b4e9e249cb847aadd0453580effba412284aae24a12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "PiDevClientBundle:Mail:mail.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<body>
Votre demande a été envoyée avec succès
</body>
 ";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Mail:mail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content %}*/
/* <body>*/
/* Votre demande a été envoyée avec succès*/
/* </body>*/
/*  {% endblock %}*/
/* */
