<?php

/* PiDevClientBundle:Commande:reponse.html.twig */
class __TwigTemplate_ba7851cfeb74857e40843614c26b167d4e117f0cc7aeabd33660cbd024010438 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<html>
    <head>  
        <!-- styles -->
        <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/owl.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- theme stylesheet -->
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/style.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-stylesheet\">

        <!-- your stylesheet with modifications -->
        <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/css/custom.css\" rel=\"stylesheet"), "html", null, true);
        echo "\">

        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/respond.min.js"), "html", null, true);
        echo "\"></script>

        <link rel=\"shortcut icon\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/img/favicon.png"), "html", null, true);
        echo "\">



    </head>

    <body>";
        // line 26
        echo "    
   <div id=\"content\">
            <div class=\"container\">

                <div class=\"col-md-12\">
                    <ul class=\"breadcrumb\">
                        <li><a href=\"#\">Home</a>
                        </li>
                        <li>Checkout - Order review</li>
                    </ul>
                </div>

                <div class=\"col-md-9\" id=\"checkout\">

                    <div class=\"box\">
                        <form method=\"post\" action=\"checkout4.html\">
                            <h1>Checkout - Order review</h1>
                            <ul class=\"nav nav-pills nav-justified\">
                                <li><a href=\"checkout1.html\"><i class=\"fa fa fa-truck\"></i><br>Livraison</a>
                                </li>
                                <li><a href=\"checkout2.html\"><i class=\"fa fa-money\"></i><br>Payment</a>
                                </li>
                                <li><a href=\"checkout3.html\"><i class=\"fa fa-eye\"></i><br>Confirmation</a>
                                </li>
                                <li class=\"active\"><a href=\"#\"><i class=\"fa fa-archive\"></i><br>Facture</a>
                                </li>
                            </ul>

                            <div class=\"content\">
                                <div class=\"table-responsive\">
                                    <table class=\"table\">
                                        <thead>
                                            <tr>
                                                <th colspan=\"5\">Merci pour votre confiance ! </th>
                                 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                 Vous pouvez consulter votre facture pdf en cliquant sur le bouton <b> Facture </b> ci-dessous .<br /> <br />
Ainsi vous allez recevoir un mail de confirmation de notre part .               
                                                </td>
                                  
                                            </tr>

                                        </tbody>

                                    </table>

                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.content -->

                            <div class=\"box-footer\">
                                <div class=\"pull-left\">
                                    <a href=\"checkout3.html\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Back to Payment method</a>
                                </div>
                                <div class=\"pull-right\">
                                    <a href=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("facture_commande", array("id" => $this->getAttribute((isset($context["commande"]) ? $context["commande"] : $this->getContext($context, "commande")), "id", array()))), "html", null, true);
        echo "\" type=\"submit\" class=\"btn btn-primary\">Place an order<i class=\"fa fa-chevron-right\"></i>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class=\"col-md-3\">

                    <div class=\"box\" id=\"order-summary\">
                        <div class=\"box-header\">
                            <h3>Order summary</h3>
                        </div>
                        <p class=\"text-muted\">Shipping and additional costs are calculated based on the values you have entered.</p>

                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>Order subtotal</td>
                                        <th>\$446.00</th>
                                    </tr>
                                    <tr>
                                        <td>Shipping and handling</td>
                                        <th>\$10.00</th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <th>\$0.00</th>
                                    </tr>
                                    <tr class=\"total\">
                                        <td>Total</td>
                                        <th>\$456.00</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
        <script src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery-1.11.0.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/waypoints.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/modernizr.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/bootstrap-hover-dropdown.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/panier/js/front.js"), "html", null, true);
        echo "\"></script>   
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PiDevClientBundle:Commande:reponse.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 145,  213 => 144,  209 => 143,  205 => 142,  201 => 141,  197 => 140,  193 => 139,  189 => 138,  134 => 86,  72 => 26,  63 => 19,  58 => 17,  53 => 15,  47 => 12,  41 => 9,  37 => 8,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* */
/* <html>*/
/*     <head>  */
/*         <!-- styles -->*/
/*         <link href="{{asset('bundles/panier/css/font-awesome.css')}}" rel="stylesheet">*/
/*         <link href="{{asset('bundles/panier/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*         <link href="{{asset('bundles/panier/css/animate.min.css')}}" rel="stylesheet">*/
/*         <link href="{{asset('bundles/panier/css/owl.carousel.css')}}" rel="stylesheet">*/
/*         <link href="{{asset('bundles/panier/css/owl.theme.css')}}" rel="stylesheet">*/
/* */
/*         <!-- theme stylesheet -->*/
/*         <link href="{{asset('bundles/panier/css/style.default.css')}}" rel="stylesheet" id="theme-stylesheet">*/
/* */
/*         <!-- your stylesheet with modifications -->*/
/*         <link href="{{asset('bundles/panier/css/custom.css" rel="stylesheet')}}">*/
/* */
/*         <script src="{{asset('bundles/panier/js/respond.min.js')}}"></script>*/
/* */
/*         <link rel="shortcut icon" href="{{asset('bundles/panier/img/favicon.png')}}">*/
/* */
/* */
/* */
/*     </head>*/
/* */
/*     <body>{# empty Twig template #}{# empty Twig template #}*/
/*     */
/*    <div id="content">*/
/*             <div class="container">*/
/* */
/*                 <div class="col-md-12">*/
/*                     <ul class="breadcrumb">*/
/*                         <li><a href="#">Home</a>*/
/*                         </li>*/
/*                         <li>Checkout - Order review</li>*/
/*                     </ul>*/
/*                 </div>*/
/* */
/*                 <div class="col-md-9" id="checkout">*/
/* */
/*                     <div class="box">*/
/*                         <form method="post" action="checkout4.html">*/
/*                             <h1>Checkout - Order review</h1>*/
/*                             <ul class="nav nav-pills nav-justified">*/
/*                                 <li><a href="checkout1.html"><i class="fa fa fa-truck"></i><br>Livraison</a>*/
/*                                 </li>*/
/*                                 <li><a href="checkout2.html"><i class="fa fa-money"></i><br>Payment</a>*/
/*                                 </li>*/
/*                                 <li><a href="checkout3.html"><i class="fa fa-eye"></i><br>Confirmation</a>*/
/*                                 </li>*/
/*                                 <li class="active"><a href="#"><i class="fa fa-archive"></i><br>Facture</a>*/
/*                                 </li>*/
/*                             </ul>*/
/* */
/*                             <div class="content">*/
/*                                 <div class="table-responsive">*/
/*                                     <table class="table">*/
/*                                         <thead>*/
/*                                             <tr>*/
/*                                                 <th colspan="5">Merci pour votre confiance ! </th>*/
/*                                  */
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         <tbody>*/
/*                                             <tr>*/
/*                                                 <td>*/
/*                                  Vous pouvez consulter votre facture pdf en cliquant sur le bouton <b> Facture </b> ci-dessous .<br /> <br />*/
/* Ainsi vous allez recevoir un mail de confirmation de notre part .               */
/*                                                 </td>*/
/*                                   */
/*                                             </tr>*/
/* */
/*                                         </tbody>*/
/* */
/*                                     </table>*/
/* */
/*                                 </div>*/
/*                                 <!-- /.table-responsive -->*/
/*                             </div>*/
/*                             <!-- /.content -->*/
/* */
/*                             <div class="box-footer">*/
/*                                 <div class="pull-left">*/
/*                                     <a href="checkout3.html" class="btn btn-default"><i class="fa fa-chevron-left"></i>Back to Payment method</a>*/
/*                                 </div>*/
/*                                 <div class="pull-right">*/
/*                                     <a href="{{path('facture_commande', {'id':commande.id})}}" type="submit" class="btn btn-primary">Place an order<i class="fa fa-chevron-right"></i>*/
/*                                     </a>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </form>*/
/*                     </div>*/
/*                     <!-- /.box -->*/
/* */
/* */
/*                 </div>*/
/*                 <!-- /.col-md-9 -->*/
/* */
/*                 <div class="col-md-3">*/
/* */
/*                     <div class="box" id="order-summary">*/
/*                         <div class="box-header">*/
/*                             <h3>Order summary</h3>*/
/*                         </div>*/
/*                         <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>*/
/* */
/*                         <div class="table-responsive">*/
/*                             <table class="table">*/
/*                                 <tbody>*/
/*                                     <tr>*/
/*                                         <td>Order subtotal</td>*/
/*                                         <th>$446.00</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Shipping and handling</td>*/
/*                                         <th>$10.00</th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>Tax</td>*/
/*                                         <th>$0.00</th>*/
/*                                     </tr>*/
/*                                     <tr class="total">*/
/*                                         <td>Total</td>*/
/*                                         <th>$456.00</th>*/
/*                                     </tr>*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/* */
/*                     </div>*/
/* */
/*                 </div>*/
/*                 <!-- /.col-md-3 -->*/
/* */
/*             </div>*/
/*             <!-- /.container -->*/
/*         </div>*/
/*         <!-- /#content -->*/
/*         <script src="{{asset('bundles/panier/js/jquery-1.11.0.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/bootstrap.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/jquery.cookie.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/waypoints.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/modernizr.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/bootstrap-hover-dropdown.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/owl.carousel.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/panier/js/front.js')}}"></script>   */
/*     </body>*/
/* </html>*/
/* */
