<?php

namespace Proxies\__CG__\PiDev\ClientBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Commande extends \PiDev\ClientBundle\Entity\Commande implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'id', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'datecommande', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'modepaiement', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'totalcommande', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'valider', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'panier', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'livraison'];
        }

        return ['__isInitialized__', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'id', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'datecommande', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'modepaiement', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'totalcommande', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'valider', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'panier', '' . "\0" . 'PiDev\\ClientBundle\\Entity\\Commande' . "\0" . 'livraison'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Commande $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setDatecommande($datecommande)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDatecommande', [$datecommande]);

        return parent::setDatecommande($datecommande);
    }

    /**
     * {@inheritDoc}
     */
    public function getDatecommande()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDatecommande', []);

        return parent::getDatecommande();
    }

    /**
     * {@inheritDoc}
     */
    public function setTotalcommande($totalcommande)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTotalcommande', [$totalcommande]);

        return parent::setTotalcommande($totalcommande);
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalcommande()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTotalcommande', []);

        return parent::getTotalcommande();
    }

    /**
     * {@inheritDoc}
     */
    public function setValider($valider)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setValider', [$valider]);

        return parent::setValider($valider);
    }

    /**
     * {@inheritDoc}
     */
    public function getValider()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getValider', []);

        return parent::getValider();
    }

    /**
     * {@inheritDoc}
     */
    public function setPanier(\PiDev\ClientBundle\Entity\Panier $panier = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPanier', [$panier]);

        return parent::setPanier($panier);
    }

    /**
     * {@inheritDoc}
     */
    public function getPanier()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPanier', []);

        return parent::getPanier();
    }

    /**
     * {@inheritDoc}
     */
    public function setLivraison(\PiDev\ClientBundle\Entity\Livraison $livraison = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLivraison', [$livraison]);

        return parent::setLivraison($livraison);
    }

    /**
     * {@inheritDoc}
     */
    public function getLivraison()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLivraison', []);

        return parent::getLivraison();
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', []);

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function getModepaiement()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getModepaiement', []);

        return parent::getModepaiement();
    }

    /**
     * {@inheritDoc}
     */
    public function setModepaiement($modepaiement)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setModepaiement', [$modepaiement]);

        return parent::setModepaiement($modepaiement);
    }

}
